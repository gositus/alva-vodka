$(document).ready(function(){

	$('input.privilege').click(function(){
		var value = $(this).val();
		
		if (value == 3)
			$('#access-table').slideDown();
		else
			$('#access-table').slideUp();
	});

	$('.access-add').click(function(){

		if ($(this).prop('checked') == true){
			$(this).parents('td').prev().find('.access-read').prop('checked', true);
		}
	});
		
	// If edit is allowed, read is too.
	$('.access-modify').click(function(){
		if ($(this).prop('checked') == true){
			$(this).parents('td').prev().prev().find('.access-read').prop('checked', true);
		}else{
			$(this).parents('td').next().find('.access-delete').prop('checked', false);
		}
	});
	
	// If delete is allowed, read & edit is too
	$('.access-delete').click(function(){
		if ($(this).prop('checked') == true){
			$(this).parents('td').prev().prev().prev().find('.access-read').prop('checked', true);
		}
	});
	
	// If read is OFF, edit is OFF too
	$('.access-read').click(function(){
		if ( $(this).prop('checked') != true) {
			$(this).parents('td').next().next().find('.access-modify').prop('checked', false);
			$(this).parents('td').next().find('.access-add').prop('checked', false);
			$(this).parents('td').next().next().next().find('.access-delete').prop('checked', false);
		}
	});
	
	// Calculate total :)
	$('tr td input').click(function(){
		
		// Which module?
		var module = $(this).attr('name');
		
		// Get all value.
		var read = parseInt($('input[name=' + module +'].access-read:checked').val()) || 0;
		var add = parseInt($('input[name=' + module +'].access-add:checked').val()) || 0;
		var modify = parseInt($('input[name=' + module +'].access-modify:checked').val()) || 0;
		var del = parseInt($('input[name=' + module +'].access-delete:checked').val()) || 0;
		
		$('#total-' + module).val(read + add + modify + del);
	});
});