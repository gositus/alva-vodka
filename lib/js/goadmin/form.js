function readImage(input, target) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function(e) {
            $(target).attr('src', e.target.result);
        }
        reader.readAsDataURL(input.files[0]);
    }
}


var formChange = false;

$(document).ready(function() {

    $('input, textarea, select').change(function() {
        formChange = true;
    });

    $('.wrap-button-action button[type=submit], #btn-continue-delete-row').on('click', function() {
        formChange = false;
    });
    // banner_end_date();
    gallery_type();
    checkseo();
    mail_protocol();

    var dateStr = $("input.datepicker_from").val();
    $("input.datepicker_from").datepicker({
        defaultDate: new Date(),
        dateFormat: 'dd M yy',
        numberOfMonths: 2,
        firstDay: 1,
        changeYear: true,
        changeMonth: true,
        showButtonPanel: true,
        onClose: function(selectedDate) {
            $('#content-form').valid();
        },
    })
    .on('change', function(dateStr){
        var dateStrs = $("input.datepicker_from").val();
       
        $("input.datepicker_to").datepicker("option", {
                minDate: new Date(dateStrs),
                numberOfMonths: 2,
            });
        setTimeout(function(){ 
             $("input.datepicker_to").focus();
         }, 100);
       
    }); 

    $('input.datepicker_to').datepicker({
        dateFormat: 'dd M yy',
        defaultDate: "-1w",
        numberOfMonths: 2,
        firstDay: 1,
        changeYear: true,
        changeMonth: true,
        showButtonPanel: true,
        onClose: function(selectedDate) {
            $('#content-form').valid();
        },
        minDate: new Date(dateStr)
    });

    $("input.datepicker").datepicker({ // Default Datepicker
        defaultDate: new Date(),
        dateFormat: 'dd M yy',
        firstDay: 1,
        numberOfMonths: 2,
        changeYear: true,
        changeMonth: true,
        showButtonPanel: true,
        onClose: function(selectedDate) {
            $('#content-form').valid();
        },
    });

    var dateStr = $("input.datepicker-start").val();
    $("input.datepicker-start").datepicker({
        defaultDate: new Date(),
        minDate: new Date(),
        dateFormat: 'dd M yy',
        firstDay: 1,
        changeYear: true,
        numberOfMonths: 2,
        changeMonth: true,
        showButtonPanel: true,

        onSelect: function(dateStr) {

            $("input.datepicker-end").datepicker("option", {
                minDate: new Date(dateStr),
                numberOfMonths: 2,
                beforeShowDay: function(date){
                    var date1 = new Date (dateStr);
                    var date2 = new Date ($("input.datepicker-end").val());
                    var isHighlight = date1 && ((date.getTime() == date1.getTime()) || (date2 && date >= date1 && date <= date2));

                     return [true, isHighlight ? "dp-highlight" : ""];
                }
            });
             $("input.datepicker-end").click();
        },
        onClose: function(selectedDate) {
            $('#content-form').valid();
        }
    });
    var dateStr = $("input.datepicker-start-member").val();
    $("input.datepicker-start-member").datepicker({
        defaultDate: new Date(),
        dateFormat: 'dd M yy',
        firstDay: 1,
        changeYear: true,
        changeMonth: true,
        showButtonPanel: true,
        onClose: function(selectedDate) {
            $('#content-form').valid();
        },
        minDate: new Date(),
        onSelect: function(dateStr) {

            $("input.datepicker-end").datepicker("option", {
                minDate: new Date(dateStr)
            })
        }
    });
    $('input.datepicker-end').datepicker({
        dateFormat: 'dd M yy',
        firstDay: 1,
        changeYear: true,
        changeMonth: true,
        showButtonPanel: true,
        onClose: function(selectedDate) {
            $('#content-form').valid();
        },
        minDate: new Date(dateStr)
    });

    $('input.datepicker-dob').datepicker({
        dateFormat: 'dd M yy',
        firstDay: 1,
        changeYear: true,
        changeMonth: true,
        showButtonPanel: true,
        onClose: function(selectedDate) {
            $('#content-form').valid();
        },
        maxDate: new Date()
    });

    $('.btn-form-back').click(function() {
        history.go(-1);
    });

    $('[data-plugin="bs-select"]').select2({
        theme: "bootstrap"
    });

    $('[data-plugin="autosize"]').each(function() {
        autosize(this);
    });

    //Language
    $('#language-bar li').click(function() {

        $('#language-bar li').removeClass('active');
        $(this).addClass('active');
        var id = $(this).attr('id');
        $('#content-form div.language').hide();
        $('#content-form div.language.' + id).fadeIn(300);
    });


    $('#content-form').validate({
        highlight: function ( element, errorClass, validClass ) {
            $( element )
                .parents( ".form-group , .form-control" )
                .addClass( "has-error-cms" );
        },

        unhighlight: function (element, errorClass, validClass) {
            $( element )
                .parents( ".form-group , .form-control" )
                .removeClass( "has-error-cms" );
        },
        messages: {
            'a_repassword': {
                equalTo: 'Enter the same password.'
            },
            'old_password': {
                remote: 'Wrong password.'
            },
            'setting_web_logo': {
                accept: 'Only pictures are allowed'
            },
            'banner_link': {
                url: 'URL must start with http://'
            }
        },

        rules: {
            old_password: {
                required: function(element) {
                    return $('#admin_password').val() != '';
                },
                remote: base_url + 'goadmin/admin/check_password/'
            },

            a_repassword: {
                equalTo: "#a_password"
            }
        }
    });

    // UPLOAD IMAGE & DELETE IMAGE
    $('.upload-image input[type="file"]').on('change', function() {
        var value = $(this).val();
        var imgName = value.replace(/C:\\fakepath\\/, "");
        if ($(this).val() == '') {
            $(this).parents('.upload-image').find('.wrap-preview-image').fadeOut();
            $(this).parents('.upload-image').find('.info-path-image').fadeOut().val('');
        } else {
            $(this).parents('.upload-image').find('.wrap-preview-image').fadeIn();
            $(this).parents('.upload-image').find('.info-path-image').fadeIn().val(imgName);
            readImage(this, $(this).parents('.upload-image').find('.upload-preview'));
        }
    });

    $('.upload-image .btn-delete-img').on('click', function() {
        var elem = $(this);
        if (confirm('Are you sure you want to delete this image?')) {

            $.post(base_url + 'goadmin/ajax/delete_image/', {
                id: $('#row_id').val(),
                table: $('#row_id').attr('data-table-name'),
                field: $(this).parents('.upload-image').find('input[type=file]').attr('name')
            }, function() {
                // elem.parents('.upload-image').find('.info-path-image').removeClass('show').fadeOut();
                elem.parents('.upload-image').find('.info-path-image').val('');
                elem.parents('.upload-image').find('.wrap-preview-image').removeClass('show').fadeOut();
                elem.parents('.upload-image').find('.wrap-delete-image').removeClass('show').fadeOut();
                showToast('success', 'bottom', 'Success, image has been deleted!');
            });
        }
    });
    // END UPLOAD IMAGE & DELETE IMAGE

    // DELETE ROW
    $('#btn-continue-delete-row').on('click', function() {
        var id = $('#row_id').val();
        var tableName = $('#row_id').attr('data-table-name');

        $.ajax({
            type: "POST",
            url: base_url + 'goadmin/sequential/delete',
            data_type: "JSON",
            data: {
                "id": "[" + id + "]",
                "table": tableName
            },
            success: function(data) {
                $('#modal-delete-row').modal('hide');
                localStorage.setItem("isDeleteRow", "true");
                location.reload();
            }

        })
    });
    // END DELETE ROW


    $('#module_parent').change(function() {
        var value = $(this).val();

        if (value == 0) {
            $('#module_url').val('javascript:;');
        } else {
            if ($('#module_url').val() == 'javascript:;')
                $('#module_url').val('')
            else
                $('#module_url').addClass('required');
        }
    });

    $('#news_type').change(function() {
        var value = $(this).val();

        // If type == event
        if (value == 2) {
            $('p.end-date').fadeIn().children('input').addClass('required').val('');
        } else {
            $('p.end-date').fadeOut().removeClass('required').children('input').val('');
            $('#news_end').removeClass('required');
        }
    });

    $('#gallery_category_type').change(function() {
        var value = $(this).val();

        $('#gallery_category_parent').load(base_url + 'goadmin/gallery_category/show_parent/' + value, function() {

        });
    });

    $('#item_category').change(function() {
        var value = $(this).val();

        $('#choose-subcategory img.load').fadeIn();

        $('#item_subcategory').load(base_url + 'goadmin/product_category/show_subcategory/' + value, function() {
            $('#choose-subcategory img.load').fadeOut();
        });
    });

    //banner section
    $("#banner_section").on('change', function() {
        var banner_width = $('select[name="section"] :selected').attr('size-width') || "";
        var banner_height = $('select[name="section"] :selected').attr('size-height') || "";
        var html = "<small>Recommended size " + banner_width + " * " + banner_height + " px</small>";
        $(".banner-size-help").html(html);
    });

     //media category
    $(".media_category").on('change', function() {
        var banner_width = $('select[name="media_category"] :selected').attr('size-width') || "";
        var banner_height = $('select[name="media_category"] :selected').attr('size-height') || "";
        var html = "<small>Recommended size " + banner_width + " * " + banner_height + " px</small>";
        $(".media-size-help").html(html);
    });

    // Section
    $("#s_type").on('change', function() {
        var icon_parent = $('select[name="s_type"] :selected').val();
        if (icon_parent == 2) {
            $(".banner-section").show();
            $(".banner-section").find('input').addClass("required");
        } else {
            $(".banner-section").hide();
            $(".banner-section").find('input').removeClass("required");

        }
    });

    // News Date

    $("#news_type").on('change', function() {
        var val = $('select[name="type"] :selected').val();
        if (val == 2) {
            $(".news-end").show();
            $(".news-end").find('input').addClass("required");

        } else {
            $(".news-end").hide();
            $(".news-end").find('input').removeClass("required");
            $(".news-end").find('input').val('');


        }
    });

    // Protocol mail 

    $("#mail_protocol").on('change', function() {
        mail_protocol();
    });

    function mail_protocol(){
        var icon_parent = $('select[name="mail_protocol"] :selected').val();
        if (icon_parent == "smtp") {
            $(".mailgun-mail").hide();
            $(".smtp-mail").show();
        }else if (icon_parent == "mailgun") {
            $(".mailgun-mail").show();
            $(".smtp-mail").hide();
        } else {
            $(".mailgun-mail").hide();
            $(".smtp-mail").hide();

        }
    }


    $("#check-end").on('click', function() {
        banner_end_date();
    });
    $("#checkseo").on('click', function() {
        checkseo();
    });

    $("#type").on('change', function() {
        gallery_type();
    });

});

$(window).on('beforeunload', function(e) {
    if (formChange) {
        return 'There is unsaved changes. Do you really want to quit?';
    }
});

function banner_end_date() {

    if ($("#check-end").prop('checked') == true) {
        $(".banner-end").show();
        $(".banner-end").addClass("required");
        $(".datepicker-end").datepicker('show');

    } else {
        $(".banner-end").hide();
        $(".banner-end").removeClass("required");


    }
}

function checkseo() {

    if ($("#checkseo").prop('checked') == true) {
        $(".checkseo-bar").show();

    } else {
        $(".checkseo-bar").hide();


    }
}


function gallery_type() {
    var val = $('select[name="type"] :selected').val();
    if (val == 1) {
        $(".type-photo").show();
        $(".type-photo").find('input[type=text]').addClass("required");
        $(".type-video").hide();
        $(".type-video").find('input').removeClass("required");

    } else if (val == 2) {
        $(".type-video").show();
        $(".type-video").find('input').addClass("required");
        $(".type-photo").hide();
        $(".type-photo").find('input[type=text]').removeClass("required");

    } else {
        $(".type-photo, .type-video").hide();
        $(".type-video").find('input').removeClass("required");
        $(".type-photo").find('input[type=text]').removeClass("required");


    }
}
$(function(){
    $("#addForm").modal('hide');
    $("#addForm").on('show.bs.modal', function (event) {
          var button = $(event.relatedTarget);
          var recipient = button.data('table-cms');
          var modal = $(this);
          modal.find('.modal-title').text('Add New ' + recipient + ' Category');
          // modal.find('.modal-body label').text( 'Name');
          modal.find('input[type=text]').val('');
    });


    $('#modal-form').validate({
        submitHandler: function(form) {
            alert('asd');
        }
    });

    $("#modal-form-cms").validate({
        submitHandler: function(form){
           $.ajax({
                url: base_url + 'goadmin/sequential/add_category', 
                type: form.method,             
                data: $(form).serialize(),
                cache: false,             
                dataType: 'JSON',     
                success: function(json) {
                    var select = document.getElementById('cms_category');
                    var opt = document.createElement('option');
                    opt.value = json.option_value;
                    opt.innerHTML = json.option;
                    console.log(opt);
                    select.appendChild(opt);
                    $("#addForm").modal('hide');
                }
            });
        }
    });
    $("#video").keydown(function(){
        var id_video = $(this).val();
        paste_video(id_video);
    });
    $("#video").bind('paste', function(event) {
        var _this = this;
        setTimeout( function() {
            var id_video = $(_this).val();
            paste_video(id_video);
        }, 100);
       
    });

    $(".path_url").keydown(function(){
        var path_url = $(this).val();
        paste_path_url(path_url);        
    });

     $(".path_url").bind('paste', function(event) {
        var _this = this;
        setTimeout( function() {
            var path_url = $(_this).val();
            paste_path_url(path_url);
        }, 100);
       
    });

});
function paste_video(id_video){
        var arra     = id_video.split('=');
        if (Array.isArray) {
            var id_video = (arra.slice(-1).pop()); 
            $("#video").val(id_video);
        }
}

function paste_path_url(path_url){
    path_url = path_url.replace(base_url,"");
    $(".path_url").val(path_url);
}