"use strict";
! function(a) {
    var b = {
        CssClasses: {
            CUSTOM_SCROLLBAR: "custom-scrollbar",
            LAYOUT: "layout",
            LAYOUT_MOBILE: "layout-mobile",
            LAYOUT_TABLET: "layout-tablet",
            LAYOUT_DESKTOP: "layout-desktop",
            LAYOUT_LEFT_SIDEBAR_COLLAPSED: "layout-left-sidebar-collapsed",
            LAYOUT_LEFT_SIDEBAR_OPENED: "layout-left-sidebar-opened",
            LAYOUT_RIGHT_SIDEBAR_OPENED: "layout-right-sidebar-opened",
            LEFT_SIDEBAR_TOGGLE: "left-sidebar-toggle",
            LEFT_SIDEBAR_COLLAPSE: "left-sidebar-collapse",
            OVERLAY: "site-overlay",
            RIGHT_SIDEBAR: "site-right-sidebar",
            RIGHT_SIDEBAR_TOGGLE: "right-sidebar-toggle",
            SIDEBAR_MENU: "sidebar-menu",
            SIDEBAR_SUBMENU: "sidebar-submenu"
        },
        Options: {
            CUSTOM_SCROLLBAR: {
                distance: "4px",
                opacity: "0.2",
                height: "100%",
                size: "4px",
                touchScrollStep: 50,
                wheelStep: 10,
                width: "100%",
                barClass: "custom-scrollbar-bar",
                railClass: "custom-scrollbar-rail",
                wrapperClass: "custom-scrollable-wrapper"
            },
            LEFT_SIDEBAR_MENU: {
                activeClass: "open",
                collapseClass: "collapse",
                collapseInClass: "in",
                collapsingClass: "collapsing"
            }
        },
        init: function() {
            this.$document = a(document), this.$layout = a("." + this.CssClasses.LAYOUT), this.$leftSidebarMenu = a("." + this.CssClasses.SIDEBAR_MENU), this.$leftSidebarToggle = a("." + this.CssClasses.LEFT_SIDEBAR_TOGGLE), this.$rightSidebarToggle = a("." + this.CssClasses.RIGHT_SIDEBAR_TOGGLE), this.$leftSidebarCollapse = a("." + this.CssClasses.LEFT_SIDEBAR_COLLAPSE), this.$overlay = a("." + this.CssClasses.OVERLAY), this.$scrollableArea = a("." + this.CssClasses.CUSTOM_SCROLLBAR), this.mediaQueryListMobile = window.matchMedia("(max-width: 767px)"), this.mediaQueryListTablet = window.matchMedia("(min-width: 768px) and (max-width: 991px)"), this.mediaQueryListDesktop = window.matchMedia("(min-width: 992px)"), this.initPlugins(), this.bindEvents(), this.handleMediaQueryChangeMobile(), this.handleMediaQueryChangeTablet(), this.handleMediaQueryChangeDesktop(), this.activeMenuItem(), this.sidebarChat()
        },
        bindEvents: function() {
            this.$leftSidebarCollapse.on("click", this.handleLeftSidebarCollapse.bind(this)), this.$leftSidebarToggle.on("click", this.handleLeftSidebarToggle.bind(this)), this.$overlay.on("click", this.handleLeftSidebarToggle.bind(this)), this.$rightSidebarToggle.on("click", this.handleRightSidebarToggle.bind(this)), this.$document.on("mouseup", this.handleOutsideClick.bind(this)), this.mediaQueryListMobile.addListener(this.handleMediaQueryChangeMobile.bind(this)), this.mediaQueryListTablet.addListener(this.handleMediaQueryChangeTablet.bind(this)), this.mediaQueryListDesktop.addListener(this.handleMediaQueryChangeDesktop.bind(this))
        },
        handleRightSidebarToggle: function(a) {
            this.$layout.hasClass(this.CssClasses.LAYOUT_RIGHT_SIDEBAR_OPENED) ? this.closeRightSidebar() : this.openRightSidebar(), a.preventDefault()
        },
        openRightSidebar: function() {
            this.$layout.addClass(this.CssClasses.LAYOUT_RIGHT_SIDEBAR_OPENED)
        },
        closeRightSidebar: function() {
            this.$layout.removeClass(this.CssClasses.LAYOUT_RIGHT_SIDEBAR_OPENED)
        },
        handleOutsideClick: function(b) {
            var c = a("." + this.CssClasses.RIGHT_SIDEBAR + ", ." + this.CssClasses.RIGHT_SIDEBAR_TOGGLE);
            c.is(b.target) || 0 !== c.has(b.target).length || this.$layout.removeClass(this.CssClasses.LAYOUT_RIGHT_SIDEBAR_OPENED)
        },
        handleLeftSidebarToggle: function(a) {
            this.$layout.hasClass(this.CssClasses.LAYOUT_LEFT_SIDEBAR_OPENED) ? this.closeLeftSidebar() : this.openLeftSidebar(), a.preventDefault()
        },
        openLeftSidebar: function() {
            this.$layout.addClass(this.CssClasses.LAYOUT_LEFT_SIDEBAR_OPENED)
        },
        closeLeftSidebar: function() {
            this.$layout.removeClass(this.CssClasses.LAYOUT_LEFT_SIDEBAR_OPENED)
        },
        handleLeftSidebarCollapse: function(a) {
            this.$layout.hasClass(this.CssClasses.LAYOUT_LEFT_SIDEBAR_COLLAPSED) ? this.expandLeftSidebar() : this.collapseLeftSidebar(), a.preventDefault()
        },
        collapseLeftSidebar: function() {
            this.$layout.addClass(this.CssClasses.LAYOUT_LEFT_SIDEBAR_COLLAPSED)
        },
        expandLeftSidebar: function() {
            this.$layout.removeClass(this.CssClasses.LAYOUT_LEFT_SIDEBAR_COLLAPSED)
        },
        handleMediaQueryChangeMobile: function() {
            this.mediaQueryListMobile.matches && (this.$layout.addClass(this.CssClasses.LAYOUT_MOBILE), this.$layout.removeClass(this.CssClasses.LAYOUT_TABLET), this.$layout.removeClass(this.CssClasses.LAYOUT_DESKTOP))
        },
        handleMediaQueryChangeTablet: function() {
            this.mediaQueryListTablet.matches && (this.$layout.addClass(this.CssClasses.LAYOUT_TABLET), this.$layout.removeClass(this.CssClasses.LAYOUT_MOBILE), this.$layout.removeClass(this.CssClasses.LAYOUT_DESKTOP))
        },
        handleMediaQueryChangeDesktop: function() {
            this.mediaQueryListDesktop.matches && (this.$layout.addClass(this.CssClasses.LAYOUT_DESKTOP), this.$layout.removeClass(this.CssClasses.LAYOUT_MOBILE), this.$layout.removeClass(this.CssClasses.LAYOUT_TABLET))
        },
        activeMenuItem: function() {
            a("." + this.CssClasses.SIDEBAR_MENU + " ul li").each(function() {
                a(this).hasClass("active") && (a(this).closest("ul").addClass("in"), a(this).closest("ul").closest("li").addClass("open"))
            })
        },
        sidebarChat: function() {
            a(".sidebar-chat a, .sidebar-chat-window a").on("click", function() {
                a(".sidebar-chat").toggle(), a(".sidebar-chat-window").toggle()
            })
        },
        initPlugins: function() {
            return this.metisMenu(), this.popover(), this.slimScroll(), this.switchery(), this.tooltip(), this
        },
        metisMenu: function() {
            var a = this.Options.LEFT_SIDEBAR_MENU;
            return this.$leftSidebarMenu.metisMenu(a), this
        },
        popover: function() {
            a('[data-toggle="popover"]').popover()
        },
        slimScroll: function() {
            var b = this.Options.CUSTOM_SCROLLBAR;
            return this.$scrollableArea.slimScroll(b), a(".custom-scrollbar-bar").hide(), this
        },
        switchery: function() {
            a(".js-switch").each(function() {
                new Switchery(a(this)[0], a(this).data())
            })
        },
        tooltip: function() {
            a('[data-toggle="tooltip"]').tooltip()
        }
    };
    b.init()
}(jQuery);