$(document).ready(function(){
	$('#username').select().focus();
	
	if ($('#login_error').html() != '')
	{
		// $('#login_error').css({ 'display' : 'block' }).animate({ 'height' : 18 });
	}

	$(document).on('click', '.close-notif', function(){
		$(this).parents('.notif').fadeOut('normal');
	});

	$('#jclock').jclock({
		format: '%I:%M:%S %P'
	});
	
	$('label#date').jclock({
		format: '%A, %d %B %Y '
	});

	$('.btn-forgot-password').on('click', function(){
		$('#forgot-password').removeClass('hide').addClass('show');
		$('#login-container').removeClass('show').addClass('hide');
		$('.title-forgot').removeClass('hide').addClass('show');
		$('.title-login').removeClass('show').addClass('hide');
	});

	$('.btn-cancel-forgot').on('click', function(){
		$('#forgot-password').removeClass('show').addClass('hide');
		$('#login-container').removeClass('hide').addClass('show');
		$('.title-forgot').removeClass('show').addClass('hide');
		$('.title-login').removeClass('hide').addClass('show');
	})

	$("#refresh-captcha").click(function(){
	    $.get(base_url + 'home/cari_captcha/' , function(data){     
	        $('#captcha_image').html(data);
	    })
	});
	
	$('#form-forgot-password').validate();

	$('#login').validate({
			
		messages : {
			'username' : { required: 'Required.' },
			'password' : { required: 'Required.' }
			},
			
		// submitHandler: function(form){
		
		// 	$('img.load').fadeIn();
			
		// 	$.ajax({
		// 		type : 'POST',
		// 		data : $('#login').serialize(),
		// 		url : base_url + 'goadmin/login/validate',
		// 		success : function(html){
		// 			$('img.load').fadeOut();
					
		// 			if (html == 'success'){
		// 				$('body').fadeOut();
		// 				window.location = base_url + 'goadmin';
		// 			}
		// 			else if (html == 'incorrect'){
		// 				$('#login_error').html('username / password anda salah').css({ 'display' : 'block' }).animate({ 'height' : 18 });
		// 			}
		// 			else if (html == 'warning'){
		// 				$('#login_error').html('username tidak aktif. hubungi bagian terkait.').css({ 'display' : 'block' }).animate({ 'height' : 18 });
		// 			}
		// 		}
		// 	});
		// }
	});
});