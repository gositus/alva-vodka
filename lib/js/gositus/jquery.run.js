function getIndex(el) {
  return Array.from(el.parentNode.children).indexOf(el)
}

$(document).ready(function(){

	$("button.menu-icon-toggle").on("click", function(){
		$('header').toggleClass('open');
	});

	$(".flex-column a").on("click", function(){
		$('header').toggleClass('open');
	});
	
	$(".vodka-slider").slick({
		slidesToShow: 1,
	    fade: true,
  		cssEase: 'linear',
	    dots: true,
	    customPaging: function(slider, i) { 
	        return '<div></div><div class="if_active" style="background-color: ' + $(slider.$slides[i]).data('color') + ';"></div><span>' + $(slider.$slides[i]).attr('title') + '</span>';
	    },
		prevArrow: $(".vodka-slider-pop-prev"),
	    nextArrow: $(".vodka-slider-pop-next"),
	});

	var biggestNum = 0;
	$('#our-vodka .hello-privet').each(function() {
			var currentNum = $(this).height();
		    if(currentNum > biggestNum) {
		        biggestNum = currentNum;
		    }
		});

	$('.item .hello-privet').css('height', biggestNum);
	

	$(".gallery-slider").slick({
		slidesToShow: 4,
		prevArrow: $(".gallery-slider-prev"),
	    nextArrow: $(".gallery-slider-next"),
	    responsive: [
	    {
	      breakpoint: 600,
	      settings: {
	        slidesToShow: 2,
	      }
	    }
	    ]
	});

	 var carousel = $(".coverflow-slider").flipster({
        style: 'carousel',
        spacing: -0.5,
        nav: false,
        buttons: true,
        scrollwheel: false,
        loop: true,
        start: 0,
        onItemSwitch: (current,prev) => {
        	$('#coverflow-slider-caption').carousel(getIndex(current));
        }
    });

	 function doAnimations( elems ) {
		var animEndEv = 'webkitAnimationEnd animationend';
		
		elems.each(function () {
			var $this = $(this),
				$animationType = $this.data('animation');
			$this.addClass($animationType).one(animEndEv, function () {
				$this.removeClass($animationType);
			});
		});
	}
	
	//Variables on page load 
	var $myCarousel = $('#coverflow-slider-caption'),
		$firstAnimatingElems = $myCarousel.find('.carousel-item:first').find("[data-animation ^= 'animate__animated']");
		
	//Initialize carousel 
	$myCarousel.carousel();
	
	//Animate captions in first slide on page load 
	doAnimations($firstAnimatingElems);
	
	//Pause carousel  
	$myCarousel.carousel('pause');
	
	
	//Other slides to be animated on carousel slide event 
	$myCarousel.on('slide.bs.carousel', function (e) {
		var $animatingElems = $(e.relatedTarget).find("[data-animation ^= 'animate__animated']");
		doAnimations($animatingElems);
	});  

	// $.scrollify({
 //    	section :"section",
 //    	scrollSpeed: 1100,
 //  	});
 //  	
 // 	var myFullpage = new fullpage('#fullpage', {
 // 		licenseKey: 'YOUR KEY HERE',
	//     anchors: ['hello', 'the-mascot', 'our-vodka', 'cocktails', 'contact'],
	//     menu: '.flex-column',
	//     afterLoad (anchorLink, index){
	//        if(index['index'] > 1){
	// 	       	$("#myheader").addClass("navbar-dark-color");
	// 	       	if(index['index'] == 4){
	// 	       		$("#myheader").addClass("hide-nav");
	// 	       	}else{
	// 	       		$("#myheader").removeClass("hide-nav");
	// 	       	}
	//        }else{
	//        		$("#myheader").removeClass("navbar-dark-color");
	//        }
	//     }
	// });

	$(".section-link").mPageScroll2id({
		highlightSelector:".section-link"
	});

	var header = $("#myheader");
	lastScrollTop        = 0,
	sticky     = $("#our-vodka").offset().top,
	contact    = $("#gallery").offset().top,
	scroll     = $(window).scrollTop();


	if (scroll >= sticky || scroll >= contact){
		header.addClass("navbar-dark-color");
	}else{
		header.removeClass("navbar-dark-color");
	}

	if(scroll >= contact){
		header.addClass("hide-nav");
	}else{
		header.removeClass("hide-nav");
	}

	$(window).scroll(function(event){
		var scroll = $(window).scrollTop();

		if (scroll > contact) {
			header.addClass("hide-nav");
		} else {
			header.removeClass("hide-nav");
		} 

		if (scroll >= sticky){
			header.addClass("navbar-dark-color");
		}
		else{
			header.removeClass("navbar-dark-color");
		}
	});
	// 
	// $('#myheader').navScroll({
 //      navItemClassName: 'section-link',
 //      activeClassName: 'active',
 //      scrollSpy : true,
 //      onScrollEnd: function(e) {
 //      	var section = $(".section-link.active").closest('.nav-item').data('menuanchor');
 //      	if(section == "our-vodka" || section == "cocktails"){
 //      		header.addClass("navbar-dark-color");
 //      	}else{
 //      		header.removeClass("navbar-dark-color");
 //      	}
 //      	if(section == "contact"){
	// 		header.addClass("hide-nav");
	// 	}else{
	// 		header.removeClass("hide-nav");
	// 	}
 //      }
 //    });


 //    $(window).scroll(function(event){
 //    	var section = $(".section-link.active").closest('.nav-item').data('menuanchor');
 //    	if(section == "our-vodka" || section == "cocktails"){
 //      		header.addClass("navbar-dark-color");
 //      	}else{
 //      		header.removeClass("navbar-dark-color");
 //      	}
 //      	if(section == "contact"){
	// 		header.addClass("hide-nav");
	// 	}else{
	// 		header.removeClass("hide-nav");
	// 	}
 //    });



});



function int(amount){
	var i = parseInt(amount);
	if (isNaN(i)) i = 0;
	return i;
}


