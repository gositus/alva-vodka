<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model_message extends CI_Model {

	public function update_message(){

		$data_post = $this->input->post();
		$id= array('id'=>$data_post['id']);
		$input =	array(
						'reply'      => ($data_post['reply']),
						'replied'	 => 1,
						'flag_memo'  => 'Replied'
					);

		$this->db->update($this->url,$input,$id);
		$row = $this->db->get_where($this->url, array('id' => $data_post['id']))->row_array();
		action_log('REPLY', $this->url, $row['id'], $row['name'], 'REPLY This Message');


		$email_data = array(
			'to'            => $row['email'],
			'title'         => "Message Reply",
			'subject'       => 'Re: ' .$row['subject'],
			'to_name'       => $row['name'],
			'from'          => setting_value('email'), // diisi sesuai dengan company "dari email"
			'reply_to'      => setting_value('email'), // diisi sesuai dengan company "reply to email"
			'name'          => setting_value('company_name'),
			'reply_to_name' => setting_value('company_name'),
			'reply_cms'		=> '1'
		);

		$message =  $data_post['reply'] .'
			<table style="border-collapse:separate!important;" cellspacing="0" cellpadding="0" border="0">
	          <tbody>
			  	<tr><td height="10"></td></tr>
	            <tr>
	              <td class="td-button" valign="middle" align="center">
	                <a class="a-button" href="'.base_url() .'">'.setting_value('web_title').'</a>
	              </td>
	            </tr>
				<tr><td height="10"></td></tr>
	          </tbody>
	        </table>

			' ;

		$email_data['message'] = $message;
		$email_data['message_quote'] = array(
			'date'		=> format_date($row['date']),
			'name'		=> $row['name'],
			'email'		=> $row['email'],
			'phone'		=> $row['phone'],
			'company'	=> $row['company'],
			'address'	=> $row['address'],
			'subject'	=> $row['subject'],
			'content'	=> $row['content']
		);
 
		sendemail($email_data);
		
		action_log('UPDATE', $this->url, $row['id'], $row['name'], 'MODIFY ' . $this->title . ' ( ' . $row['name'] . ' ) ');
	}
	
	public function update_flag($row){

		$data_post = $this->input->post();
		$id= array('id'=>$row['id']);
		$input =	array(
						'flag'      => 1,
						'flag_memo' => '',
					);

		$this->db->update($this->url,$input,$id);
		action_log('UPDATE', $this->url, $row['id'], $row['name'], 'READ ' . $this->title . ' ( ' . $row['name'] . ' )');
		
	}

	public function insert_contact($data_post){

		$data_post = $this->input->post();
		
		$input = array( 
			'name'       => input_clean($data_post['name']),
			'email'      => input_clean($data_post['email']),
			'content'    => input_clean($data_post['message']),
			'ip'         => $this->input->ip_address(),
			'user_agent' => get_user_agent(),
		);

		$insert = $this->db->insert('message', $input);
		$id     = $this->db->insert_id();
		$email_data = array(
			'to'            => setting_value('email'),
			'title'         => $input['subject'],
			'subject'       => "Website / Contact #".$id,
			'to_name'       => setting_value('web_title'),
			'from'          => setting_value('email'), // diisi sesuai dengan company "reply to email"
			'name'          => setting_value('company_name'),
			'reply_to'      => $input['email'],
			'reply_to_name' => $input['name'],
			
		);
		$message = '
		Dear Administrator,<br><br>

		Incoming message from website contact form, <br><br>
		<table style="border-collapse:separate!important;" cellspacing="0" cellpadding="0" border="0">
          <tbody>
		  	<tr><td height="10"></td></tr>
            <tr>
              <td class="td-button" valign="middle" align="center">
                <a class="a-button" href="'.base_url('goadmin/message/view/'. $id) .'">View in Website</a>
              </td>
            </tr>
			<tr><td height="10"></td></tr>
          </tbody>
        </table>

		' ;

		$email_data['message'] = $message;
		$email_data['message_quote'] = $input;

		sendemail($email_data);
		
		if(!empty($id)){
			return true;
		}
	}


}
?>
