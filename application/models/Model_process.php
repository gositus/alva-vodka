<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model_process extends CI_Model {
	
	public function flag()
	{
		$table = $this->input->post('table');
		$table_clean = input_clean($table);
		$action = ($this->input->post('new_flag') == 1) ? 'ACTIVATE ' : 'DEACTIVATE ';
		if($table_clean=='newsletter'){
			$action = ($this->input->post('new_flag') == 1) ? 'SUBSCRIBE ' : 'UNSUBSCRIBE ';
		}
		$this->db->where('id', input_clean($this->input->post('id')));
		$this->db->update($table_clean, array('flag' => input_clean($this->input->post('new_flag')), 'flag_memo' => input_clean($this->input->post('flag_memo'))));
		
		$row = $this->db->get_where($table_clean, array('id' => input_clean($this->input->post('id')))) ->row_array();

		if(empty($row['name'])) {
			$row['name'] = '';
			$rows = $this->db->get_where( 'content_to_'.  $table ,array( $table_clean .'_id' => input_clean($this->input->post('id')) )) ->row_array();
			$row['name'] = $rows['name'];
		}
		
		action_log('UPDATE', $table, input_clean($this->input->post('id')), $row['name'], $action . $table . ' ( ' . $row['name'] . ' ) - Reason: ' . input_clean($this->input->post('flag_memo')));
	}
	
	public function delete()
	{
		$this->db->where('id', $this->input->post('id'));
		$this->db->update(input_clean($this->input->post('table')), array('flag' => 3));
		
		$table_with_language = explode(',', ALLOW_LANGUAGE);
	
		if ($this->input->post('table') == 'language')
		{
			foreach ($table_with_language as $table)
			{
				$this->db->where('language_id', $this->input->post('id'));
				$this->db->update($table, array('flag' => 3));
			}
		}
		
		$row = $this->db->order_by($this->input->post('table') . '_id', 'asc')->get_where($this->input->post('table'), array('id' => $this->input->post('id')))->row_array();
		
		action_log('DELETE', $this->input->post('table'), $this->input->post('id'), $row[$this->input->post('table') . '_name'], 'DELETED ' . $this->input->post('table') . ' ( ' . $row[$this->input->post('table') . '_name'] . ' ) ');
		
	}
	
	public function delete_image()
	{
		if($this->input->post('table')=='setting')
		{
			$data = array(
					'value' => ''
				);
				
			$this->db->where('key', $this->input->post('field'));
			$this->db->update($this->input->post('table'), $data);


		} else
		{
			$data = array(
					$this->input->post('field') => ''
				);
			$image = select_all_row($this->input->post('table'), array('id'=> $this->input->post('id')), TRUE);
			if(!empty($image[$this->input->post('field')])){
				unlink(FCPATH.'lib/images/'.$this->input->post('table').'/'. $image[$this->input->post('field')]);
				if($this->input->post('table')=='banner'){
					unlink(FCPATH.'lib/images/'.$this->input->post('table').'/thumb/'. $image[$this->input->post('field')]);
				}
			}
				
			$this->db->where('id', $this->input->post('id'));
			$this->db->update($this->input->post('table'), $data);


		}

	}
}