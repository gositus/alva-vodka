<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model_career extends CI_Model {

	public function get_career(){

		$default_language = setting_value('default_language');
		$query = $this->db->select("p.* , c.name as career_name, a.name as career_category_name, c.name as c_name")
				->join("content_to_" . $this->url ." c", "c.career_id = p.id" , "left")
				->join("content_to_career_category a", "p.career_category_id = a.career_category_id" , "left")
				->get_where($this->url ." p",array('c.language_id'=> $default_language,'a.language_id'=> $default_language, 'p.flag !=' => 3))->result_array();
		return $query;
	}

	public function get_career_category($table="career_category"){

		$default_language = setting_value('default_language');
		$query = $this->db->select("p.* , c.name as career_category_name, c.name as c_name")
				->join("content_to_" . $table ." c", "c.career_category_id = p.id" , "left")
				->get_where($table ." p",array('c.language_id'=> $default_language, 'p.flag !=' => 3))->result_array();
		return $query;
	}

	public function get_career_category_detail($career_id=""){

		$default_language = setting_value('default_language');
		foreach (language(TRUE)->result_array() as $lang)
			{			
				foreach ($lang as $val)
				{
					$data[$val] = $this->db->join('content_to_'.$this->url . ' c' , 'c.'.$this->url.'_id= p.id','left')->get_where($this->url . ' p', array('p.id' => $career_id, 'flag !=' => 3, 'language_id' => $val))->row_array();
				}
			}
			return ($data);
	}

	public function insert_career_category(){

		$language         = language()->result_array();
		$default_language = setting_value('default_language');
		$default_name     = input_clean($this->input->post('name_' . $default_language));

		$input = array(
				'seo_url'    => url_title(trim(input_clean($this->input->post('name_' . $default_language))), 'dash', TRUE),
				'flag'       => input_clean($this->input->post('flag')),
				'flag_memo'  => input_clean($this->input->post('flag_memo'))
		);
				
		$this->db->insert($this->url,$input);
		$id = $this->db->insert_id();
		
		foreach ($language as $lang_data)
		{
			$data = array(
					'career_category_id'     => $id,
					'language_id' => $lang_data['id'],
					'name'        => input_clean($this->input->post('name_' . $lang_data['id']))
				);
				
			$this->db->insert('content_to_' . $this->url,$data);
		}

		$row = $this->db->get_where($this->url, array('id' => $id))->row_array();
		action_log('ADD', $this->url, $row['id'], $default_name, 'ADDED ' . $this->title. ' ( ' . $default_name . ' ) ');

	}


	public function update_career_category(){
		
		$id['id']         = input_clean($this->input->post('id'));
		$row = $this->db->get_where($this->url, array('id' => $id['id']))->row_array();
		
		$language         = language()->result_array();
		$default_language = setting_value('default_language');
		$default_name     = input_clean($this->input->post('name_' . $default_language));

		
		$input = array(
				'seo_url'    => url_title(trim(input_clean($this->input->post('name_' . $default_language))), 'dash', TRUE),
				'flag'      => input_clean($this->input->post('flag')),
				'flag_memo' => input_clean($this->input->post('flag_memo'))
		);
				
		$this->db->update($this->url,$input,$id);
		$this->db->delete('content_to_' . $this->url,array('career_category_id' => $id['id']));
		
		foreach ($language as $lang_data)
		{
			$data = array(
					'career_category_id'     => $id['id'],
					'language_id' => $lang_data['id'],
					'name'        => input_clean($this->input->post('name_' . $lang_data['id']))
				);
				
			$this->db->insert('content_to_' . $this->url,$data);
		}

		
		action_log('UPDATE', $this->url, $row['id'], $default_name, 'MODIFY ' . $this->title . ' ( ' . $default_name . ' ) ');
		

	}

	public function get_detail($career_id=""){

		$default_language = setting_value('default_language');
		foreach (language(TRUE)->result_array() as $lang)
			{			
				foreach ($lang as $val)
				{
					$data[$val] = $this->db->join('content_to_'.$this->url . ' c' , 'c.'.$this->url.'_id= p.id','left')->get_where($this->url . ' p', array('p.id' => $career_id, 'flag !=' => 3, 'language_id' => $val))->row_array();
				}
			}
			return ($data);
	}

	public function insert_career(){

		$language         = language()->result_array();
		$default_language = setting_value('default_language');
		$default_name     = input_clean($this->input->post('name_' . $default_language));

		$filename ="";

		// if(input_clean($this->input->post('image'))) {
		// $image            = str_replace(base_url(), "", input_clean($this->input->post('image')));

		// 	$info = pathinfo(input_clean($this->input->post('image')));
		// 	$filename = $info['basename'];
		// 	resize_from_url(FCPATH . $image, FCPATH . 'lib/images/career/', $this->image_width, $this->image_height,$info);
			
		// }

		$image =  file_upload_name('image', 'lib/images/career', strtolower($default_name)	, FALSE,$this->image_width, $this->image_height);
		
		if ($image)
		{
			$filename = $image['file_name'];

		}


		$input = array(
				'seo_url'            => url_title(trim(input_clean($this->input->post('name_' . $default_language))), 'dash', TRUE),
				'image'              => $filename,
				'career_category_id' => input_clean($this->input->post('career_category')),
				'flag'               => input_clean($this->input->post('flag')),
				'flag_memo'          => input_clean($this->input->post('flag_memo'))
		);
				
		$this->db->insert($this->url,$input);
		$id = $this->db->insert_id();
		
		foreach ($language as $lang_data)
		{
			$data = array(
					'career_id'   => $id,
					'language_id' => $lang_data['id'],
					'name'        => input_clean($this->input->post('name_' . $lang_data['id'])),
					'requirement' => $this->input->post('requirement_' . $lang_data['id']),
					'description' => $this->input->post('description_' . $lang_data['id'])
				);
				
			$this->db->insert('content_to_' . $this->url,$data);
		}

		$row = $this->db->get_where($this->url, array('id' => $id))->row_array();
		action_log('ADD', $this->url, $row['id'], $default_name, 'ADDED ' . $this->title. ' ( ' . $default_name . ' ) ');

	}

	public function update_career(){
		
		$id['id']         = input_clean($this->input->post('id'));
		$row = $this->db->get_where($this->url, array('id' => $id['id']))->row_array();
		
		$language         = language()->result_array();
		$default_language = setting_value('default_language');
		$default_name     = input_clean($this->input->post('name_' . $default_language));
		// $image            = str_replace(base_url(), "", input_clean($this->input->post('image')));

		// if(input_clean($this->input->post('image'))) {

		// 	$info = pathinfo(input_clean($this->input->post('image')));
		// 	$filename = $info['basename'];
		// 	resize_from_url(FCPATH . $image, FCPATH . 'lib/images/career/', $this->image_width, $this->image_height, $info);
			
		// } else 
		// {
		// 	$filename = $row['image'];
		// }
		$image =  file_upload_name('image', 'lib/images/career', strtolower($default_name), FALSE,$this->image_width, $this->image_height);
		
		if ($image)
		{
			$filename = $image['file_name'];
			unlink(FCPATH.'lib/images/'.$this->url.'/'. $row['image']);

		}
		 else 
		{
			$filename = $row['image'];
		}

		$input = array(
				'seo_url'            => url_title(trim(input_clean($this->input->post('name_' . $default_language))), 'dash', TRUE),
				'image'              => $filename,
				'career_category_id' => input_clean($this->input->post('career_category')),
				'flag'               => input_clean($this->input->post('flag')),
				'flag_memo'          => input_clean($this->input->post('flag_memo'))
		);
				
		$this->db->update($this->url,$input,$id);
		$this->db->delete('content_to_' . $this->url,array('career_id' => $id['id']));
		
		foreach ($language as $lang_data)
		{
			$data = array(
					'career_id'   => $id['id'],
					'language_id' => $lang_data['id'],
					'name'        => input_clean($this->input->post('name_' . $lang_data['id'])),
					'requirement' => $this->input->post('requirement_' . $lang_data['id']),	
					'description' => $this->input->post('description_' . $lang_data['id'])	
				);
				
			$this->db->insert('content_to_' . $this->url,$data);
		}

		action_log('UPDATE', $this->url, $row['id'], $default_name, 'MODIFY ' . $this->title . ' ( ' . $default_name . ' ) ');
		

	}
}
?>
