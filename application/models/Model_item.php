<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model_item extends CI_Model {

	public function get_item(){

		$default_language = setting_value('default_language');
		$query = $this->db->order_by('p.sort','asc')
				->select("p.* , c.name as item_name, a.name as category_name , c.name as c_name")
				->join("content_to_" . $this->url ." c", "c.item_id = p.id" , "left")
				->join("content_to_item_category a", "p.item_category_id = a.item_category_id" , "left")
				->get_where($this->url ." p",array('c.language_id'=> $default_language,'a.language_id'=> $default_language, 'p.flag !=' => 3))->result_array();
		return $query;
	}

	public function get_category($table="item_category"){

		$default_language = setting_value('default_language');
		$query = $this->db->order_by('p.id','desc')
				->select("p.* , c.name as category_name, c.name as c_name")
				->join("content_to_" . $table ." c", "c.item_category_id = p.id" , "left")
				->get_where($table ." p",array('c.language_id'=> $default_language, 'p.flag !=' => 3))->result_array();
		return $query;
	}

	public function get_category_detail($item_id=""){

		$default_language = setting_value('default_language');
		foreach (language(TRUE)->result_array() as $lang)
			{			
				foreach ($lang as $val)
				{
					$data[$val] = $this->db->join('content_to_'.$this->url . ' c' , 'c.'.$this->url.'_id= p.id','left')->get_where($this->url . ' p', array('p.id' => $item_id, 'flag !=' => 3, 'language_id' => $val))->row_array();
				}
			}
			return ($data);
	}

	public function insert_category(){

		$language         = language()->result_array();
		$default_language = setting_value('default_language');
		$default_name     = input_clean($this->input->post('name_' . $default_language));

		$input = array(
				'seo_url'    => url_title(trim(input_clean($this->input->post('name_' . $default_language))), 'dash', TRUE),
				'flag'       => input_clean($this->input->post('flag')),
				'flag_memo'  => input_clean($this->input->post('flag_memo'))
		);
				
		$this->db->insert($this->url,$input);
		$id = $this->db->insert_id();
		
		foreach ($language as $lang_data)
		{
			$data = array(
					'item_category_id'     => $id,
					'language_id' => $lang_data['id'],
					'name'        => input_clean($this->input->post('name_' . $lang_data['id']))
				);
				
			$this->db->insert('content_to_' . $this->url,$data);
		}

		$row = $this->db->get_where($this->url, array('id' => $id))->row_array();
		action_log('ADD', $this->url, $row['id'], $default_name, 'ADDED ' . $this->title. ' ( ' . $default_name . ' ) ');

	}


	public function update_category(){
		
		$id['id']         = input_clean($this->input->post('id'));
		$row = $this->db->get_where($this->url, array('id' => $id['id']))->row_array();
		
		$language         = language()->result_array();
		$default_language = setting_value('default_language');
		$default_name     = input_clean($this->input->post('name_' . $default_language));

		
		$input = array(
				'seo_url'    => url_title(trim(input_clean($this->input->post('name_' . $default_language))), 'dash', TRUE),
				'flag'      => input_clean($this->input->post('flag')),
				'flag_memo' => input_clean($this->input->post('flag_memo'))
		);
				
		$this->db->update($this->url,$input,$id);
		$this->db->delete('content_to_' . $this->url,array('item_category_id' => $id['id']));
		
		foreach ($language as $lang_data)
		{
			$data = array(
					'item_category_id'     => $id['id'],
					'language_id' => $lang_data['id'],
					'name'        => input_clean($this->input->post('name_' . $lang_data['id']))
				);
				
			$this->db->insert('content_to_' . $this->url,$data);
		}

		
		action_log('UPDATE', $this->url, $row['id'], $default_name, 'MODIFY ' . $this->title . ' ( ' . $default_name . ' ) ');
		

	}

	public function get_detail($item_id=""){

		$default_language = setting_value('default_language');
		foreach (language(TRUE)->result_array() as $lang)
			{			
				foreach ($lang as $val)
				{
					$data[$val] = $this->db->join('content_to_'.$this->url . ' c' , 'c.'.$this->url.'_id= p.id','left')->get_where($this->url . ' p', array('p.id' => $item_id, 'flag !=' => 3, 'language_id' => $val))->row_array();
				}
			}
			return ($data);
	}

	public function insert_item(){

		$language         = language()->result_array();
		$default_language = setting_value('default_language');
		$default_name     = input_clean($this->input->post('name_' . $default_language));

		$filename ="";
		$filename2 ="";

		// $image            = str_replace(base_url(), "", input_clean($this->input->post('image')));
		// if(input_clean($this->input->post('image')) && strpos($this->input->post('image'), '/') !== false) {

		// 	$info = pathinfo(input_clean($this->input->post('image')));
		// 	$filename = $info['basename'];
		// 	$filename = resize_from_url(FCPATH . $image, FCPATH . 'lib/images/item/', $this->image_width, $this->image_height, $info);
			
		// }

		$image =  file_upload_name('image', 'lib/images/item', strtolower($default_name), FALSE,$this->image_width, $this->image_height);
		
		if ($image)
		{
			$filename = $image['file_name'];

		}

		$bgimage =  file_upload_name('bgimage', 'lib/images/item', strtolower($default_name), FALSE,$this->image_width2, $this->image_height2);
		
		if ($bgimage)
		{
			$filename2 = $bgimage['file_name'];

		}



		$input = array(
				'seo_url'     => url_title(trim(input_clean($this->input->post('name_' . $default_language))), 'dash', TRUE),
				'image'       => $filename,
				'bgimage'       => $filename2,
				'item_category_id' => 1,
				'sort'        => input_clean($this->input->post('sort')),
				'flag'        => input_clean($this->input->post('flag')),
				'flag_memo'   => input_clean($this->input->post('flag_memo'))
		);
				
		$this->db->insert($this->url,$input);
		$id = $this->db->insert_id();
		
		foreach ($language as $lang_data)
		{
			$data = array(
					'item_id'     => $id,
					'language_id' => $lang_data['id'],
					'name'        => input_clean($this->input->post('name_' . $lang_data['id'])),
					'heading'        => input_clean($this->input->post('heading_' . $lang_data['id'])),
					'content' => $this->input->post('content_' . $lang_data['id'])
				);
				
			$this->db->insert('content_to_' . $this->url,$data);
		}

		$row = $this->db->get_where($this->url, array('id' => $id))->row_array();
		action_log('ADD', $this->url, $row['id'], $default_name, 'ADDED ' . $this->title. ' ( ' . $default_name . ' ) ');

	}

	public function update_item(){
		
		$id['id']         = input_clean($this->input->post('id'));
		$row = $this->db->get_where($this->url, array('id' => $id['id']))->row_array();
		
		$language         = language()->result_array();
		$default_language = setting_value('default_language');
		$default_name     = input_clean($this->input->post('name_' . $default_language));

		// $image            = str_replace(base_url(), "", input_clean($this->input->post('image')));
		// if(input_clean($this->input->post('image')) && strpos($this->input->post('image'), '/') !== false) {

		// 	$info = pathinfo(input_clean($this->input->post('image')));
		// 	$filename = $info['basename'];
		// 	$filename =  resize_from_url(FCPATH . $image, FCPATH . 'lib/images/item/', $this->image_width, $this->image_height, $info);
			
		// }
		$image =  file_upload_name('image', 'lib/images/item', strtolower($default_name), FALSE,$this->image_width, $this->image_height);
		
		if ($image)
		{
			$filename = $image['file_name'];

		}

		 else 
		{
			$filename = $row['image'];
			
			// unlink(FCPATH.'lib/images/'.$this->url.'/'. $row['image']);
		}


		$bgimage =  file_upload_name('bgimage', 'lib/images/item', strtolower($default_name), FALSE,$this->image_width2, $this->image_height2);
		
		if ($bgimage)
		{
			$filename2 = $bgimage['file_name'];

		}

		 else 
		{
			$filename2 = $row['bgimage'];
		}
		$input = array(
				'seo_url'     => url_title(trim(input_clean($this->input->post('name_' . $default_language))), 'dash', TRUE),
				'image'       => $filename,
				'bgimage'       => $filename2,
				'item_category_id' => 1,
				'sort'        => input_clean($this->input->post('sort')),
				'flag'        => input_clean($this->input->post('flag')),
				'flag_memo'   => input_clean($this->input->post('flag_memo'))
		);
				
		$this->db->update($this->url,$input,$id);
		$this->db->delete('content_to_' . $this->url,array('item_id' => $id['id']));
		
		foreach ($language as $lang_data)
		{
			$data = array(
					'item_id'     => $id['id'],
					'language_id' => $lang_data['id'],
					'name'        => input_clean($this->input->post('name_' . $lang_data['id'])),
					'heading'        => input_clean($this->input->post('heading_' . $lang_data['id'])),
					'content' => $this->input->post('content_' . $lang_data['id'])
				);
				
			$this->db->insert('content_to_' . $this->url,$data);
		}

		action_log('UPDATE', $this->url, $row['id'], $default_name, 'MODIFY ' . $this->title . ' ( ' . $default_name . ' ) ');
		

	}
}
?>
