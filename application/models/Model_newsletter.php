<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model_newsletter extends CI_Model {

	public function get_list(){

		$default_language = setting_value('default_language');
		$query = $this->db->select("p.* , c.name as newsletter_name")
				->join("content_to_" . $this->url ." c", "c.newsletter_id = p.id" , "left")
				->get_where($this->url ." p",array('c.language_id'=> $default_language, 'p.flag !=' => 3))->result_array();
		return $query;
	}

	public function get_detail($item_id=""){

		$default_language = setting_value('default_language');
		foreach (language(TRUE)->result_array() as $lang)
			{			
				foreach ($lang as $val)
				{
					$data[$val] = $this->db->join('content_to_'.$this->url . ' c' , 'c.'.$this->url.'_id= p.id','left')->get_where($this->url . ' p', array('p.id' => $item_id, 'flag !=' => 3, 'language_id' => $val))->row_array();
				}
			}
			return ($data);
	}

	public function insert_newsletter(){

		$language         = language()->result_array();
		$default_language = setting_value('default_language');
		$default_name     = input_clean($this->input->post('email'));
		$input = array(
				'name'       => input_clean($this->input->post('email')),
				'email'      => strtolower(input_clean($this->input->post('email'))),
				'flag'       => input_clean($this->input->post('flag')),
				'flag_memo'  => input_clean($this->input->post('flag_memo'))
		);
				
		$row = $this->db->get_where($this->url, array('email' => strtolower($this->input->post('email'))))->row_array();
		if(!empty($row)){
			$this->db->update($this->url,$input,array("email" =>strtolower($this->input->post('email'))));
			$id = $row['id'];

		} else{
			$this->db->insert($this->url,$input);
			$id = $this->db->insert_id();
		}


		$row = $this->db->get_where($this->url, array('id' => $id))->row_array();
		action_log('ADD', $this->url, $row['id'], $default_name, 'ADDED ' . $this->title. ' ( ' . $default_name . ' ) ');

	}

	public function update_newsletter(){
		
		$id['id']         = input_clean($this->input->post('id'));
		$row = $this->db->get_where($this->url, array('id' => $id['id']))->row_array();
		
		$language         = language()->result_array();
		$default_language = setting_value('default_language');
		$default_name     = input_clean($this->input->post('email'));

		
		$input = array(
				'name'       => input_clean($this->input->post('email')),
				'email'      => strtolower(input_clean($this->input->post('email'))),
				'flag'       => input_clean($this->input->post('flag')),
				'flag_memo'  => input_clean($this->input->post('flag_memo'))
		);
				
		$this->db->update($this->url,$input,$id);

		action_log('UPDATE', $this->url, $row['id'], $default_name, 'MODIFY ' . $this->title . ' ( ' . $default_name . ' ) ');
		

	}
	public function get_filter(){

		if($this->input->post('subscribe')){

			$subscribe = input_clean($this->input->post('subscribe'));
			$this->db->where("flag",$subscribe);
		}else {
			$this->db->where("flag !=",3);
		}
		$data = $this->db->get($this->url)->result_array();
		return $data;
	}
}
?>
