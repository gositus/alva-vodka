    <?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model_doku extends CI_Model {
    var $table = 'dokuonecheckout';

    public function addOrderDoku($data) {
        $input = array(
            'ip_address'                => isset($data['ip_address']) ? $data['ip_address'] : $this->input->ip_address(),        
            'process_type'              => isset($data['process_type']) ? $data['process_type'] : 'REQUEST',        
            'process_datetime'          => isset($data['process_datetime']) ? $data['process_datetime'] : date('Y-m-d H:i:s'),        
            'transidmerchant'           => isset($data['transidmerchant']) ? $data['transidmerchant'] : '',    
            'amount'                    => isset($data['amount']) ? $data['amount'] : 0,        
            'words'                     => isset($data['words']) ? $data['words'] : '',            
            'session_id'                => isset($data['session_id']) ? $data['session_id'] : '',    
            'message'                   => isset($data['message']) ? $data['message'] : '',   
            'doku_payment_datetime'     => isset($data['doku_payment_datetime']) ? $data['doku_payment_datetime'] : '',                 
            'notify_type'               => isset($data['notify_type']) ? $data['notify_type'] : '',    
            'response_code'             => isset($data['response_code']) ? $data['response_code'] : '',    
            'status_code'               => isset($data['status_code']) ? $data['status_code'] : '',    
            'result_msg'                => isset($data['result_msg']) ? $data['result_msg'] : '',    
            'reversal'                  => isset($data['reversal']) ? $data['reversal'] : 0,
            'approval_code'             => isset($data['approval_code']) ? $data['approval_code'] : '',    
            'payment_channel'           => isset($data['payment_channel']) ? $data['payment_channel'] : '',        
            'payment_code'              => isset($data['payment_code']) ? $data['payment_code'] : '',    
            'bank_issuer'               => isset($data['bank_issuer']) ? $data['bank_issuer'] : '',    
            'creditcard'                => isset($data['creditcard']) ? $data['creditcard'] : '',    
            'verify_id'                 => isset($data['verify_id']) ? $data['verify_id'] : '',
            'verify_score'              => isset($data['verify_score']) ? $data['verify_score'] : 0,    
            'verify_status'             => isset($data['verify_status']) ? $data['verify_status'] : '',    
            'check_status'              => isset($data['check_status']) ? $data['check_status'] : 0,    
            'count_check_status'        => isset($data['count_check_status']) ? $data['count_check_status'] : 0,            
        );

        $this->db->insert($this->table, $input);
        return $this->db->insert_id();
    }

    public function updateOrderDoku($data = array(), $where) {

        if(isset($data['ip_address'])) {
            $input['ip_address'] = isset($data['ip_address']) ? $data['ip_address'] : $this->input->ip_address();        
        }
        if(isset($data['process_type'])) {
            $input['process_type'] = isset($data['process_type']) ? $data['process_type'] : 'REQUEST';        
        }
        if(isset($data['process_datetime'])) {
            $input['process_datetime'] = isset($data['process_datetime']) ? $data['process_datetime'] : date('Y-m-d H:i:s');
        }
        if(isset($data['transidmerchant'])) {
            $input['transidmerchant'] = isset($data['transidmerchant']) ? $data['transidmerchant'] : '';  
        }
        if(isset($data['amount'])) {
            $input['amount'] = isset($data['amount']) ? $data['amount'] : 0;
        }
        if(isset($data['words'])) {
            $input['words'] = isset($data['words']) ? $data['words'] : '';      
        }
        if(isset($data['session_id'])) {
        $input['session_id'] = isset($data['session_id']) ? $data['session_id'] : '';  
        }
        if(isset($data['message'])) {
            $input['message'] = isset($data['message']) ? $data['message'] : '';   
        }
        if(isset($data['doku_payment_datetime'])) {
            $input['doku_payment_datetime'] = isset($data['doku_payment_datetime']) ? $data['doku_payment_datetime'] : '';
        }
        if(isset($data['notify_type'])) {
            $input['notify_type'] = isset($data['notify_type']) ? $data['notify_type'] : '';    
        }
        if(isset($data['response_code'])) {
            $input['response_code'] = isset($data['response_code']) ? $data['response_code'] : '';    
        }
        if(isset($data['status_code'])) {
            $input['status_code'] = isset($data['status_code']) ? $data['status_code'] : '';    
        }
        if(isset($data['result_msg'])) {
            $input['result_msg'] = isset($data['result_msg']) ? $data['result_msg'] : '';    
        }
        if(isset($data['reversal'])) {
            $input['reversal'] = isset($data['reversal']) ? $data['reversal'] : 0;
        }
        if(isset($data['approval_code'])) {
            $input['approval_code'] = isset($data['approval_code']) ? $data['approval_code'] : '';    
        }
        if(isset($data['payment_channel'])) {
            $input['payment_channel'] = isset($data['payment_channel']) ? $data['payment_channel'] : '';        
        }
        if(isset($data['payment_code'])) {
            $input['payment_code'] = isset($data['payment_code']) ? $data['payment_code'] : '';    
        }
        if(isset($data['bank_issuer'])) {
            $input['bank_issuer'] = isset($data['bank_issuer']) ? $data['bank_issuer'] : '';    
        }
        if(isset($data['creditcard'])) {
            $input['creditcard'] = isset($data['creditcard']) ? $data['creditcard'] : '';    
        }
        if(isset($data['verify_id'])) {
            $input['verify_id'] = isset($data['verify_id']) ? $data['verify_id'] : '';
        }
        if(isset($data['verify_score'])) {
            $input['verify_score'] = isset($data['verify_score']) ? $data['verify_score'] : 0;    
        }
        if(isset($data['verify_status'])) {
            $input['verify_status'] = isset($data['verify_status']) ? $data['verify_status'] : '';    
        }
        if(isset($data['check_status'])) {
            $input['check_status'] = isset($data['check_status']) ? $data['check_status'] : 0;    
        }
        if(isset($data['count_check_status'])) {
            $input['count_check_status'] = isset($data['count_check_status']) ? $data['count_check_status'] : 0;            
        }

        return $this->db->update($this->table, $input, $where);
    }

    public function getDoku($where = array(), $like = array(), $order = array(), $limit = '') {
        $table = $this->table;

        $this->db->select('*');

        if (!empty($where)) {
            $this->db->where($where);
        }
        
        if (!empty($like)) {
            $this->db->like($like);
        }

        if (!empty($order)) {
            $this->db->order_by($order['order_by'], $order['sort']);
        }

        if ($limit) {
            $this->db->limit($limit);
        }

        return $this->db->get($table);
    }
}