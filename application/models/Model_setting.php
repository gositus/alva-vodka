<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model_Setting extends CI_Model {

	public function update_web_description(){

		$data_post = $this->input->post();
		$update = "";
		$company = setting_value('company_name');
		$company = url_title(trim($company), 'dash', TRUE);
		$fileico  = file_upload_name('faviconico', 'lib/assets/favicon', 'favicon', FALSE,'','','','web');
		$file16  = file_upload_name('favicon32', 'lib/assets/favicon', 'favicon', FALSE,'','','','web');
		$file32  = file_upload_name('favicon32', 'lib/assets/favicon', 'favicon32x32', FALSE,'','','','web');
		$file180  = file_upload_name('favicon512', 'lib/assets/favicon', 'favicon180x180', FALSE,'','','','web');
		$file192  = file_upload_name('favicon512', 'lib/assets/favicon', 'favicon192x192', FALSE,'','','','web');
		$filefaviconsvg  = file_upload_name('faviconsvg', 'lib/assets/favicon', 'faviconsvg', FALSE,'','','','web');
		$file512  = file_upload_name('favicon512', 'lib/assets/favicon', 'favicon512x512', FALSE,'','','','web');

		// $logo    = file_upload_overwrite('logo', 'lib/images', FALSE);
		$logo    = file_upload_name('logo', 'lib/assets/logo', 'logo-'.$company, FALSE,'','','','web');
		// $logos    = file_upload_overwrite('logo', 'lib/assets/square', FALSE);

		if ($fileico)
		{
			$fileico = $fileico['file_name'];
			setting_update("faviconico", $fileico);

		}
		if ($file16)
		{
			$image16 = image_resize($file16, 16, 16);
			$file16 = $file16['file_name'];
			setting_update("favicon16", $file16);

		}
		if ($file32)
		{
			$image32 = image_resize($file32, 32, 32);
			$file32 = $file32['file_name'];
			setting_update("favicon32", $file32);

		}
		if ($file180)
		{
			$image180 = image_resize($file180, 180, 180);
			$file180 = $file180['file_name'];
			setting_update("favicon180", $file180);

		}
		if ($file192)
		{
			$image192 = image_resize($file192, 192, 192);
			$file192 = $file192['file_name'];
			setting_update("favicon192", $file192);

		}
		if ($file512)
		{
			$image512 = image_resize($file512, 512, 512);
			$file512 = $file512['file_name'];
			setting_update("favicon512", $file512);

		}

	
		if ($filefaviconsvg)
		{
			
			$filefaviconsvg = $filefaviconsvg['file_name'];
			setting_update("faviconsvg", $filefaviconsvg);
		}

		if ($logo)
		{
			$image = image_resize($logo, $this->image_width, $this->image_height);
			// $images = image_resize_square($logos, 100, 100);
			$logo = $image['file_name'];
			$images = resize_with_white_bg($logo,500,500,'lib/assets/logo/', false);
			setting_update("logo", $logo);
			$cache = str_replace(base_url(), '', $images);
			setting_update("logo_cache", $cache);
		}

		$setting = select_all_row($this->table, array('category' => $this->url, 'flag !=' => 3, 'input != ' => 'file'));
			foreach ($setting as $key => $value) {
				if($value['key']=='corporate_color'){
					setting_update($value['key'], "#". input_clean($data_post[$value['key']]));
				}else{
					setting_update($value['key'], input_clean($data_post[$value['key']]));

				}
			}

			action_log('UPDATE', $this->url, 1, '', 'MODIFY ' . $this->title);
	}

	public function update_contact(){

			$data_post = $this->input->post();
			$setting = select_all_row($this->table, array('category' => $this->url, 'flag !=' => 3));
			$social = select_all_row($this->table, array('category' => 'social', 'flag !=' => 3));
			
			foreach ($setting as $key => $value) {
				setting_update($value['key'], input_clean($data_post[$value['key']]));
			}

			foreach ($social as $key => $value) {
				setting_update($value['key'], input_clean($data_post[$value['key']]));
			}
			action_log('UPDATE', $this->url, 1, '', 'MODIFY ' . $this->title);
	}

	public function update_script(){

			$data_post = $this->input->post();
			$script = select_all_row($this->table, array('category' => $this->url, 'flag !=' => 3));
			foreach ($script as $key => $value) {
				setting_update($value['key'], $data_post[$value['key']]);
			}
			$captcha = select_all_row($this->table, array('category' => 'captcha', 'flag !=' => 3));
			foreach ($captcha as $key => $value) {
				setting_update($value['key'], input_clean($data_post[$value['key']]));
			}

			action_log('UPDATE', $this->url, 1, '', 'MODIFY ' . $this->title);
	}

	public function update_mail_config(){

			$data_post = $this->input->post();
			$mail_config = select_all_row($this->table, array('category' => $this->url, 'flag !=' => 3));
			foreach ($mail_config as $key => $value) {
				setting_update($value['key'], $data_post[$value['key']]);
			}

			action_log('UPDATE', $this->url, 1, '', 'MODIFY ' . $this->title);
	}

	public function update_instagram_config(){

			$data_post = $this->input->post();
			$mail_config = select_all_row($this->table, array('category' => $this->url, 'flag !=' => 3));
			foreach ($mail_config as $key => $value) {
				setting_update($value['key'], $data_post[$value['key']]);
			}

			setting_update('ig_token_date', date("Y-m-d"));
			action_log('UPDATE', $this->url, 1, '', 'MODIFY ' . $this->title);
	}
}
?>
