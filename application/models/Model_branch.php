<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model_branch extends CI_Model {

	public function insert_branch(){

		$data_post = $this->input->post();
		$input =array(
					'name'       => input_clean($data_post['name']),
					'coordinate' => input_clean(str_replace(" ", "", $data_post['coordinate'])),
					'phone'      => input_clean($data_post['phone']),
					'email'      => input_clean($data_post['email']),
					'mobile'     => input_clean($data_post['mobile']),
					'country_id' => input_clean($data_post['country_id']),
					'city_id'    => input_clean($data_post['city_id']),
					'flag'       => input_clean($data_post['flag']),
					'flag_memo'  => input_clean($data_post['flag_memo'])
					);
		$this->db->insert($this->url,$input);

		$row = $this->db->get_where($this->url, array('id' => $this->db->insert_id()))->row_array();
		
		action_log('ADD', $this->url, $row['id'], $row['name'], 'ADDED ' . $this->title. ' ( ' . $row['name'] . ' ) ');

	}

	public function update_branch(){

		$data_post = $this->input->post();
		$id= array('id'=>input_clean($data_post['id']));
		$input =array(
					'name'       => input_clean($data_post['name']),
					'coordinate' => input_clean(str_replace(" ", "", $data_post['coordinate'])),
					'phone'      => input_clean($data_post['phone']),
					'email'      => input_clean($data_post['email']),
					'mobile'     => input_clean($data_post['mobile']),
					'country_id' => input_clean($data_post['country_id']),
					'city_id'    => input_clean($data_post['city_id']),
					'flag'       => input_clean($data_post['flag']),
					'flag_memo'  => input_clean($data_post['flag_memo'])
					);

		$this->db->update($this->url,$input,$id);

		// Query for log :)
		$row = $this->db->get_where($this->url, array('id' => $data_post['id']))->row_array();
		
		action_log('UPDATE', $this->url, $row['id'], $row['name'], 'MODIFY ' . $this->title . ' ( ' . $row['name'] . ' ) ');
		
	}

}
?>
