<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model_news extends CI_Model {

	public function get_list(){

		$default_language = setting_value('default_language');
		$expired = $this->db->get_where($this->url, array('end <' => date("Y-m-d"),'end !=' =>'0000-00-00 00:00:00'))->result_array();
		if(!empty($expired)){
			foreach ($expired as $key => $value) {
				$this->db->update($this->url,array('flag'=>2, 'flag_memo'=>'expired'),array('id'=>$value['id']));
			}
		}
		$query = $this->db->order_by('p.start', 'desc')
				->select("p.* , c.name as news_name, c.name as c_name")
				->join("content_to_" . $this->url ." c", "c.news_id = p.id" , "left")
				->get_where($this->url ." p",array('c.language_id'=> $default_language, 'p.flag !=' => 3))->result_array();
		return $query;
	}

	public function get_detail($item_id=""){

		$default_language = setting_value('default_language');
		foreach (language(TRUE)->result_array() as $lang)
			{			
				foreach ($lang as $val)
				{
					$data[$val] = $this->db->join('content_to_'.$this->url . ' c' , 'c.'.$this->url.'_id= p.id','left')->get_where($this->url . ' p', array('p.id' => $item_id, 'flag !=' => 3, 'language_id' => $val))->row_array();
				}
			}
			return ($data);
	}

	public function insert_news(){

		$language         = language()->result_array();
		$default_language = setting_value('default_language');
		$default_name     = input_clean($this->input->post('name_' . $default_language));

		$filename ="";

		// $image            = str_replace(base_url(), "", input_clean($this->input->post('image')));
		// if(input_clean($this->input->post('image')) && strpos($this->input->post('image'), '/') !== false) {

		// 	$info = pathinfo(input_clean($this->input->post('image')));
		// 	$filename = $info['basename'];
		// 	$filename=resize_from_url(FCPATH . $image, FCPATH . 'lib/images/news/', $this->image_width, $this->image_height,$info);
			
		// }

		$image =  file_upload_name('image', 'lib/images/news', strtolower($default_name)	, FALSE,$this->image_width, $this->image_height);
		
		if ($image)
		{
			$filename = $image['file_name'];

		}

		$end_date="";
		if($this->input->post('end')){
			$end_date= input_clean(format_date($this->input->post('end'),TRUE));
		}
		$seo_url =input_clean($this->input->post('seo_url'));
		if(empty($seo_url)){
			$seo_url = create_seo_url(input_clean($this->input->post('name_' . $default_language)), $this->url);
		}
		$input = array(
				'seo_url'	 => $seo_url,
				'image'      => $filename,
				'type'       => input_clean($this->input->post('type')),
				'start'      => input_clean(format_date($this->input->post('start'),TRUE)),
				'end'        => $end_date,
				'admin_id'   => $this->session->userdata('admin_id'),
				'flag'       => input_clean($this->input->post('flag')),
				'flag_memo'  => input_clean($this->input->post('flag_memo')),
				'date_added' => date('Y-m-d H:i:s')
		);
				
		$this->db->insert($this->url,$input);
		$id = $this->db->insert_id();
		
		foreach ($language as $lang_data)
		{
			$data = array(
					'news_id'          => $id,
					'language_id'      => $lang_data['id'],
					'name'             => input_clean($this->input->post('name_' . $lang_data['id'])),
					'image_alt'        => input_clean($this->input->post('image_alt_' . $lang_data['id'])),
					'meta_title'       => input_clean($this->input->post('meta_title_' . $lang_data['id'])),
					'meta_keyword'     => input_clean($this->input->post('meta_keyword_' . $lang_data['id'])),
					'meta_description' => input_clean($this->input->post('meta_description_' . $lang_data['id'])),
					'content'          => $this->input->post('content_' . $lang_data['id'])
				);
				
			$this->db->insert('content_to_' . $this->url,$data);
		}

		$row = $this->db->get_where($this->url, array('id' => $id))->row_array();
		action_log('ADD', $this->url, $row['id'], $default_name, 'ADDED ' . $this->title. ' ( ' . $default_name . ' ) ');

	}

	public function update_news(){
		
		$id['id']         = input_clean($this->input->post('id'));
		$row = $this->db->get_where($this->url, array('id' => $id['id']))->row_array();
		
		$language         = language()->result_array();
		$default_language = setting_value('default_language');
		$default_name     = input_clean($this->input->post('name_' . $default_language));

		// $image            = str_replace(base_url(), "", input_clean($this->input->post('image')));
		// if(input_clean($this->input->post('image')) && strpos($this->input->post('image'), '/') !== false) {

		// 	$info = pathinfo(input_clean($this->input->post('image')));
		// 	$filename = $info['basename'];
		// 	$filename=resize_from_url(FCPATH . $image, FCPATH . 'lib/images/news/', $this->image_width, $this->image_height,$info);
			
		// }

		$image =  file_upload_name('image', 'lib/images/news', strtolower($default_name), FALSE,$this->image_width, $this->image_height);
		
		if ($image)
		{
			$filename = $image['file_name'];

		}
		 else 
		{
			$filename = $row['image'];
			
			unlink(FCPATH.'lib/images/'.$this->url.'/'. $row['image']);
		}
		$end_date="";
		if($this->input->post('end')){
			$end_date= input_clean(format_date($this->input->post('end'),TRUE));
		}
		$seo_url =input_clean($this->input->post('seo_url'));
		if(empty($seo_url)){
			$seo_url = $row['seo_url'];
		}
		$input = array(
				'seo_url'	 => $seo_url,
				'image'     => $filename,
				'type'      => input_clean($this->input->post('type')),
				'start'     => input_clean(format_date($this->input->post('start'),TRUE)),
				'end'       => $end_date,
				'admin_id'  => $this->session->userdata('admin_id'),
				'flag'      => input_clean($this->input->post('flag')),
				'flag_memo' => input_clean($this->input->post('flag_memo'))
		);
				
		$this->db->update($this->url,$input,$id);
		$this->db->delete('content_to_' . $this->url,array('news_id' => $id['id']));
		
		foreach ($language as $lang_data)
		{
			$data = array(
					'news_id'          => $id['id'],
					'language_id'      => $lang_data['id'],
					'name'             => input_clean($this->input->post('name_' . $lang_data['id'])),
					'image_alt'        => input_clean($this->input->post('image_alt_' . $lang_data['id'])),
					'meta_title'       => input_clean($this->input->post('meta_title_' . $lang_data['id'])),
					'meta_keyword'     => input_clean($this->input->post('meta_keyword_' . $lang_data['id'])),
					'meta_description' => input_clean($this->input->post('meta_description_' . $lang_data['id'])),
					'content'          => $this->input->post('content_' . $lang_data['id'])
				);
				
			$this->db->insert('content_to_' . $this->url,$data);
		}

		action_log('UPDATE', $this->url, $row['id'], $default_name, 'MODIFY ' . $this->title . ' ( ' . $default_name . ' ) ');
		

	}
}
?>
