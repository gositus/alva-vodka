<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model_front extends CI_Model {

	// get banner berdasarkan section
	public function get_banner($section="" ,$single=FALSE)
	{
		$table = 'banner';
		$array = array(
			'flag'	=> 1,
			'language_id' => current_language('id'),
			'start <= ' => date("Y-m-d")
		);
		$where_or = '(end = "0000-00-00 00:00:00" OR end >= CURDATE() )';
		if(!empty($section)){
			$array['section_id'] = $section;
		}

		$query = $this->db
            ->order_by('sort', 'asc')
            ->order_by('start', 'desc')
            ->select("*")
            ->join('content_to_'. $table . ' ct', 'ct.' . $table .'_id = t.id', 'left')
            ->where($array)
            ->where($where_or)
            ->get($table . ' t');
		if($single)
		{
			return $query->row_array();
		} else 
		{
			return $query->result_array();	
		}
	}

	public function get_page($section="" ,$single=FALSE)
	{

		$table = 'page';
		$array = array(
			'flag'	=> 1,
			'language_id' => current_language('id'),
		);
		if(!empty($section)){
			$array['section_id'] = $section;
		}

		$query = $this->db->select("*")
		->join('content_to_'. $table . ' ct', 'ct.' . $table .'_id = t.id', 'left')
		->where($array)
		->get($table . ' t');

		if($single)
		{
			return $query->row_array();
		} else 
		{
			return $query->result_array();	
		}
	}

    // Display all ACTIVE News
	public function get_news($seo_url="" ,$type="" ,$single=FALSE)
	{
		$table = 'news';
		$array = array(
			'flag'	=> 1,
			'language_id' => current_language('id'),
			'start <=' => date("Y-m-d")
		);
		$where_or = '(end = "0000-00-00 00:00:00" OR end >= CURDATE() )';
		if(!empty($type)){
			$array['type'] = $type;
		}
		if(!empty($seo_url)){
			$array['seo_url'] = $seo_url;
		}

		$query = $this->db
            ->order_by('t.start','desc')
            ->order_by('t.id','desc')
            ->select("*")
            ->join('content_to_'. $table . ' ct', 'ct.' . $table .'_id = t.id', 'left')
            ->where($array)
            ->where($where_or)
            ->get($table . ' t');

		if($single)
		{
			return $query->row_array();
		} else 
		{
			return $query->result_array();	
		}
	}

	 // Display all ACTIVE media
	public function get_media($seo_url="" ,$type="" ,$single=FALSE)
	{
		$table = 'media';
		$array = array(
			'flag'	=> 1,
			'language_id' => current_language('id')
		);
		
		if(!empty($type)){
			$array['media_category_id'] = $type;
		}
		if(!empty($seo_url)){
			$array['seo_url'] = $seo_url;
		}

		$query = $this->db
            ->order_by('t.id','desc')
            ->select("*")
            ->join('content_to_'. $table . ' ct', 'ct.' . $table .'_id = t.id', 'left')
            ->where($array)
            ->get($table . ' t');

		if($single)
		{
			return $query->row_array();
		} else 
		{
			return $query->result_array();	
		}
	}

	public function get_product($seo_url="" ,$type="" ,$single=FALSE, $category_id="")
	{

		$table = 'item';
		$array = array(
			'flag'	=> 1,
			'language_id' => current_language('id'),
		);
		if(!empty($type)){
			$array['type'] = $type;
		}
		if(!empty($seo_url)){
			$array['seo_url'] = $seo_url;
		}
		if(!empty($category_id)){
			$array['item_category_id'] = $category_id;
		}

		$query = $this->db->select("*")
		->join('content_to_'. $table . ' ct', 'ct.' . $table .'_id = t.id', 'left')
		->where($array)
		->get($table . ' t');

		if($single)
		{
			return $query->row_array();
		} else 
		{
			return $query->result_array();	
		}
	}

	public function get_product_category($seo_url="" ,$type="" ,$single=FALSE)
	{

		$table = 'item_category';
		$array = array(
			'flag'	=> 1,
			'language_id' => current_language('id'),
		);
		if(!empty($type)){
			$array['type'] = $type;
		}
		if(!empty($seo_url)){
			$array['seo_url'] = $seo_url;
		}

		$query = $this->db->select("*")
		->join('content_to_'. $table . ' ct', 'ct.' . $table .'_id = t.id', 'left')
		->where($array)
		->get($table . ' t');

		if($single)
		{
			return $query->row_array();
		} else 
		{
			return $query->result_array();	
		}
	}
	public function add_subscriber(){

		$data = $this->input->post();
		$exist = select_all_row('newsletter', array('email'=> strtolower($data['email_newsletter'])), TRUE);
		if(!empty($exist)){
			$this->db->update("newsletter",array("flag"=>1),array("id"=>$exist['id']));
		} else {
			$this->db->insert("newsletter",array('email'=>strtolower($data['email_newsletter']),'name'=> $data['email_newsletter'] ) );
		}
		return true;
		
	}
}

?>