<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model_order extends CI_Model {
 var $table = 'order';

     public function addOrder($data) {
        $name = explode(' ', $data['name'], 2);

        $input = array(
            'member_id'         => isset($data['id']) ? $data['id'] : 0,   
            'item_id'  => isset($data['item_id']) ? $data['item_id'] : $this->session->userdata('id'),     
            'first_name'        => $name[0],        
            'last_name'         => (isset($name[1])) ? $name[1] : '',        
            'email'             => $data['email'],    
            'telephone'         => $data['phone'],        
            'payment_method'    => $data['payment_method'],            
            'total'             => str_replace('.', '', $data['nominal']),    
            'comment'           => nl2br($data['comment']),    
            'status_id'         => isset($data['status_id']) ? $data['status_id'] : 0,        
            'ip'                => $this->input->ip_address(),
            'date_added'        => date('Y-m-d H:i:s'),
            'anonim'            => isset($data['anonim']) ? $data['anonim'] : 0,
            'flag'              => isset($data['flag']) ? $data['flag'] : 1,
            'flag_memo'         => isset($data['flag_memo']) ? $data['flag_memo'] : '',        
        );


                
        $this->db->insert($this->table, $input);
        return $this->db->insert_id();
    }
 	public function updateOrder($data, $where) {
        $this->db->update($this->table, $data, $where);
    }
 
    public function getOrderHistory($order_id) {
        $default_language = setting_value('default_language');

        return $this->db->select('oh.*, ctos.name as status_name, a.name as admin_name')
        ->join('content_to_order_status ctos', 'oh.status_id = ctos.order_status_id', 'left')
        ->join('admin a', 'oh.user_id = a.id', 'left')
        ->get_where('order_history oh', array('order_id' => $order_id));
    }

    public function addOrderDoku($data) {
        $input = array(
            'ip_address'                => isset($data['ip_address']) ? $data['ip_address'] : $this->input->ip_address(),        
            'process_type'              => isset($data['process_type']) ? $data['process_type'] : 'REQUEST',        
            'process_datetime'          => isset($data['process_datetime']) ? $data['process_datetime'] : date('Y-m-d H:i:s'),        
            'transidmerchant'           => isset($data['transidmerchant']) ? $data['transidmerchant'] : '',    
            'amount'                    => isset($data['amount']) ? $data['amount'] : 0,        
            'words'                     => isset($data['words']) ? $data['words'] : '',            
            'session_id'                => isset($data['session_id']) ? $data['session_id'] : '',    
            'message'                   => isset($data['message']) ? $data['message'] : '',   
            'doku_payment_datetime'     => isset($data['doku_payment_datetime']) ? $data['doku_payment_datetime'] : '',                 
            'notify_type'               => isset($data['notify_type']) ? $data['notify_type'] : '',    
            'response_code'             => isset($data['response_code']) ? $data['response_code'] : '',    
            'status_code'               => isset($data['status_code']) ? $data['status_code'] : '',    
            'result_msg'                => isset($data['result_msg']) ? $data['result_msg'] : '',    
            'reversal'                  => isset($data['reversal']) ? $data['reversal'] : 0,
            'approval_code'             => isset($data['approval_code']) ? $data['approval_code'] : '',    
            'payment_channel'           => isset($data['payment_channel']) ? $data['payment_channel'] : '',        
            'payment_code'              => isset($data['payment_code']) ? $data['payment_code'] : '',    
            'bank_issuer'               => isset($data['bank_issuer']) ? $data['bank_issuer'] : '',    
            'creditcard'                => isset($data['creditcard']) ? $data['creditcard'] : '',    
            'verify_id'                 => isset($data['verify_id']) ? $data['verify_id'] : '',
            'verify_score'              => isset($data['verify_score']) ? $data['verify_score'] : 0,    
            'verify_status'             => isset($data['verify_status']) ? $data['verify_status'] : '',    
            'check_status'              => isset($data['check_status']) ? $data['check_status'] : 0,    
            'count_check_status'        => isset($data['count_check_status']) ? $data['count_check_status'] : 0,            
        );

        $this->db->insert('dokuonecheckout', $input);
        return $this->db->insert_id();
    }

    public function addOrderHistory($order_id, $status_id, $message = '', $notify = false, $data = array()) {
        $input = array(
                'order_id'      => $order_id,
                'status_id'     => $status_id,
                'notify'        => $notify,
                'comment'       => $message,
                'user_id'       => $this->session->userdata('admin_id') ? $this->session->userdata('admin_id') : '',
                'date_added'    => date('Y-m-d H:i:s'),    
            );

        $this->db->insert('order_history', $input);


        // update arr
        $update = array(
                'status_id'             => $status_id,
            );
        // change order status
        $this->db->update('donation', $update, array('id' => $order_id));

        $detail = $this->get_detail_raw($order_id);
        $order_history_num = $this->getOrderHistory($order_id)->num_rows();

        // add donation collected
        $totalQuery = $this->db->select('sum(total) as total')
                    ->get_where('donation', array('item_id' =>$detail['item_id'], 'status_id' => 3, 'flag' => 1))
                    ->row_array();
        $update = array(
                'donation_collected'    => $totalQuery['total'],
            );
        // upadate donation collected
        $this->db->update('item', $update, array('id' => $detail['item_id']));


        if ($notify) {
        	// Actionnnya 
        }

    }

    public function getDoku($where = array(), $like = array()) {
        $table = 'dokuonecheckout';

        $this->db->select('*');

        if (!empty($where)) {
            $this->db->where($where);
        }
        
        if (!empty($like)) {
            $this->db->like($like);
        }

        return $this->db->get($table);
    }
}

?>