<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model_media extends CI_Model {

	public function get_media(){

		$default_language = setting_value('default_language');
		$query = $this->db->order_by('p.id', 'desc')->select("p.* , c.name as media_name, a.name as media_category_name, c.name as c_name")
				->join("content_to_" . $this->url ." c", "c.media_id = p.id" , "left")
				->join("content_to_media_category a", "p.media_category_id = a.media_category_id" , "left")
				->get_where($this->url ." p",array('c.language_id'=> $default_language,'a.language_id'=> $default_language, 'p.flag !=' => 3))->result_array();
		return $query;
	}

	public function get_media_category($table="media_category"){

		$default_language = setting_value('default_language');
		$query = $this->db->order_by('p.id', 'desc')->select("p.* , c.name as media_category_name, c.name as c_name")
				->join("content_to_" . $table ." c", "c.media_category_id = p.id" , "left")
				->get_where($table ." p",array('c.language_id'=> $default_language, 'p.flag !=' => 3))->result_array();

		return $query;
	}

	public function get_media_category_detail($item_id=""){

		$default_language = setting_value('default_language');
		foreach (language(TRUE)->result_array() as $lang)
			{			
				foreach ($lang as $val)
				{
					$data[$val] = $this->db->join('content_to_'.$this->url . ' c' , 'c.'.$this->url.'_id= p.id','left')->get_where($this->url . ' p', array('p.id' => $item_id, 'flag !=' => 3, 'language_id' => $val))->row_array();
				}
			}
			return ($data);
	}

	public function insert_media_category(){

		$language         = language()->result_array();
		$default_language = setting_value('default_language');
		$default_name     = input_clean($this->input->post('name_' . $default_language));

		$input = array(
				'seo_url'    => url_title(trim(input_clean($this->input->post('name_' . $default_language))), 'dash', TRUE),
				'width'      => ($this->input->post('width'))? input_clean($this->input->post('width')) : $this->image_width,
				'height'     => ($this->input->post('height'))? input_clean($this->input->post('height')) : $this->image_height,
				'flag'       => input_clean($this->input->post('flag')),
				'flag_memo'  => input_clean($this->input->post('flag_memo'))
		);
				
		$this->db->insert($this->url,$input);
		$id = $this->db->insert_id();
		
		foreach ($language as $lang_data)
		{
			$data = array(
					'media_category_id'     => $id,
					'language_id' => $lang_data['id'],
					'name'        => input_clean($this->input->post('name_' . $lang_data['id']))
				);
				
			$this->db->insert('content_to_' . $this->url,$data);
		}

		$row = $this->db->get_where($this->url, array('id' => $id))->row_array();
		action_log('ADD', $this->url, $row['id'], $default_name, 'ADDED ' . $this->title. ' ( ' . $default_name . ' ) ');

	}


	public function update_media_category(){
		
		$id['id']         = input_clean($this->input->post('id'));
		$row = $this->db->get_where($this->url, array('id' => $id['id']))->row_array();
		
		$language         = language()->result_array();
		$default_language = setting_value('default_language');
		$default_name     = input_clean($this->input->post('name_' . $default_language));

		
		$input = array(
				'seo_url'    => url_title(trim(input_clean($this->input->post('name_' . $default_language))), 'dash', TRUE),
				'width'      => ($this->input->post('width'))? input_clean($this->input->post('width')) : $this->image_width,
				'height'     => ($this->input->post('height'))? input_clean($this->input->post('height')) : $this->image_height,
				'flag'      => input_clean($this->input->post('flag')),
				'flag_memo' => input_clean($this->input->post('flag_memo'))
		);
				
		$this->db->update($this->url,$input,$id);
		$this->db->delete('content_to_' . $this->url,array('media_category_id' => $id['id']));
		
		foreach ($language as $lang_data)
		{
			$data = array(
					'media_category_id'     => $id['id'],
					'language_id' => $lang_data['id'],
					'name'        => input_clean($this->input->post('name_' . $lang_data['id']))
				);
				
			$this->db->insert('content_to_' . $this->url,$data);
		}

		
		action_log('UPDATE', $this->url, $row['id'], $default_name, 'MODIFY ' . $this->title . ' ( ' . $default_name . ' ) ');
		

	}

	public function get_detail($item_id=""){

		$default_language = setting_value('default_language');
		foreach (language(TRUE)->result_array() as $lang)
			{			
				foreach ($lang as $val)
				{
					$data[$val] = $this->db->join('content_to_'.$this->url . ' c' , 'c.'.$this->url.'_id= p.id','left')->get_where($this->url . ' p', array('p.id' => $item_id, 'flag !=' => 3, 'language_id' => $val))->row_array();
				}
			}
			return ($data);
	}

	public function insert_media(){

		$language         = language()->result_array();
		$default_language = setting_value('default_language');
		$default_name     = input_clean($this->input->post('name_' . $default_language));

		$filename ="";

		// $image            = str_replace(base_url(), "", input_clean($this->input->post('image')));
		// if(input_clean($this->input->post('image')) && strpos($this->input->post('image'), '/') !== false) {

		// 	$info = pathinfo(input_clean($this->input->post('image')));
		// 	$filename = $info['basename'];
		// 	$filename =resize_from_url(FCPATH . $image, FCPATH . 'lib/images/media/', $this->image_width, $this->image_height,$info);
			
		// }
		$media_category = select_all_row('media_category',array('id'=> $this->input->post('media_category')) , TRUE );
		if(!empty($media_category['width'])){
			$width = $media_category['width'];
		} else{
			$width =$this->image_width;
		}

		if(!empty($media_category['height'])){
			$height = $media_category['height'];
		} else{
			$height =$this->image_height;
		}


		$image =  file_upload_name('image', 'lib/images/media', strtolower($default_name), FALSE,$width, $height);
		
		if ($image)
		{
			$filename = $image['file_name'];

		}

		$input = array(
				'seo_url'    => url_title(trim(input_clean($this->input->post('name_' . $default_language))), 'dash', TRUE),
				'image'      => $filename,
				'media_category_id' => input_clean($this->input->post('media_category')),
				// 'type' => input_clean($this->input->post('type')),
				// 'video' => input_clean($this->input->post('video')),
				'flag'       => input_clean($this->input->post('flag')),
				'flag_memo'  => input_clean($this->input->post('flag_memo'))
		);
				
		$this->db->insert($this->url,$input);
		$id = $this->db->insert_id();
		
		foreach ($language as $lang_data)
		{
			$data = array(
					'media_id'     => $id,
					'language_id' => $lang_data['id'],
					'name'        => input_clean($this->input->post('name_' . $lang_data['id'])),
					'content'        => input_clean($this->input->post('content_' . $lang_data['id']))
				);
				
			$this->db->insert('content_to_' . $this->url,$data);
		}

		$row = $this->db->get_where($this->url, array('id' => $id))->row_array();
		action_log('ADD', $this->url, $row['id'], $default_name, 'ADDED ' . $this->title. ' ( ' . $default_name . ' ) ');

	}

	public function update_media(){
		
		$id['id']         = input_clean($this->input->post('id'));
		$row = $this->db->get_where($this->url, array('id' => $id['id']))->row_array();
		
		$language         = language()->result_array();
		$default_language = setting_value('default_language');
		$default_name     = input_clean($this->input->post('name_' . $default_language));

		// $image            = str_replace(base_url(), "", input_clean($this->input->post('image')));
		// if(input_clean($this->input->post('image')) && strpos($this->input->post('image'), '/') !== false) {

		// 	$info = pathinfo(input_clean($this->input->post('image')));
		// 	$filename = $info['basename'];
		// 	$filename  = resize_from_url(FCPATH . $image, FCPATH . 'lib/images/media/', $this->image_width, $this->image_height,$info);
			
		// } 
		$media_category = select_all_row('media_category',array('id'=> $this->input->post('media_category')) , TRUE );
		if(!empty($media_category['width'])){
			$width = $media_category['width'];
		} else{
			$width =$this->image_width;
		}

		if(!empty($media_category['height'])){
			$height = $media_category['height'];
		} else{
			$height =$this->image_height;
		}


		$image =  file_upload_name('image', 'lib/images/media', strtolower($default_name), FALSE,$width, $height);
		
		if ($image)
		{
			$filename = $image['file_name'];
			// unlink(FCPATH.'lib/images/'.$this->url.'/'. $row['image']);

		}
		else 
		{
			$filename = $row['image'];
		}
		$input = array(
				'seo_url'   => url_title(trim(input_clean($this->input->post('name_' . $default_language))), 'dash', TRUE),
				'image'     => $filename,
				'media_category_id' => $row['media_category_id'],
				// 'type' => input_clean($this->input->post('type')),
				// 'video'  => input_clean($this->input->post('video')),
				'flag'      => input_clean($this->input->post('flag')),
				'flag_memo' => input_clean($this->input->post('flag_memo'))
		);
				
		$this->db->update($this->url,$input,$id);
		$this->db->delete('content_to_' . $this->url,array('media_id' => $id['id']));
		
		foreach ($language as $lang_data)
		{
			$data = array(
					'media_id'     => $id['id'],
					'language_id' => $lang_data['id'],
					'name'        => input_clean($this->input->post('name_' . $lang_data['id'])),
					'content'        => input_clean($this->input->post('content_' . $lang_data['id']))
				);
				
			$this->db->insert('content_to_' . $this->url,$data);
		}

		action_log('UPDATE', $this->url, $row['id'], $default_name, 'MODIFY ' . $this->title . ' ( ' . $default_name . ' ) ');
		

	}
}
?>
