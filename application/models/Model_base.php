<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model_base extends CI_Model {

	## UNTUK MENGAMBIL TABLE 
	public function get_all_list($table){
		return $this->db->get_where($table,array("flag"=>1))->result_array();
	}

	### MODULE ###
	public function get_modules($table,$where,$sort, $sort_field){

		return $this->db
		->select("m.* , n.name as parent_name")
		->order_by($sort_field,$sort)
		->join($table .' n' , 'm.parent = n.id', 'left')
		->get_where($table.' m',array("m.flag !="=>3))->result_array();
	}

	## Mencari Parent Module ##
	public function get_module_parent(){
		 return $this->db->query("SELECT * from module x WHERE x.parent =0 and x.flag =1 union select m.* from  module m LEFT JOIN module s on (m.parent = s.id)  WHERE m.parent !=0 and s.parent=0 and m.flag =1")->result_array();

	}

	public function insert_module(){
		$data_post = $this->input->post();
		$input =	array(
						'name'      => input_clean($data_post['m_name']),
						'icon'      => input_clean($data_post['m_icon']),
						'alias'      => input_clean($data_post['m_url']),
						'parent'    => input_clean($data_post['m_parent']),
						'url'       => input_clean($data_post['m_url']),
						'notes'     => input_clean($data_post['m_notes']),
						'flag'      => input_clean($data_post['flag']),
						'flag_memo' => input_clean($data_post['flag_memo'])

					);
		$this->db->insert($this->url,$input);
		$row = $this->db->get_where($this->url, array('id' => $this->db->insert_id() ))->row_array();
		action_log('ADD', $this->url, $row['id'], $row['name'], 'ADDED ' . $this->title. ' ( ' . $row['name'] . ' ) ');
	}

	public function update_module(){
		$data_post = $this->input->post();
		$id= array('id'=>input_clean($data_post['id']));
		$input =	array(
						'name'      => input_clean($data_post['m_name']),
						'icon'      => input_clean($data_post['m_icon']),
						'alias'      => input_clean($data_post['m_url']),
						'parent'    => input_clean($data_post['m_parent']),
						'url'       => $data_post['m_url'],
						'notes'     => input_clean($data_post['m_notes']),
						'flag'      => input_clean($data_post['flag']),
						'flag_memo' => input_clean($data_post['flag_memo'])

					);
		$this->db->update($this->url,$input,$id);
		$row = $this->db->get_where($this->url, array('id' => $data_post['id']))->row_array();
		action_log('UPDATE', $this->url, $row['id'], $row['name'], 'MODIFY ' . $this->title . ' ( ' . $row['name'] . ' ) ');	
	}


	## Admin Privilege ##
	public function get_parent_module(){
		return $this->db->get_where('module', array('flag' => 1, 'parent' => 0 ,'id !=' => 33));
	}

	public function insert_admin_privilege(){
		$data_post =  $this->security->xss_clean($this->input->post());
		if($data_post['ap_privilege'] == 1 || $data_post['ap_privilege'] == 2)
		{
			$module_list ='';
		} 
		else if ($data_post['ap_privilege'] == 3 ) //custom user
		{
			$modules   = $this->db->get_where('module', array('flag !=' => 3, 'parent !=' => 0));
				foreach ($modules->result_array() as $item)
				{
					if(!empty($data_post[$item['id']]))
					{

						$module_list[$item['id']] = $data_post[$item['id']];
					}
				}
			$module_list = json_encode($module_list);
		}
		if($data_post['ap_privilege'] == 1 ) $role="Super Admin";
		else if($data_post['ap_privilege'] == 2 ) $role="Data Entry";
		else if($data_post['ap_privilege'] == 3 ) $role="Custom User";

		$input =	array(
						'name'      => htmlspecialchars($data_post['ap_name']),
						'privilege' => htmlspecialchars($data_post['ap_privilege']),
						'role'      => $role,
						'module'    => $module_list,
						'flag'      => htmlspecialchars($data_post['flag']),
						'flag_memo' => htmlspecialchars($data_post['flag_memo'])

					);
		$this->db->insert($this->url,$input);
		$row = $this->db->get_where($this->url, array('id' => $this->db->insert_id()) )->row_array();
		action_log('ADD', $this->url, $row['id'], $row['name'], 'ADDED ' . $this->title. ' ( ' . $row['name'] . ' ) ');
	}

	public function update_admin_privilege(){

		$data_post =  $this->security->xss_clean($this->input->post());
		$id= array('id'=>input_clean($data_post['id']));
		if($data_post['ap_privilege'] == 1 || $data_post['ap_privilege'] == 2)
		{
			$module_list ='';
		} 
		else if ($data_post['ap_privilege'] == 3 )
		{
			$modules   = $this->db->get_where('module', array('flag !=' => 3, 'parent !=' => 0));
				foreach ($modules->result_array() as $item)
				{
					if(!empty($data_post[$item['id']]))
					{

						$module_list[$item['id']] = $data_post[$item['id']];
					}
				}
			$module_list = json_encode($module_list);
		}
		if($data_post['ap_privilege'] == 1 ) $role="Super Admin";
		else if($data_post['ap_privilege'] == 2 ) $role="Data Entry";
		else if($data_post['ap_privilege'] == 3 ) $role="Custom User";

		$input =	array(
						'name'      => htmlspecialchars($data_post['ap_name']),
						'privilege' => htmlspecialchars($data_post['ap_privilege']),
						'role'      => $role,
						'module'    => $module_list,
						'flag'      => htmlspecialchars($data_post['flag']),
						'flag_memo' => htmlspecialchars($data_post['flag_memo'])

					);
		$this->db->update($this->url,$input,$id);

		$row = $this->db->get_where($this->url, array('id' => $data_post['id']))->row_array();
		action_log('UPDATE', $this->url, $row['id'], $row['name'], 'MODIFY ' . $this->title . ' ( ' . $row['name'] . ' ) ');
	}


	## Admin
	public function get_list_admin(){
		return $this->db->query("SELECT a.* , ap.name as ap_name FROM admin a LEFT JOIN admin_privilege ap ON (a.admin_privilege_id = ap.id) WHERE a.flag != 3 AND ap.flag !=  3 AND a.id != 1 ")->result_array();
	}
	
	public function insert_admin(){
		$data_post = $this->security->xss_clean($this->input->post());
		$password = bcrypt_it($data_post['a_password']);
		$input =	array(
						'name'               => htmlspecialchars($data_post['a_name']),
						'admin_privilege_id' => htmlspecialchars($data_post['a_privilege']),
						'username'           => htmlspecialchars($data_post['a_username']),
						'password'           => $password,
						'email'              => htmlspecialchars($data_post['a_email']),
						'phone'              => htmlspecialchars($data_post['a_phone']),
						'flag'               => htmlspecialchars($data_post['flag']),
						'flag_memo'          => htmlspecialchars($data_post['flag_memo'])

					);
		$this->db->insert($this->url,$input);

		$row = $this->db->get_where($this->url, array('id' => $this->db->insert_id()))->row_array();
		
		action_log('ADD', $this->url, $row['id'], $row['name'], 'ADDED ' . $this->title. ' ( ' . $row['name'] . ' ) ');
	}

	public function update_admin(){
		$data_post = $this->security->xss_clean($this->input->post());
		$id= array('id'=>htmlspecialchars($data_post['id']));
		$input =	array(
						'name'               => htmlspecialchars($data_post['a_name']),
						'email'              => htmlspecialchars($data_post['a_email']),
						'phone'              => htmlspecialchars($data_post['a_phone']),
						

					);
		if(!empty($data_post['a_flag'])){
			$input['flag']				 = htmlspecialchars($data_post['flag']);
			$input['flag_memo']			 = htmlspecialchars($data_post['flag_memo']) ;
		}
		if(!empty($data_post['a_password']))
		{
			$password = bcrypt_it($data_post['a_password']);
			$input['password'] =  $password;
		} 
		if(!empty($data_post['a_privilege']))
		{
			
			$input['admin_privilege_id'] = htmlspecialchars($data_post['a_privilege']);
		} 
		if($this->session->userdata('admin_role')==1 && !empty($data_post['a_username']))
		{
			$input['username'] = htmlspecialchars($data_post['a_username']);
		} 
		$this->db->update($this->url,$input,$id);

		// Query for log :)
		$row = $this->db->get_where($this->url, array('id' => $data_post['id']))->row_array();
	
		action_log('UPDATE', $this->url, $row['id'], $row['name'], 'MODIFY ' . $this->title . ' ( ' . $row['name'] . ' ) ');
	}

## Language
	public function insert_language(){
		$data_post = $this->security->xss_clean($this->input->post());

		$file = file_upload('icon', 'lib/images/flag', FALSE);
		
		if ($file)
		{
			$image = image_resize($file, $this->image_width, $this->image_height);
			$file_name = $image['file_name'];
		}
		
		else $file_name = '';
		$input =	array(
						'name'      => htmlspecialchars($data_post['name']),
						'attr'      => htmlspecialchars($data_post['attr']),
						'sort'      => htmlspecialchars($data_post['sort']),
						'icon'      => $file_name,
						'flag'      => htmlspecialchars($data_post['flag']),
						'flag_memo' => htmlspecialchars($data_post['flag_memo'])

					);
		$this->db->insert($this->url,$input);

		$row = $this->db->get_where($this->url, array('id' => $this->db->insert_id()))->row_array();
		
		action_log('ADD', $this->url, $row['id'], $row['name'], 'ADDED ' . $this->title. ' ( ' . $row['name'] . ' ) ');
	}

	public function update_language(){
		$data_post = $this->security->xss_clean($this->input->post());
		$id= array('id'=>input_clean($data_post['id']));

		$file = file_upload('icon', 'lib/images/flag', FALSE);
		$row = $this->db->get_where($this->url, array('id' => $data_post['id']))->row_array();
		
		if ($file)
		{
			$image = image_resize($file, $this->image_width, $this->image_height);
			$file_name = $image['file_name'];
		}
		
		else
		{
			$file_name = $row['icon'];
		} 
		$input =	array(
						'name'      => htmlspecialchars($data_post['name']),
						'attr'      => htmlspecialchars($data_post['attr']),
						'sort'      => htmlspecialchars($data_post['sort']),
						'icon'      => $file_name,
						'flag'      => htmlspecialchars($data_post['flag']),
						'flag_memo' => htmlspecialchars($data_post['flag_memo'])

					);

		$this->db->update($this->url,$input,$id);
		// Query for log :)
		$row = $this->db->get_where($this->url, array('id' => $data_post['id']))->row_array();
		
		action_log('UPDATE', $this->url, $row['id'], $row['name'], 'MODIFY ' . $this->title . ' ( ' . $row['name'] . ' ) ');	
	}

	## Language
	public function insert_section(){
		$data_post = $this->security->xss_clean($this->input->post());
		if(!empty($data_post['s_width']) && !empty($data_post['s_height'])){
			$size = htmlspecialchars($data_post['s_width']) . ';' . htmlspecialchars($data_post['s_height']);
		} else{
			$size = '';
		}
		$input =	array(
						'name'        => htmlspecialchars($data_post['s_name']),
						'type'        => htmlspecialchars($data_post['s_type']),
						'banner_size' => $size,
						'flag'        => htmlspecialchars($data_post['flag']),
						'flag_memo'   => htmlspecialchars($data_post['flag_memo'])

					);
		$this->db->insert($this->url,$input);

		$row = $this->db->get_where($this->url, array('id' => $this->db->insert_id()))->row_array();
		
		action_log('ADD', $this->url, $row['id'], $row['name'], 'ADDED ' . $this->title. ' ( ' . $row['name'] . ' ) ');

	}

	public function update_section(){
		$data_post = $this->security->xss_clean($this->input->post());
		$id= array('id'=>input_clean($data_post['id']));
		if(!empty($data_post['s_width']) && !empty($data_post['s_height'])){
			$size = htmlspecialchars($data_post['s_width']) . ';' . htmlspecialchars($data_post['s_height']);
		} else{
			$size = '';
		}
		$input =	array(
						'name'        => htmlspecialchars($data_post['s_name']),
						'type'        => htmlspecialchars($data_post['s_type']),
						'banner_size' => $size,
						'flag'        => htmlspecialchars($data_post['flag']),
						'flag_memo'   => htmlspecialchars($data_post['flag_memo'])

					);

		$this->db->update($this->url,$input,$id);
		// Query for log :)
		$row = $this->db->get_where($this->url, array('id' => $data_post['id']))->row_array();
		
		action_log('UPDATE', $this->url, $row['id'], $row['name'], 'MODIFY ' . $this->title . ' ( ' . $row['name'] . ' ) ');
	}

	// Dashboard
	public function get_uptime(){
		$full = TRUE;
		$now = new DateTime;
		$ago = new DateTime((setting_value("live_date")));
		$diff = $now->diff($ago);

		$diff->w = floor($diff->d / 7);
		$diff->d -= $diff->w * 7;

		$uptime = array(
			'y' => 'year',
			'm' => 'month',
			'w' => 'week',
			'd' => 'day'
		);
		foreach ($uptime as $k => &$v) {
			if ($diff->$k) {
				$v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
			} else {
				unset($uptime[$k]);
			}
		}

		if (!$full) $uptime = array_slice($uptime, 0, 1);
		return $uptime ? implode(', ', $uptime) . ' ago' : 'just now 🥳';
	}

	public function get_total_visit_monthly($category){
		$total_visit = select_all_row('dashboard', array('category'=>$category,'month'=> date('m'), 'year'=>date('Y')) , TRUE);
		return !empty($total_visit['total'])? number2str($total_visit['total']):0 ;
	}

	public function get_total_page_view($category){
		$page_view = $this->db->select('sum(total) as jumlah_total')->get_where('dashboard', array('category'=>$category))->row_array();
		return !empty($page_view['jumlah_total'])? number2str($page_view['jumlah_total']):0 ;

	}

	public function get_most_view($category="",$limit="",$trending="", $front_url=""){

		if(empty($front_url)){
			$front_url = $category;
		}
		if(!empty($trending)){
			$table_current= array();

			for ($i = 0; $i < $trending; $i++){
				$date_range = date("Y-m-d", strtotime("-". ($i)  ." months"));
				$month = date("m", strtotime($date_range));
				$year = date("Y", strtotime($date_range));
				$str[$i] = "(`month` = '".$month."' AND`year` ='".$year."' )";

			}
			$str =implode(" OR ", $str);
			$most = $this->db->query
			("SELECT `ct`.`news_id` as `id_view`, sum(total) as total_view, `name` as `name_view`, '".base_url(). $front_url ."' as `link_view` , t.seo_url 
				FROM `dashboard` `d`
				right JOIN `content_to_news` `ct` ON `ct`.`news_id` = `d`.`path_id`
				right JOIN `news` `t` ON `ct`.`news_id` = `t`.`id`
				WHERE `category` = 'news'
				AND (".$str.")
				GROUP BY path_id
				ORDER BY `total_view` DESC
				LIMIT ".$limit." ")->result_array();

		} else{
			$most = $this->db
			->group_by('path_id','category')
			->order_by('total_view','desc')
			->select('ct.'.$category.'_id as id_view, sum(total) as total_view, name as name_view,"'. base_url() . $front_url .'" as link_view , t.seo_url')
			->join('content_to_'. $category . ' ct', 'ct.' . $category .'_id = d.path_id', 'right')
			->join( $category . ' t', 'ct.' . $category .'_id = t.id', 'right')
			->get_where('dashboard d', array('category'=>$category), $limit,0)
			->result_array();
		}
		return $most;
	}

}
?>
