<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model_banner extends CI_Model {

	public function get_list(){

		$default_language = setting_value('default_language');
		$expired = $this->db->get_where($this->url, array('end <' => date("Y-m-d"),'end !=' =>'0000-00-00 00:00:00'))->result_array();
		if(!empty($expired)){
			foreach ($expired as $key => $value) {
				$this->db->update($this->url,array('flag'=>2, 'flag_memo'=>'expired'),array('id'=>$value['id']));
			}
		}
		$query = $this->db->order_by('p.id', 'desc')->
		select("p.* ,  s.name as section_name , s.banner_size")
				->join("section s", "s.id= p.section_id" , "left")
				->join("content_to_" . $this->url ." c", "c.banner_id = p.id" , "left")
				->get_where($this->url ." p",array('c.language_id'=> $default_language, 'p.flag !=' => 3))->result_array();
		return $query;
	}

	public function get_detail($item_id=""){

		$default_language = setting_value('default_language');
		foreach (language(TRUE)->result_array() as $lang)
			{			
				foreach ($lang as $val)
				{
					$data[$val] = $this->db->join('content_to_'.$this->url . ' c' , 'c.'.$this->url.'_id= p.id','left')->get_where($this->url . ' p', array('p.id' => $item_id, 'flag !=' => 3, 'language_id' => $val))->row_array();
				}
			}
			return ($data);
	}

	public function insert_banner(){

		$language         = language()->result_array();
		$default_language = setting_value('default_language');
		$section 		  = input_clean($this->input->post('section'));
		$default_name     = input_clean($this->input->post('name'));
		$filename ="";

		$size = $this->db->get_where("section", array('id' => $section))->row_array();
		$banner_size = explode(";",$size['banner_size']);

		// $image            = str_replace(base_url(), "", input_clean($this->input->post('image')));
		// if(input_clean($this->input->post('image')) && strpos($this->input->post('image'), '/') !== false) {

		// 	$info = pathinfo(input_clean($this->input->post('image')));
		// 	$filename = $info['basename'];
		// 	resize_from_url(FCPATH . $image, FCPATH . 'lib/images/banner/thumb/', 100, 100,$info,TRUE);
		// 	$filename = resize_from_url(FCPATH . $image, FCPATH . 'lib/images/banner/', $banner_size[0], $banner_size[1],$info);
		// 	if(!is_dir(FCPATH . 'lib/images/banner/thumb')){
		// 		mkdir(FCPATH . 'lib/images/banner/thumb', 0775, TRUE);
		// 	}
			
		// }

		$image =  file_upload_name('image', 'lib/images/banner', strtolower($default_name), FALSE,$banner_size[0], $banner_size[1]);
		$image =  file_upload_name('image', 'lib/images/banner/thumb', strtolower($default_name), FALSE,100,100);
		if ($image)
		{
			$filename = $image['file_name'];

		}

		$end_date="";
		if($this->input->post('end')){
			$end_date= input_clean(format_date($this->input->post('end'),TRUE));
		}
		$input = array(
				'name'		 => input_clean($this->input->post('name')),
				'sort'		 => input_clean($this->input->post('sort')),
				'url'		 => input_clean($this->input->post('url')),
				'start'     => input_clean(format_date($this->input->post('start'),TRUE)),
				'end'       => $end_date,
				'image'      => $filename,
				'section_id' => input_clean($this->input->post('section')),
				'flag'       => input_clean($this->input->post('flag')),
				'flag_memo'  => input_clean($this->input->post('flag_memo'))
		);


				
		$this->db->insert($this->url,$input);
		$id = $this->db->insert_id();
		
		foreach ($language as $lang_data)
		{
			$data = array(
					'banner_id'     => $id,
					'language_id' => $lang_data['id'],
					'caption'        => input_clean($this->input->post('caption_' . $lang_data['id'])),
				);
				
			$this->db->insert('content_to_' . $this->url,$data);
		}

		$row = $this->db->get_where($this->url, array('id' => $id))->row_array();
		action_log('ADD', $this->url, $row['id'], $input['name'], 'ADDED ' . $this->title. ' ( ' . $input['name'] . ' ) ');

	}

	public function update_banner(){
		
		$id['id']         = input_clean($this->input->post('id'));
		$row = $this->db->get_where($this->url, array('id' => $id['id']))->row_array();

		$language         = language()->result_array();
		$default_language = setting_value('default_language');
		$default_name     = input_clean($this->input->post('name'));
		$section 		  = input_clean($this->input->post('section'));
		$filename ="";

		$size = $this->db->get_where("section", array('id' => $section))->row_array();
		$banner_size = explode(";",$size['banner_size']);

		// $image            = str_replace(base_url(), "", input_clean($this->input->post('image')));
		// if(input_clean($this->input->post('image')) && strpos($this->input->post('image'), '/') !== false) {

		// 	$info = pathinfo(input_clean($this->input->post('image')));
		// 	$filename = $info['basename'];
		// 	resize_from_url(FCPATH . $image, FCPATH . 'lib/images/banner/thumb/', 100, 100,$info, TRUE);
		// 	$filename = resize_from_url(FCPATH . $image, FCPATH . 'lib/images/banner/', $banner_size[0], $banner_size[1],$info);

			
		// } 
		$image =  file_upload_name('image', 'lib/images/banner', strtolower($default_name), FALSE,$banner_size[0], $banner_size[1]);
		$image =  file_upload_name('image', 'lib/images/banner/thumb', strtolower($default_name), FALSE,100,100);
		
		if ($image)
		{
			$filename = $image['file_name'];
			unlink(FCPATH.'lib/images/'.$this->url.'/'. $row['image']);
			if($this->url=='banner'){
				unlink(FCPATH.'lib/images/'.$this->url.'/thumb/'. $row['image']);
			}

		}
		else 
		{
			$filename = $row['image'];
		}


		$end_date="";
		if($this->input->post('end')){
			$end_date= input_clean(format_date($this->input->post('end'),TRUE));
		}
		$input = array(
				'name'		 => input_clean($this->input->post('name')),
				'sort'		 => input_clean($this->input->post('sort')),
				'url'		 => input_clean($this->input->post('url')),
				'start'     => input_clean(format_date($this->input->post('start'),TRUE)),
				'end'       => $end_date,
				'image'      => $filename,
				'section_id' => input_clean($this->input->post('section')),
				'flag'       => input_clean($this->input->post('flag')),
				'flag_memo'  => input_clean($this->input->post('flag_memo'))
		);


				
		$this->db->update($this->url,$input,$id);
		$this->db->delete('content_to_' . $this->url,array('banner_id' => $id['id']));
		
		foreach ($language as $lang_data)
		{
			$data = array(
					'banner_id'     => $id['id'],
					'language_id' => $lang_data['id'],
					'caption'        => input_clean($this->input->post('caption_' . $lang_data['id'])),
				);
				
			$this->db->insert('content_to_' . $this->url,$data);
		}

		action_log('UPDATE', $this->url, $row['id'], $row['name'], 'MODIFY ' . $this->title . ' ( ' . $row['name'] . ' ) ');
		

	}
}
?>
