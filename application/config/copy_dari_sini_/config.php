<?php
defined('BASEPATH') OR exit('No direct script access allowed');

//buat setting di kcfinder (pakai slash), kalo udah online disesuaikan, domain utama isi '' saja
$config['folder_find'] = 'cmsv3/'; 
$config['base_url'] = 'http://localhost/cmsv3/';
$config['index_page'] = '';
// DIR IMAGE -> dipakai untuk bikin cache
$config['dir_image'] = '/var/www/html/dev/cmsv3/';
$config['index_page'] = '';
$config['uri_protocol']	= 'REQUEST_URI';
$config['url_suffix'] = '';

$config['language']	= 'english';
$config['language_abbr'] = "en";



/* hide the language segment (use cookie) */
$config['lang_ignore'] = TRUE; 

$config['charset'] = 'UTF-8';
$config['charset'] = 'UTF-8';
if($_SERVER['SERVER_NAME']=='localhost' ){
	$config['enable_hooks'] = FALSE;
} else {
	$config['enable_hooks'] = TRUE;
}
$config['subclass_prefix'] = 'MY_';
$config['composer_autoload'] = 'vendor/autoload.php';
$config['permitted_uri_chars'] = 'a-z 0-9~%.:&_\-\=';
$config['allow_get_array']		= TRUE;
$config['enable_query_strings'] = FALSE;
$config['controller_trigger'] = 'c';
$config['function_trigger'] = 'm';
$config['directory_trigger'] = 'd';
$config['allow_get_array'] = TRUE;

$config['log_threshold'] = 0;
$config['log_path'] = '';
$config['log_file_extension'] = '';
$config['log_file_permissions'] = 0644;
$config['log_date_format'] = 'Y-m-d H:i:s';

$config['error_views_path'] = '';

$config['cache_path'] = '';
$config['cache_query_string'] = FALSE;

$config['encryption_key'] = 'goONLINEsolusi~!@#';

$config['sess_driver']             = 'database';
$config['sess_cookie_name']        = 'go_ck';
$config['sess_expiration']         = 1200;
$config['sess_save_path']          = 'go_ss';
$config['sess_match_ip']           = FALSE;
$config['sess_time_to_update']     = 1200;
$config['sess_regenerate_destroy'] = TRUE;

$config['cookie_prefix']	= "";
$config['cookie_domain']	= "";
$config['cookie_path']		= "/";
$config['cookie_secure']	= FALSE;
$config['cookie_httponly'] 	= TRUE;


$config['standardize_newlines'] = FALSE;

$config['global_xss_filtering'] = FALSE;

$config['csrf_protection']   = FALSE;
$config['csrf_token_name']   = 'go_secure_t';
$config['csrf_cookie_name']  = 'go_secure_c';
$config['csrf_expire']       = 1200;
$config['csrf_regenerate']   = TRUE;
$config['csrf_exclude_uris'] = array('goadmin/admin/check_pattern','goadmin/ajax/flag' ,'goadmin/sequential/delete','goadmin/ajax/delete_image','goadmin/ckfinder/connector','goadmin/ckfinder','member/check_pass_regis');

$config['compress_output'] = FALSE;
$config['time_reference'] = 'local';
$config['rewrite_short_tags'] = FALSE;

$config['proxy_ips'] = '';
// IP mana aja yang bisa buka goadmin
$config['filter_ip'] = array(
	'122.102.40.253');


	require_once( BASEPATH .'database/DB.php');
	$db =& DB();
	$query = $db->get_where('setting', array('flag'=>1, 'key' => 'default_language'));
	$result = $query->row_array();
	$query = $db->get_where('language', array('id'=> $result['value']))->row_array();
	$queries = $db->get_where('language', array('flag'=> 1))->result_array();

	foreach ($queries as $key => $value) {
		$lang_uri_abbr[$value['attr']] = strtolower($value['name']); 
	}
	$config['language']	= strtolower($query['name']);
	$config['language_abbr'] = $query['attr'];
	/* set available language abbreviations */
	$config['lang_uri_abbr'] = $lang_uri_abbr;

	// Mailgun Config
	if($_SERVER['SERVER_NAME']=='lab.gositus.com' || $_SERVER['SERVER_NAME']=='localhost' )
	{
		$config['mailgun_domain']   = 'sandboxbe3baa0cc98a494c81237e0d7ed9c21d.mailgun.org';
		$config['mailgun_key']      = 'key-6113de7a84a306d8d092d1b4866fb198';

	}else {
		$query = $db->get_where('setting', array('key'=> 'mailgun_domain'))->row_array();
		$config['mailgun_domain']   = $query['value'];
		$query = $db->get_where('setting', array('key'=> 'mailgun_api_key'))->row_array();
		$config['mailgun_key']      = $query['value'];

	}
	