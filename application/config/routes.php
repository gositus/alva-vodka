<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller']      = 'home';
$route['404_override']            = '';
$route['translate_uri_dashes']    = TRUE;
$route['goadmin']                 = "goadmin/login";
$route['goadmin/forgot-password'] = "goadmin/login/forgot_password";
$route['goadmin/reset-password/(:any)'] = "goadmin/login/reset_password/$1";
$route['news']                    = "news/index";
$route['news/(:any)']             = "news/detail/$1";
$route['product']                 = "product/index";
$route['product/category/(:any)'] = "product/category/$1";
$route['product/(:any)']          = "product/detail/$1";
$route['count/(:any)/(:num)']     = "home/count/$1/$2";
$route['sitemap.xml']             = "sitemap/xml";
$route['site.webmanifest']        = "home/favicon";
// URI like '/en/about' -> use controller 'about'
// $route['lib/js/goadmin/plugins/ckfinder/core/connector/php/connector.php?(:any)'] = 'goadmin/ckfinder/connector';
// $route['goadmin/upload/(:any)']                                                = "goadmin/upload/images/$1";
// UNCOMMENT FOR INTERNATIONALIZATION


// '/en' and '/fr' URIs -> use default controller
// UNCOMMENT FOR INTERNATIONALIZATION
require_once( BASEPATH .'database/DB.php');
$db =& DB();
$query = $db->select('attr')->get_where('language', array('flag'=>1))->result_array();
$query = array_map(function($val){
	return $val['attr'];
}, $query);
$query = implode("|", $query);

$route['^('.$query.')/(.+)$'] = "$2";
$route['^('.$query.')$'] = $route['default_controller'];
// echo "<pre>";
// print_r($route);
// echo "</pre>";
