<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// debug? don't forget pre
if ( ! function_exists('pre'))
{
    function pre($data, $next = 0){
        echo '<pre>';
        print_r($data);
        echo '</pre>';
        if(!$next){ exit; }
    }
}

// Return domain name only (gositus.com / localhost)
function getDomain()
{
    $CI =& get_instance();
    return preg_replace("/^[\w]{2,6}:\/\/([\w\d\.\-]+).*$/","$1", $CI->config->slash_item('base_url'));
}

// for cleaning input before insert DB
if ( ! function_exists('input_clean'))
{
    function input_clean($post){
    	$ci =& get_instance();
        return htmlspecialchars($ci->security->xss_clean($post));
    }
}

// get meta keyword, description and title from module SEO Tools
if ( ! function_exists('get_meta'))
{
    function get_meta($uri="home/index"){
    	$ci =& get_instance();
    	$lang = $lang = current_language();
    	return $ci->db->join('content_to_'. 'meta' . ' c' , 'c.'. 'meta'.'_id= p.id','left')->get_where( 'meta' . ' p', array( 'flag !=' => 3, 'language_id' => $lang,'path_url'=> $uri))->row_array();

	}
}

// Instantly create meta title, keyword and description
if ( ! function_exists('meta_create'))
{
    function meta_create($title="" ,$category=""){
		//if you want specifically meta
    	if($category == 'web_title'){
    		return $title . ' | ' . setting_value($category);
    	}
    	else if($category == 'meta_keyword'){
    		$title = str_replace(" " , ", ", $title);
    		return  setting_value($category) .', ' .$title;
    	}
    	else if($category == 'meta_description'){
    		return  setting_value($category) .', ' .$title;
    	}
    	else{ // Bulk
			return array(
				'title'       => meta_create($title,'web_title'),
				'keyword'     => meta_create($title,'meta_keyword'),
				'description' => meta_create($title,'meta_description'),
			);

		}
	}
}

// Instantly generate CI Captcha
if ( ! function_exists('go_captcha'))
{
	function go_captcha()
	{

		$ci =& get_instance();
		if(!empty($ci->session->userdata['waktu_captcha'] ) && $ci->session->userdata['waktu_captcha'] > date("Y-m-d H:i:s"))
		{
			return false;
		} 
		else
		{
			$ci->session->userdata['waktu_captcha'] = date("Y-m-d H:i:s", strtotime('+ 5 seconds'));
			
			$vals = array(
				'img_path'    => './lib/assets/captcha/',
				'img_url'     => base_url().'lib/assets/captcha/',
				'font_path'   => './lib/css/front/fonts/Arial-BoldMT.ttf',
				'img_width'   => '200',
				'img_height'  => 50,
				'expiration'  => 300,
				'word_length' => 4,
				'font_size'   => 20,
				'img_id'      => 'Imageid',
				'pool'        => '0123456789abcdefghijkmnpqrstuvwxyzABCDEFGHJKLMNPQRSTUVWXYZ',
				'colors'		  => array(
									'background'  => array(255,255,255),
									'grid'      => array(255,197,0),
									'text'        => array(0,153,255),
									'border'        => array(153,204,51)
								)
			);
            $captha = create_captcha($vals);
   
            return $captha;
    	}
    }
}

// Instantly Verify Google Captcha
if ( ! function_exists('verify_captcha'))
{
    function verify_captcha($post,$secret_key=""){
    	$ci =& get_instance();
        $captcha = trim($post);
        if(empty($secret_key)){
        	$secret_key=setting_value("secret_key");
        }
		$verify_capt = "https://www.google.com/recaptcha/api/siteverify?secret=" . $secret_key ."&response=" . $captcha . "&remoteip=" . $_SERVER['REMOTE_ADDR'];
		$response = file_get_contents($verify_capt);
		$response = json_decode($response, true);

		return $response;
    }
}



// Encrypt anything with BCrypt
if ( ! function_exists('bcrypt_it'))
{
	function bcrypt_it($input_pasword=""){
	    $options = [
			'cost' =>12
		];
		$password = password_hash($input_pasword, PASSWORD_BCRYPT, $options);
		return $password;

    }
 }


## File Upload
// Upload anything without resize , idk its work or not
if ( ! function_exists('upload_anything')){
	function upload_anything($file, $folder = '', $type = 'txt|doc|docx|xls|xlsx|pdf|gif|jpg|jpeg|png'){
		$ci =& get_instance();
		$config = array(
			'upload_path'	=> $folder,
			'allowed_types'	=> $type
		);
				
		$ci->load->library('upload');
		$ci->upload->initialize($config);
		
		if ($ci->upload->do_upload($file)){
			return $ci->upload->data();
		}
	}
}

// multi upload, no comment...
if ( ! function_exists('file_upload_multi')){
    function file_upload_multi($image, $folder = ''){
        $result = array();
        $ci =& get_instance();
		$config = array(
			'upload_path'	=> $folder,
			'allowed_types'	=> 'gif|jpeg|jpg|png'
		);
		$ci->load->library('upload');
        $ci->upload->initialize($config);
        $files = $_FILES;
        if(!empty($_FILES[$image])){
            $limit = count($_FILES[$image]['name']);
            for($i=0;$i<$limit;$i++){
                $_FILES[$image]['name'] = $files[$image]['name'][$i];
                $_FILES[$image]['type'] = $files[$image]['type'][$i];
                $_FILES[$image]['tmp_name'] = $files[$image]['tmp_name'][$i];
                $_FILES[$image]['error'] = $files[$image]['error'][$i];
                $_FILES[$image]['size'] = $files[$image]['size'][$i];
                if($ci->upload->do_upload($image)){
                    $result[] = $ci->upload->data();
                }
            }
            return $result;
        }
    }
}

// For Upload Image
if ( ! function_exists('file_upload'))
{
	function file_upload($field_name, $folder, $debug = FALSE)
	{
		$ci =& get_instance();
		
		
		$config = array(
					'upload_path'	=> $folder,
					'allowed_types'	=> 'gif|jpeg|jpg|png'
				);
				
		$ci->load->library('upload');
		$ci->upload->initialize($config);
		
		// If upload failed, whether it's permission problem OR no chosen files,
		if ( ! $ci->upload->do_upload($field_name))
		{
			// Return errors if debug is true.
			return ($debug == TRUE) ? $ci->upload->display_errors() : '';
		}
		else return $ci->upload->data();
	}
}

// Upload adn overwrite Image
if ( ! function_exists('file_upload_overwrite'))
{
	function file_upload_overwrite($field_name, $folder, $debug = FALSE)
	{
		$ci =& get_instance();
		
		
		$config = array(
					'upload_path'	=> $folder,
					'allowed_types'	=> 'gif|jpeg|jpg|png|ico'
				);
				
		$ci->load->library('upload');
		$ci->upload->initialize($config);
		$ci->upload->overwrite = true;
		
		// If upload failed, whether it's permission problem OR no chosen files,
		if ( ! $ci->upload->do_upload($field_name))
		{
			// Return errors if debug is true.
			return ($debug == TRUE) ? $ci->upload->display_errors() : '';
		}
		else return $ci->upload->data();
	}
}

// Upload and rename image
if ( ! function_exists('file_upload_name'))
{
	function file_upload_name($field_name, $folder, $name , $debug = FALSE, $width="",$height="", $thumb="", $web="")
	{
		$ci =& get_instance();
		
		if(!empty($name)){
			$name = strtolower($name);
			$name = str_replace(" ", "-", $name);
			if(empty($web)){
				$name = "";
			}
		}
		$config = array(
					'upload_path'	=> $folder,
					'allowed_types'	=> 'svg|gif|jpeg|jpg|png|ico',
					'file_name'		=> $name
				);
				
		$ci->load->library('upload');
		$ci->upload->initialize($config);
		// $ci->upload->overwrite = true;
		
		// If upload failed, whether it's permission problem OR no chosen files,
		if ( ! $ci->upload->do_upload($field_name))
		{
			// Return errors if debug is true.
			return ($debug == TRUE) ? $ci->upload->display_errors() : '';
		}
		else{
			$data = $ci->upload->data();
			$new_name = strtolower($data['raw_name']);
			$new_name = str_replace(' ', '-', $new_name); // Replaces all spaces with hyphens.
			$new_name = str_replace('_', '-', $new_name); // Replaces all spaces with hyphens.
   			$new_name = preg_replace('/[-+()]/', '-', $new_name);
   			$new_name = preg_replace('/[^A-Za-z0-9\-_]/', '', $new_name); // Removes special chars.
			$config['file_name'] = $new_name;
			$ci->upload->initialize($config);
			$ci->upload->overwrite = true;
			$ci->upload->do_upload($field_name);
			$data = $ci->upload->data();

			if(!empty($width) && !(empty($height))){
				$data = image_resize($data,$width,$height);
			}
			if(!empty($thumb)){
				 image_resize($data,$width,$height,TRUE,$thumb);
			}
			return $data;

		} 
	}
}

// Upload document
if ( ! function_exists('file_upload_web')){
	function file_upload_web($field_name, $folder, $debug = FALSE){
		$ci =& get_instance();
		$config = array(
					'upload_path'	=> $folder,
					'allowed_types'	=> 'doc|pdf|txt|docx|xls|xlsx'
				);
				
		$ci->load->library('upload', $config);
		
		// If upload failed, whether it's permission problem OR no chosen files,
		if ( ! $ci->upload->do_upload($field_name)){
			// Return errors if debug is true.
			return ($debug == TRUE) ? $ci->upload->display_errors() : '';
		}
		else return $ci->upload->data();
	}
}

//resize image. if any problem, use pre
if ( ! function_exists('image_resize'))
{
	function image_resize($image, $width, $height, $keep_ratio = TRUE, $thumb="")
	{
		
		if ($image)
		{
			
			// Does the current resolution exceed limit?
			if ($image['image_width'] > $width || $image['image_height'] > $height)
			{
				$config = array(
							'height'		=> $height,
							'width'			=> $width,
							'source_image'	=> $image['full_path'],
							'new_image'		=> $image['file_path'],
							'maintain_ratio'=> $keep_ratio
						);
				if(!empty($thumb)){
					$config['create_thumb']   = TRUE;
					$config['thumb_marker']   = '_thumb';
				}
					
				$ci =& get_instance();
				$ci->load->library('image_lib');
				
				$ci->image_lib->initialize($config);
				 if (!$ci->image_lib->resize()) {
			        echo $ci->image_lib->display_errors();
			    }			}
			return $image;
		}
	}
}

// resize image from url , make sure the path folder is correct
if (! function_exists('resize_from_url'))
{
	 function resize_from_url($image_url="",$folder="",$width="",$height="", $info="", $stay=FALSE)
	{
	    $ci =& get_instance();

	    $temp = FCPATH . 'lib/assets/goadmin/tmp/';
	   	$image_url = str_replace(FCPATH, FCPATH . 'lib/', $image_url);
	   	$rename_image_folder =str_replace($info['filename']. '.' .$info['extension'],'',$image_url);
	    $string = str_replace(' ', '-', $info['filename']); // Replaces all spaces with hyphens.
   		$string = preg_replace('/[^A-Za-z0-9\-_()]/', '', $string);

   		$filename = preg_replace('/-+/', '-', $string);
		$filename  = $filename . '.' .$info['extension'];
	   	$rename_image = $rename_image_folder . $filename;
	   	rename($image_url, $rename_image);

	    $config_manip = array(
	        'image_library'  => 'gd2',
			'source_image'   => $rename_image,
			'new_image'      => $temp,
			'maintain_ratio' => TRUE,
			'width'          => $width,
			'height'         => $height
	    );
	    $ci->load->library('image_lib');
	    $ci->image_lib->initialize($config_manip);
	    // $ci->upload->overwrite = true;
	    $ci->image_lib->resize();
	    if (!$ci->image_lib->resize()) {
	        echo $ci->image_lib->display_errors();
	    }
	    if(!($stay) && $folder != $rename_image_folder){
	    	unlink($rename_image);
	    }

	    rename($ci->image_lib->full_dst_path, $temp.$filename);
	    rename( $temp.$filename, $folder.$filename);

	   // clear
	    $ci->image_lib->clear();
	    return $filename;
	}
}

// resize image from url and then create thumbnail
if (! function_exists('resize_from_url_thumb'))
{
	 function resize_from_url_thumb($image_url="",$folder="",$width="",$height="",$info="")
	{
	    $ci =& get_instance();
	    $temp = FCPATH . 'lib/assets/goadmin/tmp/';
	   	$image_url = str_replace(FCPATH, FCPATH . 'lib/', $image_url);
	    $config_manip = array(
			'image_library'  => 'gd2',
			'source_image'   => $image_url,
			'new_image'      => $temp,
			'maintain_ratio' => TRUE,
			'create_thumb'   => TRUE,
			'thumb_marker'   => '_thumb',
			'width'          => $width,
			'height'         => $height
	    );
	    $ci->load->library('image_lib');
	    $ci->image_lib->initialize($config_manip);
	    $ci->upload->overwrite = true;
	    $ci->image_lib->resize();
	    if (!$ci->image_lib->resize()) {
	        echo $ci->image_lib->display_errors();
	    }
	   // clear
	    $string = str_replace(' ', '-', $info['filename']); // Replaces all spaces with hyphens.
   		$string = preg_replace('/[^A-Za-z0-9\-_()]/', '', $string); // Removes special chars.

   		$filename = preg_replace('/-+/', '-', $string);
		$filename  = $filename . $config_manip['thumb_marker'] .'.' .$info['extension'];
		// pre($filename);
	    rename($ci->image_lib->full_dst_path, $temp.$filename);
	    rename( $temp.$filename, $folder.$filename);

	    $ci->image_lib->clear();
	}
}

// resize image with white background, to force image to our recommended size without strech image
if ( ! function_exists('resize_with_white_bg'))
{
    function resize_with_white_bg($filename, $width, $height,$cache, $crop = false) {
        $CI =& get_instance();
        $dir_image = $CI->config->item('dir_image'). $cache;
        if (!is_file($dir_image . $filename) || substr(str_replace('\\', '/', realpath($dir_image . $filename)), 0, strlen($dir_image)) != $dir_image) {
            return;
        }

        $extension = pathinfo($filename, PATHINFO_EXTENSION);

        $image_old = $filename;
        $image_new = 'og_image/' . substr($filename, 0, strrpos($filename, '.')) . '-' . (int)$width . 'x' . (int)$height . '.' . $extension;

        if (!is_file($dir_image . $image_new) || (filectime($dir_image . $image_old) > filectime($dir_image . $image_new))) {
            list($width_orig, $height_orig, $image_type) = getimagesize($dir_image . $image_old);
                 
            if (!in_array($image_type, array(IMAGETYPE_PNG, IMAGETYPE_JPEG, IMAGETYPE_GIF))) { 
                return $dir_image . $image_old;
            }
                        
            $path = '';

            $directories = explode('/', dirname($image_new));

            foreach ($directories as $directory) {
                $path = $path . '/' . $directory;

                if (!is_dir($dir_image . $path)) {
                    @mkdir($dir_image . $path, 0777);
                }
            }

            if ($width_orig != $width || $height_orig != $height) {
                $file = $dir_image . $image_old;
                
                $CI->load->library('image');
                $CI->image->file($file);
                if ($crop) {
                    if ($width_orig > $width || $height_orig > $height) {
                        $CI->image->resize($width, $height);
                    }

                    $top_x = ($width > $width_orig) ? 0 : ($width_orig - $width) / 2;
                    $top_y = ($height > $height_orig) ? 0 : ($height_orig - $height) / 2;

                    $CI->image->crop($top_x, $top_y, $width, $height);
                }
                else {
                    $CI->image->resize($width, $height);
                }
                $CI->image->save($dir_image . $image_new);
            } else {
                copy($dir_image . $image_old, $dir_image . $image_new);
            }
        }
        
        $image_new = str_replace(' ', '%20', $image_new);  // fix bug when attach image on email (gmail.com). it is automatic changing space " " to +
        return base_url() . $cache . $image_new;
    }
}

if ( ! function_exists('sendemail')){
	function sendemail($data = array(), $clear = ''){
       
		$ci =& get_instance();
		$ci->load->library('email');
        
        $config['protocol']         = setting_value('mail_protocol');
        $config['smtp_host']        = setting_value('mail_host');
        $config['smtp_port']        = setting_value('mail_port');
        $config['smtp_user']        = setting_value('mail_username');
        $config['smtp_pass']        = setting_value('mail_password');
        $config['smtp_timeout']     = setting_value('mail_timeout');
	
        
        $config['wordwrap']         = TRUE;
        $config['wrapchars']        = 76;
        $config['mailtype']         = 'html';
        $config['charset']          = 'utf-8';
        $config['validate']         = FALSE;
        $config['priority']         = 3;
        //$config['crlf']             = "\r\n"; //ini tergantung server kadang ada yg bisa kadang ada yg gak bisa
       // $config['newline']          = "\r\n";
        $config['bcc_batch_mode']   = FALSE;
        $config['bcc_batch_size']   = 200;
		$config['validation']	= TRUE;        
        $ci->email->initialize($config);
		
		$ci->email->set_newline("\r\n");
       	$ci->email->clear();
		
		$from_name = (!empty($data['name']))? $data['name'] : setting_value('company_name');
		$from_email = (!empty($data['from']))? $data['from'] : setting_value('email');

        $ci->email->from($from_email, $from_name);
        
        if(!empty($data['reply_to_name']) && !empty($data['reply_to']))
        {
        	$ci->email->reply_to($data['reply_to'], $data['reply_to_name']); //email penerima , nama penerima
        }
        if(!empty($data['reply_cms']) )
        {
        	 $template['reply_cms'] = $data['reply_cms'];
        }

        $ci->email->to($data['to']);
        if(!empty($data['cc'])){ $ci->email->cc($data['cc']); }
        if(!empty($data['bcc'])){ $ci->email->bcc($data['bcc']); }
        $ci->email->subject($data['subject']);

        $template['title'] = $data['title'];
        $template['message'] = $data['message'];
        $template['message_quote'] = !empty($data['message_quote'])?$data['message_quote']: array();
        $template['to'] = $data['to'];
        $template['to_name'] = $data['to_name'];
        $template['from'] = $from_email;
        $template['name'] = $from_name ;
        $template['link'] = !empty($data['link'])?$data['link']:'';
		$template['link_title'] = !empty($data['link_title'])?$data['link_title']:'';
		$template['email_view'] = !empty($data['email_view'])?$data['email_view']:'default_email'; // you can define another template view in your model
     
        $email_view = $ci->load->view('email/' .$template['email_view'], $template, TRUE);
        $ci->email->message($email_view);
        $ci->email->send();
		
		return TRUE;
	}
}

## QUERY HELPER

// untuk mengambil query tanpa ada join
if ( ! function_exists('select_all_row'))
{
	function select_all_row($table, $where="", $single=FALSE , $sort="",$sort_field="")
	{
		$ci =& get_instance();
		if(!empty($where))
		{
			$ci->db->where($where);
		}

		if($single){
			if(!empty($sort)){
				if(!empty($sort_field)){
					return $ci->db->order_by($sort_field,$sort)->get($table)->row_array();
				} else {
					return $ci->db->order_by('sort',$sort)->get($table)->row_array();
				}
			} else {
				return $ci->db->get($table)->row_array();
			}
		}
		else {
			if(!empty($sort)){
				
				if(!empty($sort_field)){
					return $ci->db->order_by($sort_field,$sort)->get($table)->result_array();
				} else {
					return $ci->db->order_by('sort',$sort)->get($table)->result_array();
				}
			} else {
				return $ci->db->order_by('id','desc')->get($table)->result_array();
			}
		}

	}
}

// untuk mengambil Setting langsung query DB
if ( ! function_exists('setting_value'))
{
	function setting_value($key="")
	{
		$ci =& get_instance();
		$query = $ci->db->get_where('setting',array('key' => $key))->row_array();
		return $query['value'];
	}
}

// another encrypt and decrypt method no need mbstring i think
if ( ! function_exists('encrypt_it'))
{
	function encrypt_it($msg="", $decrypt=FALSE)
	{
		$ci =& get_instance();
		$ci->load->library('encryption');
		$key = $ci->config->config['encryption_key'];
		$ci->encryption->initialize(
        array(
                'cipher' => 'aes-256',
                'mode' => 'ctr',
                'key' => $key
        	)
		);
		if($decrypt){
        	$string = $ci->encryption->decrypt($msg);
		} else{
        	$string = $ci->encryption->encrypt($msg);
		}

		return $string;
	}
}

// to update value in setting
if ( ! function_exists('setting_update'))
{
	function setting_update($key="",$value="")
	{
		$ci =& get_instance();
		$query = $ci->db->update('setting',array('value'=>$value),array('key' => $key));
		if(!empty($ci->session->userdata('temp_setting'))){
			$ci->session->unset_userdata('temp_setting');
		}
	}
}

// use this date format, if you want to change format, you only need to change this
if(!function_exists('format_date'))
{
    function format_date($givenDate="", $db=FALSE)
    {
    	if($db)
    	{
        	return date("Y-m-d", strtotime($givenDate));
    	} else
    	{
        	return date("d M Y", strtotime($givenDate));
    	}
    }
}

// get difference first time and last time
if ( ! function_exists('different_time'))
{
	function different_time($firstTime='', $lastTime='')
	{
		$firstTime=strtotime($firstTime);
		$lastTime=strtotime($lastTime);

		// perform subtraction to get the difference (in seconds) between times
		$timeDiff=$lastTime-$firstTime;

		// return the difference
		if($timeDiff > 86400)
		{	
			$tm = (int)($timeDiff/86400);
			$postfix = ($tm > 1) ? 'days' : 'day';
			
		} else if($timeDiff > 3600)
		{
			$tm = (int)($timeDiff/3600);
			$postfix = ($tm > 1) ? 'hours' : 'hour';
		} else
		{
			$tm =(int)($timeDiff/60);
			$postfix = ($tm > 1) ? 'mins' : 'min';
		}
		$time = $tm .' '. $postfix;
		return $time;
	}
}


// define user agent
if ( ! function_exists('get_user_agent'))
{
	function get_user_agent()
	{
		$ci =& get_instance();
        	$ci->load->library('user_agent');

		if ($ci->agent->is_browser()){
		    $agent = $ci->agent->browser().' '.$ci->agent->version();
		}
		elseif ($ci->agent->is_robot()){
		    $agent = $ci->agent->robot();
		}
		elseif ($ci->agent->is_mobile()){
		    $agent = $ci->agent->mobile();
		}
		else{
		    $agent = 'Unidentified User Agent';
		}
		return $ci->agent->platform(). ' - ' .$agent;
	}
}

// INPUT: timestamp
// OUTPUT: just now, 2 hours ago, yesterday, 5 days ago, last week, 3 weeks ago, last year, ...
if( ! function_exists('date2str'))
{
	function date2str($ts='')
	{
		$ci =& get_instance();
		if(!ctype_digit($ts)) {
			$ts = strtotime($ts);
		}
		
		$diff = time() - $ts;
		
		if($diff == 0)
		{
			return $ci->lang->line('waktu_barusan');
		}
		elseif($diff > 0)
		{
			$day_diff = floor($diff / 86400);
			if($day_diff == 0) 
			{
				if($diff < 60) return $ci->lang->line('waktu_baru_saja');
				if($diff < 3600) return floor($diff / 60) . $ci->lang->line('waktu_menit_yang_lalu');
				if($diff < 86400) return floor($diff / 3600) . $ci->lang->line('waktu_jam_yang_lalu');
			}
			
			if($day_diff == 1) return $ci->lang->line('waktu_kemarin'); 
			if($day_diff < 7)  return $day_diff . $ci->lang->line('waktu_hari_yang_lalu');
			if($day_diff < 15)  return $ci->lang->line('waktu_minggu_lalu'); 
			if($day_diff < 31)  return floor($day_diff / 7) . $ci->lang->line('waktu_minggu_yang_lalu'); 
			if($day_diff < 60)  return $ci->lang->line('waktu_bulan_kemarin');
			if($day_diff < 335)  return floor($day_diff / 30) . $ci->lang->line('waktu_bulan_yang_lalu');
			if($day_diff < 730)  return $ci->lang->line('waktu_tahun_lalu'); 
			else  return floor($day_diff / 365) . $ci->lang->line('waktu_tahun_yang_lalu');

		return date('F Y', $ts);
		}
	}
}

// INPUT: any number
// OUTPUT: 1k, 1,2k, 10k, 10.5k, 100k, 150.5k, 1m, 1.1m, ...
if ( ! function_exists('number2str'))
{
	function number2str($num) {
	    if( $num > 999 ) {
			$x               = round($num);
			$x_number_format = number_format($x);
			$x_array         = explode(',', $x_number_format);
			$x_parts         = array('k', 'm', 'b', 't');
			$x_count_parts   = count($x_array) - 1;
			$x_display       = $x;
			$x_display       = $x_array[0] . ((int) $x_array[1][0] !== 0 ? '.' . $x_array[1][0] : '');
			$x_display       .= $x_parts[$x_count_parts - 1];
	        return $x_display;
	    }
	    return $num;
	}
}

// INPUT: timestamp
// OUTPUT (option OFF): 
// 	pukul 5am - 10am = Selamat pagi
// 	pukul 11am - 14 = Selamat siang
//	pukul 15 - 18 = Selamat sore
//	pukul 19 - 4am = Selamat malam
// OPTION (option TRUE): Selamat%20pagi, Selamat%20siang, ...
if ( ! function_exists('time2str')){

	function time2str($encode=TRUE){
		$ci =& get_instance();
		
		if (date('H') >= 5 && date('H') < 11) {
			$greeting = $ci->lang->line('selamat_pagi');
		} 
		else if(date('H') >= 11 && date('H') < 15) {
			$greeting = $ci->lang->line('selamat_siang');
		} 
		else if(date('H') >= 15 && date('H') < 19){
			$greeting = $ci->lang->line('selamat_sore');
		} 
		else {
			$greeting = $ci->lang->line('selamat_malam');
		}
		if($encode){
			return rawurlencode($greeting).'%20';
		} 
		else{
			return $greeting;
		}
	}
}

// automatic generate WhatsApp link
if ( ! function_exists('link_WA')){
	function link_WA($number, $text="", $hello=""){
		// bisa dikombinasiin dengan function time2str()

		$number = preg_replace("/[^0-9]/", '', $number);

		if($text) {
		   $text = rawurlencode($text) . "%20". current_url();
		}
		$link = 'https://wa.me/'.$number.'?text='.$hello . $text;
		return $link;
	}
}

// random string
if ( ! function_exists('random_it')){
	function random_it($length = 6, $salt = 'Rune'){
        $random = substr(str_shuffle(strtolower(sha1(rand().time().$salt))),0, $length);
        return $random;
    }
}

// Count +1 di table
if ( ! function_exists('count_it')){
	function count_it($table="", $id="", $row){

		$ci =& get_instance();
       
		$count = $row['count_click'] + 1;
		$ci->db->update($table, array('count_click'=>$count,'count_click_show' => number2str($count)), array('id'=>$id));
		
    }
}

 //Create Seo URL
if ( ! function_exists('create_seo_url'))
{
 	function create_seo_url($url,$table,$id="")
 	{
	  	$ci =& get_instance();
	  	$url = url_title(trim($url), 'dash', TRUE);
	  	if(!empty($id)){
	  		$ci->db->where("id !=" , $id);
	  	}
	  	$check = $ci->db->select('seo_url')->get_where($table,array('seo_url' => $url));
		if($check->row_array()){
	   		return create_seo_url($url.'-1',$table);
	  	} else {
	   		return $url;
	   	}
 	}
}

// Create language alternate link for header
if( ! function_exists('alternate_language_link'))
{
	function alternate_language_link()
	{
		$ci =& get_instance();
		$switchlang   = str_replace(base_url(), '', current_url());

		$lang = select_all_row('language', array('flag'=>1), FALSE, 'ASC','id');
		$html = "";
		if(count($lang)>1){
			foreach ($lang as $key => $value) {
				$html .='
	<link rel="alternate" hreflang="'.$value['attr'].'" href="'. base_url($value['attr'].'/'.$switchlang). '" />';
			}
		}
		return $html;
	}
}

// calculate visitor to dashboard
// category "uptime, total_visit, page_view, news, item, item_category, leads"
// total_visit (home)
// page_view (all page)
if ( ! function_exists('total_visitor'))
{
	function total_visitor($category="",$path_id="",$count=FALSE)
	{
		if($_SERVER['SERVER_NAME'] != "localhost" && $_SERVER['SERVER_NAME'] != "lab.gositus.com")
		{
			$ci =& get_instance();

			if(empty($category)) return false;
			$month = date("m");
			$year  = date("Y");
			$table = "dashboard";

			// Jika baru pertama kali masuk, SET session TRUE, UPDATE session +1 AND UPDATE page_view +1
			if(empty($ci->session->userdata('session_pageview'))){
				
				$ci->session->set_userdata('session_pageview', TRUE);

				// Visit_home +1
				$visit_home = select_all_row($table,array('month'=>$month, 'year'=>$year, 'category'=>'visit_home'), TRUE);
				if(!empty($visit_home)) $ci->db->update($table, array('total'=> $visit_home['total'] +1), array('id'=> $visit_home['id']));
				else $ci->db->insert($table, array('month'=>$month, 'year'=>$year,'category'=>'visit_home','path_id'=>$path_id,'total'=>1));

				// Page_view +1
				$row = select_all_row($table,array('month'=>$month, 'year'=>$year, 'category'=>$category, 'path_id'=>$path_id), TRUE);
				if(!empty($row)) $ci->db->update($table, array('total'=> $row['total'] +1), array('id'=> $row['id']));
				else $ci->db->insert($table, array('month'=>$month, 'year'=>$year,'category'=>$category,'path_id'=>$path_id,'total'=>1));
				
			} else {
			// Jika sudah pernah masuk, UPDATE page_view +1 saja
				
				// Page_view +1
				$row = select_all_row($table,array('month'=>$month, 'year'=>$year, 'category'=>$category, 'path_id'=>$path_id), TRUE);
				if(!empty($row)) $ci->db->update($table, array('total'=> $row['total'] +1), array('id'=> $row['id']));
				else $ci->db->insert($table, array('month'=>$month, 'year'=>$year,'category'=>$category,'path_id'=>$path_id,'total'=>1));
				
			}
		}
	}
}