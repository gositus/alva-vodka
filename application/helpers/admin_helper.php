<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('check_login'))
{
	function check_login()
	{
		$ci =& get_instance();
		if ($ci->session->userdata('admin_login') === FALSE || !($ci->session->userdata('admin_login')))
		{
			// Save session "referral"
			$ci->session->set_userdata('referral', current_url());
			$ci->session->set_flashdata('error_message', 'Harap login terlebih dahulu');
			redirect(base_url() . 'goadmin');
		}
	}
}

if ( ! function_exists('action_log'))
{
	function action_log($action, $db, $val = '#', $name, $desc)
	{
		if($_SERVER['SERVER_NAME'] != "localhost" && $_SERVER['SERVER_NAME'] != "lab.gositus.com")
		{
			$ci =& get_instance();
			$data = array(
				'admin_id'   => $ci->session->userdata('admin_id'),
				'admin_name' => $ci->session->userdata('admin_name'),
				'action'     => $action,
				'module'     => $db,
				'value'      => $val,
				'name'       => $name,
				'desc'       => $desc,
				'ip'         =>  $ci->input->ip_address(),
				'user_agent' => get_user_agent()
				);
			$ci->db->insert('admin_log', $data);
		}
	}
}

if ( ! function_exists('language'))
{
	function language($query = FALSE)
	{
		$ci =& get_instance();
		
		$select = ($query == FALSE) ? 'id, attr,icon' : 'id';

		return $ci->db->select($select)->order_by('id', 'asc')->where('flag', 1)->get('language');
	}
}

if ( ! function_exists('default_language_attr'))
{
	function default_language_attr($query = FALSE)
	{
		$ci =& get_instance();
		$default_id = setting_value('default_language');
		$default = $ci->db->get_where("language",array('id'=>$default_id))->row_array();
		return $default['attr'];
	}
}

if ( ! function_exists('current_language'))
{
	function current_language()
	{
		$ci =& get_instance();
		
		$id = $ci->db->select('id')->where('attr', $ci->config->config['language_abbr'])->get('language')->row_array();
		if(!empty($id)){
		return $id['id'];}
	}
}
if ( ! function_exists('get_language'))
{
	function get_language($id) {
		$ci =& get_instance();
		$lang = $ci->db->select('*')->where('id', $id)->get('language')->row_array();

		if(!empty($id)){
		return $lang;}
	}
}
if ( ! function_exists('language_bar'))
{
	function language_bar()
	{
		$ci =& get_instance();
		
		// How many languages are there?
		$check = $ci->db->select('name, id, attr, icon')->order_by('id', 'asc')->where('flag', 1)->get('language');
		
		if ($check->num_rows() > 1)
		{
			if (in_array($ci->uri->segment(2), explode(',', ALLOW_LANGUAGE)))
			{
				// Show language bar
				$html = '<ul id="language-bar" class="nav nav-pills m-t-15">';
				$x = 0;
				
				foreach ($check->result_array() as $item)
				{
					$show = ($x == 0) ? ' active' : '';
					$html .= '<li  id="lang-' . $item['attr'] . '" class="' . $show . '" ><a class="" href="javascript:;"><img src="'.base_url('lib/assets/goadmin/flag/'.$item['icon']).'"/><span>' . $item['name'] . '</span></a></li>';
					$x++;
				}
				
				$html .= '</ul>';
				
				return $html;	
			}
		}
		
	}
}

if ( ! function_exists('check_access'))
{
	function check_access($url, $privilege, $redirect = FALSE)
	{
		if ($url == 'message') $url = 'inbox';
		$ci =& get_instance();
		
		$row = $ci->db->select('a.* , ap.module ')->join('admin_privilege ap','a.admin_privilege_id = ap.id','left' )->get_where('admin a', array('a.id' => $ci->session->userdata('admin_id')))->row_array();
		
		$query = $ci->db->select('id')->where(array('alias'=> $url,'flag'=>1))->get('module')->row_array();
		$id = $query['id'];
		if($id==26){
			$id = 5;
		}
		
		// Custom privilege
		if ($ci->session->userdata('admin_role') == 3)
		{

			$module_list = json_decode($row['module'],true);
			$par = array();
			foreach ($module_list as $k => $val) {
				$pr = $ci->db->get_where("module", array('id' => $k))->row_array();
				$pr = $ci->db->get_where("module", array('id' => $pr['parent']))->row_array();
				$par[] = $pr['alias'];
			}
			if ($privilege == 'menu')
			{
				// If there's no access, redirect back to home.
				if (!in_array($id, array_keys($module_list) ) && !in_array($url, $par))
				{
					if ($redirect == TRUE) redirect(base_url() . 'goadmin');
					return FALSE;
				}
				else return TRUE;
			}
			elseif ($privilege == 'add')
			{
				if (( !empty($module_list[$id]) && $module_list[$id] & 1) == 1) return TRUE;
				else
				{
					if ($redirect == TRUE) redirect(base_url() . 'goadmin/' . $url);
					else return FALSE;
				}
			}
			elseif ($privilege == 'edit')
			{
				if (( !empty($module_list[$id]) && $module_list[$id] & 2) == 2) return TRUE;
				else return FALSE;
			}
			elseif ($privilege == 'delete')
			{
				if (( !empty($module_list[$id]) && $module_list[$id] & 4) == 4) return TRUE;
				else return FALSE;
			}
			elseif ($privilege == 'read')
			{
				if(!empty($id))
				{
					if(!empty($ci->uri->rsegments[3]) && $ci->session->userdata('admin_id') == $ci->uri->rsegments[3] && $url=="admin") return TRUE;
					else if (( !empty($module_list[$id]) && $module_list[$id] & 8) == 8) return TRUE;
				}
				if ($redirect == TRUE) redirect(base_url() . 'goadmin/' . $url);
				else return FALSE;

			}
		}
		elseif ($ci->session->userdata('admin_role') == 2)
		{
			if ($privilege == 'add' || $privilege == 'edit' || $privilege == 'read' || $privilege == 'menu') return TRUE;
			elseif ($privilege == 'delete') return FALSE;
		}
		elseif ($ci->session->userdata('admin_role')) return TRUE;
	}
}

if ( ! function_exists('show_menu'))
{
	function show_menu()
	{
		$ci =& get_instance();
		
		// Get all parent modules
		if($ci->session->userdata('admin_id') == 1 || $_SERVER['SERVER_NAME'] == "localhost" || $_SERVER['SERVER_NAME'] == "lab.gositus.com") {
			$parent_modules = $ci->db->order_by('name', 'asc')->get_where('module', array('parent' => 0));
		} else {
			// $parent_modules = $ci->db->order_by('name', 'asc')->get_where('module', array('parent' => 0, 'flag' => 1, 'id !=' => 33));
			$parent_modules = $ci->db->order_by('name', 'asc')->get_where('module', array('parent' => 0, 'flag' => 1));
		}
		
		$html = '';
		
		// And get setting modules - INTERNAL USE ONLY.
		$setting = $ci->db->get_where('module', array('id' => 1))->row_array();
		
		foreach ($parent_modules->result_array() as $item)
		{
			$access = FALSE;
			
			
			// Check for submenu
			if($ci->session->userdata('admin_id')==1 || $_SERVER['SERVER_NAME'] == "localhost" || $_SERVER['SERVER_NAME'] == "lab.gositus.com") {
				$submenu = $ci->db->order_by('name', 'asc')->get_where('module', array('parent' => $item['id'], 'flag' => 1));
			} else {
				$submenu = $ci->db->order_by('name', 'asc')->get_where('module', array('parent' => $item['id'], 'id !='=>1, 'flag' => 1));
			}

			// If there's submenu, display them
			if ($submenu->num_rows() > 0)
			{
				// $temp = '<ul>';
				$temp = '';
				
				foreach ($submenu->result_array() as $subitem)
				{
						$unread = '';
						//message menu
						if ($subitem['id'] == 5)
						{
							// Get unread message count.
							$unread = count(get_filtered_message("",2));
							
							$unread = ($unread > 0) ? ' <span class="label label-warning">'.$unread.'</span>' : '';

						}
			

					if (check_access($subitem['alias'], 'menu'))
					{
						$access = TRUE;
						if ($subitem['id'] == 999)
						{
							if ($ci->session->userdata('admin_privilege') == 1)
								$temp .= '<li><a href="' . base_url() . 'goadmin/' . $subitem['url'] . '">' . $subitem['name'] . '</a></li>';
						}
						else
						{
							$subsubmenu = $ci->db->order_by('name', 'asc')->get_where('module', array('parent' => $subitem['id'], 'flag' => 1));
							if($subsubmenu->num_rows() > 0) { $class_submenu = 'with-sub'; } else { $class_submenu = ''; }
							
							if($ci->uri->rsegment(1) == $subitem['url']){ 
								$class_active = 'active'; 
							}else{ 
								$class_active = ''; 
							}
												
							$temp .= '<li class='.$class_submenu.' '.$class_active.'>
										<a href="' . base_url() . 'goadmin/' . str_replace("_", "-", $subitem['url']) . '">
											<span class="menu-icon">
						                        <i class="zmdi zmdi-'.$subitem['icon'].'"></i>
						                    </span>
											<span class="menu-text">
												' . $subitem['name'] . $unread .'
											</span>
										</a>';
							if ($subsubmenu->num_rows() > 0)
							{
								$temp .='<ul class="sidebar-submenu">';
								foreach ($subsubmenu->result_array() as $subsubitem)
								{
									if (check_access($subsubitem['alias'], 'menu'))
									{	
										if($ci->uri->rsegment(1) == $subsubitem['url']){ 
											$subclass_active = 'active'; 
										}else{ 
											$subclass_active = ''; 
										}

										$temp .= '<li class="'.$subclass_active.'">
													<a href="' . base_url() . 'goadmin/' . str_replace("_", "-", $subsubitem['url']) . '">
															' . $subsubitem['name'] . 
													'</a>
													</li>';
									}
								}
								$temp .='</ul>';
							}
							$temp .='</li>';
						}
					}
				}
				$temp .= '';
			}
			
			if ($access == TRUE)
			{
				if($item['id'] != 26){
					$html .= '<li class="menu-title">' . $item['name'] .'</li><li>';
				}
				$html .= $temp;
			}
			
			$html .= '</li>';
		}
		
		return $html;
	}
}



## search log
if ( ! function_exists('select_log'))
{
	function select_log($module,$value,$all="")
	{
		$ci =& get_instance();
		if(!empty($all)){
			return $ci->db->order_by('id', 'DESC')->get_where("admin_log", array("admin_id"=> $value),100,0)->result_array();
		}
		return $ci->db->order_by('id', 'DESC')->get_where("admin_log", array("module"=> $module, "value"=> $value),100,0)->result_array();
	}
}



if ( ! function_exists('_getDataTableColumn'))
{
	function _getDataTableColumn($where = "", $join = "")
	{
		$ci =& get_instance();
		
		if(!empty($where)){
			$ci->db->where($where);
		}

		$stringJoin = $join['from'];
		$pieces = explode(' ', $stringJoin);
		$from = array_pop($pieces);


		if(!empty($join)){
			foreach ($join as $key => $value) {
				if(is_array($value)){
					if($key == 'join'){
						$value_join = array_values($value);
						$type = '';
						if(!empty($value_join[2])){
							$type = $value_join[2];
						} else {
							$type = 'inner';
						}
						$ci->db->$key($value_join[0], $value_join[1], $type);
					} else {
						$ci->db->$key($value);
					}
				} else {
					$ci->db->$key($value);
				}
			}
			$query = $ci->db->get();
			$column = $query->list_fields();
		} else {
			$column = $ci->db->list_fields($table);
			$ci->db->from($table);
		}


		$i = 0;

		foreach ($column as $item){ // loop column
            if($ci->input->post('search')['value']) { // if datatable send POST for search
                 
                if($i===0) { // first loop
                    $ci->db->group_start(); // open bracket. query Where with OR clause better with bracket.
                    $ci->db->like(($from) ? $from.'.'.$item : $item, $ci->input->post('search')['value']);
                } else {
                    $ci->db->or_like(($from) ? $from.'.'.$item : $item, $ci->input->post('search')['value']);
                }
 
                if(count($column) - 1 == $i) //last loop
                    $ci->db->group_end(); //close bracket
            }

            $i++;
        }

        if($ci->input->post('order')) { // here order processing
            $ci->db->order_by(($from) ? $from.'.'.$column[$ci->input->post('order')['0']['column']] : $column[$ci->input->post('order')['0']['column']], $ci->input->post('order')['0']['dir']);
        }
	}
}

## untuk mengambil query dengan datatable
if ( ! function_exists('_getDataTable'))
{
	function _getDataTable($table, $where="", $join = "")
	{
		$ci =& get_instance();	

		_getDataTableColumn($where, $join);
		
		if(!empty($where)){
			$ci->db->where($where);
		}

		
		if(!empty($join)){
			foreach ($join as $key => $value) {
				if(is_array($value)){
					if($key == 'join'){
						$value_join = array_values($value);
						$type = '';
						if(!empty($value_join[2])){
							$type = $value_join[2];
						} else {
							$type = 'inner';
						}
						$ci->db->$key($value_join[0], $value_join[1], $type);
					} else {
						$ci->db->$key($value);
					}
				} else {
					$ci->db->$key($value);
				}
			}
		} else {
			$column = $ci->db->list_fields($table);
			$ci->db->from($table);
		}
		
	}
}

if ( ! function_exists('getDataTable'))
{
	function getDataTable($table, $where="", $join="")
	{
		$ci =& get_instance();
		
		_getDataTable($table, $where, $join);
		
		if($ci->input->post('length') != -1)
			$ci->db->limit($ci->input->post('length'), $ci->input->post('start'));

		$query = $ci->db->get();
		
		return $query->result();
	}
}

if ( ! function_exists('dt_countFiltered'))
{
	function dt_countFiltered($table, $where="", $join="")
	{
		$ci =& get_instance();

		_getDataTable($table, $where, $join);
		$query = $ci->db->get();
		
		return $query->num_rows();	
	}
}

if ( ! function_exists('dt_countTotal'))
{
	function dt_countTotal($table, $where="", $join="")
	{
		$ci =& get_instance();

		_getDataTable($table, $where, $join);

		return $ci->db->count_all_results();
	}
}



if ( ! function_exists('viewErrorValidation'))
{
	function viewErrorValidation()
	{
		$html = '<div class="col-md-12">
		            <div class="alert alert-danger alert-icon-bg alert-dismissable">
		                <div class="alert-icon">
		                    <i class="zmdi zmdi-close-circle-o"></i>
		                </div>
		                <div class="alert-message">
		                    <button class="close" data-dismiss="alert">
		                        <span>
		                            <i class="zmdi zmdi-close"></i>
		                        </span>
		                    </button>
		                    <ul class="custom-error-validation">
		                        '.validation_errors().'
		                    </ul>
		                </div>
		            </div>
		        </div>';
		return $html;
	}
}

if( ! function_exists('get_filtered_message'))
{
	function get_filtered_message($row=FALSE, $flag=""){

		$ci =& get_instance();
		$ban_ip = $ci->db->query("select distinct(name) from master_banned_ip where flag = 2")->result_array();
		foreach ($ban_ip as $key => $value) {
			$ban[$key]= $value['name'];
		}
		if(!empty($ban)){
			$ci->db->where_not_in('ip',$ban);
		}
		if(!empty($flag)){
			$msg = $ci->db
		->order_by('date','desc')
		->get_where("message",array('flag' =>  $flag));
		} else{
			$msg = $ci->db
			->order_by('date','desc')
			->get_where("message",array('flag !=' =>  3));
			}		
		
		if($row){
			return $msg->row_array();
		}else{
			return $msg->result_array();
		}
	}
}


if(!function_exists('view_custom_table'))
{
	function view_custom_table($parent= array() ,$child=array(), $read="", $add="", $edit="" , $delete="",$value=0){
		$html ='';
		if($parent['parent']==10) return $html;
		 $html .= '<tr class="child-' . $parent['alias'] . ' child-module custom-controls-stacked" align="center">';
	        $html .= '<td>' . $child['name'] . '</td>';
	        $html .= '<td align="center">
	                    <label class="custom-control custom-control-primary custom-checkbox">
	                        <input type="checkbox" value="8" class="access-read custom-control-input" name="' . $child['alias'] . '" ' . $read . ' />
	                        <span class="custom-control-indicator"></span>
	                    </label>
	                  </td>';
	        $html .= '<td align="center">
	                    <label class="custom-control custom-control-primary custom-checkbox">
	                        <input type="checkbox" value="1" class="access-add custom-control-input" name="' . $child['alias'] . '" ' . $add . ' />
	                        <span class="custom-control-indicator"></span>
	                    </label>
	                  </td>';
	        $html .= '<td align="center">
	                    <label class="custom-control custom-control-primary custom-checkbox">
	                        <input type="checkbox" value="2" class="access-modify custom-control-input" name="' . $child['alias'] . '" ' . $edit . ' />
	                        <span class="custom-control-indicator"></span>
	                    </label>
	                  </td>';
	        $html .= '<td align="center">
	                    <label class="custom-control custom-control-primary custom-checkbox">
	                        <input type="checkbox" value="4" class="access-delete custom-control-input" name="' . $child['alias'] . '" ' . $delete . ' />
	                        <span class="custom-control-indicator"></span>
	                    </label>
	                  </td>';
	        $html .= '<td style="display:none;"><input id="total-' . $child['alias'] . '" type="hidden" name="' . $child['id'] . '" class="access_total" value="'.$value.'" /></td>';
	        $html .= '</tr>';
		return $html;	      
                                                      
	}
}
if ( ! function_exists('update_flag'))
{
	function update_flag($table)
	{
		$ci =& get_instance();
		$end = select_all_row($table,array('end <'=> date("Y-m-d"),'flag'=>1,'end !='=> '0000-00-00 00:00:00'));
		if (!empty($end)){
			foreach ($end as $key => $value) {
				$ci->db->update($table,array('flag'=>2,'flag_memo'=>'Expired'),array('id'=>$value['id']));
			}
		}
		$end = select_all_row($table,array('end >='=> date("Y-m-d"), 'flag'=>2,'end !='=> '0000-00-00 00:00:00'));
		if (!empty($end)){
			foreach ($end as $key => $value) {
				$ci->db->update($table,array('flag'=>1,'flag_memo'=>''),array('id'=>$value['id']));
			}
		}
	}
}
