<?php
//Greetings
$lang['selamat_pagi']  = 'Selamat pagi';
$lang['selamat_siang'] = 'Selamat siang';
$lang['selamat_sore']  = 'Selamat sore';
$lang['selamat_malam'] = 'Selamat malam';

// Time
$lang['waktu_barusan']          ='barusan';
$lang['waktu_baru_saja']        ='baru saja';
$lang['waktu_menit_yang_lalu']  =' menit yang lalu';
$lang['waktu_jam_yang_lalu']    =' jam yang lalu';
$lang['waktu_kemarin']          ='kemarin';
$lang['waktu_hari_yang_lalu']   =' hari yang lalu';
$lang['waktu_minggu_lalu']      ='minggu lalu';
$lang['waktu_minggu_yang_lalu'] =' minggu yang lalu';
$lang['waktu_bulan_kemarin']    ='bulan kemarin';
$lang['waktu_bulan_yang_lalu']  =' bulan yang lalu';
$lang['waktu_tahun_lalu']       ='tahun lalu';
$lang['waktu_tahun_yang_lalu']  =' tahun yang lalu';