<?php 
//Greetings
$lang['selamat_pagi']  = 'Good morning';
$lang['selamat_siang'] = 'Good afternoon';
$lang['selamat_sore']  = 'Good day';
$lang['selamat_malam'] = 'Good evening';

//Time
$lang['waktu_barusan']          ='just now';
$lang['waktu_baru_saja']        ='just now';
$lang['waktu_menit_yang_lalu']  =' mins ago';
$lang['waktu_jam_yang_lalu']    =' hours ago';
$lang['waktu_kemarin']          ='yesterday';
$lang['waktu_hari_yang_lalu']   =' days ago';
$lang['waktu_minggu_lalu']      ='last week';
$lang['waktu_minggu_yang_lalu'] =' weeks ago';
$lang['waktu_bulan_kemarin']    ='last month';
$lang['waktu_bulan_yang_lalu']  =' month ago';
$lang['waktu_tahun_lalu']       ='last year';
$lang['waktu_tahun_yang_lalu']  =' years ago';