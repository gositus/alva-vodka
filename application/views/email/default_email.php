<?php 
$bgcolor      = setting_value('corporate_color');
$txtcolor     = "#fff";
$logo         = base_url('lib/assets/logo/'.setting_value('logo'));
$address      = setting_value("address");
$email_to     = setting_value("email");
$phone        = array(setting_value('phone')) ; //can multiple with explode 
$base_url     = str_replace('http://', '', base_url());
$base_url     = str_replace('https://', '', $base_url);
$company_name = setting_value("company_name");

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">

<!--[if (mso)|(mso 16)]>
<style type="text/css">
	a {text-decoration:none;}
	.form-label { float:left; }
</style>
<![endif]-->
	
<style type="text/css">
	.wrap {
		margin:20px auto 0;
		background-color:#fff;
	}
	
	.subject-title {
		font-size:24px;
		color:#444;
		font-family:Roboto, Arial, sans-serif;
		font-weight:bold;
	}
	
	.body-content, .quote-content {
		padding:10px 0;
		font-family:Roboto, Arial, sans-serif;
		color:#666;
		line-height:180%;
		font-size:16px;
	}
	
	.quote-content {
		border-left:5px solid #eee;
		padding:2px 20px;
	}
	
	.tambahan-baris {
		display:none;
	}
	
	.form-label {
		float:left;
		width:100px;
		color:#888;
	}
	
	.form-textarea {
		float:left;
		line-height: 150%;
	}
	
	.td-button {
		font-size:16px;
		padding:15px;
		border-radius:3px;
		background-color:<?php echo $bgcolor; ?>;
	}
	
	.a-button{
		letter-spacing:normal;
		line-height:100%;
		text-align:center;
		text-decoration:none;
		text-underline:none;
		color:<?php echo $txtcolor; ?> !important;
		display:block;
		text-transform:uppercase;
	}
	.a-button:hover {
		font-weight:bold;
	}
	
	.footer-content { 
		font-family:Roboto, Arial, sans-serif;
		font-size:11.5px;
		background-color:<?php echo $bgcolor; ?>;
		color:<?php echo $txtcolor; ?>;
		mso-line-height-rule:exactly;
		line-height:1.6;
		padding:20px 10px
	}
	  
	.footer-link {
		text-decoration:none;
		text-underline:none;
	}
	
	.footer-link:hover {
	    border-bottom:1px dashed #fff;
	}

	.gositus-mail {
		font-size:11px;
		font-family:Roboto, Arial, sans-serif;
		text-decoration:none;
		text-underline:none;
	}
	
@media screen and (max-width: 768px){
	.wrap {
		min-width:0 !important;
		margin-top:0;
	}
	
	.left-sidebar,.right-sidebar{
		width:85px !important;
		min-width:85px !important;
		max-width:85px !important;
	}

}

@media screen and (max-width: 720px){
	.full-width {
		width:100% !important;
	}

}
	
@media screen and (max-width: 568px){
	.header-logo, .subject-title, .body-content, .quote-content, .blog-post, .garis-warna{
		padding-left:15px !important;
		padding-right:15px !important;
	}
	
	.subject-title{
		font-size:20px !important;
	}
	
	.posts-container,.blog-post {
		padding-top:5px !important;
		padding-bottom:10px !important;
	}
	
	.quote-content {
		border-left:none;
		background:#fafafa;
		padding:10px 20px;
	}
	
	.tambahan-baris {
		display:block;
		background:#fafafa;
	}

	.post-container{
		width:100% !important;
		min-width:0 !important;
	}
	
	.left-sidebar,.right-sidebar{
		display:none !important;
	}
	
	.a-button{
		font-size:14px !important;
	}
	
	.form-label {
		float:none !important;
		width:auto !important;
		line-height: 85% !important;
		font-size:13px !important;
	}
	
	.form-input, .form-textarea {
		margin-bottom:2px;
		line-height: 150%;
		float: none;
	}
	
	.titik-dua {
		display:none;
	}

	.alamat{
		text-align:center !important;
	}

	.alamat img{
		float:none !important;
		margin-right:0 !important;
		margin-bottom:5px !important;
	}

	.alamat div{
		text-align:center !important;
	}

	.body-content img{
		max-width:100% !important;
		height:auto !important;
	}
}
</style>
<!--[if !mso]><!--><link href="https://fonts.googleapis.com/css?family=Roboto:400,700&display=swap" rel="stylesheet"><!--<![endif]-->
</head>

<body style="background:#fafafa;padding:10px 0 0;margin: 0;-ms-text-size-adjust:100%;-webkit-text-size-adjust: 100%;">
<!-- Email Wrapper -->
<table class="wrap" style="min-width:700px;max-width:875px;border-collapse:collapse;mso-table-lspace:0;mso-table-rspace:0;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
<tbody>
	<tr>
		<td style="border-collapse:collapse;mso-table-lspace:0;mso-table-rspace:0;margin:0;" align="center">

			<!-- Content Container -->
			<table style="max-width:910px;border-collapse:collapse;mso-table-lspace:0;mso-table-rspace:0;margin:0;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
			<tbody>
				<tr>
					<td style="border-collapse:collapse;mso-table-lspace:0;mso-table-rspace:0;margin:0;" align="center">
                    
						<!-- LOGO -->
						<table style="border-collapse:collapse;mso-table-lspace:0;mso-table-rspace:0;" width="100%" cellspacing="0" cellpadding="0" border="0">
						<tbody>
							<tr>
								<td class="left-sidebar" style="border-collapse:collapse;mso-table-lspace:0;mso-table-rspace:0;margin:0;" width="105"></td>
								<td class="header-logo" style="border-collapse:collapse;mso-table-lspace:0;mso-table-rspace:0;margin:0;padding:15px 0" width="700" valign="middle">
									<img src="<?php echo $logo; ?>" style="height:50px" height="50">
								</td>
								<td class="right-sidebar" style="border-collapse:collapse;mso-table-lspace:0;mso-table-rspace:0;margin:0;" width="105"></td>
							</tr>
						</tbody>
						</table>
						<!-- End of LOGO -->

						<!-- SUBJECT -->
						<table style="border-collapse:collapse;mso-table-lspace:0;mso-table-rspace:0;margin:0;" width="100%" cellspacing="0" cellpadding="0" border="0">
						<tbody>
							<tr>
								<td class="left-sidebar" style="border-collapse:collapse;mso-table-lspace:0;mso-table-rspace:0;margin:0;" width="105"></td>
								<td class="subject-title" style="border-collapse:collapse;mso-table-lspace:0;mso-table-rspace:0;margin:0;" width="700">
									<?php echo $title; ?>
								</td>
								<td class="right-sidebar" style="border-collapse:collapse;mso-table-lspace:0;mso-table-rspace:0;margin:0;" width="105"></td>
							</tr>
						</tbody>
						</table>
						<!-- End of SUBJECT -->
						
						<!-- BODY CONTENT -->
						<table style="border-collapse:collapse;mso-table-lspace:0;mso-table-rspace:0;margin:0;" width="100%" cellspacing="0" cellpadding="0" border="0">
						<tbody>
							<tr>
								<td class="left-sidebar" style="border-collapse:collapse;mso-table-lspace:0;mso-table-rspace:0;margin:0;" width="105"></td>
								<td class="body-content" style="border-collapse:collapse;mso-table-lspace:0;mso-table-rspace:0;margin:0;mso-line-height-rule:exactly;" width="700">
									<?php echo $message; ?>
								</td>
								<td class="right-sidebar" style="border-collapse:collapse;mso-table-lspace:0;mso-table-rspace:0;margin:0;" width="105"></td>
							</tr>
						</tbody>
						</table>
						<!-- End of BODY CONTENT -->
						
						<!-- QUOTE CONTENT -->
						<table style="border-collapse:collapse;mso-table-lspace:0;mso-table-rspace:0;margin:0;" width="100%" cellspacing="0" cellpadding="0" border="0">
						<tbody>
						    <tr>
								<td class="tambahan-baris" colspan="3">&nbsp;</td>
							</tr>

							<?php foreach ($message_quote as $key => $value) { ?>
							
							<!-- LOOPING HERE -->
							<?php if($key !=='content'){?>
							<tr>
								<td class="left-sidebar" style="border-collapse:collapse;mso-table-lspace:0;mso-table-rspace:0;margin:0;" width="105"></td>
								<td class="quote-content" style="border-collapse:collapse;mso-table-lspace:0;mso-table-rspace:0;margin:0;mso-line-height-rule:exactly;" width="700">
									<div class="form-label"><?php echo str_replace("_"," ", ucwords($key)); ?></div> <div class="form-input"><span class="titik-dua">: </span><?php echo $value; ?></div>
								</td>
								<td class="right-sidebar" style="border-collapse:collapse;mso-table-lspace:0;mso-table-rspace:0;margin:0;" width="105"></td>
							</tr>
							<?php } else { ?>	
							<tr>
								<td class="left-sidebar" style="border-collapse:collapse;mso-table-lspace:0;mso-table-rspace:0;margin:0;" width="105"></td>
								<td class="quote-content" style="border-collapse:collapse;mso-table-lspace:0;mso-table-rspace:0;margin:0;mso-line-height-rule:exactly;" width="700">
									<div class="form-label">Message</div> <div class="form-input"><span class="titik-dua">: </span><div class="form-textarea"><?php echo nl2br($value); ?> </div>
								</td>
								<td class="right-sidebar" style="border-collapse:collapse;mso-table-lspace:0;mso-table-rspace:0;margin:0;" width="105"></td>
							</tr>

							<?php } ?>
							<?php } ?>
							
							
							<!-- TEXTAREA -->
							<!-- End of TEXTAREA -->
							
							<!-- EOL -->
							<tr>
								<td class="tambahan-baris" colspan="3">&nbsp;</td>
							</tr>
						</tbody>
						</table>
						<!-- End of QUOTE CONTENT -->
						
						
						<!-- Contact -->
						<table class="full-width" style="border-collapse:collapse;mso-table-lspace:0;mso-table-rspace:0;" width="700" cellspacing="0" cellpadding="0" border="0" align="center">
						<tbody>
							<tr><td height="20"></td></tr>
							<tr>
								<td class="posts-container" style="border-collapse:collapse;mso-table-lspace:0;mso-table-rspace:0;margin:0;">
									<!-- Table LEFT -->
									<table class="post-container" style="min-width:290px;width:41%;border-collapse:collapse;mso-table-lspace:0;mso-table-rspace:0;margin:0;" width="290" cellspacing="0" cellpadding="0" border="0" align="left">
									<tbody>
										<tr>
											<td class="blog-post" style="border-collapse:collapse;mso-table-lspace:0;mso-table-rspace:0;margin:0;" valign="middle">
												<table style="border-collapse:collapse;mso-table-lspace:0;mso-table-rspace:0;margin:0;" width="100%" cellspacing="0" cellpadding="0" border="0">
												<tbody>
													<tr>
														<td class="alamat" style="font-family:Roboto, Arial, sans-serif;font-size:12px;color:#666;" valign="middle">
															<img src="<?php echo base_url('lib/assets/mail/icon-pin.png'); ?>" alt="icon-location" width="30" style="float: left;margin-right: 10px">
															<div style="text-align:left;">
																<a href="#" style="text-decoration:none;text-underline:none;color:#666;">
																	<?php echo $address; ?>
																</a>
															</div>
														</td>
													</tr>
												</tbody>
												</table>
											</td>
										</tr>
									</tbody>
									</table>
									<!-- End of Table LEFT -->

									<!-- Table MID -->
									<table class="post-container" style="min-width:230px;width:33%;border-collapse:collapse;mso-table-lspace:0;mso-table-rspace:0;margin:0;" width="230" cellspacing="0" cellpadding="0" border="0" align="left">
									<tbody>
										<tr>
											<td class="blog-post" style="border-collapse:collapse;mso-table-lspace:0;mso-table-rspace:0;margin:0;" valign="middle">
												<table style="border-collapse:collapse;mso-table-lspace:0;mso-table-rspace:0;margin:0;" width="100%" cellspacing="0" cellpadding="0" border="0">
												<tbody>
													<tr>
														<td class="alamat" style="font-family:Roboto, Arial, sans-serif;font-size:12px;color:#666;" valign="middle">
															<img src="<?php echo base_url('lib/assets/mail/icon-phone.png'); ?>" alt="icon-phone" width="30" style="float: left;margin-right: 10px">
															<div style="text-align:left;">
																<?php foreach ($phone as $key => $value) {?>
																
																<a style="text-decoration:none !important;text-underline:none;color:#666;" href="tel:<?php echo preg_replace('/[^0-9]/','', $value) ?>" target="_blank"><?php echo $value;; ?></a><br>
																<?php } ?>
															</div>
														</td>
													</tr>
												</tbody>
												</table>
											</td>
										</tr>
									</tbody>
									</table>
									<!-- End of Table MID -->

									<!-- Table RIGHT -->
									<table class="post-container" style="min-width:175px;width:25%;border-collapse:collapse;mso-table-lspace:0;mso-table-rspace:0;margin:0;" width="175" cellspacing="0" cellpadding="0" border="0" align="left">
									<tbody>
										<tr>
											<td class="blog-post" style="border-collapse:collapse;mso-table-lspace:0;mso-table-rspace:0;margin:0;" valign="middle">
												<table style="border-collapse:collapse;mso-table-lspace:0;mso-table-rspace:0;margin:0;" width="100%" cellspacing="0" cellpadding="0" border="0">
												<tbody>
													<tr>
														<td class="alamat" style="font-family:Roboto, Arial, sans-serif;font-size:12px;color:#666;" valign="middle">
															<img src="<?php echo base_url('lib/assets/mail/icon-mail.png'); ?>" alt="icon-email" width="30" style="float: left;margin-right: 10px">
															<div style="text-align:left;">
																<a style="text-decoration:none;text-underline:none;color:#666;" href="mailto:<?php echo $email_to; ?>"><?php echo $email_to; ?></a><br>
																<a style="text-decoration:none;text-underline:none;color:#666;" href="<?php echo $base_url; ?>"><b><?php echo $base_url; ?></b></a>
															</div>
														</td>
													</tr>
												</tbody>
												</table>
											</td>
										</tr>
									</tbody>
									</table>
									<!-- End of Table RIGHT -->
								</td>
							</tr>
						</tbody>
						</table>
						<!-- End of Contact -->

					</td>
				</tr>
			</tbody>
			</table>
			<!-- End of Content -->
	
			<!-- Footer -->
			<table style="border-collapse:collapse;mso-table-lspace:0;mso-table-rspace:0;" width="100%" cellspacing="0" cellpadding="0" border="0">
			<tbody>
				<tr>
					<td height="25"></td>
				</tr>
				<tr>
					<td class="footer-content" align="center">
						<a class="footer-link" href="<?php echo $base_url; ?>"  style="text-decoration:none;text-underline:none;color:<?php echo $txtcolor; ?>"><?php echo $base_url; ?></a><br>
						–<br>
						Copyright &copy; <?php echo date("Y") .' '. $company_name;?><br>
						This email was sent to <a class="footer-link" href="mailto:<?php echo $to; ?>" style="text-decoration:none;text-underline:none;color:<?php echo $txtcolor; ?>"><?php echo $to; ?></a>.
					</td>
				</tr>
			</tbody>
			</table>
			<!-- End Footer -->
			
		</td>
	</tr>
</tbody>
</table>

<!-- 3 color lines -->
<table style="border-collapse:collapse;mso-table-lspace:0;mso-table-rspace:0;" width="100%" cellspacing="0" cellpadding="0" border="0">
<tbody>
	<tr>
		<td colspan="3">&nbsp;</td>
	</tr>
	<tr>
		<td class="left-sidebar" style="border-collapse:collapse;mso-table-lspace:0;mso-table-rspace:0;margin:0;" width="105"></td>
		<td class="garis-warna" style="border-collapse:collapse;mso-table-lspace:0;mso-table-rspace:0;margin:0;" width="700" valign="middle" align="center">
			<img src="<?php echo base_url('lib/assets/mail/garis-warna.png'); ?>">
		</td>
		<td class="right-sidebar" style="border-collapse:collapse;mso-table-lspace:0;mso-table-rspace:0;margin:0;" width="105"></td>
	</tr>
	<tr>
		<td class="left-sidebar" style="border-collapse:collapse;mso-table-lspace:0;mso-table-rspace:0;margin:0;" width="105"></td>
		<td align="center" style="padding:10px 10px 20px"><a class="gositus-mail" href="https://www.gositus.com" style="text-decoration:none;text-underline:none;color:#aaa">Mailed by Gositus.com</a></td>
		<td class="right-sidebar" style="border-collapse:collapse;mso-table-lspace:0;mso-table-rspace:0;margin:0;" width="105"></td>
	</tr>
</tbody>
</table>
<!-- End of 3 color lines -->

</body>
</html>