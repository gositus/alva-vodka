
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <?php
    $btn_color = setting_value('corporate_color');
    $btn_text = '#ffffff'; ?>
    <style>
body,u+.body .gwfw{width:100%}.btnx{background-color:<?php echo $btn_color; ?>;color:#fff;display:inline-block;font-family:Arial,sans-serif;font-size:13px;line-height:16px;text-align:center;text-decoration:none;width:auto;padding-left:11px;padding-right:11px;border-radius:5px;text-transform:uppercase}.hotlines,.subdesk{text-align:left}.paper-box,.rgblock{max-width:320px}.pcover{display:block;border-width:0;width:100%;max-width:640px;height:auto}.only-footer{padding:0;margin:0}.icn-phone{padding:0}.hotlines{padding-left:10px;font-size:14px}@media screen and (max-width:400px){.hotlines,.subdesk{text-align:center}.icn-phone{padding-bottom:20px}.only-footer{padding:10px}.paper-box,.rgblock{max-width:100%}.hotlines{padding-left:0;padding-bottom:20px;font-size:12px} .phone-big{font-size: 16px;}}
    </style>
</head>

<body style="margin:0;padding:0;border:0;min-width=100%">


    <div style="Margin:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;min-width:100%;background-color:#ffffff">
        <center style="width:100%;table-layout:fixed">
            <div style="max-width:640px">

                <!-- _component -->

                <table align="center" style="border-spacing:0;font-family:arial,sans-serif;color:#ffffff;Margin:0 auto;width:100%;border-bottom:1px solid #ddd;max-width:640px">
                    <tbody>
                        <tr>
                            <td class="" style="padding-top:0;padding-bottom:0;padding-left:20px;padding-right:20px;font-size:0;text-align:center">

                                <div class="" style="vertical-align:middle;display:inline-block;width:100%;max-width:200px">
                                    <table width="100%" style="border-spacing:0;font-family:arial,sans-serif;color:#ffffff;">
                                        <tbody>
                                            <tr>
                                                <td style="padding-top:20px;padding-bottom:20px;padding-right:20px;padding-left:20px">
                                                    <a href="<?php echo base_url(); ?>" title="<?php echo setting_value('web_title') ?>" style="text-decoration:underline" target="_blank" data-saferedirecturl="#">
                                                        <img src="<?php echo base_url('lib/images/' . setting_value('logo')) ?>" width="160" alt="<?php echo setting_value('web_title') ?>" border="0" style="display:block;width:100%;height:auto;max-width:160px;border-width:0">
                                                    </a>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div style="vertical-align:middle;display:inline-block;width:100%;max-width:400px"></div>
                            </td>
                        </tr>
                    </tbody>
                </table>
            <?php /* Start Default Template  [Image] - [Text] - ['Button']*/ ?>
                
                <table width="100%" style="border-spacing:0;font-family:arial,sans-serif;color:#ffffff">
                    <tbody>
                        <tr>
                            <td style="padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;line-height:1px;font-size:1px;height:30px;background-color:#ffffff">
                                <p style="Margin:0;text-align:left;color:#0a1f33;font-family:Arial,'Helvetica Neue',Helvetica,sans-serif;font-size:20px;Margin-bottom:30px;margin-top:40px;text-align:center;font-weight:normal;text-transform:uppercase;line-height:25px;">
                                   <strong><?php echo $title; ?></strong>
                                </p>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <table align="center" style="border-spacing:0;font-family:sans-serif;color:#ffffff;Margin:0 auto;width:100%;max-width:640px">
                    <tbody>
                        <tr>
                            <td style="padding-top:0;padding-bottom:0;padding-right:0;padding-left:0">

                                <table width="100%" style="border-spacing:0;font-family:arial,sans-serif;color:#999999">
                                    <tbody>
                                        <tr align="left">
                                            <td class="subdesk" style="padding-top:0;padding-bottom:0;padding-right:0;">
                                                <div style="Margin:0;color:#999999;padding-left:40px;padding-right:40px;padding-bottom:20px;font-family:'arial','Lucida Grande','Lucida Sans Unicode','Lucida Sans',Tahoma,sans-serif;text-align:left;font-size:16px;line-height:2em;">
                                                    <?php echo ($message); ?>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="text-align:center;padding-top:20px;padding-bottom:40px;">

                                                <div>
                                                    <!--[if mso]>
                                                <v:roundrect xmlns:v="urn:schemas-microsoft-com:vml" href="<?php echo base_url(); ?>" style="height:40px;v-text-anchor:middle;width:230px;" arcsize="10%" stroke="f" fillcolor="<?php echo $btn_color; ?>">
                                                    <center>
                                                        <![endif]-->
                                                    <a class="btnx" style="font-family:Roboto, Arial, Geneva, sans-serif;background-color:<?php echo $btn_color; ?>;color:<?php echo $btn_text ?>;text-decoration:none;border-radius:4px;padding:15px 20px;margin:0 auto;text-align:center;font-family: 'Arial',sans-serif;"
                                                        href="<?php echo base_url(); ?>"><?php echo setting_value('web_title') ?></a>
                                                    <!--[if mso]>
                                                    </center>
                                                </v:roundrect>
                                                <![endif]-->
                                                </div>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                    </tbody>
                </table>


               <?php /* End Default Template */ ?> 
               
            </div>


            <!-- footer -->
            <table width="100%" style="border-spacing:0;font-family:arial,sans-serif;">
                <tbody>

                    <tr>
                        <td align="center" valign="top" style="background:#275F98 none no-repeat;background-color:<?php echo $btn_color; ?>;background-repeat:no-repeat;background-position:center bottom;background-size:cover;border-top:3px solid <?php echo $btn_color; ?>;border-bottom:0;padding-top:0px;padding-bottom:0px;mso-hide:all;width:100%;">

                            <!--[if mso]>
                <div style="width:100%;height:20px;">a</div>
                <![endif]-->

                            <table align="center" width="100%" style="border-spacing:0;font-family:arial,sans-serif;color:#ffffff;padding-top:40px;padding-bottom:20px;width:100%;max-width:640px">
                                <tbody>
                                    <tr>
                                        <td class="" style="padding-top:0;padding-bottom:0;padding-left:20px;padding-right:20px;font-size:0;text-align:center">

                                            <!--[if mso]>
                                <table width="600" align="center">
                                    <tbody>
                                        <tr>
                                            <td width="50">
                                <![endif]-->
                                            <div class="icn-phone" style="vertical-align:middle;display:inline-block;width:100%;max-width:40px;">
                                                <a href="tel:<?php echo setting_value('phone');?>" title="" style="text-decoration:underline;text-align:center;" target="_blank">
                                                    <img src="<?php echo base_url('lib/images/mail_template') ?>/phone.png" width="40" alt=" images" border="0" style="display:block;width:40px;height:auto;max-width:160px;border-width:0">
                                                </a>
                                            </div>
                                            <!--[if mso]>
                                </td>
                                <td width="300">
                                <![endif]-->
                                            <div class="" style="vertical-align:middle;display:inline-block;width:100%;max-width:300px">
                                                <table align="left" style="padding:5px;width:100%;">
                                                    <tbody>
                                                        <tr>
                                                            <td class="hotlines" style="color:#ffffff;font-family:Arial,'Helvetica Neue',Helvetica,sans-serif;line-height:20px;">
                                                                <span style="font-weight:bold;">PHONE</span>
                                                                <br>
                                                                <span dir="ltr">
                                                                    <a class="phone-big" href="tel:<?php echo  preg_replace("/([^0-9]+)/","",setting_value('phone'));?>" style="color:#ffffff;text-decoration:none;"><?php echo setting_value('phone'); ?></a>
                                                                </span>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                            <!--[if mso]>
                                </td>
                                            <td width="250">
                                <![endif]-->
                                            <div class="" style="vertical-align:middle;display:inline-block;width:100%;max-width:250px">
                                                <table align="center" style="padding:5px;">
                                                    <tr>
                                                        <td>
                                                            <a href="http://www.facebook.com/<?php echo setting_value('facebook'); ?>" style="text-decoration:none;" target="_blank">
                                                                <img alt="Facebook" src="<?php echo base_url('lib/images/mail_template') ?>/icon_facebook.png" style="padding:0;width:40px;border:0;height:auto;outline:none;text-decoration:none"
                                                                    width="40" class="CToWUd">
                                                            </a>
                                                        </td>
                                                        <td>
                                                            <a href="http://www.twitter.com/<?php echo setting_value('twitter'); ?>" style="text-decoration:none;" target="_blank">
                                                                <img alt="Twitter" src="<?php echo base_url('lib/images/mail_template') ?>/icon_twitter.png" style="padding:0;width:40px;border:0;height:auto;outline:none;text-decoration:none"
                                                                    width="40" class="CToWUd">
                                                            </a>
                                                        </td>
                                                        <td>
                                                            <a href="http://www.instagram.com/<?php echo setting_value('instagram'); ?>" style="text-decoration:none;" target="_blank">
                                                                <img alt="Instagram" src="<?php echo base_url('lib/images/mail_template') ?>/icon_instagram.png" style="padding:0;width:40px;border:0;height:auto;outline:none;text-decoration:none"
                                                                    width="40" class="CToWUd">
                                                            </a>
                                                        </td>
                                                        <td>
                                                            <a href="http://www.linkedin.com/in/<?php echo setting_value('linkedin') ?>" style="text-decoration:none;" target="_blank">
                                                                <img alt="Linkedin" src="<?php echo base_url('lib/images/mail_template') ?>/icon_linkedin.png" style="padding:0;width:40px;border:0;height:auto;outline:none;text-decoration:none"
                                                                    width="40" class="CToWUd">
                                                            </a>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                            <!--[if mso]>
                                 </td>
                                        </tr>
                                    </tbody>
                                </table>
                                <![endif]-->

                                        </td>
                                    </tr>
                                </tbody>
                            </table>





                            <table align="center" cellpadding="0" cellspacing="0" border="0" style="padding-bottom:40px;font-family:'arial', Helvetica, Arial, Verdana, sans-serif;color:#ffffff;width:100%;max-width:640px">
                                <tr>
                                    <td align="center" style="padding:5px;font-weight:bold;vertical-align:middle;">
                                        <a href="<?php echo base_url(); ?>" style="text-decoration:none;color:#fff;">
                                            <img src="<?php echo base_url('lib/images/mail_template') ?>/world2.png" width="20" alt=" images" border="0" style="display:inline-block;width:20px;height:20px;vertical-align:middle;margin-right:4px;">
                                            <span style="display:inline-block;vertical-align:middle;line-height:14px;font-size:16px;"><?php $url = str_replace('http://', '', base_url());
                                            $url = str_replace('https://', '', $url);
                                            echo $url; ?></span>
                                        </a>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" style="padding:10px 20px;font-size:12px;line-height:20px;">
                                        <span>
                                            <a href="<?php echo base_url(); ?>menu" target="_blank" style="text-decoration:none;color:#ffffff;">Menu</a>&nbsp;&nbsp;|&nbsp;&nbsp;
                                            <a href="<?php echo base_url(); ?>menu" target="_blank" style="text-decoration:none;color:#ffffff;">Menu</a>&nbsp;&nbsp;|&nbsp;&nbsp;
                                            <a href="<?php echo base_url(); ?>menu" target="_blank" style="text-decoration:none;color:#ffffff;">Menu</a>&nbsp;&nbsp;|&nbsp;&nbsp;
                                            <a href="<?php echo base_url(); ?>menu" target="_blank" style="text-decoration:none;color:#ffffff;">Menu</a>&nbsp;&nbsp;|&nbsp;&nbsp;
                                            <a href="<?php echo base_url(); ?>menu" target="_blank" style="text-decoration:none;color:#ffffff;">Menu</a>&nbsp;&nbsp;|&nbsp;&nbsp;
                                            <a href="<?php echo base_url(); ?>menu" target="_blank" style="text-decoration:none;color:#ffffff;">Menu</a>&nbsp;&nbsp;|&nbsp;&nbsp;
                                            <a href="<?php echo base_url(); ?>menu" target="_blank" style="text-decoration:none;color:#ffffff;">Menu</a>
                                        </span>
                                    </td>
                                </tr>
                            </table>


                            <!--[if mso]>
                <div style="width:100%;height:20px;">a</div>
                <![endif]-->

                        </td>
                    </tr>

                    <!-- bagian bawah -->
                    <tr>
                        <td align="center" valign="top" style="background:#eee none no-repeat;border-bottom:0;padding-top:0px;padding-bottom:0px;">
                            <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse:collapse;max-width:640px;">
                                <tbody>
                                    <tr>
                                        <td style="padding-top:20px;padding-bottom:20px;">
                                            <table class="only-footer" align="center" style="text-align:center;width:100%;ont-family:Arial, Verdana, sans-serif;font-size:11px;color:#999999;">
                                                <tr>
                                                    <td>Copyright � <?php echo  date("Y") . ' - ' . setting_value('site_name') ?></td>
                                                </tr>
                                                <tr>
                                                    <td style="padding-bottom: 20px;">This email was sent to
                                                        <a href="mailto:<?php echo $to; ?>" style="text-decoration:none;color:#0b3868;"><?php echo $to; ?></a>.
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="padding-top:17px;border-top:1px solid #ddd;margin-top:10px;text-align:center;">
                                                        <a href="http://www.gositus.com" target="_blank" style="text-decoration:none;color:#999999;color:#999999;margin:0 auto;">
                                                            <img src="<?php echo base_url('lib/images/mail_template') ?>/icon_gositus.jpg" width="16" alt="PT. Go Online Solusi" border="0"
                                                                style="display:inline-block;width:16px;height:auto;vertical-align:middle;padding-right:5px;">
                                                            <span style="display:inline-block;vertical-align:middle;padding-right:5px;font-size:10px;">Gositus Email</span>
                                                        </a>

                                                    </td>
                                                </tr>


                                            </table>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    <!-- end bawah -->
                </tbody>
            </table>
            <!-- end footer -->


        </center>
    </div>

</body>

</html>