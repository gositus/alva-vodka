<h2>Product Category </h2>
<ul>
<?php 
foreach ($product_category as $key => $cat) { ?>
	<li><a href="<?php echo base_url('product/category/'.$cat['seo_url']) ?>"><?php echo $cat['name'] ; ?></a></li>
<?php } ?>
	
</ul>


<h2>Product List</h2>

<?php foreach ($product as $key => $value) { ?>

<a href="<?php echo base_url('product/'.$value['seo_url']); ?>"><img src="<?php echo (!empty($value['image'])) ? base_url('lib/images/item/') . $value['image'] : base_url('lib/assets/gositus/default-image.png') ;?>" alt="<?php echo (!empty($value['image_alt'])) ? $value['image_alt'] : $value['name']; ?>"></a>

<h3><a href="<?php echo base_url('product/'.$value['seo_url']); ?>"><?php echo $value['name']; ?></a></h3>
<p><?php echo word_limiter($value['content'],20) ; ?></p>
<p><a href="<?php echo base_url('product/'.$value['seo_url']); ?>">Read More ..</a></p>

<?php } ?>