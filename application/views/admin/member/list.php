<?php if($this->session->flashdata('success')) { ?>
	<script>
		$(function(){
			showToast('success', 'bottom', "<?php echo $this->session->flashdata('success');?>");
		})
	</script>
<?php } ?>
<div class="row">
	<div class="col-md-12">
		<?php $this->load->view('admin/template/fixed_heading', array('type' => 'list')); ?>
	</div>
    
    <div class="col-md-12">	
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="m-y-0 font-bold">List <?php echo $title; ?></h4>
                <div class="error-input"><?php echo validation_errors();?></div>
            </div>
            <div class="panel-body">
                <div class="form-horizontal">
                	<input type="text" name="search_name" class="table_search form-control input-sm" data-index="1" placeholder="Name">
                    <input type="text" name="search_email" class="table_search form-control input-sm" data-index="2" placeholder="Email">
                    <input type="text" name="search_start_date" class="table_search form-control input-sm datepicker_from" id="datepicker_from" placeholder="Regis Date From">
                    <input type="text" name="search_end_date" class="table_search form-control input-sm datepicker_to" id="datepicker_to" placeholder="Regis Date To">
                	<div class="table-responsive">
						
						<table id="list-table" data-table-name="<?php echo $url; ?>" class="table table-striped dataTable">
							<thead>
								<tr>
									<th>No</th>
									<th>Name</th>
									<th>Email</th>
									<th>Regis Date</th>
									<th>Last Login</th>
									<?php $this->load->view('admin/template/list_table_heading')?>
								</tr>
							</thead>
							<tbody></tbody>
						</table>
                	</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php 
	//SHOW DATA TO DATATABLE
	// ====================================================================================
	$showData = array(

		//NAMA FIELD BERDASARKAN ATTRIBUTE YG ADA PADA TABLE
		'field' => array(
			'name',
			'email',
			'date_added',
			'last_login'
		),

		// ================================================================================
		// CUSTOM CONTENT (KOSONGKAN BILA TIDAK DIPAKAI)
		// ROW = HASIL OBJECT DARI AJAX 
		// (CONTOH: UNTUK MENGAMBIL VALUE DARI TABLE (row.icon))
		// CONTOH PENGGUNAAN : 'icon'  => '<img src="\'+base_url+\'lib/images/\'+row.icon+\'"/>',
		// row.icon = "icon" NAMA ATTRIBUTE TABEL. YG DIRUBAH HANYALAH ATTRIBUTE TABLE!!
		// ================================================================================
		'custom_content' => array(
			'date_added' => 'var date = new Date(row.date_added.replace(/-/g, "/"));
					var monthNames = ["Jan", "Feb", "Mar","Apr", "May", "Jun", "Jul","Aug", "Sep", "Oct","Nov", "Dec"];
					var day = date.getDate();
					var monthIndex = date.getMonth();
					var year = date.getFullYear();
					return day + " " + monthNames[monthIndex] + " " + year;' ,

			'last_login' => '
					if(!row.last_login) return "-";
					else {
					var dates = new Date(row.last_login.replace(/-/g, "/"));
					var monthNames = ["Jan", "Feb", "Mar","Apr", "May", "Jun", "Jul","Aug", "Sep", "Oct","Nov", "Dec"];
					var day = dates.getDate();
					var monthIndex = dates.getMonth();
					var year = dates.getFullYear();
					
					return day + " " + monthNames[monthIndex] + " " + year;
				}' ,
		)
	);
?>
<?php $this->load->view('admin/template/list_table_data', $showData); ?>