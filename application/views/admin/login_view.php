<?php 
    if($this->session->flashdata('message')){ ?>
        <div id="login_error" class="notif danger absolute">
            <p><?php echo $this->session->flashdata('message'); ?></p>
            <div class="close-notif">
                <a href="javascript:;">
                    <i class="fa fa-close"></i>
                </a>
            </div>
        </div>
<?php } ?>
<?php 
    if($this->session->flashdata('success')){ ?>
        <div id="login_error" class="notif success absolute">
            <p><?php echo $this->session->flashdata('success'); ?></p>
            <div class="close-notif">
                <a href="javascript:;">
                    <i class="fa fa-close"></i>
                </a>
            </div>
        </div>
<?php } ?>

<div id="home-new">     
    <div class="square-green"></div>
    <div class="line-grey"></div>
    <div class="triangle-yellow"></div>
    <div class="circle-blue"></div>
    <div class="wrap-bg-text vertical">         
        <div class="bg-text"><h1>login</h1></div>
    </div>

    <div class="wrap-form-home">
        <div class="container">
            <div class="row inline">
                <div class="col inline verti-middle l-7 m-4 s-12">
                    <div id="home-logo">
                        <img src="<?php echo base_url('lib/assets/logo/') . setting_value('logo');?>" alt="Gositus">
                    </div>
                </div>
                <div class="col inline verti-middle l-5 m-8 s-12">
                    
                    <div id="wrap-login">
                        <h1 class="title-login">
                            <span>Gositus - CMS</span>
                        </h1>
                        <h1 class="title-forgot hide">
                            <span>Forgot Your Password?</span>
                        </h1>


                        <div class="clock">
                            <label id="date"></label>&nbsp;&nbsp;//&nbsp;&nbsp;<label id="jclock"></label>
                        </div>

                        <div>
                            <div id="login-container">
                                <form id="login" method="post" action="<?php echo base_url('goadmin/login/validate'); ?>">
                                    <p>
                                        <label for="username">Username</label>
                                        <input type="text" class="input-text required input_field" name="gousername" id="username" />
                                    </p>
                                    <p>
                                        <label for="password">Password</label>
                                        <input type="password" class="input-text required input_field" name="gopassword" id="password" />
                                        <span class="eyes">
                                            <a href="javascript:;"><i class="fa fa-eye-slash"></i></a>
                                        </span>
                                     </p>
                                     <?php if(!empty($captcha))
                                     {?>
                                         <div class="form-oneline login-captcha" >
                                            <div  id="captcha_image">
                                                <?php echo $captcha['image']; ?>
                                            </div>
                                            <a href="javascript:" id="refresh-captcha">[perbarui gambar]</a>
                                        </div>
                                    <p>
                                        <label for="captcha">Captcha</label>
                                        <input type="captcha" class="input-text required input_field" name="captcha" id="captcha"   autocomplete="off"/>
                                     </p>
                                     <?php } ?>

                                     <p class="text-right wrap-forgot-password">
                                         <span><a href="javascript:;" class="btn-forgot-password">Forgotten Password</a></span>
                                     </p>
                                     <p>
                                        <button type="submit" class="btn-custom btn-scale btn-grey">Login</button>
                                     </p>
                                     <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" id="csrf-token">
                                </form>
                            </div>

                            <div id="forgot-password" class="hide">
                                <form action="<?php echo base_url('goadmin/forgot-password'); ?>" method="post" id="form-forgot-password" >
                                    <p>
                                        <label for="email_forgot">Email</label>
                                        <input type="text" class="input-text required input_field email" name="email_forgot" id="email_forgot">
                                        <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" id="csrf-token">
                                    </p>
                                    <div class="row inline">
                                        <div class="col inline l-6 m-6 s-12">
                                            <p>
                                               <button type="button" class="btn-custom btn-scale btn-grey btn-cancel-forgot">Cancel</button>
                                            </p>
                                        </div>
                                        <div class="col inline l-6 m-6 s-12">
                                            <p>
                                               <button type="submit" class="btn-custom btn-scale btn-grey">Send</button>
                                            </p>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

<footer>
    <div class="orbit">
        <div class="orbit-large"></div>
        <div class="orbit-medium"></div>
        <div class="orbit-small"></div>
        <div class="moon"></div>
    </div>
    <div class="wave">
        <div class="wave-grey"></div>   
        <div class="wave-grey-2"></div> 
    </div>
</footer>