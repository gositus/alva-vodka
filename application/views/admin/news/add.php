
<div class="row">
    <form method="post" id="content-form" action="<?php echo base_url('goadmin/'.$this->url.'/add')?>" data-toggle="validator" enctype="multipart/form-data">
        
        <div class="col-md-12">
            <?php $this->load->view('admin/template/fixed_heading', array('type' => 'add')); ?>
        </div>
        
        <?php if(validation_errors()){ ?>
            <!-- SHOW ERROR -->
            <?php echo viewErrorValidation();?>
            <!-- END SHOW ERROR -->
        <?php } ?>

        <div class="col-md-8 col-sm-12 col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="m-y-0 font-bold">Information</h4>
                </div>


                <div class="panel-body">
                    <div class="form-horizontal">

                        

                        <?php $x = 0; foreach (language()->result_array() as $lang) : ?>
                        <div class="language lang-<?php echo $lang['attr']?>" <?php if ($x == 0) echo 'style="display:block"'; ?>>
                            <div class="form-group">
                                <label for="name_<?php echo $lang['id']?>" class="control-label col-md-3">Name </label>
                                <div class="col-md-7">
                                    <input type="text" name="name_<?php echo $lang['id']?>" id="name_<?php echo $lang['id']?>" class="form-control required news-name" required>
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                        </div>
                        <?php $x++; endforeach; ?>


                        <div class="form-group">
                            <label for="type" class="control-label col-md-3">Type</label>
                            <div class="col-md-7">
                                <select name="type" id="news_type" class="form-control required" required>
                                    <option value="">--</option>
                                    <option value="1">News</option>
                                    <option value="2">Event</option>
                                    <option value="3">Announcement</option>
                                    <option value="4">Blog</option>
                                </select>
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="start" class="control-label col-md-3">Start Date</label>
                            <div class="col-md-7">
                                <input type="text" name="start" class="required form-control datepicker-start" id="start" required readonly >
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>

                        <div class="form-group news-end gohide">
                            <label for="end" class="control-label col-md-3">End Date</label>
                            <div class="col-md-7">
                                <input type="text" name="end" class=" form-control datepicker-end" id="end" readonly>
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>

                        <div class="form-group upload-image">
                            <label class="control-label col-md-3">Image</label>
                            <div class="col-md-7">
                                <div class="row">
                                    <div class="col-md-4 col-sm-12 col-xs-12">
                                        <label class="btn btn-default file-upload-btn">
                                            Choose file...
                                            <button class="file-upload-input" name="icon" id="ckfinder-upload" type="button"></button>
                                        </label>
                                    </div>
                                    <div class="col-md-8 col-sm-12 col-xs-12">
                                        <div class="wrap-info-path">
                                            <input type="file" name="image" style="display: none;">
                                            <input type="text" class="info-path-image  show form-control" readonly id="finder-image" name="image">
                                        </div>
                                    </div>
                                </div>
                                <p class="help-block">
                                    <small>Recommended size <?php echo $this->image_width . ' * ' . $this->image_height ; ?> px</small>
                                </p>
                                <div class="row">
                                    <div class="col-md-9">
                                        <div class="wrap-preview-image show">
                                            <img src="" class="upload-preview">
                                        </div>      
                                    </div>
                                    
                                </div>
                                
                            </div>
                        </div>
                        <div class="form-group ">
                            <label for="checkseo" class="control-label col-md-3">SEO</label>
                            <div class="col-md-7 checkseo">
                                <input type="checkbox" name="checkseo" id="checkseo">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        

                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4 col-sm-12 col-xs-12">
            <?php $this->load->view('admin/template/add_flag');?>
        </div>
        <div class="col-md-8 col-sm-12 col-xs-12 checkseo-bar">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="m-y-0 font-bold">SEO</h4>
                </div>


                <div class="panel-body">
                    <div class="form-horizontal">



                        <div class="form-group news-url">
                            <label for="seo_url" class="control-label col-md-3">URL</label>
                            <div class="col-md-7">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                      <span class="input-group-text" id="basic-addon1"><?php echo base_url('news'); ?>/</span>
                                    </div>
                                    <input type="text" name="seo_url" class=" form-control seo_url path_url" id="seo_url" >
                                </div>
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>


                        <div <?php echo ($this->session->userdata('admin_role') != 1) ? 'style="display: none;"' :"";?>>
                            <?php $x = 0; foreach (language()->result_array() as $lang) : ?>

                            <div class="language lang-<?php echo $lang['attr']?>" <?php if ($x == 0) echo 'style="display:block"'; ?>>

                                <div class="form-group">
                                    <label for="image_alt_<?php echo $lang['id']?>" class="control-label col-md-3">Image Alt</label>
                                    <div class="col-md-7">
                                        <input type="text" name="image_alt_<?php echo $lang['id']?>" id="image_alt_<?php echo $lang['id']?>" class="form-control " >
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="meta_title_<?php echo $lang['id']?>" class="control-label col-md-3">Meta Title </label>
                                    <div class="col-md-7">
                                        <textarea name="meta_title_<?php echo $lang['id']?>" id="meta_title_<?php echo $lang['id']?>" class="form-control" ></textarea>
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="meta_keyword_<?php echo $lang['id']?>" class="control-label col-md-3">Meta Keyword </label>
                                    <div class="col-md-7">
                                        <textarea name="meta_keyword_<?php echo $lang['id']?>" id="meta_keyword_<?php echo $lang['id']?>" class="form-control" ></textarea>
                                        <p class="help-block">Separate with Comma (,)</p>
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="meta_description_<?php echo $lang['id']?>" class="control-label col-md-3">Meta Description </label>
                                    <div class="col-md-7">
                                        <textarea name="meta_description_<?php echo $lang['id']?>" id="meta_description_<?php echo $lang['id']?>" class="form-control" ></textarea>
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>

                            </div>
                            <?php $x++; endforeach; ?>
                        </div>
                    </div>

                </div>
            </div>
        </div>


        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="m-y-0 font-bold">Content</h4>
                </div>
                <div class="panel-body">
                    <div class="form-horizontal">        
                    <?php $x = 0; foreach (language()->result_array() as $lang) : ?>
                    <div class="language lang-<?php echo $lang['attr']?>" <?php if ($x == 0) echo 'style="display:block"'; ?>>
                        <div class="form-group">
                            <div class="col-md-12">
                                <textarea class="form-control content" name="content_<?php echo $lang['id']?>"  rows="10" id="content_<?php echo $lang['id']?>"></textarea>
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                    </div>
                    <?php $x++;  endforeach; ?>

                        <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" id="csrf-token">
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
<script type="text/javascript">
    $(function(){
        $("#name_<?php echo setting_value('default_language'); ?>").blur(function(){
            var news_name = $(this).val();
            $.ajax({
                 type: "POST",
                 url: base_url + "goadmin/sequential/seo_url",
                 data: {
                    seo_url : news_name,
                    table : "news"
                },
                 success: function (data) {
                    $(".seo_url").val(data);
                 }
             });
             return false;
        })
    })
</script>