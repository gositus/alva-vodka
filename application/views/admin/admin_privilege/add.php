 <div class="row">
    <form method="post" id="content-form" action="<?php echo base_url('goadmin/'.$this->url.'/add')?>" data-toggle="validator">
        
        <div class="col-md-12">
            <?php $this->load->view('admin/template/fixed_heading', array('type' => 'add')); ?>
        </div>
        
        <?php if(validation_errors()){ ?>
            <!-- SHOW ERROR -->
            <?php echo viewErrorValidation(); ?>
            <!-- END SHOW ERROR -->
        <?php } ?>

        <div class="col-md-8 col-sm-12 col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="m-y-0 font-bold">Information</h4>
                </div>


                <div class="panel-body">
                    <div class="form-horizontal">
                        <div class="form-group">
                            <label for="ap_name" class="control-label col-md-3">Name</label>
                            <div class="col-md-7">
                                <input type="text" name="ap_name" id="ap_name" class="form-control required" required>
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="ap_privilege" class="control-label col-md-3">Privilege</label>
                            <div class="col-md-7">
                                <div class="custom-controls-stacked">
                                    <label class="custom-control custom-control-primary custom-radio">
                                        <input type="radio" name="ap_privilege" class="privilege custom-control-input" value="1" checked="">
                                        <span class="custom-control-indicator"></span>
                                        <span class="custom-control-label">Administrator</span>
                                    </label>

                                    <label class="custom-control custom-control-primary custom-radio">
                                        <input type="radio" name="ap_privilege" class="privilege custom-control-input" value="2" >
                                        <span class="custom-control-indicator"></span>
                                        <span class="custom-control-label">Data Entry</span>
                                    </label>

                                    <label class="custom-control custom-control-primary custom-radio">
                                        <input type="radio" name="ap_privilege" class="privilege custom-control-input" value="3">
                                        <span class="custom-control-indicator"></span>
                                        <span class="custom-control-label">Custom User</span>
                                    </label>
                                </div>
                            </div>
                        </div>

                        <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" id="csrf-token">
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4 col-sm-12 col-xs-12">
            <?php $this->load->view('admin/template/add_flag');?>
        </div>

        <div class="col-md-8 col-sm-12 col-xs-12" id="access-table">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="m-y-0 font-bold">Custom User</h4>
                </div>

                <div class="panel-body">
                    <div class="form-horizontal">
                        <div class="table-custom-user">
                            <table class="table table-striped dataTable">
                                <thead align="center">
                                    <tr>
                                        <th width="52%">Module</th>
                                        <th class="text-center" width="12%">Read</th>
                                        <th class="text-center" width="12%">Add</th>
                                        <th class="text-center" width="12%">Modify</th>
                                        <th class="text-center" width="12%">Delete</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $html = '';
                                    foreach ($parent_modules->result_array() as $parent) :
                                    
                                        // Get child modules.
                                        $child_modules = $this->db->order_by('name','asc')->get_where('module', array('parent' => $parent['id'], 'flag' => 1));
                                        
                                        if ($child_modules->num_rows() > 0) :
                                            $html .= '<tr class="module-parent" id="' . $parent['alias'] . '">';
                                            $html .= '<td colspan="5" align="center" class="font-bold tr-parent">' . $parent['name'] . '</td>';
                                            $html .= '</tr>';
                                        
                                            foreach ($child_modules->result_array() as $child) :

                                                $last_child_modules =  $this->db->order_by('name','asc')->get_where('module', array('parent' => $child['id'], 'flag' => 1));
                                                
                                                if ($last_child_modules->num_rows() > 0)
                                                {
                                                    foreach ($last_child_modules->result_array() as $last) :
                                                        $html .= view_custom_table($child , $last);
                                                    endforeach;
                                                } else {
                                                    $html .= view_custom_table($parent, $child);
                                                } 
                                            endforeach;
                                        endif;
                                    endforeach;
                                    
                                    echo $html;
                                    ?>
                                    
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
<script type="text/javascript">
    $(function(){
            $('#access-table').hide();
    })
</script>