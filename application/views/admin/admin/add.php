<div class="row">
    <form method="post" id="content-form" action="<?php echo base_url('goadmin/'.$this->url.'/add')?>" data-toggle="validator" enctype="multipart/form-data">
        
        <div class="col-md-12">
            <?php $this->load->view('admin/template/fixed_heading', array('type' => 'add')); ?>
        </div>
        
        <?php if(validation_errors()){ ?>
            <!-- SHOW ERROR -->
            <?php echo viewErrorValidation();?>
            <!-- END SHOW ERROR -->
        <?php } ?>

        <div class="col-md-8 col-sm-12 col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="m-y-0 font-bold">Information</h4>
                </div>


                <div class="panel-body">
                    <div class="form-horizontal">
                        <div class="form-group">
                            <label for="a_name" class="control-label col-md-3">Name</label>
                            <div class="col-md-7">
                                <input type="text" name="a_name" id="a_name" class="form-control required" required  autocomplete="off">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label for="a_privilege" class="control-label col-md-3">Admin Privilege</label>
                            <div class="col-md-7">
                                <select name="a_privilege" id="a_privilege" class="form-control required" required>
                                    <option value="">--</option>
                                    <?php foreach ($admin_privilege as $key => $value) { ?>
                                        <option value="<?php echo $value['id'] ?>"><?php echo $value['name']?></option>
                                    <?php } ?>
                                </select>
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="a_username" class="control-label col-md-3">Username</label>
                            <div class="col-md-7">
                                <input type="text" name="a_username" id="a_username" class="form-control username required" required  autocomplete="off">
                                <div class="help-block">
                                    <p>Note: Digunakan untuk Login dan tidak dapat diubah</p>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="a_password" class="control-label col-md-3">Password</label>
                            <div class="col-md-7">
                                <input type="password" name="a_password" id="a_password" class="form-control required pass_word" required minlength="8">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="a_repassword" class="control-label col-md-3">Re - Password</label>
                            <div class="col-md-7">
                                <input type="password" name="a_repassword" id="a_repassword" class="form-control required" required >
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="a_email" class="control-label col-md-3">Email</label>
                            <div class="col-md-7">
                                <input type="text" name="a_email" id="a_email" class="email form-control required check_email" required autocomplete="off">
                                <div class="help-block">
                                    <p>Note: Digunakan untuk Forgot Password</p>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="a_phone" class="control-label col-md-3">Phone Number</label>
                            <div class="col-md-7">
                                <input type="text" name="a_phone" id="a_phone" class="number form-control">
                            </div>
                        </div>


                        <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" id="csrf-token">
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4 col-sm-12 col-xs-12">
            <?php $this->load->view('admin/template/add_flag');?>
        </div>
    </form>
</div>

<script type="text/javascript">
    $(function(){
        var username , email, password = null;
        $.validator.addMethod("username", function(value) {
            $.ajax({
                type: "POST",
                url: '<?php echo base_url("goadmin/admin/check_username"); ?>',
                data:{
                        'key':value,
                        '<?php echo $this->security->get_csrf_token_name();?>':$("#csrf-token").val()
                    },
                    success: function(data){
                        var obj = JSON.parse(data);
                        username= obj.a;
                        $("#csrf-token").val(obj.b);
                        
                    }
                });
             if (username == 'false'){
                return false;
             } else{
                return true;
             }
        },'Username Already Taken');

        $.validator.addMethod("check_email", function(value) {
            $.ajax({
                type: "POST",
                url: '<?php echo base_url("goadmin/admin/check_email"); ?>',
                data:{
                        'key':value,
                        '<?php echo $this->security->get_csrf_token_name();?>':$("#csrf-token").val()
                    },
                    success: function(data){
                        var obj = JSON.parse(data);
                        email= obj.a;
                        $("#csrf-token").val(obj.b);
                        
                    }
                });
             if (email == 'false'){
                return false;
             } else{
                return true;
             }
        },'Email Already Taken');

        // $.validator.addMethod("pass_word", function(value) {
        //    return /^[A-Za-z0-9\d=!\-@._*]*$/.test(value) // consists of only these
        //        && /[a-z]/.test(value) // has a lowercase letter
        //        && /\d/.test(value) // has a digit
        //        && /\W/.test(value) // has a special characters
        // },'Must contain Uppercase, Lowercase, Digit and special characters');


        $.validator.addMethod("pass_word", function(value) {
            $.ajax({
                type: "POST",
                url: '<?php echo base_url("goadmin/admin/check_pattern"); ?>',
                data:{
                        'key':value,
                        '<?php echo $this->security->get_csrf_token_name();?>':$("#csrf-token").val()
                    },
                    success: function(data){
                        var obj = JSON.parse(data);
                        password= obj.a;
                        $("#csrf-token").val(obj.b);
                        
                    }
                });
             if (password == 'false'){
                return false;
             } else{
                return true;
             }
        },'Must contain Uppercase, Lowercase, Digit and special characters');
    })

</script>