<?php if($this->session->flashdata('success')) { ?>
	<script>
		$(function(){
			showToast('success', 'bottom', "<?php echo $this->session->flashdata('success');?>");
		})
	</script>
<?php } ?>
<div class="row">
	<div class="col-md-12">
		<?php $this->load->view('admin/template/fixed_heading', array('type' => 'list')); ?>
	</div>
    
    <div class="col-md-12">	
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="m-y-0 font-bold">List <?php echo $title; ?></h4>
                <div class="error-input"><?php echo validation_errors();?></div>
            </div>
            <div class="panel-body">
                <div class="form-horizontal">
                	<div class="table-responsive">
						
						<table id="list-table" data-table-name="<?php echo $url; ?>" class="table table-striped dataTable">
							<thead>
								<tr>
									<th>No</th>
									<th>Name</th>
									<th>Image</th>
								
									<?php $this->load->view('admin/template/list_table_heading')?>
								</tr>
							</thead>
							<tbody></tbody>
						</table>
                	</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php 
	//SHOW DATA TO DATATABLE
	// ====================================================================================
	$showData = array(

		//NAMA FIELD BERDASARKAN ATTRIBUTE YG ADA PADA TABLE
		'field' => array(
			'item_name',
			'image',
		
		),

		// ================================================================================
		// CUSTOM CONTENT (KOSONGKAN BILA TIDAK DIPAKAI)
		// ROW = HASIL OBJECT DARI AJAX 
		// (CONTOH: UNTUK MENGAMBIL VALUE DARI TABLE (row.icon))
		// CONTOH PENGGUNAAN : 'icon'  => '<img src="\'+base_url+\'lib/images/\'+row.icon+\'"/>',
		// row.icon = "icon" NAMA ATTRIBUTE TABEL. YG DIRUBAH HANYALAH ATTRIBUTE TABLE!!
		// ================================================================================
		'custom_content' => array(
			'type'=> 'if(row.type==1) return "Photo"; else if(row.type==2) return "Video"',
			'image' => '
				var strg = row.image;
				// var oke = str.split(".");
				// var strg = oke[0] + "_thumb." + oke[1];
				return "<img src=" +base_url+ "lib/images/item/" +strg+ " / height=100>"
			',
		)
	);
?>
<?php $this->load->view('admin/template/list_table_data', $showData); ?>