<div class="row">
    <form method="post" id="content-form" action="<?php echo base_url('goadmin/'.$this->url.'/add')?>" data-toggle="validator" enctype="multipart/form-data">
        
        <div class="col-md-12">
            <?php $this->load->view('admin/template/fixed_heading', array('type' => 'add')); ?>
        </div>
        
        <?php if(validation_errors()){ ?>
            <!-- SHOW ERROR -->
            <?php echo viewErrorValidation();?>
            <!-- END SHOW ERROR -->
        <?php } ?>

        <div class="col-md-8 col-sm-12 col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="m-y-0 font-bold">Information</h4>
                </div>


                <div class="panel-body">
                    <div class="form-horizontal">
    
                        <?php $x = 0; foreach (language()->result_array() as $lang) : ?>
                        <div class="language lang-<?php echo $lang['attr']?>" <?php if ($x == 0) echo ' style="display:block"'; ?> >
                            <div class="form-group">
                                <label for="name_<?php echo $lang['id']?>" class="control-label col-md-3">Name </label>
                                <div class="col-md-7">
                                    <input type="text" name="name_<?php echo $lang['id']?>" id="name_<?php echo $lang['id']?>" class="form-control required" required >
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            
                        </div>
                        <?php $x++; endforeach; ?>

                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4 col-sm-12 col-xs-12">
            <?php $this->load->view('admin/template/add_flag');?>
             <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" id="csrf-token">
        </div>
    </form>
</div>