<div class="row">
    <form method="post" id="content-form" action="<?php echo base_url('goadmin/'.$this->url.'/add')?>" data-toggle="validator" enctype="multipart/form-data">
        
        <div class="col-md-12">
            <?php $this->load->view('admin/template/fixed_heading', array('type' => 'add')); ?>
        </div>
        
        <?php if(validation_errors()){ ?>
            <!-- SHOW ERROR -->
            <?php echo viewErrorValidation();?>
            <!-- END SHOW ERROR -->
        <?php } ?>

        <div class="col-md-8 col-sm-12 col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="m-y-0 font-bold">Information</h4>
                </div>

                <div class="panel-body">
                    <div class="form-horizontal">


                        <?php $x = 0; foreach (language()->result_array() as $lang) : ?>
                        <div class="language lang-<?php echo $lang['attr']?>" <?php if ($x == 0) echo 'style="display:block"'; ?>>
                            <div class="form-group">
                                <label for="name_<?php echo $lang['id']?>" class="control-label col-md-3">Name </label>
                                <div class="col-md-7">
                                    <input type="text" name="name_<?php echo $lang['id']?>" id="name_<?php echo $lang['id']?>" class="form-control required" required>
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                        </div>
                        <?php $x++; endforeach; ?>

                        
                        <div class="form-group">
                            <label for="media_category" class="control-label col-md-3">Category</label>
                            <div class="col-md-7">
                                <div class="input-group">
                                    <select name="media_category" id="cms_category" class="form-control required media_category" required>
                                        <option value="">--</option>
                                        <?php foreach ($media_category as $key => $value) {?>
                                        <option value="<?php echo $value['id'] ?>" size-width="<?php echo $value['width']; ?>" size-height="<?php echo $value['height']; ?>"><?php echo $value['media_category_name'] ?></option>
                                        <?php } ?>
                                    </select>
                                    <?php /*/ ?>
                                    <div class="input-group-btn">
                                        <button class="btn btn-outline-secondary" type="button" data-toggle="modal" data-target="#addForm" data-table-cms="<?php echo $this->url; ?>">
                                        <i class="zmdi zmdi-plus-square zmdi-hc-lg zmdi-hc-fw"></i>
                                        </button>
                                    </div>
                                    <?php /*/ ?>
                                </div>

                                <div class="help-block with-errors"></div>
                            </div>
                        </div>

                        <?php $x = 0; foreach (language()->result_array() as $lang) : ?>
                        <div class="language lang-<?php echo $lang['attr']?>" <?php if ($x == 0) echo 'style="display:block"'; ?>>

                            <div class="form-group desc" style="display: none">
                                <label for="content_<?php echo $lang['id']?>" class="control-label col-md-3">Description </label>
                                <div class="col-md-7">
                                    <textarea class="form-control" name="content_<?php echo $lang['id']?>"  rows="10" id="content_<?php echo $lang['id']?>"></textarea>
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>

                        </div>
                        <?php $x++; endforeach; ?>
                        <?php /*/ ?>
                        <div class="form-group">
                            <label for="type" class="control-label col-md-3">Type</label>
                            <div class="col-md-7">
                                <select name="type" id="type" class="form-control required" required>
                                    <option value="">--</option>
                                    <option value="1">Photo</option>
                                    <option value="2">Video</option>
                                    
                                </select>
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <?php /*/ ?>

                        <div class="form-group upload-image">
                            <label class="control-label col-md-3">Image</label>
                            <div class="col-md-7">
                                <div class="row">
                                    <div class="col-md-4 col-sm-12 col-xs-12">
                                        <label class="btn btn-default file-upload-btn">
                                            Choose file...
                                            <button class="file-upload-input" name="icon" id="ckfinder-upload" type="button"></button>
                                        </label>
                                    </div>
                                    <div class="col-md-8 col-sm-12 col-xs-12">
                                        <div class="wrap-info-path">
                                            <input type="file" name="image" style="display: none;">
                                            <input type="text" class="info-path-image  show form-control" readonly id="finder-image" name="image">
                                        </div>
                                    </div>
                                </div>
                                <p class="media-size-help help-block">
                                    <small>Recommended size <?php echo $this->image_width . ' * ' . $this->image_height ; ?> px</small>
                                </p>
                                <div class="row">
                                    <div class="col-md-9">
                                        <div class="wrap-preview-image show">
                                            <img src="" class="upload-preview">
                                        </div>      
                                    </div>
                                    
                                </div>
                                
                            </div>
                        </div>
                        <?php /*/ ?>
                        <div class="form-group type-video" style="display: none;">
                            <label for="video" class="control-label col-md-3">Video Url</label>
                            <div class="col-md-7">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                      <span class="input-group-text" id="basic-addon1">https://www.youtube.com/watch?v=</span>
                                    </div>
                                        <input type="text" name="video" id="video" class="form-control">
                                </div>
                                        <div class="help-block with-errors"><small>Input only youtube ID</small></div>

                            </div>
                        </div>
                        <?php /*/ ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4 col-sm-12 col-xs-12">
            <?php $this->load->view('admin/template/add_flag');?>
        </div>

        <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" id="csrf-token">
    </form>
</div>
<script type="text/javascript">
$(function (){
    $("#cms_category").on("change", function(){
        var val = $(this).val();
        if(val == 2){
            $(".desc").show();
        }else{
            $(".desc").hide();
        }
    });
});
</script>
