<div class="row">
	<div class="col-md-12">
		<!-- BREADCRUMBS -->
		<ol class="breadcrumb">
    		<li>
		        <a href="<?php echo base_url('goadmin'); ?>">
		            <i class="zmdi zmdi-home"></i>
		        </a>
		    </li>
		    <li><a href="<?php echo base_url('goadmin'); ?>">Dashboard</a></li>
		</ol>
<!-- END BREADCRUMBS -->
	</div>
	<?php $dashboard_md3 = array(
								array('title'=>'Uptime','value'=>$uptime),
								array('title'=>'Sessions (' . date("M Y") .')', 'value'=>$total_visit_home),
								array('title'=>'Pageviews','value'=>$total_page_view),
								array('title'=>'Leads','value'=>$total_leads)
							);
	//kalo ada yg gak dibutuhin, tinggal comment aja array nya
	foreach ($dashboard_md3 as $key => $value) { ?>
	<div class="col-md-3">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h4 class="m-y-0"><?php echo $value['title']; ?></h4>
			</div>
			<div class="panel-body">
				<div class="form-horizontal">
					<h2 class="panel-dashboard"><?php echo $value['value']; ?></h2>
				</div>
			</div>
		</div>
	</div>
	<?php } ?>
	<div class="clearfix"></div>
	<?php $dashboard_md6 = array(
								//array('title'=>'Most View Category','value'=>$most_view_category),
								//array('title'=>'Most View Product','value'=>$most_view_product),
								//array('title'=>'Most View News','value'=>$most_view_news),
								//array('title'=>'Trending News','value'=>$current_trending_news)
							);
	?>
	<?php foreach ($dashboard_md6 as $k => $val) { ?>
	<div class="col-md-6">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h4 class="m-y-0"><?php echo $val['title']; ?></h4>
			</div>
			<div class="panel-body">
				<div class="form-horizontal">
					<table class="table table-striped dataTable">
						<thead>
							<tr>
								<td>Title</td>
								<td>Views</td>
							</tr>
						</thead>
						<tbody>
							<?php foreach ($val['value'] as $key => $value) { ?>
							<tr>
								<td><a href="<?php echo $value['link_view'].'/'. $value['seo_url'] ?>" target="_blank"><?php echo $value['name_view']; ?></a></td>
								<td><?php echo number2str($value['total_view']); ?></td>
							</tr>
							<?php } ?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
	<?php } ?>
	

</div>
