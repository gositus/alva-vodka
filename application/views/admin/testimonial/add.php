<div class="row">
    <form method="post" id="content-form" action="<?php echo base_url('goadmin/'.$this->url.'/add')?>" data-toggle="validator" enctype="multipart/form-data">
        
        <div class="col-md-12">
            <?php $this->load->view('admin/template/fixed_heading', array('type' => 'add')); ?>
        </div>
        
        <?php if(validation_errors()){ ?>
            <!-- SHOW ERROR -->
            <?php echo viewErrorValidation();?>
            <!-- END SHOW ERROR -->
        <?php } ?>

        <div class="col-md-8 col-sm-12 col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="m-y-0 font-bold">Information</h4>
                </div>


                <div class="panel-body">
                    <div class="form-horizontal">
                        <div class="form-group">
                            <label for="name" class="control-label col-md-3">Name </label>
                            <div class="col-md-7">
                                <input type="text" name="name" id="name" class="form-control required" required>
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="email" class="control-label col-md-3">Email </label>
                            <div class="col-md-7">
                                <input type="text" name="email" id="email" class="form-control required" required>
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="phone" class="control-label col-md-3">Phone </label>
                            <div class="col-md-7">
                                <input type="text" name="phone" id="phone" class="form-control required" required>
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>

                        <div class="form-group upload-image">
                            <label class="control-label col-md-3">Image</label>
                            <div class="col-md-7">
                                <div class="row">
                                    <div class="col-md-4 col-sm-12 col-xs-12">
                                        <label class="btn btn-default file-upload-btn">
                                            Choose file...
                                            <button class="file-upload-input" name="icon" id="ckfinder-upload" type="button"></button>
                                        </label>
                                    </div>
                                    <div class="col-md-8 col-sm-12 col-xs-12">
                                        <div class="wrap-info-path">
                                            <input type="file" name="image" style="display: none;">
                                            <input type="text" class="info-path-image  show form-control" readonly id="finder-image" name="image">
                                        </div>
                                    </div>
                                </div>
                                <p class="help-block">
                                    <small>Recommended size <?php echo $this->image_width . ' * ' . $this->image_height ; ?> px</small>
                                </p>
                                <div class="row">
                                    <div class="col-md-9">
                                        <div class="wrap-preview-image show">
                                            <img src="" class="upload-preview">
                                        </div>      
                                    </div>
                                    
                                </div>
                                
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="testimonial" class="control-label col-md-3">Testimonial </label>
                            <div class="col-md-7">
                                <textarea class="form-control" name="content"  rows="4" id="content"></textarea>
                            </div>
                                <div class="help-block with-errors"></div>
                            </div>
                         <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" id="csrf-token">
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4 col-sm-12 col-xs-12">
            <?php $this->load->view('admin/template/add_flag');?>
        </div>
       </form>
</div>
