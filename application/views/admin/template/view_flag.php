<div class="panel panel-default">
    <div class="panel-heading">
        <h4 class="m-y-0 font-bold">Status</h4>
    </div>
    <?php 
     $default_lang = setting_value('default_language');
    if(!empty($row['flag'])){
        $view_flag = $row['flag'];
    } else if(isset($row[$default_lang]['flag'])){
        $view_flag = $row[$default_lang]['flag'];
    } else {
        $view_flag ="";
    }
    if(!empty($row['flag_memo'])){
        $view_flag_memo = $row['flag_memo'];
    } else if(isset($row[$default_lang]['flag_memo'])){
        $view_flag_memo = $row[$default_lang]['flag_memo'];
    } else {
        $view_flag_memo ="";
    }?>

    <?php if($this->uri->segments[2] =='newsletter'){
        $active_text = "Subscribe";
    } elseif ($this->uri->segments[2] =='message') {
        $active_text ="Read";
    } else{
        $active_text = "Active";
    } ?>

    <?php if($this->uri->segments[2] =='newsletter'){
        $inactive_text = "Unsubscribe";
    } elseif ($this->uri->segments[2] =='message') {
        $inactive_text ="Unread";
    } else{
        $inactive_text = "Inactive";
    } ?>

    <div class="panel-body">
        <div class="form-horizontal">
            <div class="form-group text-center wrap-status">
                <div class="col-md-12">
                    <div class="btn-group" data-toggle="buttons">
                        <label class="btn btn-outline-success m-y-5 m-w-100 <?php echo (!empty($view_flag ) && $view_flag==1 )? 'active' : '';?>">
                            <input type="radio" name="flag" value="1" <?php echo (!empty($view_flag ) && $view_flag==1 )? 'checked' : '';?>> <?php echo $active_text; ?>
                        </label>
                        <label class="btn btn-outline-danger m-y-5 m-w-100 <?php echo (!empty($view_flag ) && $view_flag==2 )? 'active' : '';?>">
                            <input type="radio" name="flag" value="2" <?php echo (!empty($view_flag ) && $view_flag==2 )? 'checked' : '';?>> <?php echo $inactive_text; ?>
                        </label>
                    </div>
                </div>
            </div>
        
            <div class="form-group">
                <label class="col-md-12">Memo</label>
                <div class="col-md-12">
                    <textarea class="form-control" id="flag_memo" name="flag_memo" data-plugin="autosize"><?php echo $view_flag_memo; ?></textarea>
                </div>
            </div>
            <?php if (check_access($this->url, 'delete')) : ?>
                <div class="form-group m-b-0">
                    <label class="col-md-12 m-b-0">
                        Delete this data? <a href="#" data-toggle="modal" data-target="#modal-delete-row">Click Here!</a>
                    </label>
                </div>
            <?php endif; ?>
        </div>
    </div>
</div>

<!-- MODAL CONFIRM DELETE DATA -->
<div id="modal-delete-row" class="modal">
    <div class="modal-dialog modal-sm">
        <div class="modal-content bg-warning animated bounceIn">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    <span>
                        <i class="zmdi zmdi-close"></i>
                    </span>
                </button>
            </div>

            <div class="modal-body">
                <div class="text-center">
                    <div>
                        <i class="zmdi zmdi-alert-circle-o zmdi-hc-5x"></i>
                    </div>
                    <h3>Warning</h3>
                    <p>Are you sure to delete this data ?</p>
                    <div class="m-y-30">
                        <button type="button" id="btn-continue-delete-row" class="btn btn-default">Continue</button>
                        <a href="#" data-dismiss="modal" class="btn btn-warning">Cancel</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END MODAL CONFIRM DELETE DATA -->