<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" id="csrf-token">
<?php $temp_url = str_replace("_", "-", $this->url); ?>

<!-- DIALOG CONFIRM DELETE-->
<div id="dialog-confirm" class="modal">
    <div class="modal-dialog modal-sm">
        <div class="modal-content bg-warning animated bounceIn">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    <span>
                        <i class="zmdi zmdi-close"></i>
                    </span>
                </button>
            </div>
            <div class="modal-body">
                <div class="text-center">
                    <div>
                        <i class="zmdi zmdi-alert-circle-o zmdi-hc-5x"></i>
                    </div>
                    <h3>Warning</h3>
                    <p>These <span class="font-bold count-item-delete"></span> items will be permanently deleted and cannot be recovered. Are you sure ?</p>
                    <div class="m-y-30">
                        <button type="button" id="btn-continue-delete" class="btn btn-default">Continue</button>
                        <button type="button" data-dismiss="modal" class="btn btn-warning">Cancel</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END DIALOG CONFIRM DELETE-->


<!-- DIALOG ALERT DELETE-->
<div id="dialog-alert" class="modal">
    <div class="modal-dialog modal-sm">
        <div class="modal-content bg-info animated bounceIn">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    <span>
                        <i class="zmdi zmdi-close"></i>
                    </span>
                </button>
            </div>

            <div class="modal-body">
                <div class="text-center">
                    <div>
                        <i class="zmdi zmdi-help zmdi-hc-5x"></i>
                    </div>
                    <h3>Information</h3>
                    <p>Please select item(s) to delete first.</p>
                    <div class="m-y-30">
                        <button type="button" data-dismiss="modal" class="btn btn-default">Continue</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END DIALOG ALERT DELETE-->


<!-- DIALOG CONFIRM CHANGE STATUS-->
<div id="dialog-confirm-change-status" class="modal">
    <div class="modal-dialog modal-sm">
        <div class="modal-content bg-warning animated bounceIn">
            <div class="modal-header"></div>
            <div class="modal-body">
                <div class="text-center">
                    <div>
                        <i class="zmdi zmdi-alert-circle-o zmdi-hc-5x"></i>
                    </div>
                    <h3>Warning</h3>
                    <p>Are you sure to change data ?</p>
                    <div class="form-group">
                        <label class="col-md-12 control-label">Reason</label>
                        <textarea name="status_reason" rows="1" class="form-control" autofocus id="warning-reason"></textarea>
                    </div>
                    <div class="m-y-30">
                        <button type="button" id="btn-continue-change-status" class="btn btn-default">Continue</button>
                        <a href="#" id="btn-cancel-change-status" data-dismiss="modal" class="btn btn-warning">Cancel</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END DIALOG CONFIRM CHANGE STATUS-->
<script src="<?php echo base_url('lib/js/goadmin/plugins/dataTable/jquery.dataTables.min.js'); ?>"></script>

<script type="text/javascript">
function updateDataTableCheckAll(table){
    var $table             = table.table().node();
    var $chkbox_all        = $('tbody .deletechecked', $table);
    var $chkbox_checked    = $('tbody .deletechecked:checked', $table);
    var chkbox_select_all  = $('thead input[name="select_all"]', $table).get(0);

    if($chkbox_checked.length === 0){
        chkbox_select_all.checked = false;
        if('indeterminate' in chkbox_select_all){
            chkbox_select_all.indeterminate = false;
        }

    } else if ($chkbox_checked.length === $chkbox_all.length){
        chkbox_select_all.checked = true;
        if('indeterminate' in chkbox_select_all){
            chkbox_select_all.indeterminate = false;
        }

    } else {
        chkbox_select_all.checked = true;
        if('indeterminate' in chkbox_select_all){
            chkbox_select_all.indeterminate = true;
        }
    }
}

$(document).ready(function(){
    var rows_selected = [];
    // ==========================================DATATABLES====================================================
    var table = $('#list-table').DataTable({
        <?php if($this->dt_serverside){ ?>
            "processing": true,
            "serverSide": true,
        <?php } ?>
        "ajax": {
            "url" : "<?php echo site_url('goadmin/'.$this->url.'/list-data');?>",
            "type": "POST",
            "data": function(data){
                data.<?php echo $this->security->get_csrf_token_name(); ?> = $("#csrf-token").val();
            },
            "headers": {
                '<?php echo $this->security->get_csrf_token_name(); ?>' : $("#csrf-token").val()
            }
        },
        "pageLength": 100,
        "lengthMenu": [ 100, 250, 500, 1000 ],
        "columnDefs": [
        <?php $no = 0; ?>
            {
                "targets"   : <?php echo $no++; ?>,
                "searchable": false,
                "width"     : 40,
                "render"    : function(data, type, row, meta){
                    return meta.row + meta.settings._iDisplayStart + 1;
                }
            },
            <?php
                // $no = 0;
                if($field){
                    foreach ($field as $key => $value) {
                        ?>
                            {
                                "targets" : <?php echo $no;?>,
                                "data" : "<?php echo (is_array($value)) ? $key : $value;?>",
                                <?php
                                    if(is_array($value)){
                                        foreach ($value as $options => $searchOrder) {
                                            if($options && !empty($searchOrder)){
                                                ?>
                                                    "<?php echo $options;?>": <?php echo $searchOrder ;?>,
                                                <?php
                                            }
                                        }
                                    }
                                ?>
                                <?php if ($key == 0) { ?>
                                "render" : function(data, type, row){
                                        if(!row.name){
                                            row.name = row.c_name;
                                        }
                                        return '<a title="Edit &quot;'+ row.name +'&quot;" href="<?php echo base_url(), 'goadmin/', $temp_url, '/view/'; ?>'+ row.id +'" class="btn-views ">'+ row.name +'</a>';
                                },
                                <?php } ?>
                                <?php 
                                    if($custom_content){
                                        foreach ($custom_content as $keys => $val) {
                                            $field = (is_array($value)) ? $key : $value;
                                            if($field == $keys){
                                                ?>
                                                "render" : function(data, type, row){
                                                    <?php echo $val;?>;
                                                }
                                                <?php
                                            }
                                        }
                                    }
                                ?>
                            },
                        <?php
                        $no++;
                    }
                }
            ?>
            <?php  if($this->uri->rsegments[1]!= "master" || $this->uri->rsegments[2]== "banned_ip" ) {?>
            {
                "targets": <?php echo $no++; ?>,
                "orderable": false,
                "searchable": false,
                "render": function(data, type, row){
                    var html_check = '';
                    <?php $enable_popup='';
                            $disable_input = ' disabled ';
                    if (check_access($this->url, 'edit')) {
                        $disable_input ='';
                        $enable_popup = ' data-target="#dialog-confirm-change-status" ';
                    }?>
                    var enable_popup ='<?php echo $enable_popup; ?>';
                    var disable_input ='<?php echo $disable_input; ?>';
                    if(row.flag == 1){
                        html_check = '<input type="checkbox" class="s-input" name="change_status" value="1"  '+ disable_input +'checked>';  
                    } else {
                        html_check = '<input type="checkbox" class="s-input" name="change_status" value="1" '+ disable_input +'>';
                    }
                    return  '<div class="switches-stacked warp-change-status" data-id="'+row.id+'" data-current-flag="'+row.flag+'">'+
                                    '<label class="switch switch-success">'+
                                        html_check +
                                        '<span class="s-content" data-toggle="modal" '+enable_popup+' data-backdrop="static" data-keyboard="false">'+
                                            '<span class="s-track"></span>'+
                                            '<span class="s-handle"></span>'+
                                        '</span>'+
                                    '</label>'+
                                '</div>'+
                            '</div>';
                }
            },
            {
                "targets": <?php echo $no++; ?>,
                "orderable": false,
                "render": function(data, type, row){
                    if(row.flag_memo){
                        return row.flag_memo;
                    } else {
                        return '-';
                    }
                }
            }
            <?php if (check_access($this->url, 'delete'))  : ?>
            ,{
                "targets": <?php echo $no++; ?>,
                "orderable": false,
                "render" : function(data, type, row){
                    return '<?php if (check_access($this->url, 'delete')) : ?>'+
                                '<div class="custom-control-stacked text-center wrap-checkbox-delete">'+
                                    '<label class="custom-control custom-control-primary custom-checkbox">'+
                                        '<input class="deletechecked custom-control-input" type="checkbox" value="'+ row.id +'" />'+
                                        '<span class="custom-control-indicator"></span>'+
                                    '</label>'+
                                '</div>'+
                            '<?php endif; ?>';
                }
            }
        <?php endif; ?>
            <?php } ?>
        ],
        "drawCallback": function(){
            $('#list-table tbody .warp-change-status input[name=change_status] + span.s-content').each(function(index, el) {
                $(this).on('click', function(){
                    var checkbox     = $(this).parents('.warp-change-status').find('input[name=change_status]');
                    var current_val  = $(this).parents('.warp-change-status').find('input[name=change_status]').prop('checked');
                    var table_name   = $('#list-table').attr('data-table-name');
                    var item_id      = $(this).parents('.warp-change-status').attr('data-id');
                    var current_flag = $(this).parents('.warp-change-status').attr('data-current-flag');

                    if(current_flag == 1){
                        var changeTo = '2';
                    } else if(current_flag == 2) {
                        var changeTo = '1';
                    }
                    
                    $('.data-change').remove();
                    $('#dialog-confirm-change-status').append('<div class="data-change" data-new-flag="' + changeTo + '" data-table="' + table_name + '" data-id="' + item_id + '"></div>');    
                    setTimeout(function(){ $("#warning-reason").focus(); }, 500);
                    $('#btn-cancel-change-status').on('click', function(){
                        $('textarea[name=status_reason]').val('');
                        checkbox.prop('checked', current_val);
                        $('.data-change').remove();
                    });
                });
            });
        },
        "rowCallback": function(row, data, dataIndex){
             var rowId = data['id'];
             if(data.flag==2){
                $(row).addClass('disable-flag');
             }
             if($.inArray(rowId, rows_selected) !== -1){
                $(row).find('.deletechecked').prop('checked', true);
                $(row).addClass('selected');
             }
          }
    });
    // ==========================================END DATATABLES====================================================


    // CHANGE STATUS
    $('#btn-continue-change-status').on('click', function(e){
        var memo = $('textarea[name=status_reason]').val();
        $.ajax({
            type: "POST",
            url: base_url + 'goadmin/ajax/flag/',
            data:{
                "new_flag": $('.data-change').attr('data-new-flag'), 
                "table": $('.data-change').attr('data-table'), 
                "id": $('.data-change').attr('data-id'), 
                "flag_memo": memo
            },
            success: function(data){
                $('#dialog-confirm-change-status').modal('hide');
                $('.data-change').remove();
                $('textarea[name=status_reason]').val('');
                $("#csrf-token").val(data);
                showToast('success', 'bottom', 'Congratulations, status has been changed!');    
                table.ajax.reload();
            },
            error: function(){
                $('#dialog-confirm-change-status').modal('hide');
                $('.data-change').remove();
                $('textarea[name=status_reason]').val('');
                showToast('error', 'bottom', 'Error, status unchanged!');  
                table.ajax.reload();
            }
        })
    });
    // END CHANGE STATUS


    // ==========================================DELETE LIST====================================================
    $('#uncheck').click(function(){
        $('#list-table tbody .deletechecked:checked').trigger('click');
    });

    $('#delcheck').click(function(e){
        var n = $('#list-table tbody .wrap-checkbox-delete input:checked').length;
        
        $('.count-item-delete').text(n);

        if(n>0){
            $('#dialog-confirm').modal('show');
        }else{
            $('#dialog-alert').modal('show');
        }
    });
    
    $('#dialog-confirm #btn-continue-delete').on('click', function(){
        var n = $('#list-table tbody .wrap-checkbox-delete input:checked').length;
        var id = '';
        var tableName = $('#list-table').attr('data-table-name');
        $('#list-table .wrap-checkbox-delete input:checked').each(function(){
            id += '"' + $(this).val() + '",';
        });
        var idLen = id.length;
        id = id.slice(0,idLen-1);

        $.ajax({
            type: 'POST',
            data: {
                'id':'['+id+']',
                'table': tableName
            },
            dataType: 'JSON',
            url: '<?php echo base_url('goadmin/sequential/delete'); ?>',
            complete: function(data){
                $('#dialog-confirm').modal('hide');
                $("#csrf-token").val(data.responseText);
                table.ajax.reload();
                showToast('success', 'bottom', 'Congratulations, '+n+' data has been deleted');
            },
        });
    });
    <?php  if(!empty($filter_date_range)){?>
        $.fn.dataTableExt.afnFiltering.push(
            function( oSettings, aData, iDataIndex ) {
                var iFini = document.getElementById('datepicker_from').value;
                var iFfin = document.getElementById('datepicker_to').value;
                var iStartDateCol = 3;
                var iEndDateCol = 3;
                if(iFini !== ""){
                    iFini=new Date(iFini.substring(7,11) + ' ' + iFini.substring(3,6)+ ' ' + iFini.substring(0,2));  
                }
                if(iFfin !== ""){
                    iFfin=new Date(iFfin.substring(7,11) + ' ' + iFfin.substring(3,6)+ ' ' + iFfin.substring(0,2));
                }
                var datofini=new Date(aData[iStartDateCol].substring(7,11) + ' ' + aData[iStartDateCol].substring(3,6) + ' ' + aData[iStartDateCol].substring(0,2));
                var datoffin=new Date(aData[iEndDateCol].substring(7,11) + ' ' + aData[iEndDateCol].substring(3,6) + ' ' + aData[iEndDateCol].substring(0,2));
                if ( iFini === "" && iFfin === "" )
                {
                    return true;
                }
                else if ( iFini <= datofini && iFfin === "")
                {
                    return true;
                }
                else if ( iFfin >= datoffin && iFini === "")
                {
                    return true;
                }
                else if (iFini <= datofini && iFfin >= datoffin)
                {
                    return true;
                }
                return false;
            }
        );
        $('#datepicker_from').on('change', function() { table.draw(); } );
        $('#datepicker_to').on('change', function() { table.draw(); } );
 
            <?php } ?>

        $(".table_search").on( 'keyup', function () {
            table
                .column( $(this).data('index') )
                .search( this.value )
                .draw();
        } );
    // ==========================================END DELETE LIST====================================================


    // ==========================================NOTIFICATION DELETE ROW====================================================
    if (typeof(Storage) !== "undefined") {
        if(localStorage.getItem("isDeleteRow") == "true"){
            showToast('success', 'bottom', 'Congratulations data has been deleted')
            localStorage.setItem("isDeleteRow","false");
        }
    }
    // ==========================================END NOTIFICATION DELETE ROW====================================================
    
    // ==========================================CHECKBOX====================================================
    // Handle click on checkbox
   $('#list-table tbody').on('click', '.deletechecked, .wrap-checkbox-delete .custom-control-indicator', function(e){
      var $row = $(this).closest('tr');
      var data = table.row($row).data();
      var rowId = data['id'];

      var index = $.inArray(rowId, rows_selected);

      if(this.checked && index === -1){
         rows_selected.push(rowId);

      } else if (!this.checked && index !== -1){
         rows_selected.splice(index, 1);
      }

      if(this.checked){
         $row.addClass('selected');
      } else {
         $row.removeClass('selected');
      }

      updateDataTableCheckAll(table);
      e.stopPropagation();
   });

   $('#list-table').on('click', 'tbody td, thead th:last-child', function(e){
         $(this).find('.wrap-checkbox-delete .deletechecked').trigger('click');
         $(this).find('.wrap-check-all .deletechecked').trigger('click');
      });


   // Handle click on "Select all" control
   $('thead th:last-child input[name=select_all]', table.table().container()).on('click', function(e){
    // console.log(this.checked)
      if(this.checked){
         $('#list-table tbody .deletechecked:not(:checked)').trigger('click');
      } else {
         $('#list-table tbody .deletechecked:checked').trigger('click');
      }
      e.stopPropagation();
   });

   // Handle table draw event
    table.on('draw', function(){
        updateDataTableCheckAll(table);
    });
    // ==========================================END CHECKBOX====================================================

});
</script>