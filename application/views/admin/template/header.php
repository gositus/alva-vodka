<!DOCTYPE html>
<html>
<head>
	<!-- Gositus -- www.gositus.com -- go@gositus.com -- instagram.com/gositus -->
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="robots" content="NOINDEX, NOFOLLOW" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
	<title><?php echo $title; ?> | Gositus CMS</title>

	<meta name="theme-color" content="#2b88c6" />
	<meta name="msapplication-TileColor" content="#2b88c6" />
	<link rel="manifest" href="<?php echo base_url('lib/assets/goadmin/favicon/site.webmanifest'); ?>" />
	<link rel="mask-icon" href="<?php echo base_url('lib/assets/goadmin/favicon/favicon.svg');?>" color="#2b88c6" />
	<link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url('lib/assets/goadmin/favicon/favicon.ico');?>" />
	<link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url('lib/assets/goadmin/favicon/favicon-16.png');?>" />
	<link rel="icon" type="image/png" sizes="32x32" href="<?php echo base_url('lib/assets/goadmin/favicon/favicon-32.png');?>" />
	<link rel="icon" type="image/png" sizes="192x192" href="<?php echo base_url('lib/assets/goadmin/favicon/favicon-192.png');?>" />
	<link rel="apple-touch-icon" type="image/png" sizes="180x180" href="<?php echo base_url('lib/assets/goadmin/favicon/favicon-180.png');?>" />
	<link rel="apple-touch-icon" type="image/png" sizes="512x512" href="<?php echo base_url('lib/assets/goadmin/favicon/favicon-512.png');?>" />

	<link rel="stylesheet" type="text/css" href="<?php echo base_url('lib/css/goadmin/plugins/font-awesome/font-awesome.css');?>" />
	<?php foreach ($css as $style) echo '<link rel="stylesheet" type="text/css" href="' . base_url() . 'lib/css/goadmin/' . $style . '.css" />', "\n"; ?>
	<?php if($this->uri->rsegment(1) != 'login' || $this->session->userdata('admin_login') != ''){?>
		<link rel="stylesheet" type="text/css" href="<?php echo base_url('lib/css/goadmin/new-themes/new-app.css');?>" />
		<link rel="stylesheet" type="text/css" href="<?php echo base_url('lib/css/goadmin/new-themes/new-themes.css');?>" />
		<link rel="stylesheet" type="text/css" href="<?php echo base_url('lib/css/goadmin/new-themes/new-custom.css');?>" />
	<?php } else { ?>
		<link rel="stylesheet" type="text/css" href="<?php echo base_url('lib/css/goadmin/new-themes/login.css');?>" />
	<?php } ?>
	
	<script type="text/javascript">
		var base_url = '<?php echo base_url(); ?>';
		var base_folder = '<?php echo $this->config->config['folder_find']; ?>';
		<?php /*var csrf_name = '<?php echo $this->security->get_csrf_token_name(); ?>';
		var csrf_value = '<?php echo $this->security->get_csrf_hash(); ?>';*/ ?>
	</script>
	<script type="text/javascript" src="<?php echo base_url('lib/js/goadmin/plugins/jquery-3.2.1.min.js');?>"></script>
	<?php if($this->uri->rsegment(1) != 'login' || $this->session->userdata('admin_login') != ''){ ?>
		<script type="text/javascript" src="<?php echo base_url('lib/js/goadmin/plugins/jquery-ui.min.js');?>"></script>
		<script type="text/javascript" src="<?php echo base_url('lib/js/goadmin/plugins/select2/jquerySelect2.min.js');?>"></script>
	<?php } ?>
	<script type="text/javascript" src="<?php echo base_url('lib/js/goadmin/goadmin.js');?>"></script>
	<?php foreach ($js as $script) echo '<script type="text/javascript" src="' . base_url() . 'lib/js/goadmin/' . $script . '.js"></script>', "\n"; ?>

</head>
<body<?php echo ($this->uri->rsegment(1) != 'login' || $this->session->userdata('admin_login') != '')? ' class="home layout layout-header-fixed layout-left-sidebar-fixed"' : '';?>>
	
	<?php if($this->uri->rsegment(1) != 'login' || $this->session->userdata('admin_login') != ''){?>
		<div class="site-overlay"></div>
		
		<!-- HEADER -->
		<div class="site-header">
			<div class="headercolor-gositus"></div>
			<nav class="navbar navbar-default">
				<div class="navbar-header">
					<a class="navbar-brand" href="<?php echo site_url('goadmin');?>">
						<img src="<?php echo base_url('lib/assets/goadmin/gositus-logo.png');?>" alt="" height="30">
						<span>gositus</span>
					</a>
					<button class="navbar-toggler left-sidebar-toggle pull-left visible-xs" type="button">
						<span class="hamburger"></span>
					</button>
					<button class="navbar-toggler pull-right visible-xs-block" type="button" data-toggle="collapse" data-target="#navbar">
						<span class="more"></span>
					</button>
				</div>
				<div class="navbar-collapsible">
					<div id="navbar" class="navbar-collapse collapse">
						<button class="navbar-toggler left-sidebar-collapse pull-left hidden-xs" type="button">
							<span class="hamburger"></span>
						</button>
						<ul class="nav navbar-nav">
							<li class="visible-xs-block">
								<div class="nav-avatar">
									<img class="img-circle" src="<?php echo base_url('lib/assets/goadmin/gositus-logo.png');?>" alt="" width="48" height="48">
								</div>
								<h4 class="navbar-text text-center">Welcome, <?php echo $this->session->userdata('admin_name'); ?>!</h4>
							</li>
						</ul>
						
						<ul class="nav navbar-nav navbar-right">
							<li class="nav-table dropdown visible-xs-block">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
									<span class="nav-cell nav-icon">
										<i class="zmdi zmdi-account-o"></i>
									</span>
									<span class="hidden-md-up m-l-5">Account</span>
								</a>
								<ul class="dropdown-menu">
									<li><a href="<?php echo base_url('goadmin/admin/view/'. $this->session->userdata('admin_id')); ?>">Profile</a></li>
									<li role="separator" class="divider"></li>
									<li><a href="<?php echo base_url('goadmin/logout'); ?>">Logout</a></li>
								</ul>
							</li>
							<li class="nav-table dropdown">
								<a href="javascript:void();">
									<span class="nav-cell nav-icon wrap-icon-front">
										<i class="zmdi zmdi-dns"></i>
										<span class="font-10 m-l-5"><?php echo $this->input->ip_address(); ?></span>
									</span>
								</a>
							</li>
							<li class="nav-table dropdown">
								<a href="<?php echo base_url();?>" target="_blank">
									<span class="nav-cell nav-icon wrap-icon-front">
										<i class="zmdi zmdi-globe"></i>
										<span class="font-10 m-l-5"><?php echo site_url('/');?></span>
									</span>
								</a>
							</li>
							<?php
						if(check_access('message', 'read')){

							 $message = get_filtered_message("",2);
							 $unread = count($message) ;
							  ?>
							
							<li class="nav-table dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
									<span class="nav-cell nav-icon">
										<i class="zmdi zmdi-email-open"></i>
									</span>
									<span class="hidden-md-up m-l-5">Messages</span>
									<?php if($unread > 0){?> 
									<span class="label label-warning"><?php echo $unread; ?></span>
									<?php } ?>
								</a>
								<div class="dropdown-menu custom-dropdown dropdown-messages dropdown-menu-right">
									<div class="dropdown-header">
										<span>Recent messages</span>
									</div>
									<div class="m-items">
										<div class="custom-scrollbar">
											<?php if(count($message) == 0){?>
											<div>
												<p  class="no-message">No New Message</p>
											</div>

											<?php }?>
										<?php foreach ($message as $key => $value) { ?>
											<div class="m-item">
												<a href="<?php echo base_url('goadmin/message/view/' . $value['id']);?>">
													<div class="mi-icon bg-primary">
														<i class="zmdi zmdi-email"></i>
													</div>
													<div class="mi-time"><?php echo date2str($value['date'] ); ?></div>
													<div class="mi-title"><?php echo $value['subject']; ?></div>
													<div class="mi-text text-truncate"><?php echo substr($value['content'],0,30).' ...'; ?></div>
												</a>
											</div>
										
										<?php } ?>
											
											
										</div>
									</div>
									<div class="dropdown-footer">
										<a href="<?php echo base_url('goadmin/message'); ?>">View all messages</a>
									</div>
								</div>
							</li>
							<?php } ?>
							<li class="nav-table dropdown hidden-sm-down">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
									<span class="nav-cell p-r-10">
										<img src="<?php echo base_url('lib/assets/goadmin/gositus-logo.png');?>" alt="" width="30" height="30">
									</span>
									<span class="nav-cell"><?php echo $this->session->userdata('admin_name'); ?>
										<span class="caret"></span>
									</span>
								</a>
								<ul class="dropdown-menu">
									<li>
										<a href="<?php echo base_url('goadmin/admin/view/'.$this->session->userdata('admin_id')); ?>">
											<i class="zmdi zmdi-account-o m-r-10"></i> Profile
										</a>
									</li>
									<li role="separator" class="divider"></li>
									<li>
										<a href="<?php echo base_url('goadmin/logout'); ?>">
											<i class="zmdi zmdi-power m-r-10"></i> Logout
										</a>
									</li>
								</ul>
							</li>
						</ul>
					</div>
				</div>
			</nav>
		</div>
		<!-- END HEADER -->

		<!-- MAIN -->
		<div class="site-main">
			
	<?php } ?>