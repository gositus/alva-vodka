<div class="panel panel-default">
    <div class="panel-heading">
        <h4 class="m-y-0 font-bold">Status</h4>
    </div>
<?php if($this->uri->segments[2] =='newsletter'){
    $active_text = "Subscribe";
} elseif ($this->uri->segments[2] =='message') {
    $active_text ="Read";
} else{
    $active_text = "Active";
} ?>

<?php if($this->uri->segments[2] =='newsletter'){
    $inactive_text = "Unsubscribe";
} elseif ($this->uri->segments[2] =='message') {
    $inactive_text ="Unread";
} else{
    $inactive_text = "Inactive";
} ?>
    <div class="panel-body">
        <div class="form-horizontal">
            <div class="form-group text-center wrap-status">
                <div class="col-md-12">
                    <div class="btn-group" data-toggle="buttons">
                        <label class="btn btn-outline-success m-w-120 m-y-5 active">
                            <input type="radio" name="flag" value="1" checked="checked"><?php echo $active_text; ?>
                        </label>
                        <label class="btn btn-outline-danger m-w-120 m-y-5">
                            <input type="radio" name="flag" value="2"><?php echo $inactive_text; ?>
                        </label>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-12">Memo</label>
                <div class="col-md-12">
                    <textarea class="form-control" id="flag_memo" name="flag_memo" data-plugin="autosize"></textarea>
                </div>
            </div>
        </div>
    </div>
</div>