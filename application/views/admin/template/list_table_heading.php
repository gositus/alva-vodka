<th width="50"><?php echo ($this->uri->segments[2] =='newsletter')? '(Un)Subscribe':'Status'; ?></th>
<th width="100">Memo</th>
<?php /* if($this->uri->rsegments[1]!= "master"){ ?><th width="50">Action</th><?php } else{ echo '<th></th>'; } */?>
<?php if (check_access($this->url, 'delete'))  : ?>
<th width="30" align="center">
	Delete<br>
	<div class="custom-control-stacked text-center wrap-check-all">
		<label class="custom-control custom-control-primary custom-checkbox">
			<input type="checkbox" class="deletechecked custom-control-input delete-check-all" name="select_all">
			<span class="custom-control-indicator"></span>
		</label>
	</div>
</th>
<?php endif; ?>