<!-- BREADCRUMBS -->
<?php 
if(!empty($row)){
    if(!empty($row['id'])){
        $id_edit= $row['id'];
    }
    else if(!empty($row[$default]['id'])){
        $id_edit= $row[$default]['id'];
    }
}
?>
<ol class="breadcrumb">
    <li>
        <a href="<?php echo site_url('goadmin');?>">
            <i class="zmdi zmdi-home"></i>
        </a>
    </li>
    <li><a href="<?php echo site_url('goadmin/'.$this->url);?>"><?php echo $title; ?></a></li>
    <li class="active"><?php echo ucwords($type) .' '. $title;?></li>
</ol>
<!-- END BREADCRUMBS -->
<?php if($this->uri->rsegments[1]!= "master" || $this->uri->rsegments[2]== "doku" || $this->uri->rsegments[2]== "midtrans" || $this->uri->rsegments[2]== "banned_ip") { ?>
<div class="panel panel-default">
    <div class="panel-heading">
        <?php if ($type == 'view'){ ?>
        <div class="row">
            <div class="col-md-4">
                <h4 class="m-y-0 font-bold"><?php echo $title; ?> Action</h4>
            </div>
            <div class="col-md-8 text-right">
                <?php if($this->url == 'message'){ ?>
                    <h4 class="m-y-0 font-bold">Message from: <?php echo $row['name']; ?></h4>
                <?php } else { ?>
                    <h4 class="m-y-0 font-bold">Edit <?php echo $title, ': ', $name; ?></h4>
                <?php } ?>
            </div>
        </div>
        <?php } else { ?>
            <h4 class="m-y-0 font-bold"><?php echo $title; ?> Action</h4>
        <?php } ?>
    </div>
    <div class="panel-body">
        <div class="heading">
            <div class="row">

            	<?php if ($type == 'list') : ?>


                    <div class="wrap-button-action">
                        <?php if (check_access($this->url, 'delete'))  : ?>
                        <div class="col-md-6 col-sm-8 col-xs-12">
                            <div class="wrap-back-reset ">
                                <a id="delcheck" class="input-submit btn btn-warning m-w-120 m-y-5" href="javascript:void(0);"><i class="zmdi zmdi-delete zmdi-hc-lg zmdi-hc-fw"></i>&nbsp;Delete Selected</a>
                            </div>
                        </div>
                        <?php endif; ?>
                        <div class="col-md-6 col-sm-4 col-xs-12">
                            <div class="wrap-back-reset right ">
                            <?php if (check_access($this->url, 'add') && $this->uri->segment(2) != 'message' && $this->uri->segment(2) != 'member'&& $this->uri->segment(2) != 'master') : ?>
                                <a class="input-submit btn btn-primary m-y-5" href="<?php echo base_url(), 'goadmin/', str_replace("_", "-", $this->url), '/add' ?>"><i class="zmdi zmdi-plus-square zmdi-hc-lg zmdi-hc-fw"></i>&nbsp;Add <?php echo $title; ?></a>
                            <?php endif; ?>
                            </div>
                        </div>
                        
                    </div>
 

            	<?php elseif ($type == 'add' ) : ?>

                    
                    <div class="wrap-button-action">
                        <div class="col-md-6 col-sm-7 col-xs-12">
                            <div class="wrap-back-reset">
                                <a class="btn btn-warning m-w-120 btn-form-back m-y-5" href="<?php echo site_url('goadmin/'.$this->url);?>"><i class="zmdi zmdi-arrow-left zmdi-hc-lg zmdi-hc-fw"></i>&nbsp;Back</a>
                                <button type="reset" class="btn btn-warning m-w-120 m-y-5"><i class="zmdi zmdi-rotate-left zmdi-hc-lg zmdi-hc-fw"></i>&nbsp;Reset</button>
                            </div>
                        </div>

                        <div class="col-md-6 col-sm-5 col-xs-12">
                            <div class="wrap-add">
                                <button type="submit" class="btn btn-primary m-y-5"><i class="zmdi zmdi-plus-square zmdi-hc-lg zmdi-hc-fw"></i>&nbsp;Save <?php echo str_replace("Add ", "", $title) ; ?></button>
                            </div>
                        </div>
                    </div>
                
                    
                <?php elseif ($type == 'view') : ?>

                
                    <div class="wrap-button-action">
                        <div class="col-md-6 col-sm-7 col-xs-12">
                            <div class="wrap-back-reset">
                                <a href="<?php echo site_url('goadmin/'.$this->url);?>" class="btn btn-warning m-w-120 back m-y-5"><i class="zmdi zmdi-arrow-left zmdi-hc-lg zmdi-hc-fw"></i>&nbsp;Back</a>
                                <button type="reset" class="btn btn-warning m-w-120 m-y-5"><i class="zmdi zmdi-rotate-left zmdi-hc-lg zmdi-hc-fw"></i>&nbsp;Reset</button>
                                <?php if (check_access($this->url, 'edit') && $this->url == 'message') { ?>
                                <a class="btn btn-danger m-w-120 m-y-5" href="#" data-toggle="modal" data-target="#modal-delete-row"><i class="zmdi zmdi-delete zmdi-hc-lg zmdi-hc-fw"></i>&nbsp;Delete</a>
                                <?php } ?>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-5 col-xs-12">
                            <div class="wrap-add">
                                <?php if (check_access($this->url, 'edit') || $this->session->userdata('admin_id') == $id_edit && $this->url=="admin") : ?>
                                    <?php if ($this->url == 'message') : ?>
                                        <?php if ($row['replied'] == 0) : ?>
                                            <button type="submit" class="btn btn-primary m-w-120 m-y-5"><i class="zmdi zmdi-mail-reply zmdi-hc-lg zmdi-hc-fw"></i>&nbsp;Reply Message</button>
                                        <?php endif; ?>
                                    <?php else : ?>
                                        <button type="submit" class="btn btn-primary m-w-120 m-y-5"><i class="zmdi zmdi-edit zmdi-hc-lg zmdi-hc-fw"></i>&nbsp;Update <?php echo $title; ?></button>
                                    <?php endif; ?>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                <?php endif; ?>

            </div>
        </div>
    </div>
</div>
<?php } ?>
<?php 
    if ($type == 'add' || $type == 'view'){
        echo language_bar();

        // $cookie_name = "coo";
        // $cookie_value = "kie";
        // setcookie($cookie_name, $cookie_value, time() + 900, '/'); 
    } 
    // else{
    //     delete_coo_kie();
        
    // }
?>