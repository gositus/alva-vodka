<!-- LEFT SIDEBAR -->
<div class="site-left-sidebar">
    <div class="sidebar-backdrop"></div>
    <div class="custom-scrollbar">
        <ul class="sidebar-menu">
            <?php echo show_menu();?>
        </ul>
    </div>
</div>
<!-- END LEFT SIDEBAR -->

<!-- CONTENT -->
<div class="site-content">
	<div class="tagline"><h1>gositus</h1></div>