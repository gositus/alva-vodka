<?php if($this->session->flashdata('success')) { ?>
	<script>
		$(function(){
			showToast('success', 'bottom', "<?php echo $this->session->flashdata('success');?>");
		})
	</script>
<?php } ?>
<div class="row">
	<div class="col-md-12">
		<?php $this->load->view('admin/template/fixed_heading', array('type' => 'list')); ?>
	</div>
       <div class="col-md-12">	
    	<div class="panel panel-default">
            <div class="panel-body">
                <div class="col-md-1"><h4 class="m-y-0 font-bold">Filter</h4>
                	<form action="<?php echo base_url('goadmin/newsletter/export'); ?>" method="post">
                </div>
                <div class="col-md-3">
	                <select name="subscribe" class="form-control">
	                	<option value="">-- ALL --</option>
	                	<option value="1">Subscribed</option>
	                	<option value="2">Unsubscribed</option>
	                </select>
                </div>
                <div class="col-md-3">
                	<button type="submit" class="input-submit btn btn-primary">Export</button>
                	</form>
                </div>
            </div>   	
        </div>
    </div>
    <div class="col-md-12">	
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="m-y-0 font-bold">List <?php echo $title; ?></h4>
                <div class="error-input"><?php echo validation_errors();?></div>
            </div>
            <div class="panel-body">
                <div class="form-horizontal">
                	<div class="table-responsive">
						
						<table id="list-table" data-table-name="<?php echo $url; ?>" class="table table-striped dataTable">
							<thead>
								<tr>
									<th>No</th>
									<th>Email</th>
									<th>Status</th>
								
									<?php $this->load->view('admin/template/list_table_heading')?>
								</tr>
							</thead>
							<tbody></tbody>
						</table>
                	</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php 
	//SHOW DATA TO DATATABLE
	// ====================================================================================
	$showData = array(

		//NAMA FIELD BERDASARKAN ATTRIBUTE YG ADA PADA TABLE
		'field' => array(
			'email',
			'flag',
		
		),

		// ================================================================================
		// CUSTOM CONTENT (KOSONGKAN BILA TIDAK DIPAKAI)
		// ROW = HASIL OBJECT DARI AJAX 
		// (CONTOH: UNTUK MENGAMBIL VALUE DARI TABLE (row.icon))
		// CONTOH PENGGUNAAN : 'icon'  => '<img src="\'+base_url+\'lib/images/\'+row.icon+\'"/>',
		// row.icon = "icon" NAMA ATTRIBUTE TABEL. YG DIRUBAH HANYALAH ATTRIBUTE TABLE!!
		// ================================================================================
		'custom_content' => array(
			'flag' => 'if(row.flag==1) return "Subscribe";
			else if(row.flag==2) return "Unsubscribe"; else return"";',
			
		)
	);
?>
<?php $this->load->view('admin/template/list_table_data', $showData); ?>