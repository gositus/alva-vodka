<!DOCTYPE html>
<html>
<head>
	<title>Export Newsletter</title>
</head>
<body>
	<h1><?php echo $title; ?></h1>
	<table border="1">
		<thead >
			<tr style="color:white; background: <?php echo setting_value("corporate_color"); ?>">
				<th>No</th>
				<th>EMAIL</th>
				<th>STATUS</th>
				<th>DATE</th>
				
			</tr>
		</thead>
		<tbody>
			<?php foreach ($detail as $key => $value) { ?>
			<tr>
				<td><?php echo ($key+1); ?></td>
				<td><?php echo $value['email']; ?></td>	
				<td><?php echo ($value['flag']==1)?"Subscribe":"Unsubscribe"; ?></td>
				<td><?php echo format_date($value['date_added']); ?></td>	
			</tr>
			<?php } ?>
		</tbody>
	</table>
</body>
</html>