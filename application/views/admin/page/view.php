 <div class="row">
    <form method="post" id="content-form" action="<?php echo base_url('goadmin/'.$this->url.'/view/'.$row[$default]['id'])?>" data-toggle="validator" enctype="multipart/form-data">
        <div class="col-md-12">
            <?php $this->load->view('admin/template/fixed_heading', array('type' => 'view', 'name' => $row[$default]['name'])); ?>
        </div>
        <input type="hidden" name="id" id="row_id" data-table-name="<?php echo $this->url;?>" value="<?php echo $row[$default]['id'] ?>">

        
        <?php if(validation_errors()){ ?>
            <!-- SHOW ERROR -->
            <?php echo viewErrorValidation();?>
            <!-- END SHOW ERROR -->
        <?php } ?>

        
                <div class="col-md-8 col-sm-12 col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="m-y-0 font-bold">Information</h4>
                </div>


                <div class="panel-body">
                    <div class="form-horizontal">


                        <?php $x = 0; foreach (language()->result_array() as $lang) : ?>
                        <div class="language lang-<?php echo $lang['attr']?>" <?php if ($x == 0) echo 'style="display:block"'; ?>>
                            <div class="form-group">
                                <label for="name_<?php echo $lang['id']?>" class="control-label col-md-3">Name </label>
                                <div class="col-md-7">
                                    <input type="text" name="name_<?php echo $lang['id']?>" id="name_<?php echo $lang['id']?>" class="form-control required" required value="<?php echo $row[$lang['id']]['name'];?>">
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="heading_<?php echo $lang['id']?>" class="control-label col-md-3">Heading </label>
                                <div class="col-md-7">
                                    <input type="text" name="heading_<?php echo $lang['id']?>" id="heading_<?php echo $lang['id']?>" class="form-control " value="<?php echo $row[$lang['id']]['heading'];?>" >
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            
                        </div>
                        <?php $x++; endforeach; ?>

                        <div class="form-group">
                            <label for="section" class="control-label col-md-3">Section</label>
                            <div class="col-md-7">
                                <select name="section" id="section" class="form-control required" required>
                                    <option value="">--</option>
                                    <?php foreach ($section as $key => $value) {?>
                                    <option value="<?php echo $value['id'] ;?>" <?php echo ($value['id']==$row[$default]['section_id'])? 'selected ' : '' ?> ><?php echo $value['name'] ?></option>
                                    <?php } ?>
                                </select>
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>

                        <div class="form-group upload-image">
                            <label class="control-label col-md-3">Image</label>
                            <div class="col-md-7">
                                <div class="row">
                                    <div class="col-md-4 col-sm-12 col-xs-12">
                                        <label class="btn btn-default file-upload-btn">
                                            Choose file...
                                            <button class="file-upload-input" name="icon" id="ckfinder-upload" type="button"></button>
                                        </label>
                                    </div>
                                    <div class="col-md-8 col-sm-12 col-xs-12">
                                        <div class="wrap-info-path">
                                            <input type="file" name="image" style="display: none;">
                                            <input type="text" class="info-path-image show form-control" readonly id="finder-image" name="image" value="<?php echo  $row[$default]['image']; ?>">
                                        </div>
                                    </div>
                                </div>
                                <p class="help-block">
                                    <small>Recommended size <?php echo $this->image_width . ' * ' . $this->image_height ; ?> px</small>
                                </p>
                                 <div class="row">
                                    <div class="col-md-9">
                                        <div class="wrap-preview-image <?php echo ($row[$default]['image']) ? 'show' : ''; ?>">
                                            <img src="<?php echo base_url('lib/images/page/') . $row[$default]['image']; ?>" class="upload-preview">
                                        </div>      
                                    </div>
                                    <div class="col-md-3">
                                        <div class="wrap-delete-image <?php echo ($row[$default]['image']) ? 'show' : ''; ?>">
                                            <button type="button" class="btn btn-danger btn-delete-img"><i class="zmdi zmdi-close"></i></button>
                                        </div>  
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4 col-sm-12 col-xs-12">
            <?php $this->load->view('admin/template/view_flag');?>
        </div>
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="m-y-0 font-bold">Content</h4>
                </div>
                <div class="panel-body">
                    <div class="form-horizontal">        
                    <?php $x = 0; foreach (language()->result_array() as $lang) : ?>
                    <div class="language lang-<?php echo $lang['attr']?>" <?php if ($x == 0) echo 'style="display:block"'; ?>>
                        <div class="form-group">
                            <div class="col-md-12">
                                <textarea class="form-control content" name="content_<?php echo $lang['id']?>"  rows="10" id="content_<?php echo $lang['id']?>"><?php echo $row[$lang['id']]['content'] ;?> </textarea>
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                    </div>
                    <?php $x++;  endforeach; ?>

                        <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" id="csrf-token">
                    </div>
                </div>
            </div>
        </div>
    </form>
    <?php $this->load->view('admin/template/log'); ?>
</div>

