<div class="row">
    <form method="post" id="content-form" action="<?php echo base_url('goadmin/'.$this->url.'/add')?>" data-toggle="validator" enctype="multipart/form-data">
        
        <div class="col-md-12">
            <?php $this->load->view('admin/template/fixed_heading', array('type' => 'add')); ?>
        </div>
        
        <?php if(validation_errors()){ ?>
            <!-- SHOW ERROR -->
            <?php echo viewErrorValidation();?>
            <!-- END SHOW ERROR -->
        <?php } ?>

        <div class="col-md-8 col-sm-12 col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="m-y-0 font-bold">Information</h4>
                </div>


                <div class="panel-body">
                    <div class="form-horizontal">
                        <div class="form-group">
                            <label for="m_name" class="control-label col-md-3">Name</label>
                            <div class="col-md-7">
                                <input type="text" name="m_name" id="m_name" class="form-control required" required>
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label for="m_name" class="control-label col-md-3">Parent</label>
                            <div class="col-md-7">
                                <select name="m_parent" id="m_parent" class="form-control required" required>
                                    <!-- <option value="">--</option> -->
                                    <option value="0">(Parent Module)</option>
                                    <?php foreach ($modules as $key => $value) { ?>
                                        <option value="<?php echo $value['id'] ?>" <?php if($value['parent']==0) echo 'class=show-icon'; ?>><?php echo ($value['parent']==0)? $value['name'] :" -" . $value['name']?></option>
                                    <?php } ?>
                                </select>
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="m_url" class="control-label col-md-3">URL</label>
                            <div class="col-md-7">
                                <input type="text" name="m_url" id="m_url" class="form-control required">
                                <div class="help-block">
                                    <p>Note: URL yang ditujukan / Nama Controller</p>
                                </div>
                            </div>
                        </div>

                        <div class="form-group form-icon">
                            <label for="m_icon" class="control-label col-md-3">Icon</label>
                            <div class="col-md-7">
                                <div class="row">
                                    <div class="col-md-8">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class=""></i></span>
                                            <input type="text" readonly name="m_icon" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-md-4 text-right">
                                        <a href="#" class="show-icon btn btn-default" data-toggle="modal" data-target="#list-icon">Show Icon</a>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="m_notes" class="control-label col-md-3">Notes</label>
                            <div class="col-md-7">
                                <textarea name="m_notes" id="m_notes" class="form-control"></textarea>
                            </div>
                        </div>

                        <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" id="csrf-token">
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4 col-sm-12 col-xs-12">
            <?php $this->load->view('admin/template/add_flag');?>
        </div>
    </form>
</div>

<?php $this->load->view('admin/module/list_icon');?>

<script type="text/javascript">
    $(".form-icon").hide();
    $("#m_parent").on('change',function(){
       var icon_parent = $('select[name="m_parent"] :selected').attr('class'); 
       if(icon_parent=='show-icon'){
            $(".form-icon").show();
       } else {
            $(".form-icon").hide();
       }
       var values = $('select[name="m_parent"] :selected').val(); 
       if(values == 0){
        $("#m_url").val('javascript:;');
       } else {
        $("#m_url").val('');
       }
    });
</script>

