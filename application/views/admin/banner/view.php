 <div class="row">
    <form method="post" id="content-form" action="<?php echo base_url('goadmin/'.$this->url.'/view/'.$row[$default]['id'])?>" data-toggle="validator" enctype="multipart/form-data">
        <div class="col-md-12">
            <?php $this->load->view('admin/template/fixed_heading', array('type' => 'view', 'name' => $row[$default]['name'])); ?>
        </div>
        <input type="hidden" name="id" id="row_id" data-table-name="<?php echo $this->url;?>" value="<?php echo $row[$default]['id'] ?>">

        
        <?php if(validation_errors()){ ?>
            <!-- SHOW ERROR -->
            <?php echo viewErrorValidation();?>
            <!-- END SHOW ERROR -->
        <?php } ?>

        
        <div class="col-md-8 col-sm-12 col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="m-y-0 font-bold">Information</h4>
                </div>


                <div class="panel-body">
                    <div class="form-horizontal">
                        <div class="form-group">
                            <label for="name" class="control-label col-md-3">Name</label>
                            <div class="col-md-7">
                                <input type="text" name="name" id="name" class="form-control required" required value="<?php echo $row[$default]['name']; ?>">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="sort" class="control-label col-md-3">Sort</label>
                            <div class="col-md-7">
                                <input type="number" name="sort" id="sort" class="number form-control"  value="<?php echo $row[$default]['sort']; ?>">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>

                        <?php $x = 0; foreach (language()->result_array() as $lang) : ?>
                        <div class="language lang-<?php echo $lang['attr']?>" <?php if ($x == 0) echo 'style="display:block"'; ?>>
                            <div class="form-group">
                                <label for="caption_<?php echo $lang['id']?>" class="control-label col-md-3">Caption </label>
                                <div class="col-md-7">
                                    <textarea name="caption_<?php echo $lang['id']?>" id="caption_<?php echo $lang['id']?>" class="form-control" ><?php echo $row[$lang['id']]['caption']; ?></textarea>
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            
                        </div>
                        <?php $x++; endforeach; ?>

                        <div class="form-group">
                            <label for="url" class="control-label col-md-3">URL</label>
                            <div class="col-md-7">
                                <input type="text" name="url" id="url" class="url form-control" value="<?php echo $row[$default]['url']; ?>">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>

                        <div class="form-group" <?php echo (count($section) > 1)? '' :'style="display:none"'; ?>>
                            <label for="section" class="control-label col-md-3">Section</label>
                            <div class="col-md-7">
                                <select name="section" id="banner_section" class="form-control required" required>
                                    <option value="">--</option>
                                    <?php foreach ($section as $key => $value) {
                                        $size= explode(";",$value['banner_size']);?>

                                    <option value="<?php echo $value['id'] ?>" size-width="<?php echo $size[0]; ?>" size-height="<?php echo $size[1]; ?>" 
                                        <?php echo ($value['id']==$row[$default]['section_id'])? 'selected': ''; ?>
                                        ><?php echo $value['name'] ?></option>
                                    <?php } ?>
                                </select>
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                         <div class="form-group">
                            <label for="start" class="control-label col-md-3">Start Date</label>
                            <div class="col-md-7">
                                <input type="text" name="start" class="required form-control datepicker-start" id="start" <?php if($row[$default]['start'] != '0000-00-00 00:00:00'){?> value="<?php echo format_date($row[$default]['start']);  ?>" <?php } ?> readonly>
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="end" class="control-label col-md-3">End Date</label>
                            <div class="col-md-7">
                                <input type="checkbox" name="enddate" id="check-end" <?php if($row[$default]['end'] != '0000-00-00 00:00:00'){ echo "checked";}?>>
                                <input type="text" name="end" class=" form-control datepicker-end  banner-end" id="end" <?php if($row[$default]['end'] != '0000-00-00 00:00:00'){?> value="<?php echo format_date($row[$default]['end']);    ?>" <?php }?> readonly>
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>

                        <div class="form-group upload-image">
                            <label class="control-label col-md-3">Image</label>
                            <div class="col-md-7">
                                <div class="row">
                                    <div class="col-md-4 col-sm-12 col-xs-12">
                                        <label class="btn btn-default file-upload-btn">
                                            Choose file...
                                            <button class="file-upload-input" name="icon" id="ckfinder-upload" type="button"></button>
                                        </label>
                                    </div>
                                    <div class="col-md-8 col-sm-12 col-xs-12">
                                        <div class="wrap-info-path">
                                            <input type="file" name="image" style="display: none;">
                                            <input type="text" class="info-path-image show form-control" readonly id="finder-image" name="image" value="<?php echo  $row[$default]['image']; ?>">
                                        </div>
                                    </div>
                                </div>
                                <p class="help-block">
                                    <small>Recommended size <?php echo $size[0] . ' * ' . $size[1] ; ?> px</small>
                                </p>
                                 <div class="row">
                                    <div class="col-md-9">
                                        <div class="wrap-preview-image <?php echo ($row[$default]['image']) ? 'show' : ''; ?>">
                                            <img src="<?php echo base_url('lib/images/banner/thumb/') . $row[$default]['image']; ?>" class="upload-preview">
                                        </div>      
                                    </div>
                                    <div class="col-md-3">
                                        <div class="wrap-delete-image <?php echo ($row[$default]['image']) ? 'show' : ''; ?>">
                                            <button type="button" class="btn btn-danger btn-delete-img"><i class="zmdi zmdi-close"></i></button>
                                        </div>  
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4 col-sm-12 col-xs-12">
            <?php $this->load->view('admin/template/view_flag');?>
        </div>
        <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" id="csrf-token">
    </form>
    <?php $this->load->view('admin/template/log'); ?>
</div>

