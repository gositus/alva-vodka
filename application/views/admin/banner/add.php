<div class="row">
    <form method="post" id="content-form" action="<?php echo base_url('goadmin/'.$this->url.'/add')?>" data-toggle="validator" enctype="multipart/form-data">
        
        <div class="col-md-12">
            <?php $this->load->view('admin/template/fixed_heading', array('type' => 'add')); ?>
        </div>
        
        <?php if(validation_errors()){ ?>
            <!-- SHOW ERROR -->
            <?php echo viewErrorValidation();?>
            <!-- END SHOW ERROR -->
        <?php } ?>

        <div class="col-md-8 col-sm-12 col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="m-y-0 font-bold">Information</h4>
                </div>


                <div class="panel-body">
                    <div class="form-horizontal">
                        <div class="form-group">
                            <label for="name" class="control-label col-md-3">Name</label>
                            <div class="col-md-7">
                                <input type="text" name="name" id="name" class="form-control required" required>
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="sort" class="control-label col-md-3">Sort</label>
                            <div class="col-md-7">
                                <input type="number" name="sort" id="sort" class="number form-control" >
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>

                        <?php $x = 0; foreach (language()->result_array() as $lang) : ?>
                        <div class="language lang-<?php echo $lang['attr']?>" <?php if ($x == 0) echo ' style="display:block"'; ?> >
                            <div class="form-group">
                                <label for="caption_<?php echo $lang['id']?>" class="control-label col-md-3">Caption </label>
                                <div class="col-md-7">
                                    <textarea name="caption_<?php echo $lang['id']?>" id="caption_<?php echo $lang['id']?>" class="form-control" ></textarea>
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            
                        </div>
                        <?php $x++; endforeach; ?>

                        <div class="form-group">
                            <label for="url" class="control-label col-md-3">URL</label>
                            <div class="col-md-7">
                                <input type="text" name="url" id="url" class="url form-control">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>

                        <div class="form-group" <?php echo (count($section) > 1)? '' :'style="display:none"'; ?>>
                            <label for="section" class="control-label col-md-3">Section</label>
                            <div class="col-md-7">
                                <select name="section" id="banner_section" class="form-control required" required>
                                    <?php echo (count($section) > 1)? '<option value="">--</option>' : ''; ?>
                                    <?php foreach ($section as $key => $value) {
                                        $size= explode(";",$value['banner_size']);?>

                                    <option value="<?php echo $value['id'] ?>" size-width="<?php echo $size[0]; ?>" size-height="<?php echo $size[1]; ?>"><?php echo $value['name'] ?></option>
                                    <?php } ?>
                                </select>
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="start" class="control-label col-md-3">Start Date</label>
                            <div class="col-md-7">
                                <input type="text" name="start" class="required form-control datepicker-start" id="start" readonly>
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="end" class="control-label col-md-3">End Date</label>
                            <div class="col-md-7">
                                <input type="checkbox" name="enddate" id="check-end">
                                <input type="text" name="end" class="gohide form-control datepicker-end banner-end" id="end" readonly>
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>

                        <div class="form-group upload-image">
                            <label class="control-label col-md-3">Image</label>
                            <div class="col-md-7">
                                <div class="row">
                                    <div class="col-md-4 col-sm-12 col-xs-12">
                                        <label class="btn btn-default file-upload-btn">
                                            Choose file...
                                            <button class="file-upload-input" name="icon" id="ckfinder-upload" type="button"></button>
                                        </label>
                                    </div>
                                    <div class="col-md-8 col-sm-12 col-xs-12">
                                        <div class="wrap-info-path">
                                            <input type="file" name="image" style="display: none;">
                                            <input type="text" class="info-path-image  show form-control"  id="finder-image" name="image" readonly>
                                        </div>
                                    </div>
                                </div>
                                <p class="help-block banner-size-help">
                                    <?php if (count($section) == 1){ ?>
                                    <small>Recommended size <?php echo $size[0] . ' * ' . $size[1] ; ?> px</small>
                                    <?php } ?>
                                </p>
                                <div class="row">
                                    <div class="col-md-9">
                                        <div class="wrap-preview-image show">
                                            <img src="" class="upload-preview">
                                        </div>      
                                    </div>
                                    
                                </div>
                                
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4 col-sm-12 col-xs-12">
            <?php $this->load->view('admin/template/add_flag');?>
             <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" id="csrf-token">
        </div>
    </form>
</div>