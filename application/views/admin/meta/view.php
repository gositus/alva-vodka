 <div class="row">
    <form method="post" id="content-form" action="<?php echo base_url('goadmin/'.$this->url.'/view/'.$row[$default]['id'])?>" data-toggle="validator" enctype="multipart/form-data">
        <div class="col-md-12">
            <?php $this->load->view('admin/template/fixed_heading', array('type' => 'view', 'name' => $row[$default]['name'])); ?>
        </div>
        <input type="hidden" name="id" id="row_id" data-table-name="<?php echo $this->url;?>" value="<?php echo $row[$default]['id'] ?>">

        
        <?php if(validation_errors()){ ?>
            <!-- SHOW ERROR -->
            <?php echo viewErrorValidation();?>
            <!-- END SHOW ERROR -->
        <?php } ?>

        
                <div class="col-md-8 col-sm-12 col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="m-y-0 font-bold">Information</h4>
                </div>


                <div class="panel-body">
                    <div class="form-horizontal">
                        
                            <div class="form-group">
                                <label for="path_url" class="control-label col-md-3">Path URL</label>
                                <div class="col-md-7">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                          <span class="input-group-text" id="basic-addon1"><?php echo base_url(); ?></span>
                                        </div>
                                        <input name="path_url" id="path_url" class="form-control required path_url" required value="<?php echo $row[$default]['path_url'];?>">
                                    </div>
                                    <div class="help-block with-errors"><small>ex: <?php echo base_url(); ?>news/gositus-web-design only input "<strong>news/gositus-web-design</strong>"</small></div>
                                </div>
                            </div>
                        <?php $x = 0; foreach (language()->result_array() as $lang) : ?>
                        <div class="language lang-<?php echo $lang['attr']?>" <?php if ($x == 0) echo 'style="display:block"'; ?>>
                            <div class="form-group">
                                <label for="title_<?php echo $lang['id']?>" class="control-label col-md-3">Meta Title</label>
                                <div class="col-md-7">
                                    <input name="title_<?php echo $lang['id']?>" id="title_<?php echo $lang['id']?>" class="form-control "  value="<?php echo $row[$lang['id']]['title'];?>"> 
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="description_<?php echo $lang['id']?>" class="control-label col-md-3">Meta Description</label>
                                <div class="col-md-7">
                                    <textarea name="description_<?php echo $lang['id']?>" id="description_<?php echo $lang['id']?>" class="form-control "  maxLength="300"><?php echo $row[$lang['id']]['description'];?></textarea>
                                 <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="keyword_<?php echo $lang['id']?>" class="control-label col-md-3">Meta Keywords</label>
                                <div class="col-md-7">
                                    <textarea name="keyword_<?php echo $lang['id']?>" id="keyword_<?php echo $lang['id']?>" class="form-control "  maxLength="300"><?php echo $row[$lang['id']]['keyword'];?></textarea>
                                    <div class="help-block with-errors"><small>Separate with coma (,)</small></div>
                                </div>
                            </div>
                        </div>
                        <?php $x++; endforeach; ?>

                       

                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4 col-sm-12 col-xs-12">
            <?php $this->load->view('admin/template/view_flag');?>
        </div>

    </form>
    <?php $this->load->view('admin/template/log'); ?>
</div>

