 <div class="row">
    <form method="post" id="content-form" action="<?php echo base_url('goadmin/'.$this->url);?>" data-toggle="validator" enctype="multipart/form-data">
        <div class="col-md-12">
            <?php $this->load->view('admin/template/fixed_heading', array('type' => 'view', 'name' => $row['name'])); ?>
        </div>
        <input type="hidden" name="id" id="row_id" data-table-name="setting" value="<?php echo $row['id'] ?>">

        
        <?php if(validation_errors()){ ?>
            <!-- SHOW ERROR -->
            <?php echo viewErrorValidation();?>
            <!-- END SHOW ERROR -->
        <?php } ?>

        
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="m-y-0 font-bold">Information</h4>
                </div>

                <div class="panel-body">
                    <div class="form-horizontal">   
                        <div class="form-group">
                            <label for="default_language" class="control-label col-md-3">Default Language</label>
                            <div class="col-md-7">
                                <select name="default_language" id="default_language" class="required form-control" required>
                                    <option value="">--</option>
                                    <?php foreach ($master_language as $key => $value) {     ?>
                                        <option value="<?php echo $value['id'] ?>" <?php if($value['id']== setting_value('default_language')) echo ' selected'; ?> ><?php echo $value['name'];?></option>
                                    <?php } ?>
                                </select>
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="corporate_color" class="control-label col-md-3">Corporate Color</label>
                            <div class="col-md-7">
                                <input type="text" name="corporate_color" id="corporate_color" class="required form-control jscolor" required maxlength="7" value="<?php echo setting_value('corporate_color');?>">
                                 
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        
                        
                    <?php foreach ($setting as $key => $value) {?>

                    <?php if($value['input']=='field')
                     { ?>
                        <div class="form-group">
                            <label for="<?php echo $value['key'] ?>" class="control-label col-md-3"><?php echo $value['label']; ?></label>
                            <div class="col-md-7">
                                <input type="text" name="<?php echo $value['key'] ?>" id="<?php echo $value['key'] ?>" class="form-control" value="<?php echo $value['value']; ?>" <?php echo ($value['maxlength'] != 0)? 'maxlength="'.$value['maxlength'] .'"': '';  ?>>
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                    <?php } ?>
                    <?php if($value['input']=='textarea')
                     { ?>
                        <div class="form-group">
                            <label for="<?php echo $value['key'] ?>" class="control-label col-md-3"><?php echo $value['label'] ?></label>
                            <div class="col-md-7">
                                <textarea name="<?php echo $value['key'] ?>" id="<?php echo $value['key'] ?>" class=" form-control" <?php echo ($value['maxlength'] != 0)? 'maxlength="'.$value['maxlength'] .'"': '';  ?>><?php echo $value['value'] ?></textarea>
                                <div class="help-block with-errors"></div>
                                
                                <small><?php echo $value['note']; ?></small>

                            </div>
                        </div>
                    <?php } ?>
                    <?php  if($value['input']=='date' && ($this->session->userdata('admin_id') == 1 || getDomain() == "localhost" || getDomain() == "lab.gositus.com")) 
                     { ?>
                        <div class="form-group">
                            <label for="<?php echo $value['key'] ?>" class="control-label col-md-3"><?php echo $value['label']; ?></label>
                            <div class="col-md-7">
                                <input type="text" name="<?php echo $value['key'] ?>" id="<?php echo $value['key'] ?>" class="form-control datepicker-dob" value="<?php echo $value['value']; ?>" <?php echo ($value['maxlength'] != 0)? 'maxlength="'.$value['maxlength'] .'"': '';  ?>>
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                    <?php } ?>

                    <?php } ?>     

                        <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" id="csrf-token">
                    </div>
                </div>
            </div>
        </div>
        
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="m-y-0 font-bold">Logo & Favicon</h4>
                </div>

                <div class="panel-body">
                    <div class="form-horizontal"> 
                    <?php foreach ($setting_file as $key => $value) {?>
                    <?php if($value['input']=='file')
                     { ?>
                        <div class="form-group upload-image view-image show" data-a="<?php echo $value['key']; ?>"> 
                            <label class="control-label col-md-3"><?php echo $value['label'] ?></label>
                            <div class="col-md-7">
                                <?php if($value['key']=='logo') {?>
                                <p class="help-block">
                                    <small>Recommended size <?php echo $this->image_width . ' * ' . $this->image_height ; ?> px</small>
                                </p>
                                <?php } else if($value['key']=='favicon384'){
                                    ?>
                                    <p class="help-block">
                                        <small>SVG only</small>
                                </p>
                                <?php } ?>
                                <div class="row">
                                    <div class="col-md-4 col-sm-12 col-xs-12">
                                        <label class="btn btn-default file-upload-btn">
                                            Choose file...
                                            <input type="file" <?php if($value['key']=='favicon384') echo 'accept=".svg"' ?> class="file-upload-input" name="<?php echo $value['key'] ?>">
                                        </label>
                                    </div>
                                    <div class="col-md-7 col-sm-10 col-xs-10">
                                        <div class="wrap-info-path">
                                            <input type="text" class="info-path-image form-control show" disabled value="<?php echo $value['value']; ?>">
                                        </div>
                                    </div>
                                    <div class="col-md-1">
                                        <div class="wrap-delete-image <?php echo ($value['value']) ? 'show' : ''; ?>">
                                            <button type="button" class="btn btn-danger btn-delete-img"><i class="zmdi zmdi-close"></i></button>
                                        </div>  
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-9">
                                        <div class="wrap-preview-image <?php echo ($value['value']) ? 'show' : ''; ?>">
                                           <?php if($value['key']=='logo') {?>
                                            <img src="<?php echo base_url('lib/assets/logo/'.$value['value']); ?>" class="upload-preview">
                                            <?php } else {?>
                                            <img src="<?php echo base_url('lib/assets/favicon/'.$value['value']); ?>" class="upload-preview">
                                            <?php } ?>
                                        </div>      
                                    </div>
                                    
                                </div>
                            </div>
                         </div>
                        <?php } ?>

                    <?php } ?>     

                        <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" id="csrf-token">
                    </div>
                </div>
            </div>
        </div>
    </form>
    <?php $this->load->view('admin/template/log'); ?>
</div>
