<div id="home-new">     
    <div class="square-green"></div>
    <div class="line-grey"></div>
    <div class="triangle-yellow"></div>
    <div class="circle-blue"></div>
    <div class="wrap-bg-text vertical">         
        <div class="bg-text"><h1>login</h1></div>
    </div>

    <div class="wrap-form-home">
        <div class="container">
            <div class="row inline">
                <div class="col inline verti-middle l-7 m-4 s-12">
                    <div id="home-logo">
                        <img src="<?php echo base_url('lib/images/goadmin/gositus-logo.png');?>" alt="Gositus">
                    </div>
                </div>
                <div class="col inline verti-middle l-5 m-8 s-12">
                    
                    <div id="wrap-login">
                        <h1>
                            <span>Reset Password</span>
                        </h1>

                        <div class="clock">
                            <label id="date"></label>&nbsp;&nbsp;//&nbsp;&nbsp;<label id="jclock"></label>
                        </div>

                        <div>
                            <div id="login-container">
                                <form id="login" method="post">
                                    <p>
                                        <label for="email"></label>
                                        <input type="text" class="input-text required input_field email" name="goemail" id="email" disabled readonly  value="<?php echo $user['email'] ;?>"/>
                                    </p>
                                    <p>
                                        <label for="password">Password</label>
                                        <input type="password" class="input-text required input_field pass_word" name="gopassword" id="password" />
                                        <span class="eyes">
                                            <a href="javascript:;"><i class="fa fa-eye-slash"></i></a>
                                        </span>
                                     </p>
                                     <p>
                                        <label for="repassword">Password confirmation</label>
                                        <input type="password" class="input-text required input_field " name="gorepassword" id="repassword" equalTo="#password"/>
                                        <span class="eyes">
                                            <a href="javascript:;"><i class="fa fa-eye-slash"></i></a>
                                        </span>
                                     </p>

                                     <p>
                                        <button type="submit" name="reset" class="btn-custom btn-scale btn-grey" value="1">Reset</button>
                                     </p>
                                     <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" id="csrf-token">
                                </form>
                            </div>

                            
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

<footer>
    <div class="orbit">
        <div class="orbit-large"></div>
        <div class="orbit-medium"></div>
        <div class="orbit-small"></div>
        <div class="moon"></div>
    </div>
    <div class="wave">
        <div class="wave-grey"></div>   
        <div class="wave-grey-2"></div> 
    </div>
</footer>
<script type="text/javascript">
    $(function(){
         $.validator.addMethod("pass_word", function(value) {
           return /^[A-Za-z0-9\d=!\-@$%#\^._*]*$/.test(value) // consists of only these
               && /[a-z]/.test(value) // has a lowercase letter
               && /\d/.test(value) // has a digit
               && /\W/.test(value) // has a special characters
        },'Password Not Valid');

    })

</script>