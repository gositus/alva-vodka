<div class="row">
    <form method="post" id="content-form" action="<?php echo base_url('goadmin/'.$this->url.'/add')?>" data-toggle="validator" enctype="multipart/form-data">
        
        <div class="col-md-12">
            <?php $this->load->view('admin/template/fixed_heading', array('type' => 'add')); ?>
        </div>
        
        <?php if(validation_errors()){ ?>
            <!-- SHOW ERROR -->
            <?php echo viewErrorValidation();?>
            <!-- END SHOW ERROR -->
        <?php } ?>

        <div class="col-md-8 col-sm-12 col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="m-y-0 font-bold">Information</h4>
                </div>


                <div class="panel-body">
                    <div class="form-horizontal">
                        <div class="form-group">
                            <label for="s_name" class="control-label col-md-3">Name</label>
                            <div class="col-md-7">
                                <input type="text" name="s_name" id="s_name" class="form-control required" required>
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="s_type" class="control-label col-md-3">Type</label>
                            <div class="col-md-7">
                                <select name="s_type" id="s_type" class="form-control required" required>
                                    <option value="">--</option>
                                    <option value="1">Article</option>
                                    <option value="2">Banner</option>
                                </select>
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="form-group banner-section">
                            <label for="s_width" class="control-label col-md-3">Banner Width</label>
                            <div class="col-md-7">
                                <input type="text" name="s_width" id="s_width" class="form-control number" placeholder="(px)">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="form-group banner-section">
                            <label for="s_height" class="control-label col-md-3">Banner Height</label>
                            <div class="col-md-7">
                                <input type="text" name="s_height" id="s_height" class="form-control number" placeholder="(px)">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>


                        <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" id="csrf-token">
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4 col-sm-12 col-xs-12">
            <?php $this->load->view('admin/template/add_flag');?>
        </div>
    </form>
</div>

<script type="text/javascript">
    $(".banner-section").hide();
    $("#s_type").on('change',function(){
       var icon_parent = $('select[name="s_type"] :selected').val(); 
       if(icon_parent==2){
            $(".banner-section").show();
            $(".banner-section").find('input').addClass("required");
       } else {
            $(".banner-section").hide();
            $(".banner-section").find('input').removeClass("required");

       }
    });
</script>

