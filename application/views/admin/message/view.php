 <?php if($this->session->flashdata('success')) { ?>
    <script>
        $(function(){
            showToast('success', 'bottom', "<?php echo $this->session->flashdata('success');?>");
        })
    </script>
<?php } ?>
 <div class="row">
    <form method="post" id="content-form" action="<?php echo base_url('goadmin/'.$this->url.'/view/'.$row['id'])?>" data-toggle="validator" enctype="multipart/form-data">
        <div class="col-md-12">
            <?php $this->load->view('admin/template/fixed_heading', array('type' => 'view', 'name' => $row['name'])); ?>
        </div>
        <input type="hidden" name="id" id="row_id" data-table-name="<?php echo $this->url;?>" value="<?php echo $row['id'] ?>">

        
        <?php if(validation_errors()){ ?>
            <!-- SHOW ERROR -->
            <?php echo viewErrorValidation();?>
            <!-- END SHOW ERROR -->
        <?php } ?>

        
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="m-y-0 font-bold">Information</h4>
                </div>


                <div class="panel-body">
                    <div class="form-horizontal"> 
                    <div class="form-group">
                            <label for="m_date" class="control-label col-md-3">Date</label>
                            <div class="col-md-7">
                                <input type="text"  id="m_date" class="form-control disabled " disabled value="<?php echo format_date($row['date']); ?>">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>       
                        <div class="form-group">
                            <label for="m_name" class="control-label col-md-3">Name</label>
                            <div class="col-md-7">
                                <input type="text"  id="m_name" class="form-control disabled " disabled value="<?php echo $row['name'] ?>">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="m_name" class="control-label col-md-3">Email</label>
                            <div class="col-md-7">
                                <input type="text"  id="m_name" class="form-control disabled " disabled value="<?php echo $row['email'] ?>">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="m_name" class="control-label col-md-3">Phone</label>
                            <div class="col-md-7">
                                <input type="text"  id="m_name" class="form-control disabled " disabled value="<?php echo $row['phone'] ?>">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="m_name" class="control-label col-md-3">Company</label>
                            <div class="col-md-7">
                                <input type="text"  id="m_name" class="form-control disabled " disabled value="<?php echo $row['company'] ?>">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="m_name" class="control-label col-md-3">Address</label>
                            <div class="col-md-7">
                                <textarea id="m_name" class="form-control disabled " disabled > <?php echo $row['address'] ?></textarea>
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>


                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="m-y-0 font-bold">Information</h4>
                </div>


                <div class="panel-body">
                    <div class="form-horizontal">        
                        <div class="form-group">
                            <label for="m_subject" class="control-label col-md-3">Subject</label>
                            <div class="col-md-7">
                                <input type="text"  id="m_subject" class="form-control disabled " disabled value="<?php echo $row['subject'] ?>">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="m_contact" class="control-label col-md-3">Message</label>
                            <div class="col-md-7">
                                <textarea id="m_contact" class="form-control disabled " disabled rows="10"><?php echo $row['content'] ?></textarea>
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>                        
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="m-y-0 font-bold">Reply to: <?php echo $row['name']; ?> &lt;<?php echo $row['email']; ?>&gt; </h4>
                </div>


                <div class="panel-body">
                    <div class="form-horizontal">        
                        
                        <div class="form-group">
                            <div class="col-md-12">
								<?php if($row['replied'] == 1) echo $row['reply'];
								else { ?>
                                <textarea id="reply" class="form-control content" name="reply"  rows="10"><?php echo $row['reply'] ?></textarea>
                                <div class="help-block with-errors"></div>
								<?php } ?>
                            </div>
                        </div>

                        <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" id="csrf-token">
                    </div>
                </div>
            </div>
        </div>

    </form>
    <?php $this->load->view('admin/template/log'); ?>
</div>


<!-- MODAL CONFIRM DELETE DATA -->
<div id="modal-delete-row" class="modal">
    <div class="modal-dialog modal-sm">
        <div class="modal-content bg-warning animated bounceIn">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    <span>
                        <i class="zmdi zmdi-close"></i>
                    </span>
                </button>
            </div>

            <div class="modal-body">
                <div class="text-center">
                    <div>
                        <i class="zmdi zmdi-alert-circle-o zmdi-hc-5x"></i>
                    </div>
                    <h3>Warning</h3>
                    <p>Are you sure to delete this data ?</p>
                    <div class="m-y-30">
                        <button type="button" id="btn-continue-delete-row" class="btn btn-default">Continue</button>
                        <a href="#" data-dismiss="modal" class="btn btn-warning">Cancel</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END MODAL CONFIRM DELETE DATA -->