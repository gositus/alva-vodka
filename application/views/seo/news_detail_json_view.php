<?php 
header('Content-Type: application/json');
$myObj = new stdClass();
$myObj->version = "1.0";
$myObj->provider_name = setting_value("site_name");
$myObj->provider_url = base_url();
$myObj->author_name = "Gositus";
$myObj->title = $news['name'];
$myObj->type = "rich";
$myObj->width = 800;
$myObj->height = 600;
$myObj->html = htmlentities($news['content']);;
$myObj->thumbnail_url = base_url('lib/images/news/').$news['image'];
$myObj->thumbnail_width = 600;
$myObj->thumbnail_height = 400;

$myJSON = json_encode($myObj);

echo $myJSON;?>