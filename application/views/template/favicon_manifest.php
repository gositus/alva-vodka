<?php 
header('Content-Type: text/webmanifest');
$array = array(
    'name'       => setting_value("web_title"),
    'short_name' => setting_value("web_title"),
    'icons'       => array(
        	array(
        		'src'        => 'lib/images/favicon/' . setting_value("favicon192"),
        		'sizes'       => "192x192",
        		'type'       => "image/png"
    		),
			array(
        		'src'        => 'lib/images/favicon/' . setting_value("favicon512"),
        		'sizes'       => "512x512",
        		'type'       => "image/png"
    		)
        ),
    'theme_color' => setting_value("corporate_color"),
    'background_color' => setting_value("corporate_color"),
    "display"=> "standalone"
    );
$array = json_encode($array, JSON_PRETTY_PRINT);
echo($array);
?>
