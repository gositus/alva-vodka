<!DOCTYPE html>
<html lang="<?php echo get_language(current_language())['attr']; ?>">
<head>
<?php 
	total_visitor('page_view');
	$uri = substr($this->uri->uri_string,1);
	$seo_setting = get_meta($uri); //get from seo tools

	if(!empty($seo_setting)) {
		$title = $seo_setting['title'];
		$description = $seo_setting['description'];
		$keyword = $seo_setting['keyword'];
	} else {
		//get from controller
		$title       = (!empty($meta['title'])) ? $meta['title'] : setting_value('web_title');
		$keyword     = (!empty($meta['keyword'])) ? $meta['keyword'] : setting_value("meta_keyword");
		$description = (!empty($meta['description'])) ? $meta['description'] : setting_value("meta_description");
	} 
	?>

<!-- Gositus Web Development (PT Go Online Solusi) | www.gositus.com | fb.com/gositus | instagram.com/gositus -->
	<title><?php echo $title; ?></title>
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="author" content="Gositus, www.gositus.com" />
	<meta name="description" content="<?php echo $description; ?>" />
	<meta name="keywords" content="<?php echo $keyword; ?>" />
	<meta name="theme-color" content="<?php echo setting_value("corporate_color"); ?>" />
	<meta name="msapplication-TileColor" content="<?php echo setting_value("corporate_color"); ?>" />
	<meta name="Language" content="<?php echo get_language(current_language())['name']; ?>" />
	<?php
	if (getDomain() == 'lab.gositus.com') echo '<meta name="robots" content="noindex, nofollow">';
	?>
	
	<link rel="canonical" href="<?php echo current_url(); ?>"/><?php echo alternate_language_link(); ?>

	<?php
	// UNTUK SEO WAJIB DI PASANG > disesuaikan dengan url artikel > $type: news, blog, announcement atau event, dll
	if (!empty($this->uri->segments[1]) &&  ($this->uri->segments[1] == 'news' || $this->uri->segments[1] == 'blog') && $this->uri->rsegments[2] == 'detail') {
	echo '<meta property="og:type" content="Article" />
	';
	echo '<meta property="article:section" content="'.$type.'" />
	';
	echo '<meta property="article:published_time" content="'.date("Y-m-d", strtotime($news['start'])).'T'.date("H:m:s", strtotime($news['start'])).'+00:00" />
	';
	echo '<meta property="article:modified_time" content="'.date("Y-m-d", strtotime($news['date_edited'])).'T'.date("H:i:s", strtotime($news['date_edited'])).'+00:00" />
	';
	echo '<meta property="og:updated_time" content="'.date("Y-m-d", strtotime($news['date_edited'])).'T'.date("H:i:s", strtotime($news['date_edited'])).'+00:00" />
	';
	echo '<link rel="alternate" type="application/json+oembed" href="'.base_url().$type.'/json/'.$this->uri->rsegments[3].'" />
	';
	echo '<link rel="alternate" type="text/xml+oembed" href="'.base_url().$type.'/xml/'.$this->uri->rsegments[3].'" />
	';
	} else { 
	echo '<meta property="og:type" content="website" />
'; } ?>
	<meta property="og:title" content="<?php echo $title; ?>" /> 
	<meta property="og:description" content="<?php echo $description; ?>" />
	<meta property="og:url" content="<?php echo current_url(); ?>" />
	<meta property="og:site_name" content="<?php echo setting_value("site_name"); ?>" />
	<meta property="og:locale" content="<?php echo get_language(current_language())['attr']; ?>" /> 
	<meta property="og:image" content="<?php echo (!empty($meta['image'])) ?  $meta['image'] : base_url() . setting_value('logo_cache'); ?>"/> 
	<meta property="og:image:secure_url" content="<?php echo (!empty($meta['image'])) ?  $meta['image'] : base_url() . setting_value('logo_cache'); ?>" /> 

	<link rel="manifest" href="<?php echo base_url('lib/assets/favicon/site.webmanifest'); ?>" />
	<link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url('lib/assets/favicon/') . setting_value('favicon16') ?>" />
	<link rel="icon" type="image/png" sizes="32x32" href="<?php echo base_url('lib/assets/favicon/') . setting_value('favicon32') ?>" />
	<link rel="icon" type="image/png" sizes="192x192" href="<?php echo base_url('lib/assets/favicon/') . setting_value('favicon192') ?>" />
	<link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url("lib/assets/favicon/") .setting_value('faviconico') ?>"/>
	<link rel="mask-icon" href="<?php echo base_url("lib/assets/favicon/") .setting_value('faviconsvg') ?>" color="<?php echo setting_value('corporate_color'); ?>" />
	<link rel="apple-touch-icon" type="image/png" sizes="180x180" href="<?php echo base_url('lib/assets/favicon/') . setting_value('favicon180') ?>" />
	<link rel="apple-touch-icon" type="image/png" sizes="512x512" href="<?php echo base_url('lib/assets/favicon/') . setting_value('favicon512') ?>" />
	
	<link rel="dns-prefetch" href="//s7.addthis.com" />
<?php if(!empty(setting_value('live_chat'))){ ?>
	<link rel="dns-prefetch" href="//static.chatra.io" />
 	<link rel="dns-prefetch" href="//call.chatra.io" />
<?php }?>
<?php if(!empty(setting_value('google_analytic'))){ ?>
 	<link rel="dns-prefetch" href="//www.googletagmanager.com" />
 	<?php }?>

	<link rel="stylesheet" type="text/css" href="<?php echo base_url('lib/css/gositus/bootstrap.min.css'), ''; ?>"/>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('lib/css/gositus/fontawesome/fontawesome.min.css'), ''; ?>"/>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('lib/css/gositus/fontawesome/brands.min.css'), ''; ?>"/>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('lib/css/gositus/fontawesome/regular.min.css'), ''; ?>"/>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('lib/css/gositus/animate.css'), ''; ?>"/>
<?php foreach ($css as $style) echo '<link rel="stylesheet" type="text/css" href="', base_url(), 'lib/', $style ,'.css" />'; ?>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('lib/css/gositus/style.css'), ''; ?>"  />
	
	<script type="text/javascript">var base_url = '<?php echo base_url(); ?>';</script>
	<script type="text/javascript" src="<?php echo base_url('lib/js/gositus/jquery-3.3.1.min.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo base_url('lib/js/gositus/popper.min.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo base_url('lib/js/gositus/bootstrap.min.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo base_url('lib/js/gositus/plugins/jquery.validate.min.js'); ?>"></script>
	<?php foreach ($js as $script) echo '<script type="text/javascript" src="', base_url(), 'lib/', $script ,'.js"></script>';
	// delete_coo_kie(); ?>

	<script type="text/javascript" src="<?php echo base_url('lib/js/gositus/jquery.run.js'); ?>"></script>
</head>
<body class="overhide">


<header id="myheader" class="">
	<nav class="navbar navbar-expand-lg">
		<div class="container-xl">
			<div class="menu-navigation">
			  <a class="navbar-brand" href="<?php echo base_url() ?>">
			    <img class="filter-white" src="<?php echo base_url('lib/assets/logo/'.setting_value('logo')) ?>" alt="">
			  </a>
			  	<span></span>
			  </button>
			  <div class="wrapper-menu">
				  <ul class="nav flex-column">
					  <li class="nav-item" data-menuanchor="hello">
					    <a class="nav-link section-link" href="#hello">Hello / <span>Привет</span></a>
					  </li>
					  <li class="nav-item" data-menuanchor="the-mascot">
					    <a class="nav-link section-link" href="#the-mascot">The Mascot / <span>Талисман</span></a>
					  </li>
					  <li class="nav-item" data-menuanchor="our-vodka">
					    <a class="nav-link section-link" href="#our-vodka">Our Vodka / <span>наша водка</span></a>
					  </li>
					  <li class="nav-item" data-menuanchor="cocktails">
					    <a class="nav-link section-link" href="#cocktails">Cocktails / <span>Коктейли</span></a>
					  </li>
					  <li class="nav-item" data-menuanchor="contact">
					    <a class="nav-link section-link" href="#contact">Contact / <span>Контакт</span></a>
					  </li>
					</ul>
				</div>
			</div>

		    <button class="menu-icon-toggle">
		    	<span></span>
		    </button>

		  <!-- <div class="collapse navbar-collapse">
		    <ul class="navbar-nav ml-auto">
		      <li class="nav-item">
		        <a class="nav-link" href="#">Copyright <?php echo date('Y') ?> Alva Vodka</a>
		      </li>
		    </ul>
		  </div> -->
		</div>
	</nav>
</header>
