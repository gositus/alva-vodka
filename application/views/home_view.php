<div class="modal" id="legalage" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-lg">
    <div class="modal-content">
      <div class="modal-body">
      	<div class="wrapper-center text-center">
	        <h2 class="main-title x3 text-uppercase mb-4">I Certify that I’m of legal drinking age in my country of residence</h2>
	        <button type="button" class="btn btn-modal active" data-dismiss="modal">YES, I AM OVER 21</button>
	        <button type="button" class="btn btn-modal">NO, I AM BELOW 21</button>
		</div>
      </div>
    </div>
  </div>
</div>

<div id="fullpage">
<section id="hello" data-section-name="hello" class="section">
	<div class="container-xl relative">
		<a class="copyright-top" href="#">Copyright <?php echo date('Y') ?> Alva Vodka</a>
	</div>
	<div class="bg-section blue-freeze content-center white">
	<?php /*/ ?>
	<img class="img-center" src="<?php echo base_url('lib/images/item/alva-vodka.png') ?>" alt="Alva Vodka">
	<?php /*/ ?>
	<div class="container-xl">
		<div class="row justify-content-center">
			<div class="col-lg-8 col-md-9 col-sm-10">

				<div class="hello-privet text-center">
					<h1 class="main-title">FREEZE THE DAY</h1>
					<div class="short-text px-5">
						<p>
						Whether you are enjoying your time alone or
						with friends, enjoy our vodka shots or cocktails
						for that cool, rich and smooth taste.
						</p>
					</div>
				</div>
		</div>
		</div>
	</div>
	</div>
</section>

<section id="the-mascot" data-section-name="the-mascot" class="section">
	<div class="bg-section wolf white">
	<div class="container-xl">
		<div class="row justify-content-end">
			<div class="col-lg-4 col-md-6">

				<div class="hello-privet text-right">
					<h1 class="main-title x2 mb-3">LEAD THE PACK</h1>
					<div class="short-text">
						<p>
						Alva vodka is a concept born from purity, to meet
						the demand of fine tasting and versatile vodka in the market. The Siberian wolf in the winter snow
						 is our symbol of strength and resilience.</p>

						<p>The
						 character of Alva vodka.
						An Alpha male wolf is destined to lead the pack,
						so set your own pathfor others to follow.
						Overcoming challenges and obstacles. 
						</p>
					</div>
				</div>
			</div>
		</div>
	</div>
	</div>
</section>

<section id="our-vodka" data-section-name="our-vodka" class="section">
	<div class="relative our-vodka">
		<div class="vodka-slider">
			<?php foreach ($item as $key => $value) { 
				$color = array("#85c441", "#144cd7");
				if($value['id'] % 2 == 0){
					$c = 1;
				}else{
					$c = 0;
				}
			?>
			<div class="item bg-section cucumber white" style="background-image: url(<?php echo base_url('lib/images/item/'.$value['bgimage']) ?>);" title="<?php echo $value['name'] ?>" data-color="<?php echo $color[$c] ?>">
				<div class="container-xl relative">
					<div class="image">
						<img src="<?php echo base_url('lib/images/item/'.$value['image']) ?>" alt="<?php echo $value['name'] ?>">
					</div>
					<div class="hello-privet text-left text-md-right">
						<h1 class="main-title x2 mb-3 text-uppercase"><?php echo $value['heading']?></h1>
						<div class="short-text overheight">
							<?php echo nl2br($value['content']) ?>
						</div>
					</div>
				</div>
			</div>
			<?php } ?>

			
		</div>
		<div class="container-xl relative">
			<div class="slider-navigation">
				<button type="button" class="btn btn-prev vodka-slider-pop-prev"><i class="far fa-chevron-left"></i></button>
	            <button type="button" class="btn btn-next vodka-slider-pop-next"><i class="far fa-chevron-right"></i></button>
			</div>
		</div>
	</div>
</section>

<section id="cocktails" data-section-name="cocktails" class="section fp-auto-height">
	<div class="bg-section texture section-auto pb-0">
	<div class="container-xl">
		<div class="row justify-content-end">
			<div class="col-lg-4 col-md-6 col-sm-8 col-10">
				<div class="hello-privet text-right">
					<h1 class="main-title x2 mb-3">IN THE MIX</h1>
					<div class="short-text">
						<p>Squezze your creative juice,
						send us your Alva cocktails recipe.
						We will pick the favourite every month
						and give away prizes to winners.</p>
					</div>
				</div>
			</div>
		</div>
		<div class="row justify-content-center">
			<div class="col-12">

				<div class="row align-items-center">
					<div class="col-md-7">
						<div class="relative px-0 px-md-5">
							<div class="coverflow-slider">
								<ul class="flip-items">

									<?php foreach ($cocktails as $key => $value) { ?>
									<li class="flipster__item  flipster__item--current" data-index="<?php echo $key ?>">
										<img src="<?php echo base_url('lib/images/media/'.$value['image']) ?>" alt="<?php echo $value['name'] ?>">
									</li>
									<?php } ?>

								</ul>
							</div>
						</div>
					</div>
					<div class="col-md-5">
						<div id="coverflow-slider-caption" class="carousel slide" data-ride="carousel" data-interval="false">
						  <div class="carousel-inner">

						  	<?php foreach ($cocktails as $key => $value) { ?>
						    <div class="carousel-item <?php echo ($key == 0) ? 'active' : ''; ?>">
						    	<div data-animation="animate__animated" class="">
						      		<h2 class="main-title x3 mb-3 text-center text-md-left text-uppercase"><?php echo $value['name'] ?></h2>
							     	<div class="text light-color">
							     		<p>
							        	<?php echo nl2br($value['content']) ?>
							     		</p>
									</div>
								</div>
						    </div>
						    <?php } ?>
						    
						  </div>
						</div>
					</div>
				</div>

			</div>
			<div class="col-12 text-center mt-3">
				<a href="#contact" class="btn btn-mix section-link">SUBMIT Y0UR MIX</a>
			</div>
		</div>
	</div>
	<div id="gallery" class="gallery">
		<div class="container-xl">
			<h1 class="main-title x2 mb-4 text-center">GALLERY</h1>
		</div>
		<div class="relative overflow">
			<div class="gallery-slider">
				<?php 
				foreach ($gallery as $key => $value) { ?>
				<div class="item">
					<div class="image-wrapper">
						<img src="<?php echo base_url('lib/images/media/'.$value['image']) ?>" alt="<?php echo $value['name'] ?>">
					</div>
				</div>
				<?php } ?>
			</div>
			<div class="slider-navigation center-side">
				<button type="button" class="btn btn-prev gallery-slider-prev"><i class="far fa-chevron-left"></i></button>
	            <button type="button" class="btn btn-next gallery-slider-next"><i class="far fa-chevron-right"></i></button>
			</div>
		</div>
		<div class="container-xl text-center py-4">
			<?php if(!empty(setting_value('instagram'))){ ?>
			<a href="https://www.instagram.com/<?php echo setting_value('instagram') ?>" target="_blank"><span class="user-instagram"><i class="fab fa-instagram fa-bold"></i> <?php echo strtoupper(setting_value('instagram')) ?></span></a>
			<?php } ?>
			
		</div>
	</div>
	</div>
</section>

<section id="contact" data-section-name="contact" class="section">
	<div class="bg-contact relative">
	<div class="container-xl">
		<div class="form-box">
			<h2 class="main-title x3 text-center">STAY IN TOUCH</h2>

			<?php if($this->session->flashdata('success')){ ?>
				<div class="alert alert-info my-3" role="alert">
				  <?php echo $this->session->flashdata('success') ?>
				  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
				    <span aria-hidden="true">&times;</span>
				  </button>
				</div>
				<script>
				    fbq('track', 'Lead');
				</script>
			<?php } else if($this->session->flashdata('error')) { ?>
				<div class="alert alert-info my-3" role="alert">
				  <?php echo $this->session->flashdata('error') ?>
				  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
				    <span aria-hidden="true">&times;</span>
				  </button>
				</div>
			<?php } ?>

			<form class="pt-2" action="" method="post" id="contact-form">
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
						    <label for="name">NAME</label>
						    <input type="text" class="form-control" id="name" name="name" placeholder="TYPE HERE" required value="<?php echo (!empty($this->session->flashdata('input')['name'])) ? $this->session->flashdata('input')['name'] : ''; ?>">
						</div>
						<div class="form-group">
						    <label for="email">EMAIL</label>
						    <input type="email" class="form-control" id="email" name="email" placeholder="TYPE HERE" required value="<?php echo(!empty($this->session->flashdata('input')['email']))?$this->session->flashdata('input')['email'] : ''; ?>">
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
						    <label for="message">MESSAGE</label>
						    <textarea class="form-control" id="message" rows="5" name="message" placeholder="TYPE HERE" required><?php echo(!empty($this->session->flashdata('input')['message']))?$this->session->flashdata('input')['message'] : ''; ?></textarea>
						 </div>
					</div>
					<input type="hidden" name="g-recaptcha-response" id="recaptchaResponse">
					<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" id="csrf-token">
					<input type="text" name="kotakkosong" id="kotakkosong" maxlength="20">

					<div class="col-md-12 text-center">
						<button type="submit" class="btn btn-mix mt-3">SEND</button>
					</div>
				</div>
			</form>
		</div>
	</div>
	<div class="foot">
		<div class="container-xl">
			<div class="row align-items-end">
				<div class="col-5">
					<div class="brand-foot">
						<img src="<?php echo base_url('lib/assets/logo/head.png') ?>">
					</div>
				</div>
				<div class="col-7 text-right">
					<div class="foot-right">
						<div>Follow Our Social Media</div>
						<ul class="pl-0">
							<?php if(!empty(setting_value('instagram'))){ ?>
							<li><a href="https://www.instagram.com/<?php echo setting_value('instagram') ?>" target="_blank"><i class="fab fa-instagram"></i></a></li>
							<?php } if(!empty(setting_value('facebook'))){ ?>
							<li><a href="https://www.facebook.com/<?php echo setting_value('facebook') ?>" target="_blank"><i class="fab fa-facebook"></i></a></li>
							<?php } if(!empty(setting_value('youtube'))){?>
							<li><a href="https://www.youtube.com/<?php echo setting_value('youtube') ?>" target="_blank"><i class="fab fa-youtube"></i></a></li>
							<?php } ?>
							<li><a href="mailto:<?php echo setting_value('email') ?>" target="_blank"><i class="far fa-envelope"></i></a></li>
						</ul>
						
					</div>
				</div>
				<div class="col-sm-12 col-12 align-self-end">
					<div class="foot-right text-center text-center pt-3 pt-sm-0">
						<small>Copyright <?php echo date('Y') ?> Alva Vodka - <span class="d-block d-sm-inline">Website by&nbsp;&nbsp;<a id="copyright-gositus" target="_blank" href="https://www.gositus.com" title="Jasa Pembuatan Website"><span>Gositus</span></span></a></small>
					</div>
				</div>
			</div>
		</div>
	</div>
	</div>
</section>
</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>
<script src="https://www.google.com/recaptcha/api.js?render=<?php echo setting_value('invisible_site_key'); ?>"></script>
<script>
    grecaptcha.ready(function () {
        grecaptcha.execute('<?php echo setting_value('invisible_site_key'); ?>', { action: 'contact' }).then(function (token) {
            var recaptchaResponse = document.getElementById('recaptchaResponse');
            recaptchaResponse.value = token;
        });
    });
</script>
<script type="text/javascript" src="<?php echo base_url('lib/js/gositus/plugins/jquery.validate.min.js');?>"></script>
<script type="text/javascript">
	$(document).ready(function(){
		<?php if($this->session->flashdata('success') != "" || $this->session->flashdata('error') != ""){ } else { ?>
		$('#legalage').modal('show');
		<?php } ?>

		$("#contact-form").validate({
	        rules: {
	         "hiddenRecaptcha": {
	               required : function() {
	                   if(grecaptcha.getResponse() == '') {
	                       return true;
	                   } else {
	                       return false;
	                   }
	               }
	           }
	        },
	        messages: {
	        	"hiddenRecaptcha": "Captcha is required"
	      	},
	      	errorPlacement: function(){
	            return false;
	        }
	   });
	})
</script>