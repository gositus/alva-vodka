<?php $url = load_class('Config')->config['base_url']; ?>

<!DOCTYPE html>
<html>
<head>
	<!-- Gositus -- www.gositus.com -- go@gositus.com -- instagram.com/gositus -->
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="robots" content="NOINDEX, NOFOLLOW" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
	<title>404 Error</title>

	<link rel="shortcut icon" type="image/x-icon" href="<?php echo $url;?>lib/images/goadmin/favicon/favicon.ico"/>
	<link rel="apple-touch-icon" href="<?php echo $url;?>lib/images/goadmin/favicon/favicon-60x60.png">
	<link rel="apple-touch-icon" sizes="76x76" href="<?php echo $url;?>lib/images/goadmin/favicon/favicon-76x76.png">
	<link rel="apple-touch-icon" sizes="120x120" href="<?php echo $url;?>lib/images/goadmin/favicon/favicon-120x120.png">
	<link rel="apple-touch-icon" sizes="152x152" href="<?php echo $url;?>lib/images/goadmin/favicon/favicon-152x152.png">
	<link rel="stylesheet" type="text/css" href="<?php echo $url;?>lib/css/error404/404.css"/>

</head>
<body>

	<div id="home-new">     
	    <div class="square-green"></div>
	    <div class="line-grey"></div>
	    <div class="triangle-yellow"></div>
	    <div class="circle-blue"></div>
	    <div class="wrap-bg-text vertical">         
	        <div class="bg-text"><h1>ERROR</h1></div>
	    </div>

	    <div class="wrap-form-home">
	        <div class="container">
	            <div class="row inline">
	            	<div class="col inline verti-middle l-3 m-3 s-12">&nbsp;</div>
	                <div class="col inline verti-middle l-6 m-6 s-12">
						<div class="error">
							<div class="e-icon text-warning">
								<i class="zmdi zmdi-alert-circle-o"></i>
							</div>
							<h1>404</h1>
							<h2>Page not found</h2>
							<div class="e-text">Sorry, we couldn't find your page. We've created the link for your wayout.</div>
							<a href="<?php echo $url;?>" class="btn-custom btn-scale btn-grey">Go to home page</a>
						</div>
	                </div>
	            	<div class="col inline verti-middle l-3 m-3 s-12">&nbsp;</div>
	            </div>
	        </div>
	    </div>
	</div>

	<footer>
	    <div class="orbit">
	        <div class="orbit-large"></div>
	        <div class="orbit-medium"></div>
	        <div class="orbit-small"></div>
	        <div class="moon"></div>
	    </div>
	    <div class="wave">
	        <div class="wave-grey"></div>   
	        <div class="wave-grey-2"></div> 
	    </div>
	</footer>


</body>
</html>