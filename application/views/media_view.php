<h2><a href="<?php echo base_url('media/instagram') ?>">Lihat Instagram</a></h2>
<?php

// Foto
echo " Photo <br/>";
foreach ($media_photo as $key => $value) { 
	//tolong pake alt image dan name tinggal kasih style aja ?>
	<div style="display: inline-block;">
		
		<img src="<?php echo (!empty($value['image'])) ? base_url('lib/images/media/') . $value['image'] : base_url('lib/assets/gositus/default-image.png') ;?>" alt="<?php echo (!empty($value['image_alt'])) ? $value['image_alt'] : $value['name']; ?>">

		<h3><?php echo $value['name']; ?></h3>
	</div>

<?php 
} 



// Video
echo "<br/> Video <br/>";
foreach ($media_video as $key => $value) { 
	//tolong pake alt image dan name tinggal kasih style aja ?>
	<iframe width="560" height="315" src="https://www.youtube.com/embed/<?php echo $value['video'] ?>" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
	<h3><?php echo $value['name']; ?></h3>

<?php 
} ?>