<h2><?php echo $category['name']; ?></h2>
<img src="<?php echo (!empty($category['image'])) ? base_url('lib/images/item/') . $category['image'] : base_url('lib/assets/gositus/default-image.png') ;?>" alt="<?php echo (!empty($category['image_alt'])) ? $category['image_alt'] : $category['name']; ?>">

<h4>Products of <?php echo $category['name']; ?></h4>
<?php foreach ($product as $key => $value) { ?>

<a href="<?php echo base_url('product/'.$value['seo_url']); ?>"><img src="<?php echo (!empty($value['image'])) ? base_url('lib/images/item/') . $value['image'] : base_url('lib/assets/gositus/default-image.png') ;?>" alt="<?php echo (!empty($value['image_alt'])) ? $value['image_alt'] : $value['name']; ?>"></a>

<h5><a href="<?php echo base_url('product/'.$value['seo_url']); ?>"><?php echo $value['name']; ?></a></h5>
<p><?php echo word_limiter($value['content'],20) ; ?></p>
<p><a href="<?php echo base_url('product/'.$value['seo_url']); ?>">Read More ..</a></p>

<?php } ?>

<script>
	// FB Pixels
    fbq('track', 'ViewContent');
</script>