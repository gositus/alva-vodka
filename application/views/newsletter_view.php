
<?php echo($this->session->flashdata("newsletter"))? $this->session->flashdata("newsletter"): ''; ?>
<form action="<?php echo base_url('newsletter/add')?>" method="post" id="newsletter-form">
	<input type="text" name="email_newsletter" class="email required" placeholder="Your email">
	<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" id="csrf-token">
	<input type="submit" name="submit" id="submit-newsletter" value="Subscribe Newsletter">
</form>
<script type="text/javascript" src="<?php echo base_url('lib/js/goadmin/plugins/jqueryValidate/jquery.validate.min.js');?>"></script>
<script type="text/javascript">
	$("#submit-newsletter").click(function(){
		$("#newsletter-form").valid();
	})
</script>