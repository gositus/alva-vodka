<div class="container">
    <?php 
    // Condition: Submit Form = Success
    if($this->session->flashdata('success'))
    { ?>
		<div class="col-xs-12 notif-contact success"><?php echo $this->session->flashdata('success') ?></div>
		<script>
			// FB Pixels
		    fbq('track', 'Lead');
		</script>
	<?php
    }
    // Condition: Submit Form = Failed
	else if($this->session->flashdata('error'))
    { ?>
		<div class="col-xs-12 notif-contact failed"><?php echo $this->session->flashdata('error') ?></div>
	<?php } ?>
    
	<?php if(!empty(validation_errors())){ echo validation_errors() ;} ?>
	<form action="" method="post" id="contact-form">
		<div class="form-group"> 
			<label for="name">Name</label>
			<input id="name" maxlength="50" type="text" name="name" class="form-control name required" autocomplete="off" value="<?php echo (!empty($this->session->flashdata('input')['name'])) ? $this->session->flashdata('input')['name'] : ''; ?>" >
		</div>
		<div class="form-group"> 
			<label for="email">Email</label>
			<input id="email" maxlength="50" type="text" name="email" class="form-control email required" autocomplete="off" value="<?php echo(!empty($this->session->flashdata('input')['email']))?$this->session->flashdata('input')['email'] : ''; ?>">
		</div>
		<div class="form-group"> 
			<label for="phone">Phone</label>
			<input id="phone" maxlength="20" type="text" name="phone" class="form-control phone required" autocomplete="off" value="<?php echo(!empty($this->session->flashdata('input')['phone']))?$this->session->flashdata('input')['phone'] : ''; ?>">
		</div>
		<div class="form-group"> 
			<label for="company">Company</label>
			<input id="company" maxlength="50" type="text" name="company" class="form-control  " autocomplete="off" value="<?php echo(!empty($this->session->flashdata('input')['company']))?$this->session->flashdata('input')['company'] : ''; ?>">
		</div>
		<div class="form-group"> 
			<label for="address">Address</label>
			<textarea id="address" type="text" name="address" class="form-control  " autocomplete="off" ><?php echo(!empty($this->session->flashdata('input')['address']))?$this->session->flashdata('input')['address'] : ''; ?></textarea>
		</div>
		<div class="form-group"> 
			<label for="subject">Subject</label>
			<input id="subject" maxlength="150" type="text" name="subject" class="form-control  required" autocomplete="off" value="<?php echo(!empty($this->session->flashdata('input')['subject']))?$this->session->flashdata('input')['subject'] : ''; ?>">
		</div>
		<div class="form-group"> 
			<label for="message">Message</label>
			<textarea id="message" type="text" name="message" class="form-control  " autocomplete="off" ><?php echo(!empty($this->session->flashdata('input')['message']))?$this->session->flashdata('input')['message'] : ''; ?></textarea>
		</div>

		
<!-- 		<div class="form-group">
			<label>Captcha</label>
	      	<div class="recaptcha">
	            <div class="g-recaptcha pull-left" data-sitekey="<?php echo setting_value('site_key');?>"></div>
	            <div id='captcha-err'></div>
	            <div class="clearfix"></div>
	            <input type="text" class="hiddenRecaptcha" name="hiddenRecaptcha" id="hiddenRecaptcha" style="display: none">
	        </div>
		</div> -->
			<input type="hidden" name="g-recaptcha-response" id="recaptchaResponse">
			<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" id="csrf-token">
			<input type="text" name="kotakkosong" id="kotakkosong" maxlength="20"> <!-- ini wajib ada --> 
			<input type="submit" name="submit" value="Send" class="btn btn-default">
	</form>
</div>

<script src="https://www.google.com/recaptcha/api.js?render=<?php echo setting_value('invisible_site_key'); ?>"></script>
<script>
    grecaptcha.ready(function () {
        grecaptcha.execute('<?php echo setting_value('invisible_site_key'); ?>', { action: 'contact' }).then(function (token) {
            var recaptchaResponse = document.getElementById('recaptchaResponse');
            recaptchaResponse.value = token;
        });
    });
</script>
<script type="text/javascript" src="<?php echo base_url('lib/js/goadmin/plugins/jqueryValidate/jquery.validate.min.js');?>"></script>
<script type="text/javascript">
	$(document).ready(function(){
		$("#contact-form").validate({
	        rules: {
	         "hiddenRecaptcha": {
	               required : function() {
	                   if(grecaptcha.getResponse() == '') {
	                       return true;
	                   } else {
	                       return false;
	                   }
	               }
	           }
	      },
	        messages: {
	         "hiddenRecaptcha": "Captcha is required"
	      }
	   });
	})
</script>