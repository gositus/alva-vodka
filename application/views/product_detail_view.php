<img src="<?php echo (!empty($product['image'])) ? base_url('lib/images/item/') . $product['image'] : base_url('lib/assets/gositus/default-image.png') ;?>" alt="<?php echo (!empty($product['image_alt'])) ? $product['image_alt'] : $product['name']; ?>">
<h2><?php echo $product['name']; ?></h2>
<p><?php echo $product['content'] ; ?></p>
<script>
	// FB Pixels
    fbq('track', 'ViewContent');
</script>