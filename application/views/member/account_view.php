<div class="container">
  <div class="information">
    <?php if(!empty(validation_errors())){ echo validation_errors() ;} ?>
  </div>
  <form action="" method="post" id="regis-form">
    <div class="form-group">
      <label for="name">Full Name</label>
      <input id="name" type="text" name="name" class="required form-control" value="<?php echo $user['name'] ?>" autocomplete="off">
    </div>

    <div class="form-group">
      <label for="gender">Gender</label>
      <div class="radio">
        <label><input type="radio" name="gender" value="m" class="required " <?php echo ($user['gender']=='m')? 'checked' : ''; ?> >Male</label>
      </div>
      <div class="radio">
        <label><input type="radio" name="gender" value="f" class="required " <?php echo ($user['gender']=='f')? 'checked' : ''; ?> >Female</label>
      </div>
    </div>

    <div class="form-group">
      <label for="phone">Phone</label>
      <input id="phone" type="text" name="phone" class="required form-control" value="<?php echo $user['phone'] ?>" autocomplete="off">
    </div>
    <div class="form-group">
      <label for="email">Email</label>
      <input id="email" type="text" disabled class="email disabled form-control" value="<?php echo $user['email'] ?>" autocomplete="off">
    </div>
    <div class="form-group">
      <label for="dob">Date of Birth</label>
      <input id="dob" type="text" name="dob" class="datepicker required form-control" value="<?php echo ($user['dob']) ?  format_date($user['dob'] ) : '' ;?>" autocomplete="off">
    </div>
    <div class="form-group">
      <label for="password">Password</label>
      <input id="password" type="password" name="password" class="password form-control pwd-reg" minlength="8" autocomplete="off">
    </div>
    <div class="form-group">
      <label for="re_password">Re-Password</label>
      <input id="re_password" type="password" name="re_password" class="password  form-control" equalTo="#password" minlength="8">
    </div>
    <div class="form-group ">
      <label>Captcha</label>
          <div class="recaptcha">
              <div class="g-recaptcha pull-left" data-sitekey="<?php echo setting_value('site_key');?>"></div>
              <div id='captcha-err'></div>
              <div class="clearfix"></div>
              <input type="text" class="hiddenRecaptcha" name="hiddenRecaptcha" id="hiddenRecaptcha" style="display: none">
          </div>
    </div>
    <div>
      <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" id="csrf-token">
      <input type="submit" name="submit" value="Update Profile" class="btn btn-default"> | <a href="<?php echo base_url('member'); ?>"> Back</a>
    </div>
  </form>
</div>
<br>
<script src="https://www.google.com/recaptcha/api.js"></script>
<script type="text/javascript" src="<?php echo base_url('lib/js/goadmin/plugins/jqueryValidate/jquery.validate.min.js');?>"></script>
<script type="text/javascript">
  $(document).ready(function(){

    $("#regis-form").validate({
          rules: {
           "hiddenRecaptcha": {
                 required: function() {
                     if(grecaptcha.getResponse() == '') {
                         return true;
                     } else {
                         return false;
                     }
                 }
             },
            password: {
              remote: {
                url: "<?php echo base_url()?>member/check_pass_regis",
                type: "post",
                data: { pass: function(){ return $(".pwd-reg").val(); }},
              }
            }
        },
          messages: {
           "hiddenRecaptcha": "Captcha is required",
           password: "Password must contain special character, uppercase, lowercase and number"
        }
     });

  });
  // $("input[name=password]").on('blur',function(){
  //     var password = $("input[name=password]").val();
  //     if(password != ''){
  //       $("input[name=password]").addClass('required');
  //     } else {
  //       $("input[name=password]").removeClass('requiered');
  //     }
  // });
</script>