                        <div class="hidden-xs">
                            <div class="login-type nopadding">
                                <button class="btn btn-facebook" onclick="authUser('fb')">
                                    <span class="type-icon"><i class="fa fa-facebook fa-fw"></i></span>
                                    <span style="margin-left: 20px;">MASUK DENGAN FACEBOOK</span>
                                </button>
                            </div>
                            <div class="login-type nopadding">
                                <button class="btn btn-twitter" onclick="loginTwitter()">
                                    <span class="type-icon"><i class="fa fa-twitter fa-fw"></i></span>
                                    <span style="margin-left: 20px;">MASUK DENGAN TWITTER</span>
                                </button>
                            </div>
                            <div class="login-type nopadding">
                                <button class="btn btn-google g-signin" data-scope="https://www.googleapis.com/auth/plus.login https://www.googleapis.com/auth/userinfo.email" data-requestvisibleactions="http://schemas.google.com/AddActivity" data-clientId="<?php echo setting_value('g_login_key'); ?>" data-accesstype="offline" data-callback="mycoddeSignIn" data-theme="dark" data-cookiepolicy="single_host_origin">
                                    <span class="type-icon"><i class="fa fa-google-plus fa-fw"></i></span>
                                    <span style="margin-left: 20px;">MASUK DENGAN GOOGLE+</span>
                                </button>
                            </div>
                        </div>


                    <form id="register-twitter" action="<?php echo base_url();?>member/register_twitter" method="POST">
                        <input type="hidden" id="tName" name="tName" />
                        <input type="hidden" id="tId" name="tId" />
                        <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
                     </form>
                     <form id="register-google" action="<?php echo base_url()?>member/register_google" method="POST">
                        <input type="hidden" id="gFName" name="gFName" />
                        <input type="hidden" id="gLName" name="gLName" />
                        <input type="hidden" id="gId" name="gId" />
                        <input type="hidden" id="gGender" name="gGender" />
                        <input type="hidden" id="gEmail" name="gEmail" />
                        <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
                     </form>

<!-- <meta name="google-signin-client_id" content="768871286646-2djm2es289svu3fa1qmanv0dlvdopq64.apps.googleusercontent.com"> -->

<!-- facebook -->

<script>
  function authUser(param){
      FB.login(function(response) {
        if (response.status === 'connected') {
            register_facebook();
        }
      }, {scope: 'public_profile,email'});
  }

  window.fbAsyncInit = function() {
    FB.init({
      appId      : '<?php echo setting_value('fb_login_key'); ?>', //ambil dari setting
      cookie     : true,
      xfbml      : true,
      version    : 'v2.8'
    });
    FB.AppEvents.logPageView();   
  };

  (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "//connect.facebook.net/en_US/sdk.js";
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));

  function register_facebook() {
    FB.api('/me?fields=name,email', function(response) {
        $.ajax({
            url: "<?php echo base_url()?>" + 'member/register_facebook',
            type: 'post',
            data: {
              name: response.name,
              email: response.email,
              id: response.id
            },
            success: function(data) {
                window.location.href = "<?php echo $this->session->userdata('redirect'); ?>"
            }
        });
    });
  }

</script>

<!-- twitter -->
<?php echo setting_value('firebase'); // ambil dari setting  copy dari fire base web app script?> 

<script type="text/javascript">
    function submitForm(param) {
        setTimeout(function() {
            if(param == 'twitter') {
                $('#register-twitter').submit();
            } else if(param == 'google') {
                $('#register-google').submit();
            }
        }, 500);
    }

    var provider = new firebase.auth.TwitterAuthProvider();

    function loginTwitter() {
        firebase.auth().signInWithPopup(provider).then(function(result) {
           
            var token = result.credential.accessToken;
            var secret = result.credential.secret;
            var user = result.user;
            $('#tName').val(user.displayName);
            $('#tId').val(user.uid);
            submitForm('twitter');
        }).catch(function(error) {
         
            var errorCode = error.code;
            var errorMessage = error.message;
            var email = error.email;
            var credential = error.credential;
            // console.log(errorCode + ' ' + errorMessage + ' ' + email + ' ' + credential);
        });
    }
</script>

<!-- g+ -->
<script src="https://apis.google.com/js/platform.js" async defer></script>
<script type="text/javascript">
      (function() {
        var po = document.createElement('script');
        po.type = 'text/javascript'; po.async = true;
        po.src = 'https://plus.google.com/js/client:plusone.js';
        var s = document.getElementsByTagName('script')[0];
        s.parentNode.insertBefore(po, s);
      })();

      

      var gpclass;
    $('.g-signin').click(function() {
        gpclass = (function(){
            var response = undefined;
            return {
                mycoddeSignIn:function(response){
                    if (response['access_token']) {
                        //Get User Info from Google Plus API
                        gapi.client.load('plus','v1',this.getUserInformation);
                        
                    } else if (response['error']) {
                        // There was an error, which means the user is not signed in.
                        //alert('There was an error: ' + authResult['error']);
                    }
                    // alert("aa");
                },
                
                getUserInformation: function(){
                    var request = gapi.client.plus.people.get( {'userId' : 'me'} );
                    request.execute( function(profile) {
                        console.log( profile);
                        var email = profile['emails'].filter(function(v) {
                            return v.type === 'account'; // Filter out the primary email
                        })[0].value;
                        var fName = profile.name.givenName;
                        var lName = profile.name.familyName;
                        var id = profile.id;
                        var gender = profile.gender;
                        $("#gFName").val(fName);
                        $("#gLName").val(lName);
                        $("#gEmail").val(email);
                        $("#gId").val(id);
                        $("#gGender").val(gender);
                        submitForm('google');
                    });
                }
            
            };
        })();
    });
    
    function mycoddeSignIn(gpSignInResponse){
        if (gpclass != null) {
            gpclass.mycoddeSignIn(gpSignInResponse);
        }
    }


</script>
