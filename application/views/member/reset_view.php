<?php if(!empty($this->session->flashdata("pesan")) )  echo $this->session->flashdata("pesan");?>

<div class="container">
	<?php if(!empty(validation_errors())){ echo validation_errors() ;} ?>
	<form action="" method="post" id="reset-form">
		<div class="form-group"> 
			<label for="email">Email</label>
			<input id="email" type="text" class="form-control email required" autocomplete="off" disabled=""  value="<?php echo $user['email'] ;?>">
		</div>
		<div class="form-group">
	      <label for="password">New Password</label>
	      <input id="password" type="password" name="password" class="password required form-control pwd-reg" minlength="8" autocomplete="off">
	    </div>
	    <div class="form-group">
	      <label for="re_password">Re-Password</label>
	      <input id="re_password" type="password" name="re_password" class="password  form-control" equalTo="#password" minlength="8">
	    </div>
		<div class="form-group">
			<label>Captcha</label>
	      	<div class="recaptcha">
	            <div class="g-recaptcha pull-left" data-sitekey="<?php echo setting_value('site_key');?>"></div>
	            <div id='captcha-err'></div>
	            <div class="clearfix"></div>
	            <input type="text" class="hiddenRecaptcha" name="hiddenRecaptcha" id="hiddenRecaptcha" style="display: none">
	        </div>
		</div>
		<div>
			<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" id="csrf-token">
			<input type="submit" name="reset" value="Change Password" class="btn btn-default">
		</div>
	</form>
</div>
<script src="https://www.google.com/recaptcha/api.js"></script>
<script type="text/javascript" src="<?php echo base_url('lib/js/goadmin/plugins/jqueryValidate/jquery.validate.min.js');?>"></script>
<script type="text/javascript">
  $(document).ready(function(){
    $("#reset-form").validate({
          rules: {
           "hiddenRecaptcha": {
                 required: function() {
                     if(grecaptcha.getResponse() == '') {
                         return true;
                     } else {
                         return false;
                     }
                 }
             },
            password: {
              remote: {
                url: "<?php echo base_url()?>member/check_pass_regis",
                type: "post",
                data: { pass: function(){ return $(".pwd-reg").val(); }},
              }
            }
        },
          messages: {
           "hiddenRecaptcha": "Captcha is required",
           password: "Password must contain special character, uppercase, lowercase and number"
        }
     });

  })
</script>