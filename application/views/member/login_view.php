<?php if(!empty($this->session->flashdata("pesan")) )  echo $this->session->flashdata("pesan");
echo validation_errors();?>

<div class="container">
	<?php if(!empty(validation_errors())){ echo validation_errors() ;} ?>
	<form action="" method="post" id="login-form">
		<div class="form-group"> 
			<label for="email">Email</label>
			<input id="email" type="text" name="email" class="form-control email required" autocomplete="off">
		</div>
		<div class="form-group"> 
			<label for="password">Password</label>
			<input id="password" type="password" name="password" class="form-control password required" autocomplete="off">
		</div>
		<div class="form-group">
			<label>Captcha</label>
	      	<div class="recaptcha">
	            <div class="g-recaptcha pull-left" data-sitekey="<?php echo setting_value('site_key');?>"></div>
	            <div id='captcha-err'></div>
	            <div class="clearfix"></div>
	            <input type="text" class="hiddenRecaptcha" name="hiddenRecaptcha" id="hiddenRecaptcha" style="display: none">
	        </div>
		</div>
		<div>
			<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" id="csrf-token">
			<input type="submit" name="submit" value="Login" class="btn btn-default"> | <a href="<?php echo base_url('member/register'); ?>">Create Account</a> | <a href="<?php echo base_url('member/forgot'); ?>">Forgot Password</a>
		</div>
	</form>
</div>
<script src="https://www.google.com/recaptcha/api.js"></script>
<script type="text/javascript" src="<?php echo base_url('lib/js/goadmin/plugins/jqueryValidate/jquery.validate.min.js');?>"></script>
<script type="text/javascript">
	$(document).ready(function(){
		$("#login-form").validate({
	        rules: {
	         "hiddenRecaptcha": {
	               required: function() {
	                   if(grecaptcha.getResponse() == '') {
	                       return true;
	                   } else {
	                       return false;
	                   }
	               }
	           }
	      },
	        messages: {
	         "hiddenRecaptcha": "Captcha is required"
	      }
	   });
	})
</script>
<?php //$this->load->view('member/social_login_view'); // kalo mau pake login pake socmed, isi jsnya dulu di admin?>