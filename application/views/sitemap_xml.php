<?php 
header('Content-Type: text/xml');
echo '<?xml version="1.0" encoding="UTF-8"?>';
echo '<urlset
      xmlns="http://www.sitemaps.org/schemas/sitemap/0.9"
      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
      xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9
            http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd">';
?>

<url>
  <loc><?php echo base_url();?></loc>
  <changefreq>weekly</changefreq>
  <priority>1.00</priority>
</url>
<url>
  <loc><?php echo base_url();?>#hello</loc>
  <changefreq>monthly</changefreq>
  <priority>0.5</priority>
</url>
<url>
  <loc><?php echo base_url();?>#the-mascot</loc>
  <changefreq>monthly</changefreq>
  <priority>0.5</priority>
</url>
<url>
  <loc><?php echo base_url();?>#our-vodka</loc>
  <changefreq>monthly</changefreq>
  <priority>0.5</priority>
</url><url>
  <loc><?php echo base_url();?>#cocktails</loc>
  <changefreq>monthly</changefreq>
  <priority>0.5</priority>
</url>
<url>
  <loc><?php echo base_url();?>#contact</loc>
  <changefreq>monthly</changefreq>
  <priority>0.5</priority>
</url>

<?php echo '</urlset>';
?>