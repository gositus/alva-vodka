<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Contact extends CI_Controller {

	var $model = 'model_message';

	public function __construct()
	{
		parent::__construct();
		$this->load->model($this->model);
	}

	public function index()
	{
		$title = 'Contact';
		$lang = current_language();
		
		if($this->input->post()){

			$post =$this->input->post();

			// Validasi
			$this->form_validation->set_rules('name', 'Name', 'required');
			$this->form_validation->set_rules('email', 'Email', 'required');
			$this->form_validation->set_error_delimiters('<li>', '</li>');
			
			// Honeypot
			if ($this->form_validation->run() == TRUE && empty($post['kotakkosong'])){	

				//Whitelist Domain
				$email_ext    = substr($post['email'], strrpos($post['email'], '.') +1);
				$banned_domain = select_all_row('master_banned_domain',array('flag'=>1));
				foreach ($banned_domain as $key => $value){
					$block_domain[] = $value['name'];
				}
				
				if(!in_array($email_ext, $block_domain, true)){
					// Whitelist IP
					$cek_ip = select_all_row('master_banned_ip',array('name'=>$this->input->ip_address()), TRUE);					
					if(empty($cek_ip)){

						// Verifikasi Captcha
						$captcha = trim($this->input->post("g-recaptcha-response"));
				       	$cek = verify_captcha($captcha, setting_value('invisible_secret_key'));
						if(!empty($cek['success']) || $cek['success'] == 1){
							// All criteria success
							$contact = $this->model_message->insert_contact($post);
							total_visitor('leads'); //count lead for dashboard
						}
					}
				}
			}
			
			if(!empty($contact)){
				$this->session->set_flashdata('success','Thank you, your message has been sent.');
			}
			else{
				$this->session->set_flashdata('error','Error occurred when sending your message');
				$this->session->set_flashdata('input',$post);
			}

			redirect(base_url() . 'contact');

		} else{

			$asset = array(
				'js'	=> array(),
				'css'	=> array('gositus/bootstrap.min'),
				'meta'  => meta_create($title)
			);

			$this->load->view('template/header', $asset);	
			$this->load->view('contact_us_view');
			$this->load->view('template/footer');
		}
        
	}
}
?>