<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Doku extends CI_Controller {
    public $ip_range = "103.10.129.";

    public function index() {
        $assets = array();
        $payment_method = $this->session->userdata('payment_method');

        $assets['data'] = array(
                        'id'                => $this->session->userdata('member_id') ? $this->session->userdata('member_id') : '',
                        'name'              => $this->session->userdata('name'),
                        'item_id'              => $this->session->userdata('item_id'),
                        'email'             => $this->session->userdata('email'),
                        'phone'             => $this->session->userdata('phone') ? $this->session->userdata('phone') : '',
                        'nominal'           => $this->session->userdata('nominal'),
                        'comment'           => $this->session->userdata('comment') ? $this->session->userdata('comment') : '',
                        'payment_method'    => $this->session->userdata('payment_method'),
                    );

        $data = $assets['data'];
        
        // add order
        $this->load->model('model_doku');
        $this->load->model('model_order');
        $this->session->set_userdata('donation_id', $this->model_order->addOrder($data));

        $assets['payment_channel'] = '';

        // if doku type selected
        if (in_array($this->session->userdata('payment_method'), json_decode(setting_value('doku_payment_code'), true))) {
            $payment_method = 'doku';

            $key = array_search($this->session->userdata('payment_method'), json_decode(setting_value('doku_payment_code'), true));
            $assets['payment_channel'] = json_decode(setting_value('doku_payment_list'), true)[$key];
        }

        // get doku server settings
        $serverconfig = $this->getServerConfig();

        // set doku parameter
        $assets['action']               = $serverconfig['action'];

        $assets['mall_id']              = $serverconfig['mallid'];
        $assets['shared_key']           = $serverconfig['sharedkey'];
        $assets['chain_number']         = $serverconfig['chain'];
        $assets['amount']               = number_format($this->session->userdata('nominal'), 2, '.', '');
        $assets['transidmerchant']      = $this->session->userdata('donation_id');
        $assets['words']                = sha1(trim($assets['amount']).
                                            trim($assets['mall_id']).
                                            trim($assets['shared_key']).
                                            trim($assets['transidmerchant']));
        $assets['request_datetime']     = date("YmdHis"); // for add order
        $assets['currency']             = 360; // IDR currency only : 360
        $assets['session_id']           = session_id();
        $assets['name']                 = $this->session->userdata('name');
        $assets['email']                = $this->session->userdata('email');
        $assets['phone']                = $this->session->userdata('phone');
        $assets['message']              = "Transaction request start";

        // $this->session->userdata('order_name') set on contoller campaign_detail
        $assets['basket']               = $this->session->userdata('order_name') . ',' . $assets['amount'] . ',1,' . $assets['amount'] .';'; 
        // $assets['basket']               = 'tes,' . $assets['amount'] . ',1,' . $assets['amount'] .';'; 
        
        // add doku order
        // pre($assets);
        $this->model_doku->addOrderDoku($assets);

        $this->load->view('payment/' . $payment_method . '_view', $assets);
    }

    public function getServerConfig()
    {
        $dokuconfig = array();
        $data['server_set'] = setting_value("doku_mode");

        $dokuconfig['mallid']       = setting_value("doku_mallid");
        $dokuconfig['sharedkey']    = setting_value("doku_sharedkey");
        $dokuconfig['chain']        = 'NA';
                
        if ( $data['server_set']=='development')
        {               
            $dokuconfig['action']       = 'http://staging.doku.com/Suite/Receive';
            $dokuconfig['check_status'] = 'http://staging.doku.com/Suite/CheckStatus';                      
        }
        else
        {
            $dokuconfig['action']       = 'https://pay.doku.com/Suite/Receive';                     
            $dokuconfig['check_status'] = 'https://pay.doku.com/Suite/CheckStatus';                     
        }
                
        return $dokuconfig;
    }

    public function dokuidentify() {
        $use_identify = 1;
        $post = $this->input->post();

        // add return from doku
        $array = array(
            'message' => json_encode($post),
            'process_type' => 'Identify'
        );

        $this->db->insert('dokuonecheckout', $array);

        if ( intval($use_identify) == 1 )
        {   
            if ( empty($post) )
            {
                echo "Stop : Access Not Valid";
                action_log('DOKU', 'doku', '', 'Doku', "DOKU Identify Not in Correct Format - IP Logged ".$this->getipaddress());     
                die;
            }
                            
            if (substr($this->getipaddress(),0,strlen($this->ip_range)) !== $this->ip_range)
            {

                echo "Stop : IP Not Allowed";
                action_log('DOKU', 'doku', '', 'Doku', "DOKU Identify From IP Not Allowed - IP Logged ".$this->getipaddress());
            }
            else
            {
                $this->load->model('model_order');
                $this->load->model('model_doku');

                // get doku server settings
                $serverconfig = $this->getServerConfig();
                
                $trx['amount']           = $post['AMOUNT'];
                $trx['transidmerchant']  = $post['TRANSIDMERCHANT']; 
                $trx['payment_channel']  = $post['PAYMENTCHANNEL'];
                $trx['session_id']       = $post['SESSIONID'];
                $trx['payment_code']     = $post['PAYMENTCODE'];
                $trx['process_datetime'] = date("Y-m-d H:i:s");
                $trx['process_type']     = 'IDENTIFY';
                $trx['ip_address']       = $this->getipaddress();
                $trx['message']          = "Identify process message come from DOKU";

                $data = array(
                        'status_id'     => 1,
                        'comment'       => 'DOKU Payment Process'
                    );
                $where = array(
                        'id'    => $trx['transidmerchant']
                    );
                
                if ($post['PAYMENTCHANNEL'] == 36) {
                    $this->model_order->updateOrder($data, $where);
                    $this->model_doku->addOrderDoku($trx);

                    $this->model_order->addOrderHistory($trx['transidmerchant'], 1, 'DOKU Payment Initiate', true, $trx);
                }
            }
        }
    }

    public function dokunotify() {
        $this->load->model('model_order');
        $post = $this->input->post();

        // add return from doku
        $array = array(
            'message' => json_encode($post),
            'process_type' => 'Notify'
        );

        $this->db->insert('dokuonecheckout', $array);

        if ( empty($post) )
        {
            echo "Stop : Access Not Valid";
            action_log('DOKU', 'doku', '', 'Doku', "DOKU Notify Not in Correct Format - IP Logged ".$this->getipaddress());     
            die;
        }
                        
        if (substr($this->getipaddress(),0,strlen($this->ip_range)) !== $this->ip_range)
        {
            echo "Stop : IP Not Allowed";
            action_log('DOKU', 'doku', '', 'Doku', "DOKU Notify From IP Not Allowed - IP Logged ".$this->getipaddress());
        }
        else
        {       
            $trx = array();
            
            $trx['words']                     = $post['WORDS'];
            $trx['amount']                    = $post['AMOUNT'];
            $trx['transidmerchant']           = $post['TRANSIDMERCHANT'];
            $trx['result_msg']                = $post['RESULTMSG'];            
            $trx['verify_status']             = $post['VERIFYSTATUS'];       
            
            $serverconfig = $this->getServerConfig();
            
            $words = sha1(trim($trx['amount']).
                        trim($serverconfig['mallid']).
                        trim($serverconfig['sharedkey']).
                        trim($trx['transidmerchant']).
                        trim($trx['result_msg']).
                        trim($trx['verify_status']));

            if ( $trx['words']==$words )
            {           
                $trx['ip_address']            = $this->getipaddress();
                $trx['response_code']         = $post['RESPONSECODE'];
                $trx['approval_code']         = $post['APPROVALCODE'];
                $trx['payment_channel']       = $post['PAYMENTCHANNEL'];
                $trx['payment_code']          = $post['PAYMENTCODE'];
                $trx['session_id']            = $post['SESSIONID'];
                $trx['bank_issuer']           = $post['BANK'];
                $trx['creditcard']            = $post['MCN'];                   
                $trx['doku_payment_datetime'] = $post['PAYMENTDATETIME'];
                $trx['process_datetime']      = date("Y-m-d H:i:s");
                $trx['verify_id']             = $post['VERIFYID'];
                $trx['verify_score']          = $post['VERIFYSCORE'];
                $trx['notify_type']           = $post['STATUSTYPE'];
            
                switch ( $trx['notify_type'] )
                {
                    case "P":
                    $trx['process_type'] = 'NOTIFY';
                    break;
            
                    case "V":
                    $trx['process_type'] = 'REVERSAL';
                    break;
                }
            
                $result = $this->checkTrx($trx);

                if ( $result < 1 )
                {
                        echo "Stop : Transaction Not Found";
                        action_log('DOKU', 'doku', '', 'Doku', "DOKU Notify Can Not Find Transactions - IP Logged ".$this->getipaddress());
                        die;            
                }
                else
                {                                       
                    $this->load->model('model_order');
                    $this->load->model('model_doku');

                    $use_edu = intval(setting_value('doku_review_edu'));
                
                    switch (TRUE)
                    {
                        case ( $trx['result_msg']=="SUCCESS" && $trx['notify_type']=="P" && in_array($trx['payment_channel'], array("36","14")) ):
                        $trx['message'] = "Notify process message come from DOKU. Payment Success : Completed";
                        $this->model_order->addOrderHistory($trx['transidmerchant'], 4, $trx['message'], false);
                        $trx['message'] = "Dana sudah masuk";
                        $this->model_order->addOrderHistory($trx['transidmerchant'], 3, $trx['message'], true);
                        break;

                        case ( $trx['result_msg']=="SUCCESS" && $trx['notify_type']=="P" && $use_edu == 1 ):
                        $trx['message'] = "Notify process message come from DOKU. Payment success but wait for EDU verification : Processed";
                        $this->model_order->addOrderHistory($trx['transidmerchant'], 4, $trx['message'], true);
                        break;

                        case ( $trx['result_msg']=="SUCCESS" && $trx['notify_type']=="P" && $use_edu == 0 ):
                        $trx['message'] = "Notify process message come from DOKU. Payment Success : Completed";
                        $this->model_order->addOrderHistory($trx['transidmerchant'], 4, $trx['message'], false);
                        $trx['message'] = "Dana sudah masuk";
                        $this->model_order->addOrderHistory($trx['transidmerchant'], 3, $trx['message'], true);
                        break;

                        case ( $trx['result_msg']=="FAILED" && $trx['notify_type']=="P" ):
                        $trx['message'] = "Notify process message come from DOKU. Payment Failed";
                        $this->model_order->addOrderHistory($trx['transidmerchant'], 5, $trx['message'], true);
                        break;

                        case ( $trx['notify_type']=="V" ):
                        $trx['message'] = "Notify process message come from DOKU. Payment Void by EDU : Denied";
                        $this->model_order->addOrderHistory($trx['transidmerchant'], 5, $trx['message']);
                        break; 

                        default:
                        $trx['message'] = "Notify process message come from DOKU. Payment Failed by default : Cancelled";
                        $this->model_order->addOrderHistory($trx['transidmerchant'], 5, $trx['message']);
                        break;
                    }
                
                    $this->model_doku->addOrderDoku($trx);
                    
                    echo "Continue";
                }
            }
            else
            {
                echo "Stop : Request Not Valid";
                action_log('DOKU', 'doku', '', 'Doku', "DOKU Notify Words Not Correct - IP Logged ".$this->getipaddress());
                die;
            }
        }
    }

    public function dokuredirect()
    {
        $post = $this->input->post();

        // add return from doku
        $array = array(
            'message' => json_encode($post),
            'process_type' => 'Redirect'
        );

        $this->db->insert('dokuonecheckout', $array);

        if ( empty($post) )
        {
            echo "Stop : Access Not Valid";
            action_log('DOKU', 'doku', '', 'Doku', "DOKU Redirect Not in Correct Format - IP Logged ".$this->getipaddress());
            redirect(base_url('checkout/doku_cancel'));        
            //die;
        }
        
        $trx['words']                = $post['WORDS'];
        $trx['amount']               = $post['AMOUNT'];
        $trx['transidmerchant']      = $post['TRANSIDMERCHANT']; 
        $trx['status_code']          = $post['STATUSCODE'];
        
        if ( isset($post['PAYMENTCODE']) )
            {
                $trx['payment_code']                 = $post['PAYMENTCODE'];
            }

        $serverconfig = $this->getServerConfig();
        
        $words = sha1(trim($trx['amount']).
                    trim($serverconfig['sharedkey']).
                    trim($trx['transidmerchant']).
                    trim($trx['status_code']));
        
        if ( $trx['words'] == $words )
        {
            // $this->load->model('model_order');
            $use_edu  = intval(setting_value('doku_review_edu'));
            
            $trx['payment_channel']  = $_POST['PAYMENTCHANNEL'];
            $trx['session_id']       = $_POST['SESSIONID'];
            $trx['ip_address']       = $this->getipaddress();
            $trx['process_datetime'] = date("Y-m-d H:i:s");
            $trx['process_type']     = 'REDIRECT';
            
            # Skip notify checking for VA / ATM / ALFA Payment
            if ( in_array($trx['payment_channel'], array("36","14")) && $trx['status_code'] == "5511" )
            {
                $trx['message'] = "Redirect process come from DOKU. Payment channel using VA / Alfa, transaction is pending for payment";  
                $status         = "pending";                                    
                $return_message = "This is your Payment Code : ".$trx['payment_code']."<br>Please do the payment before expired.<br>If you need help for payment, please contact our customer service.<br>";
                // $this->model_order->addOrderHistory($trx['transidmerchant'], 1, $trx['message']);
            }
            else
            {
                switch ($trx['status_code'])
                {
                    case "0000":
                    $result_msg = "SUCCESS";
                    break;
                    
                    default:
                    $result_msg = "FAILED";
                    break;
                }
                    
                $result = $this->checkTrx($trx, 'NOTIFY', $result_msg);
                
                if ( $result < 1 )
                {
                    $check_result_msg = $this->doku_check_status($trx);
                    
                    if ( $check_result_msg == 'SUCCESS' )
                    {
                        if ( intval($use_edu) == 1 )
                        {                   
                            $trx['message'] = "Redirect process with no notify message come from DOKU. Transaction is Success, wait for EDU Verification. Please check on Back Office.";  
                            $status         = "on-hold";                                    
                            $return_message = "Thank you for shopping with us. We will process your payment soon.";
                            // $this->model_order->addOrderHistory($trx['transidmerchant'], 4, $trx['message'], false);                                                       
                        }
                        else
                        {
                            $trx['message'] = "Redirect process with no notify message come from DOKU. Transaction is Success. Please check on Back Office.";  
                            $status         = "completed";              
                            $return_message = "Your payment is success. We will process your order. Thank you for shopping with us.";
                            // $this->model_order->addOrderHistory($trx['transidmerchant'], 4, $trx['message'], false);                                                                                 
                        }               
                    }
                    else
                    {
                        $trx['message'] = "Redirect process with no notify message come from DOKU. Transaction is Failed. Please check on Back Office."; 
                        $status         = "failed";             
                        $return_message = "Your payment is failed. Please check your payment detail or please try again later.";
                        // $this->model_order->addOrderHistory($trx['transidmerchant'], 5, $trx['message'], false);                                               
                    }
                }
                else
                {                               
                    if ( $trx['status_code']=="0000" )
                    {
                        if ( intval($use_edu) == 1 )
                        {                   
                            $trx['message'] = "Redirect process message come from DOKU. Transaction is Success, wait for EDU Verification";  
                            $status         = "on-hold";                                    
                            $return_message = "Thank you for shopping with us. We will process your payment soon.";
                            // $this->model_order->addOrderHistory($trx['transidmerchant'], 4, $trx['message'], false);
                        }
                        else
                        {
                            $trx['message'] = "Redirect process message come from DOKU. Transaction is Success";  
                            $status         = "completed";              
                            $return_message = "Your payment is success. We will process your order. Thank you for shopping with us.";
                            // $this->model_order->addOrderHistory($trx['transidmerchant'], 4, $trx['message'], false);
                        }
                    }
                    else
                    {
                        $trx['message'] = "Redirect process message come from DOKU. Transaction is Failed";  
                        $status         = "failed";             
                        $return_message = "Your payment is failed. Please check your payment detail or please try again later.";
                        // $this->model_order->addOrderHistory($trx['transidmerchant'], 5, $trx['message']);                                             
                    }                                                       
                }
            }

            # Insert transaction redirect to table onecheckout
            $this->load->model('model_doku');
            $this->model_doku->addOrderDoku($trx);                    
                                    
            switch ( $status )
            {
                case "completed":
                $this->dokusuccess($trx);
                break;
                
                case "failed":
                $this->dokucancel();        
                break;
        
                case "pending":
                $this->dokupending($trx);
                break;
                
                case "on-hold":
                $this->dokuonhold();
                break;
            }                       
                        
        }
        else
        {
            echo "Stop : Request Not Valid";
            action_log('DOKU', 'doku', '', 'Doku', "DOKU Redirect Words Not Correct - IP Logged ".$this->getipaddress());      
            die;
        }  
    }

    public function dokureview()
    {       
        $post = $this->input->post();

        // add return from doku
        $array = array(
            'message' => json_encode($post),
            'process_type' => 'Review'
        );

        $this->db->insert('dokuonecheckout', $array);

        if ( empty($post) )
        {
            echo "Stop : Access Not Valid";
            action_log('DOKU', 'doku', '', 'Doku', "DOKU Review Not in Correct Format - IP Logged ". $this->getipaddress());      
            die;
        }

        $use_review = setting_value('doku_review_edu');
        if ( $use_review==1 )
        {                           
            if (substr($this->getipaddress(),0,strlen($this->ip_range)) !== $this->ip_range)
            {
                echo "Stop : IP Not Allowed";
                action_log('DOKU', 'doku', '', 'Doku', "DOKU Review From IP Not Allowed - IP Logged ".$this->getipaddress());
            }
            else
            {
                $serverconfig = $this->getServerConfig();

                $trx['amount']                = $post['AMOUNT'];
                $trx['transidmerchant']       = $post['TRANSIDMERCHANT'];
                $trx['result_msg']            = $post['RESULTMSG'];            
                $trx['verify_status']         = $post['VERIFYSTATUS'];        
                $trx['words']                 = $post['WORDS'];
                                                
                $words = sha1(trim($trx['amount']).
                              trim($serverconfig['mallid']).
                              trim($serverconfig['sharedkey']).
                              trim($trx['transidmerchant']).
                              trim($trx['result_msg']).
                              trim($trx['verify_status']));

                if ( $trx['words'] == $words )
                {                              
                    $trx['process_datetime']      = date("Y-m-d H:i:s");
                    $trx['process_type']          = 'REVIEW';
                    $trx['ip_address']            = $this->getipaddress();
                    $trx['notify_type']           = $post['STATUSTYPE'];                
                    $trx['notify_type']           = $post['STATUSTYPE'];
                    $trx['response_code']         = $post['RESPONSECODE'];
                    $trx['approval_code']         = $post['APPROVALCODE'];
                    $trx['payment_channel']       = $post['PAYMENTCHANNEL'];
                    $trx['payment_code']          = $post['PAYMENTCODE'];
                    $trx['session_id']            = $post['SESSIONID'];
                    $trx['bank_issuer']           = $post['BANK'];
                    $trx['creditcard']            = $post['MCN'];                   
                    $trx['doku_payment_datetime'] = $post['PAYMENTDATETIME'];
                    $trx['verify_id']             = $post['VERIFYID'];
                    $trx['verify_score']          = $post['VERIFYSCORE'];
                    
                    $result = $this->checkTrx($trx);
                    
                    if ( $result < 1 )
                    {
                        echo "Stop : Transaction Not Found";
                        action_log('DOKU', 'doku', '', 'Doku', "DOKU Notify Can Not Find Transactions - IP Logged ".$this->getipaddress());
                        die;            
                    }
                    else
                    {                    
                        $this->load->model('model_order');
                        $this->load->model('model_doku');
                        $this->model_doku->addOrderDoku($trx);
                        
                        switch (TRUE)
                        {
                            case ( $trx['verify_status']=="APPROVE" ):
                            $this->model_order->addOrderHistory($trx['transidmerchant'], 4, 'Payment Process Success'.$trx['verify_status'], true);
                            break;
                            
                            case ( $trx['verify_status']=="REVIEW" ):
                            $this->model_order->addOrderHistory($trx['transidmerchant'], 4, 'Payment Process Success'.$trx['verify_status'], true);
                            break;
                            
                            case ( $trx['verify_status']=="REJECT" || $trx['verify_status']=="HIGHRISK" || $trx['verify_status']=="NA" ):
                            $this->model_order->addOrderHistory($trx['transidmerchant'], 5, 'DOKU Verification result is bad : '.$trx['verify_status'], true);
                            break;
                            
                            default:
                            $this->model_order->addOrderHistory($trx['transidmerchant'], 5, 'DOKU Verification result is bad', true);
                            break;
                        }
                        
                        echo "Continue";
                    }
                }
                else
                {
                    echo "Stop : Request Not Valid";
                    action_log('DOKU', 'doku', '', 'Doku', "DOKU Redirect Words Not Correct - IP Logged ".$this->getipaddress());        
                    die;                    
                }
            }        
        }
    }

    public function dokucancel()
    {
        redirect(base_url('checkout/doku_cancel'));
    }

    public function dokusuccess($data = array()) 
    {
        if (isset($data['transidmerchant'])) {
            $this->session->set_flashdata('flash_donation_id', $data['transidmerchant']);
        }
        
        redirect(base_url('checkout/doku_success'));
    }

    public function dokupending($data = array())
    {
        if (isset($data['payment_code'])) {
            $this->session->set_flashdata('flash_payment_code', $data['payment_code']);
        }
        redirect(base_url('checkout/doku_pending'));
    }

    public function dokuonhold()
    {
        redirect(base_url('checkout/doku_onhold'));
    }

    public function getipaddress()    
    {
        if (!empty($_SERVER['HTTP_CLIENT_IP']))
        {
            $ip=$_SERVER['HTTP_CLIENT_IP'];
        }
        elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))
        {
            $ip=$_SERVER['HTTP_X_FORWARDED_FOR'];
        }
        else
        {
            $ip=$_SERVER['REMOTE_ADDR'];
        }

        return $ip;
    }

    public function checkTrx($trx, $process='REQUEST', $result_msg='')
    {
        if ( $result_msg == "PENDING" ) return 0;
        
        $this->load->model('model_order');

        $where = array(
                'process_type'      => $process,
                'transidmerchant'   => $trx['transidmerchant'],
                'amount'            => $trx['amount'],
                'session_id'        => $trx['session_id'],
            );

        if ( !empty($result_msg) )
        {
            $where['result_msg'] = $result_msg;
        }       
        
        $query = $this->model_order->getDoku($where);        
        return $query->num_rows();
    }

    public function doku_check_status($transaction)
    {       
        $serverconfig = $this->getServerConfig();
        $result = $this->getCheckStatusList($transaction);
        
        if ( empty($result) )
        {
            return "FAILED";
        }

        $trx = $result;
        
        $words = sha1(trim($serverconfig['mallid']).
                    trim($serverconfig['sharedkey']).
                    trim($trx['transidmerchant']) );
                                                
        $data = "MALLID=".$serverconfig['mallid']."&CHAINMERCHANT=".$serverconfig['chain']."&TRANSIDMERCHANT=".$trx['transidmerchant']."&SESSIONID=".$trx['session_id']."&PAYMENTCHANNEL=&WORDS=".$words;
        
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $serverconfig['check_status']);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 20); 
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, true);        
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        $output = curl_exec($ch);
        $curl_errno = curl_errno($ch);
        $curl_error = curl_error($ch);
        curl_close($ch);        
        
        if ($curl_errno > 0)
        {
            #return "Stop : Connection Error";
        }             
        
        libxml_use_internal_errors(true);
        $xml = simplexml_load_string($output);
        
        if ( !$xml )
        {
            $this->updateCountCheckStatusTrx($transaction);
        }                
        else
        {
            $trx = array();
            $trx['ip_address']            = $this->getipaddress();
            $trx['process_type']          = "CHECKSTATUS";
            $trx['process_datetime']      = date("Y-m-d H:i:s");
            $trx['transidmerchant']       = (string) $xml->TRANSIDMERCHANT;
            $trx['amount']                = (string) $xml->AMOUNT;
            $trx['notify_type']           = (string) $xml->STATUSTYPE;
            $trx['response_code']         = (string) $xml->RESPONSECODE;
            $trx['result_msg']            = (string) $xml->RESULTMSG;
            $trx['approval_code']         = (string) $xml->APPROVALCODE;
            $trx['payment_channel']       = (string) $xml->PAYMENTCHANNEL;
            $trx['payment_code']          = (string) $xml->PAYMENTCODE;
            $trx['words']                 = (string) $xml->WORDS;
            $trx['session_id']            = (string) $xml->SESSIONID;
            $trx['bank_issuer']           = (string) $xml->BANK;
            $trx['creditcard']            = (string) $xml->MCN;
            $trx['verify_id']             = (string) $xml->VERIFYID;
            $trx['verify_score']          = (int) $xml->VERIFYSCORE;
            $trx['verify_status']         = (string) $xml->VERIFYSTATUS;            
            
            # Insert transaction check status to table onecheckout
            $this->load->model('model_doku');
            $this->model_doku->addOrderDoku($trx);
            
            return $xml->RESULTMSG;
        }                     
    }

    public function getCheckStatusList($trx='')
    {
        $this->load->model('model_doku');

        $where = array();
        if ( !empty($trx) )
        {
            $where['transidmerchant'] = $trx['transidmerchant'];
            $where['amount'] = $trx['amount'];
            $where['session_id'] = $trx['session_id'];
        }
        else
        {
            $where['check_status'] = 0;
        }
        
        $where['process_type'] = 'REQUEST';
        $where['count_check_status <='] = '3';

        $order = array(
                'order_by'  => 'trx_id',
                'sort'      => 'desc',
            );
        return $this->model_doku->getDoku($where, $like = array(), $order, 1)->row_array();
    }           

    public function updateCountCheckStatusTrx($trx)
    {
        $this->load->model('model_doku');
        $data = array(
                'count_check_status'        => 'count_check_status + 1',
                'check_status'              => 0,
            );
        $where = array(
                'process_type'              => 'REQUEST',
                'transidmerchant'           => $trx['transidmerchant'],
                'amount'                    => $trx['amount'],
                'session_id'                => $trx['session_id']
            );

        $this->model_doku->updateOrderDoku($data, $where);      
    }

    public function processdoku()
    {
        if ($this->input->post('TRANSIDMERCHANT'))
        {
            $transidmerchant = $this->input->post('TRANSIDMERCHANT');
            $this->load->model('model_order');

            if ($this->input->post('PAYMENTCHANNEL') != 36) {
                $this->model_order->addOrderHistory($transidmerchant, 1, 'DOKU Payment Initiate', true);
            }
        }
        else
        {
            echo "Stop : Access Not Valid";
            action_log('DOKU', 'doku', '', 'Doku', "DOKU Process Not in Correct Format - IP Logged ".$this->getipaddress());
        }
    }           
        
}