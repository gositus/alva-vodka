<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
use EspressoDev\InstagramBasicDisplay\InstagramBasicDisplay;

class Media extends CI_Controller {

	var $model         = 'model_front';

	public function __construct()
	{
		parent::__construct();
		$this->load->model($this->model);
	}

	public function index()
	{
		$title = 'Media';
		$media_photo = $this->model_front->get_media('',1);
		$media_video = $this->model_front->get_media('',2);

	
		
		$asset = array(
			'js'	=> array(),
			'css'	=> array(),
			'media_photo'	=> $media_photo,
			'media_video'	=> $media_video,
			'meta'	=> meta_create($title),
		);
		
		$this->load->view('template/header', $asset);	
		$this->load->view('media_view');
		$this->load->view('template/footer');
	}

	public function instagram()
	{
		$title = 'Instagram';
		$news = $this->model_front->get_news();
	
		
		$asset = array(
			'js'	=> array(),
			'css'	=> array(),
			'meta'	=> meta_create($title),
		);
		

        $instagram = new InstagramBasicDisplay([
            'appId' => setting_value('ig_app_id'),
            'appSecret' => setting_value('ig_app_secret'),
            'redirectUri' => setting_value('ig_redirect_url')
        ]);

        $token=setting_value('ig_token');
        $instagram->setAccessToken($token);

        $today= new DateTime(date('Y-m-d'));
//        $today= new DateTime('2020-12-01');
        $token_date = setting_value('ig_token_date');

        $token_date=new DateTime($token_date);
        $diff=date_diff($token_date,$today);

        if($diff->days>50){
            $refresh= $instagram->refreshToken($token, TRUE);
  			setting_update('ig_token',$refresh);
            setting_update('ig_token_date',date("Y-m-d"));
        }



        $media=($instagram->getUserMedia('me',12));
        $asset['instagram']=$media->data;
        // pre($asset);

		$this->load->view('template/header', $asset);	
		$this->load->view('media_ig_view');
		$this->load->view('template/footer');
	}
}