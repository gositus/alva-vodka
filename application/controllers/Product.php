<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Product extends CI_Controller {

	var $model         = 'model_front';

	public function __construct()
	{
		parent::__construct();
		$this->load->model($this->model);
	}

	public function index()
	{
		$title            = 'Product';
		$product_category = $this->model_front->get_product_category();
		$product          = $this->model_front->get_product();

		
		$asset = array(
			'js'               => array(),
			'css'              => array(),
			'product'          => $product,
			'product_category' => $product_category,
			'meta'             => meta_create($title),
		);

		$this->load->view('template/header', $asset);	
		$this->load->view('product_view');
		$this->load->view('template/footer');
	}

	public function detail($seo_url="")
	{
		$product	= $this->model_front->get_product($seo_url,'',TRUE);
		$title		= ($product['name']);
		$image		= (!empty($product['image'])) ?  base_url('lib/images/item/' . $product['image']) :  base_url('lib/assets/gositus/default-image.png') ;
		
		if(count($product) < 1 ) redirect(base_url('product'));
		total_visitor('item',$product['id']);

		
		$asset = array(
			'js'          => array(),
			'css'         => array(),
			'product'     => $product,
			'meta'        => array(
				'title'       => meta_create($title,'web_title'),
				'keyword'     => meta_create($title,'meta_keyword'),
				'description' => meta_create($title,'meta_description'),
				'image'       => $image
			),
		);
	
		$this->load->view('template/header', $asset);	
		$this->load->view('product_detail_view');
		$this->load->view('template/footer');
	}

	public function category($seo_url="")
	{
		$category = $this->model_front->get_product_category($seo_url,'',TRUE);
		$product  = $this->model_front->get_product('','','',$category['id']);	
		$title    = $category['name'];
		$image    = (!empty($category['image'])) ?  base_url('lib/images/item/' . $category['image']) :  base_url('lib/assets/gositus/default-image.png') ;

		if(count($category) < 1 ) redirect(base_url('product'));
		total_visitor('item_category',$category['id']); // for dashboard report

		
		$asset = array(
			'js'       => array(),
			'css'      => array(),
			'product'  => $product,
			'category' => $category,
			'meta'     => array(
				'title'       => meta_create($title,'web_title'),
				'keyword'     => meta_create($title,'meta_keyword'),
				'description' => meta_create($title,'meta_description'),
				'image'       => $image,
			),	
		);
	
		$this->load->view('template/header', $asset);	
		$this->load->view('category_detail_view');
		$this->load->view('template/footer');
	}
}