<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class About extends CI_Controller {

	var $model         = 'model_front';

	public function __construct()
	{
		parent::__construct();
		$this->load->model($this->model);
	}

	public function index()
	{		
		$title = 'About';
		$page  = $this->model_front->get_page(2,FALSE); // section about us
		
		
		$asset = array(
			'js'	=> array(),
			'css'	=> array(),
			'page'  => $page,
			'meta'  => meta_create($title)
		);
		
		$this->load->view('template/header', $asset);	
		$this->load->view('about_view');
		$this->load->view('newsletter_view');
		$this->load->view('template/footer');
	}
}