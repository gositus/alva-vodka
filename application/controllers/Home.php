<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {

	var $model         = 'model_front';

	public function __construct()
	{
		parent::__construct();
		$this->load->model(array($this->model, 'model_message'));
	}

	public function index()
	{
		$this->form_validation->set_rules('name', 'Name', 'required');
		$this->form_validation->set_rules('email', 'Email', 'required');
		$this->form_validation->set_error_delimiters('<li>', '</li>');

		if ($this->form_validation->run() == TRUE && empty($this->input->post('kotakkosong'))){
			$post          = $this->input->post();
			$email_ext     = substr($post['email'], strrpos($post['email'], '.') +1);
			$banned_domain = select_all_row('master_banned_domain',array('flag'=>1));

			foreach ($banned_domain as $key => $value){
				$block_domain[] = $value['name'];
			}

			if(!in_array($email_ext, $block_domain, true)){
				// Whitelist IP
				$cek_ip = select_all_row('master_banned_ip',array('name'=>$this->input->ip_address()), TRUE);				
				if(empty($cek_ip)){
					// Verifikasi Captcha
					$captcha = trim($this->input->post("g-recaptcha-response"));
					$cek     = verify_captcha($captcha, setting_value('invisible_secret_key'));
					if(!empty($cek['success']) || $cek['success'] == 1){
						// All criteria success
						$contact = $this->model_message->insert_contact($post);
						total_visitor('leads'); //count lead for dashboard
					}
				}
			}

			if(!empty($contact)){
				$this->session->set_flashdata('success', '<strong>Thank you</strong>, your message has been sent.');
			}
			else{
				$this->session->set_flashdata('error', '<strong>Error</strong> occurred when sending your message');
				$this->session->set_flashdata('input', $post);
			}

			redirect(base_url('#contact'));
		}
		else{
			
			total_visitor(); // count leads from dashboard
			//, 'plugins/scrollpage/jquery.scrollify',
			//'plugins/scrollpage/jquery.malihu.PageScroll2id',
			$asset = array(
				'js'        => array('plugins/slick-1.8.1/slick/slick.min',  'plugins/flipster/dist/jquery.flipster.min', 'plugins/scrollpage/jquery.malihu.PageScroll2id', 'plugins/scrollpage/jquery.scrollify'),
				'css'       => array('plugins/slick-1.8.1/slick/slick', 'plugins/flipster/dist/jquery.flipster.min', 'plugins/fullPage/fullpage'),
				'item'      => $this->model_front->get_product(),
				'cocktails' => $this->model_front->get_media("", 2),
				'gallery'   => $this->model_front->get_media("", 1)
			);
			
			$this->load->view('template/header', $asset);	
			$this->load->view('home_view');
			$this->load->view('template/footer');
		}
	}
	public function cari_captcha()
	{
		$cap = go_captcha();
		
		$this->session->set_userdata('captchaWord',$cap['word']);
		
		if(isset($cap['image'])) echo $cap['image'];
		elseif(isset($this->session->userdata['anti_spam'])) echo 'You have failed too many times, Please try again in an hour.';
		else echo 'Clicking too fast.' ;		
	}
	
	public function cek_ses($key=""){
		$ses_cap =  $this->session->userdata['captchaWord'];
		
		if($key== $ses_cap || strtolower($ses_cap) == $key) echo 'true';
		else echo 'false';
	}
	
	public function new_token(){
		echo $this->security->get_csrf_hash();
	}
	
	public function count($table='', $id='')
	{
 		$row = $this->db->get_where($table, array('id' =>$id))->row_array();

		if(!empty($row)) count_it($table,$id,$row);

		if(!empty($row['seo_url'])) redirect(base_url($table . '/' . $row['seo_url']));
		else redirect($row['url']);
	}
	
	public function favicon(){
		$this->load->view("template/favicon_manifest");
	}
}