<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Member extends CI_Controller {

	public function __construct()
    {
        parent::__construct();
        $this->load->model('model_member');

    }

	public function index()
	{
		if(empty($this->session->userdata('member_id'))) redirect(base_url('member/login'));

		
    	$asset = array(
			'title'	=> 'Member Registration',
			'js'	=> array(),
			'css'	=> array()
		);

		$this->load->view('template/header', $asset);	
		$this->load->view('member/dashboard_view');
		$this->load->view('template/footer');
    }
	
	public function login()
	{
		if(!empty($this->session->userdata('member_id'))) redirect(base_url('member'));

		if($this->input->post())
		{
    		$captcha = trim($this->input->post("g-recaptcha-response"));
       		$cek = verify_captcha($captcha);
			
       		if($cek['success']==1)
       		{
       			$this->form_validation->set_rules('email', 'Email', 'trim|valid_email|required');
       			$this->form_validation->set_rules('password', 'Password', 'trim|required');
       			$this->form_validation->set_error_delimiters('<li>', '</li>');
				
       			if ($this->form_validation->run() == TRUE)
				{					
					$login = $this->model_member->check_login();
					
					if($login== true)
					{
						$this->session->set_flashdata("pesan", "Sukses");
						redirect(base_url('member'));
					}
					else
					{
						$this->session->set_flashdata("pesan", "Password atau email salah");
						redirect(base_url('member/login'));
					}
				}
				else
				{
					$this->session->set_flashdata("pesan", "Validasi salah");
					redirect(base_url('member/login'));
				}
       		}
			else
			{
				$this->session->set_flashdata("pesan", "Captcha Salah");
				redirect(base_url('member/login'));
			}

		} else 
		{
			$asset = array();
			$title = 'Member Login';
			$asset = array_merge( $asset, 
				array(
					'js'	=> array(),
					'css'	=> array('gositus/bootstrap.min'),
					'meta'  => meta_create($title)
				)
			);

			$this->load->view('template/header', $asset);	
			$this->load->view('member/login_view');
			$this->load->view('template/footer');
		}
	}

   	public function register()
	{

   		if(!empty($this->session->userdata('member_id'))){
    		 redirect(base_url('member'));
    	}

		if($this->input->post())
		{
    		$captcha = trim($this->input->post("g-recaptcha-response"));
       		$cek = verify_captcha($captcha);
       		if($cek['success']==1)
       		{
       			$this->form_validation->set_rules('email', 'Email', 'trim|valid_email|required|callback_check_email_exist');
       			$this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[8]');
       			
       			$this->form_validation->set_rules('name', 'Full Name', 'trim');
       			$this->form_validation->set_rules('gender', 'Gender', 'trim|required');
       			$this->form_validation->set_error_delimiters('<li>', '</li>');
       			if ($this->form_validation->run() == TRUE)
				{				
					$this->session->set_flashdata('pesan', 'Account Created');	
					$register = $this->model_member->insert_member();
					redirect(base_url('member/login'));
					
				}
				else
				{
					$this->session->set_flashdata('pesan', 'Check Your input');
					redirect(base_url('member/register'));
				}
       		}
			else
			{
				$this->session->set_flashdata('pesan', 'Invalid Captcha');
				redirect(base_url('member/register'));
			}

		} else 
		{
			$asset = array(
					'title'	=> 'Member Register',
					'js'	=> array('goadmin/plugins/jquery-ui.min','gositus/datepicker-run'),
					'css'	=> array('gositus/bootstrap.min','goadmin/plugins/jqueryUI/jquery-ui.min')
				);

			$this->load->view('template/header', $asset);	
			$this->load->view('member/register_view');
			$this->load->view('template/footer');
		}
	}

   	public function account()
	{
   		if(empty($this->session->userdata('member_id'))){
    		 redirect(base_url('member/login'));
    	}

		if($this->input->post())
		{
    		$captcha = trim($this->input->post("g-recaptcha-response"));
       		$cek = verify_captcha($captcha);
       		if($cek['success']==1)
       		{
       			
       			$this->form_validation->set_rules('name', 'Full Name', 'trim');
       			$this->form_validation->set_rules('gender', 'Gender', 'trim|required');

       			if($this->input->post('password') != ''){
       				$this->form_validation->set_rules('re_password', 'Re- Password', 'trim|min_length[8]|required');
       			}
       			$this->form_validation->set_error_delimiters('<li>', '</li>');
       			if ($this->form_validation->run() == TRUE)
				{					
					$register = $this->model_member->update_member();
					redirect(base_url('member/account'));
					
				}
				else
				{
					redirect(base_url('member/account'));
				}
       		}
			else
			{
				redirect(base_url('member/account'));
			}

		} else 
		{
			$title = 'Member Register';

			$asset = array(
				'js'	=> array('goadmin/plugins/jquery-ui.min','gositus/datepicker-run'),
				'css'	=> array('gositus/bootstrap.min','goadmin/plugins/jqueryUI/jquery-ui.min'),
				'user'	=> select_all_row('member',array('id' => $this->session->userdata('member_id')) , TRUE),
				'meta'  => meta_create($title)
			);

			$this->load->view('template/header', $asset);	
			$this->load->view('member/account_view');
			$this->load->view('template/footer');
		}
	}

	public function logout()
	{
		if(!empty($this->session->userdata('member_id'))){				
			$input = array( 
				'member_id'  => $this->session->userdata('member_id'),
				'ip'         => $this->input->ip_address(),
				'user_agent' => get_user_agent(),
				'action'	 => 'LOGOUT'
			);
			
			$this->db->insert('member_log', $input);
			$this->session->unset_userdata('member_id');		
			
		}
		redirect(base_url('member'));
	}

	public function check_pass_regis()
	{
		$str = $this->security->xss_clean($this->input->post('password'));
		
		if(preg_match('#[0-9]+#', $str) && preg_match('#[A-Z]+#', $str) && preg_match('#[a-z]+#', $str) && preg_match('#[\W]+#', $str)){
			echo json_encode(TRUE);
		}
		else{
			echo json_encode(FALSE);
		}
	}

	public function check_email_exist($str)
	{
		$check = select_all_row("member", array('email'=>$str),TRUE);
		
		if($check) return FALSE;
		else return TRUE;
	}


	public function register_twitter()
	{
		$name	= input_clean($this->input->post('tName'));
		$id		= input_clean($this->input->post('tId'));
		$exist	= $this->db->get_where('member', array('social_media_account'=>'tw','social_media_id'=> $id, 'flag'=>1))->row_array();

		if($exist){
			$sess_data = array(
				'member_id'		=> $exist['id']
			);
			
			$this->session->set_userdata($sess_data);
			
	        redirect(base_url('member'));
		}else{
			$data = array(
				'name'                 => $name,
				'phone'                => '',
				'dob'                  => '',
				'gender'               => '',
				'email'                => '',
				'forget_token'         => '',
				'forget_times'         => '',
				'last_forget'          => '',
				'social_media_id'      => $id,
				'social_media_account' => 'tw'
			);
			
            $this->db->insert('member', $data);

            $member = $this->db->get_where('member', array('social_media_account'=>'tw','social_media_id'=> $id, 'flag'=>1))->row_array();

			$sess_data = array(
				'member_id'		=> $member['id'],
			);
			
			$this->session->set_userdata($sess_data);

            redirect(base_url('member'));
		}
	}

	public function register_google($gender="")
	{
		$fname	= input_clean($this->input->post('gFName'));
		$lname	= input_clean($this->input->post('gLName'));
		$email	= input_clean($this->input->post('gEmail'));
		$id 	= input_clean($this->input->post('gId'));
		$gender = input_clean($this->input->post('gGender'));
		
		$exist = $this->db->get_where('member', array('email'=>$email, 'flag'=>1))->row_array();

		if($exist){
			$sess_data = array(
				'member_id'		=> $exist['id']
			);
			
			$this->session->set_userdata($sess_data);
	        redirect(base_url('member'));
		}

		else{
			$data = array(
				'name'                 => $fname . ' ' . $lname ,
				'phone'                => '',
				'dob'                  => '',
				'gender'               => substr($gender,0,1),
				'email'                => $email,
				'forget_token'         => '',
				'forget_times'         => '',
				'last_forget'          => '',
				'social_media_id'      => $id,
				'social_media_account' => 'g+'
			);

            $this->db->insert('member', $data);

            $member = $this->db->get_where('member', array('email'=> $email, 'flag'=>1))->row_array();

            $sess_data = array(
				'member_id'		=> $member['id']
			);

			$this->session->set_userdata($sess_data);

            redirect(base_url('member'));
		}
	}

    public function register_facebook()
    {
        $name	= input_clean($this->input->post('name'));
        $id		= input_clean($this->input->post('id'));
        $email	= input_clean($this->input->post('email'));
        $exist	= $this->db->get_where('member', array(
        	'email' => $email,
        	'flag' => 1
        ))->row_array();

        if($exist) {
            $sess_data = array(
				'member_id'		=> $exist['id']
			);

			$this->session->set_userdata($sess_data);
            echo "success";
        } else {

        	$data = array(
				'name'                 => $name,
				'phone'                => '',
				'dob'                  => '',
				'gender'               => '',
				'email'                => $email,
				'forget_token'         => '',
				'forget_times'         => '',
				'last_forget'          => '',
				'social_media_id'      => $id,
				'social_media_account' => 'fb'
			);

            $this->db->insert('member', $data);

            $member = $this->db->get_where('member', array('email'=> $email, 'flag'=>1))->row_array();

            $sess_data = array(
				'member_id'		=> $member['id']
			);

			$this->session->set_userdata($sess_data);
            echo "success";
        }
    }
	
    public function forgot()
	{
		$title = 'Forgot Password';
		if($this->input->post()){
			$captcha = trim($this->input->post("g-recaptcha-response"));
       		$cek = verify_captcha($captcha);
       		
			if($cek['success']==1) {

				$check = $this->model_member->forgot();
				if($check == 'success') $this->session->set_flashdata('pesan', 'Please check your email');
				else $this->session->set_flashdata('pesan', $check);
				
			} else {
					$this->session->set_flashdata('pesan', 'Invalid Captcha');
			}
			redirect(base_url('member/forgot'));
		}

		
		$asset = array(
			'js'	=> array('goadmin/plugins/jquery-ui.min','gositus/datepicker-run'),
			'css'	=> array('gositus/bootstrap.min','goadmin/plugins/jqueryUI/jquery-ui.min'),
			'meta'  => meta_create($title)
		);
		
		$this->load->view('template/header', $asset);	
		$this->load->view('member/forgot_view');
		$this->load->view('template/footer');
	}
	
	public function reset_password($token="")
	{
		$check = $this->model_member->reset($token);
		$title = "Reset Password";

		
		if(is_array($check)) {
			$asset = array(
				'js'	=> array('goadmin/plugins/jquery-ui.min','gositus/datepicker-run'),
				'css'	=> array('gositus/bootstrap.min','goadmin/plugins/jqueryUI/jquery-ui.min'),
				'meta'  => meta_create($title),
				'user'	=> $check,
			);
			
			$this->load->view('template/header', $asset);
			$this->load->view('member/reset_view');
			$this->load->view('template/footer');
		} else {
			if($check == 'success')	$this->session->set_flashdata('pesan', 'Password Changed');
			else $this->session->set_flashdata('pesan', $check);
	
			redirect(base_url('member/login'));
		}
	}
}