<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class News extends CI_Controller {

	var $model         = 'model_front';

	public function __construct()
	{
		parent::__construct();
		$this->load->model($this->model);
	}

	public function index()
	{
		$title = 'News';
		$news = $this->model_front->get_news();
	
		
		$asset = array(
			'js'	=> array(),
			'css'	=> array(),
			'news'	=> $news,
			'meta'	=> meta_create($title),
		);
		
		$this->load->view('template/header', $asset);	
		$this->load->view('news_view');
		$this->load->view('template/footer');
	}

	public function detail($seo_url="")
	{
		$news 		   = $this->model_front->get_news($seo_url,'',TRUE);
		$title         = $news['name'];
		$image 		   = (!empty($news['image'])) ?  base_url('lib/images/news/' . $news['image']) :  base_url('lib/assets/gositus/default-image.png') ;

		count_it('news',$news['id'],$news); // add count di detail news
		total_visitor('news',$news['id']); // to dashboard report
		if(count($news) < 1) redirect(base_url('news'));

		// disesuaikan dengan url yang akan di buat untuk header json dan xml
		switch($news['type']){
			case 1: $type = 'news'; break;
			case 2: $type = 'event'; break;
			case 3: $type = 'announcement'; break;
			case 4: $type = 'blog'; break;
		}

		
		$asset = array(
			'js'	=> array(),
			'css'	=> array(),
			'type'  => $type,
			'news'	=> $news,
			'meta'	=> array(
				'title'       => !empty($news['meta_title'])? $news['meta_title'] : meta_create($title,'web_title'),
				'keyword'     => !empty($news['meta_keyword'])? $news['meta_keyword'] : meta_create($title,'meta_keyword'),
				'description' => !empty($news['meta_description'])? $news['meta_description'] : meta_create($title,'meta_description'),
				'image'         => $image
			),
		);

		$this->load->view('template/header', $asset);	
		$this->load->view('news_detail_view');
		$this->load->view('template/footer');
	}

	//Untuk SEO xml khusus News detail dan Event detail > Tolong sesuaikan > link url ada di header
	public function xml($seo_url = "")
	{
		$news = $this->model_front->get_news($seo_url, '', TRUE);

		if (empty($news)) {
			$seo_url = str_replace("-", "_", $seo_url);
			$news = $this->model_front->get_news($seo_url, '', TRUE);
		}
		if (empty($news)) {
			$seo_url = str_replace("_", "-", $seo_url);
			$news = $this->model_front->get_news($seo_url, '', TRUE);
		}
		
		if (count($news) < 1) redirect(base_url('blog'));
		$image = (!empty($news['image'])) ?  base_url('lib/images/news/' . $news['image']) : base_url('lib/assets/gositus/default-image.png');
		
		$asset = array(
			'news'	=> $news,
			'image'	=> $image
		);

		$this->load->view('seo/news_detail_xml_view', $asset);
	}
	
	//Untuk SEO json khusus News detail dan Event detail > Tolong sesuaikan > link url ada di header
	public function json($seo_url = "")
	{
		$news = $this->model_front->get_news($seo_url, '', TRUE);
		if (empty($news)) {
			$seo_url = str_replace("-", "_", $seo_url);
			$news = $this->model_front->get_news($seo_url, '', TRUE);
		}
		if (empty($news)) {
			$seo_url = str_replace("_", "-", $seo_url);
			$news = $this->model_front->get_news($seo_url, '', TRUE);
		}
		
		if (count($news) < 1) redirect(base_url('blog'));
		$image = (!empty($news['image'])) ?  base_url('lib/images/news/' . $news['image']) : base_url('lib/assets/gositus/default-image.png');

		
		$asset = array(
			'title'	=> $news['name'],
			'news'	=> $news,
			'image'	=> $image
		);

		$this->load->view('seo/news_detail_json_view', $asset);
	}
}