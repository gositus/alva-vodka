<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Logout extends CI_Controller {

	public function index()
	{	
		if(!empty($this->session->userdata['admin_id'])){
			action_log('LOGOUT', 'admin', $this->session->userdata('admin_id'), $this->session->userdata('admin_name'), 'Logout');
				
			$sess_data = array(
						'admin_login',
						'admin_name',
						'admin_id',
						'admin_privilege',
						'admin_module'
					);
					
			$this->session->unset_userdata($sess_data);
			$table_ss = $this->config->config['sess_save_path'];
			$this->db->empty_table($table_ss);
			
			// Delete unused rows in table go_ss
			$go_ss = strtotime(date("Y-m-d", strtotime("-7 days")));
			$this->db->delete("go_ss", array('timestamp <' => $go_ss));
			
			$this->load->helper('cookie');
		}
		
		redirect(base_url() . 'goadmin');
	}
}