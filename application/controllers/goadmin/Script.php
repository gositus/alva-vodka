<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class script extends CI_Controller {

	// Also for table name
	var $url           = 'script'; 
	var $table         = 'setting'; 
	var $model         = 'model_setting';
	var $title         = 'Script and Key';
	var $dt_serverside = FALSE;
	public function __construct()
	{
		parent::__construct();
		check_login();

		$this->load->model($this->model);
	}
	
	public function index()
	{
		$model_name = $this->model;
		check_access($this->url, 'read', TRUE);
		
		
		$asset = array(
					'title'	=> $this->title,
					'url'	=> $this->url,
					'js'	=> array('form','list'),
					'css'	=> array()
				);
		$asset['row']['id'] = '1';
		$asset['row']['name'] = $this->title;
		$asset['script'] = select_all_row($this->table, array('category' => $this->url, 'flag !=' => 3) , FALSE , 'asc','id');
		$asset['captcha'] = select_all_row($this->table, array('category' => 'captcha', 'flag !=' => 3) , FALSE , 'asc','id');

		$where = array('flag !=' => 3);		

		$this->form_validation->set_rules($asset['script'][0]['key'], $asset['script'][0]['label'], 'required');

		$this->form_validation->set_error_delimiters('<li>', '</li>');

		if ($this->form_validation->run() == FALSE)
		{
			$this->load->view('admin/template/header', $asset);
			$this->load->view('admin/template/menu');
			$this->load->view('admin/setting/script');
			$this->load->view('admin/template/footer');
		}
		else
		{
			
			$this->$model_name->update_script();
			$this->session->set_flashdata('success', 'Data has been changed!');
			redirect(base_url() . 'goadmin/' . $this->url);
		}
		
	}

}