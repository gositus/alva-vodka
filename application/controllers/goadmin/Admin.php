<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class admin extends CI_Controller {

	// Also for table name
	var $url           = 'admin'; //  nama table disarankan sama dengan  nama url
	var $model         = 'model_base';
	var $title         = 'Admin';
	var $dt_serverside = FALSE;
	
	public function __construct()
	{
		parent::__construct();
		check_login();

		$this->load->model($this->model);
	}
	
	public function index()
	{
		check_access($this->url, 'menu', TRUE);
		
		$asset = array(
					'title'	=> $this->title,
					'url'	=> $this->url,
					'js'	=> array(),
					'css'	=> array()
				);

		$this->load->view('admin/template/header', $asset);
		$this->load->view('admin/template/menu');
		$this->load->view('admin/' . $this->url . '/list');
		$this->load->view('admin/template/footer');
	}

	public function list_data()
	{
		// SEND DATA TO DATATABLE
		$model_name = $this->model;
		check_access($this->url, 'menu', TRUE);

		$where = array('a.flag !=' => 3);
		if($this->dt_serverside){

			$join = array(
							'select' => ' a.* , ap.name as ap_name',
							'from'	=> 'admin a ',
							'join'	=> array('admin_privilege ap','a.admin_privilege_id=ap.id','left')
			);
			$asset['data'] = getDataTable($this->url, $where, $join);
			$output        = array(
				'draw'            => $this->input->post('draw'),
				"recordsTotal"    => dt_countTotal($this->url, $where, $join),
				"recordsFiltered" => dt_countFiltered($this->url, $where, $join),
				"data"            => $asset['data'],
			);

		} else {
			$asset = array(
				'data' => $this->$model_name->get_list_admin()
			);
			$output = array('data' => $asset['data']);	
		}
		echo json_encode($output);
	}
	
	public function add()
	{
		check_access($this->url, 'add', TRUE);

		$model_name = $this->model;
		$asset = array(
					'title'	=> "Add " . $this->title,
					'url'	=> $this->url,
					'js'	=> array('form'),
					'css'	=> array()
				);
				
		// Get all parent admins
		$where = array('flag !=' => 3);

		$asset['admin_privilege'] = select_all_row('admin_privilege', $where);
		
		
		$this->form_validation->set_rules('a_name', 'Name', 'alpha_numeric_spaces|required');
		$this->form_validation->set_rules('a_privilege', 'Privilege', 'trim|required');
		$this->form_validation->set_rules('a_username', 'Username', 'trim|required|callback_cek_username|alpha_numeric');
		$this->form_validation->set_rules('a_password', 'Password', 'trim|required|min_length[8]');
		$this->form_validation->set_rules('a_email', 'Email', 'trim|valid_email');

		$this->form_validation->set_message('cek_username', 'Invalid Username');
		
		$this->form_validation->set_error_delimiters('<li>', '</li>');
		
		if ($this->form_validation->run() == FALSE)
		{
			$this->load->view('admin/template/header', $asset);
			$this->load->view('admin/template/menu');
			$this->load->view('admin/' . $this->url . '/add');
			$this->load->view('admin/template/footer');
		}
		else
		{
			
			$this->$model_name->insert_admin();
			$this->session->set_flashdata('success', 'Data succesfully saved!');
			redirect(base_url() . 'goadmin/' . $this->url);
		}
	}
	
	public function view($item_id)
	{
		check_access($this->url, 'read', TRUE);
		// if($this->session->userdata('admin_role') != 1 && $this->session->userdata('admin_id') != $item_id) redirect(base_url('goadmin')); 
		
		$check = select_all_row($this->url, array('id' => $item_id, 'flag !=' => 3),TRUE);
		$model_name = $this->model;
		if ($check)
		{
			$asset = array(
						'title'	=> $this->title  .' - ' . $check['name'], 
						'url'	=> $this->url,
						'js'	=> array('form','log'),
						'css'	=> array(),
						'row'	=> $check
					);
			$where = array('flag !=' => 3);		
			$asset['admin_privilege'] = select_all_row('admin_privilege', $where);
			

		
			// pre($this->session);
			$this->form_validation->set_rules('a_name', 'Name', 'alpha_numeric_spaces|required');
			$this->form_validation->set_rules('a_email', 'Email', 'trim|valid_email');
			$this->form_validation->set_error_delimiters('<li>', '</li>');

			if($this->input->post('a_password') &&  $this->session->userdata('admin_id')  == $item_id)
			{
				$this->form_validation->set_rules('o_password', 'Old Password', 'trim|callback_cek_password|required');
				$this->form_validation->set_rules('a_password', 'Password', 'trim|min_length[8]|required');
			}
		
			if ($this->form_validation->run() == FALSE)
			{
				$this->load->view('admin/template/header', $asset);
				$this->load->view('admin/template/menu');
				$this->load->view('admin/' . $this->url . '/view');
				$this->load->view('admin/template/footer');
			}
			else
			{
				
				$this->$model_name->update_admin();
				$this->session->set_flashdata('success', 'Data has been changed!');
				redirect(base_url() . 'goadmin/' . $this->url);
			}
		}
		else redirect(base_url() . 'goadmin/' . $this->url);
	}

	public function check_username()
	{

		$row = $this->db->get_where('admin', array('username' => $this->input->post('key'), 'flag !=' => 3))->row_array();
		if($this->input->post('admin_id')){
			$row = $this->db->get_where('admin', array('username' => $this->input->post('key'), 'flag !=' => 3 ,'id !='=> $this->input->post('admin_id')))->row_array();
		}
		
		if(!empty($row)) $html = 'false' ;
		else $html = 'true';

		$csrf = $this->security->get_csrf_hash();
		echo json_encode(array("a" => $html, "b" => $csrf));
		
	}

	public function check_email()
	{
		if($this->input->post('id'))
		{
			$row = $this->db->get_where('admin', array('email' => $this->input->post('key'), 'flag !=' => 3, 'id !=' => $this->input->post('id') ))->row_array();	
		} else 
		{
			$row = $this->db->get_where('admin', array('email' => $this->input->post('key'), 'flag !=' => 3))->row_array();
		}
	
		
		if(!empty($row)) $html = 'false' ;
		else $html = 'true';

		$csrf = $this->security->get_csrf_hash();
		echo json_encode(array("a" => $html, "b" => $csrf));
		
	}
	public function cek_username($str="")
	{
	
		$row = $this->db->get_where('admin', array('username' => $str, 'flag !=' => 3))->row_array();
		if(!empty($row))
		{
			return false;
		} else
		{
			return true;
		}
		
	}

	public function cek_email($str="")
	{
	
		$row = $this->db->get_where('admin', array('email' => $str, 'flag !=' => 3))->row_array();
		if(!empty($row))
		{
			return false;
		} else
		{
			return true;
		}
		
	}
	public function cek_password($str="")
	{
	
		$id = $this->session->userdata['admin_id'];
		$row = $this->db->get_where('admin', array('id' => $id, 'flag !=' => 3))->row_array();
		
		if(!empty($row))
		{
			if(password_verify($str, $row['password']))
			{
				$html='true';
			} else
			{
				$html ='false';
			}
		} else
		{
			$html = 'false';
		}

		return $html;
		
	}

	public function old_password()
	{
	
		// $id = $this->session->userdata['admin_id'];
		$id = $this->input->post('admin_id');

		$row = $this->db->get_where('admin', array('id' => $id, 'flag !=' => 3))->row_array();
		
		if(!empty($row))
		{
			if(password_verify($this->input->post('key'), $row['password']))
			{
				$html='true';
			} else
			{
				$html ='false';
			}
		} else
		{
			$html = 'false';
		}


		$csrf = $this->security->get_csrf_hash();
		echo json_encode(array("a" => $html, "b" => $csrf));
		
	}

	public function check_pattern(){
    	$str = $this->input->post("key");
		if(preg_match('#[0-9]+#', $str) && preg_match('#[A-Z]+#', $str) && preg_match('#[a-z]+#', $str) && preg_match('#[\W]+#', $str)){

			$html = 'true';
			}
		else{
			$html = 'false';
		}
		$csrf = $this->security->get_csrf_hash();
		echo json_encode(array("a" => $html, "b" => $csrf));
		
	
	}
}