<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class web_description extends CI_Controller {

	// Also for table name
	var $url           = 'web_description'; 
	var $table         = 'setting'; 
	var $model         = 'model_setting';
	var $title         = 'Web Description';
	var $dt_serverside = FALSE;
	var $image_width         = 800;
	var $image_height        = 800;

	public function __construct()
	{
		parent::__construct();
		check_login();

		$this->load->model($this->model);
	}
	
	public function index()
	{
		$model_name = $this->model;
		check_access($this->url, 'read', TRUE);
		
		$asset = array(
					'title'	=> $this->title,
					'url'	=> $this->url,
					'js'	=> array('form','log','jscolor'),
					'css'	=> array('plugins/jqueryUI/jquery-ui.min'),
				);
		$asset['row']['id'] = '1';
		$asset['row']['name'] = $this->title;
		$asset['setting'] = select_all_row($this->table, array('category' => $this->url, 'flag !=' => 3 , 'input !=' => 'file') , FALSE , 'asc','id'); 
		$asset['setting_file'] = select_all_row($this->table, array('category' => $this->url, 'flag !=' => 3 , 'input' => 'file') , FALSE , 'asc','id');
		$asset['setting_language'] = select_all_row($this->table, array('key' => 'default_language', 'flag !=' => 3),TRUE);
		$asset['master_language'] =select_all_row('language', array('flag' => 1	));

		$where = array('flag !=' => 3);		

		$this->form_validation->set_rules($asset['setting'][0]['key'], $asset['setting'][0]['label'], 'trim|required');

		$this->form_validation->set_error_delimiters('<li>', '</li>');

		if ($this->form_validation->run() == FALSE)
		{
			$this->load->view('admin/template/header', $asset);
			$this->load->view('admin/template/menu');
			$this->load->view('admin/setting/web_description');
			$this->load->view('admin/template/footer');
		}
		else
		{
			
			$this->$model_name->update_web_description();
			$this->session->set_flashdata('success', 'Data has been changed!');
			redirect(base_url() . 'goadmin/' . $this->url);
		}
		
	}

}