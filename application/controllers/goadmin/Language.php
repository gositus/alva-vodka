<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Language extends CI_Controller {

	// Also for table name
	var $url           = 'language'; //  nama table disarankan sama dengan  nama url
	var $model         = 'model_base';
	var $title         = 'Language';
	var $dt_serverside = FALSE;
	var $image_width         = 50;
	var $image_height        = 50;
	
	public function __construct()
	{
		parent::__construct();
		check_login();

		$this->load->model($this->model);
	}
	
	public function index()
	{
		check_access($this->url, 'menu', TRUE);
		
		$asset = array(
					'title'	=> $this->title,
					'url'	=> $this->url,
					'js'	=> array(),
					'css'	=> array()
				);

		$this->load->view('admin/template/header', $asset);
		$this->load->view('admin/template/menu');
		$this->load->view('admin/' . $this->url . '/list');
		$this->load->view('admin/template/footer');
	}

	public function list_data()
	{
		// SEND DATA TO DATATABLE
		$model_name = $this->model;
		check_access($this->url, 'menu', TRUE);

		$where = array('a.flag !=' => 3);
		if($this->dt_serverside){
			$asset['data'] = select_all_row($this->url);
			$output        = array(
				'draw'            => $this->input->post('draw'),
				"recordsTotal"    => dt_countTotal($this->url, $where, $join),
				"recordsFiltered" => dt_countFiltered($this->url, $where, $join),
				"data"            => $asset['data'],
			);

		} else {
		$where = array('flag !=' => 3);

			$asset = array(
				'data' => select_all_row($this->url,$where, FALSE, $sort="asc")
			);
			$output = array('data' => $asset['data']);	
		}
		echo json_encode($output);
	}
	
	public function add()
	{
		check_access($this->url, 'add', TRUE);

		$model_name = $this->model;
		$asset = array(
					'title'	=> "Add " . $this->title,
					'url'	=> $this->url,
					'js'	=> array('form','log','plugins/ckfinder/ckfinder'),
					'css'	=> array()
				);
				
		// Get all parent languages
		$where = array('flag !=' => 3);
		
		$this->form_validation->set_rules('name', 'Name', 'required|alpha_numeric_spaces');
		$this->form_validation->set_rules('attr', 'Atribut', 'trim|required|alpha_numeric');
		$this->form_validation->set_rules('flag', 'Status', 'trim|required');
		$this->form_validation->set_rules('sort', 'Sort', 'trim|is_natural');

		
		$this->form_validation->set_error_delimiters('<li>', '</li>');
		
		if ($this->form_validation->run() == FALSE)
		{
			$this->load->view('admin/template/header', $asset);
			$this->load->view('admin/template/menu');
			$this->load->view('admin/' . $this->url . '/add');
			$this->load->view('admin/template/footer');
		}
		else
		{
			
			$this->$model_name->insert_language();
			$this->session->set_flashdata('success', 'Data succesfully saved!');
			redirect(base_url() . 'goadmin/' . $this->url);
		}
	}
	
	public function view($item_id)
	{
		check_access($this->url, 'read', TRUE);
		
		$check = select_all_row($this->url, array('id' => $item_id, 'flag !=' => 3),TRUE);
		$model_name = $this->model;
		if ($check)
		{
			$asset = array(
						'title'	=> $this->title .' - ' . $check['name'] , 
						'url'	=> $this->url,
						'js'	=> array('form','list'),
						'css'	=> array(),
						'row'	=> $check
					);
			$where = array('flag !=' => 3);		

			$this->form_validation->set_rules('name', 'Name', 'alpha_numeric_spaces|required');
			$this->form_validation->set_rules('attr', 'Attribut', 'trim|required|alpha_numeric');
			$this->form_validation->set_rules('flag', 'Status', 'trim|required');
			$this->form_validation->set_rules('sort', 'Sort', 'trim|is_natural');

			$this->form_validation->set_error_delimiters('<li>', '</li>');

			if ($this->form_validation->run() == FALSE)
			{
				$this->load->view('admin/template/header', $asset);
				$this->load->view('admin/template/menu');
				$this->load->view('admin/' . $this->url . '/view');
				$this->load->view('admin/template/footer');
			}
			else
			{
				
				$this->$model_name->update_language();
				$this->session->set_flashdata('success', 'Data has been changed!');
				redirect(base_url() . 'goadmin/' . $this->url);
			}
		}
		else redirect(base_url() . 'goadmin/' . $this->url);
	}

}