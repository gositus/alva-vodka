<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin_privilege extends CI_Controller {

	var $url = 'admin_privilege';
	var $model = 'model_base';
	var $title = 'Admin Privilege';
	var $dt_serverside = FALSE;

	public function __construct(){
		parent::__construct();
		check_login();
		$this->load->model($this->model);
	}

	public function index(){
		check_access($this->url, 'menu', TRUE);
		$model_name = $this->model;
		$asset = array(
			'title'	=> $this->title,
			'url'	=> $this->url,
			'js'	=> array(),
			'css'	=> array(),
			'query' => select_all_row($this->url,array('flag != '=>3))
		);
		$this->load->view('admin/template/header', $asset);
		$this->load->view('admin/template/menu');
		$this->load->view('admin/admin_privilege/list');
		$this->load->view('admin/template/footer');
		
	}
	public function list_data()
	{
		// SEND DATA TO DATATABLE
		check_access($this->url, 'menu', TRUE);

		$where = array('flag !=' => 3);

		// IF AJAX SERVER SIDE
		if($this->dt_serverside){

			$asset['data'] = getDataTable($this->url, $where);
			$output        = array(
				'draw'            => $this->input->post('draw'),
				"recordsTotal"    => dt_countTotal($this->url, $where),
				"recordsFiltered" => dt_countFiltered($this->url, $where),
				"data"            => $asset['data'],
			);

		} else {
			
			$asset = array(
				'data' => select_all_row($this->url, $where)
			);
			$output = array('data' => $asset['data']);	
		}
		
		
		echo json_encode($output);
	}

	public function add(){
		check_access($this->url, 'add', TRUE);
		$model_name = $this->model;
		$asset = array(
			'title'	=> "Add " .$this->title,
			'url'	=> $this->url,
			'js'	=> array('privilege','form'),
			'css'	=> array()
		);
		$asset['parent_modules'] = $this->$model_name->get_parent_module();
		$this->form_validation->set_rules('ap_name', 'Name', 'alpha_numeric_spaces|required');
		$this->form_validation->set_rules('ap_privilege', 'Privilege', 'trim|required');
		$this->form_validation->set_error_delimiters('<li>', '</li>');
		
		if ($this->form_validation->run() == FALSE)
		{
			$this->load->view('admin/template/header', $asset);
			$this->load->view('admin/template/menu');
			$this->load->view('admin/' . $this->url . '/add');
			$this->load->view('admin/template/footer');
		}
		else
		{
			
			$this->$model_name->insert_admin_privilege();
			$this->session->set_flashdata('success', 'Data succesfully saved!');
			redirect(base_url() . 'goadmin/' . $this->url);
		}
		
		
	}

	public function view($id=""){
		
		check_access($this->url, 'edit', TRUE);
		$id= (int)($id);	// id pasti angka, untuk prevent get selain angka
		$check = select_all_row($this->url,array('flag !='=>3,'id'=>$id), TRUE) ;
		if($check)
		{
			$model_name = $this->model;
			$asset = array(
				'title'	=> $this->title  .' - ' . $check['name'], 
				'url'	=> $this->url,
				'js'	=> array('privilege','form','log'),
				'css'	=> array(),
				'row' 	=> $check
			);
			
			$asset['parent_modules'] = $this->$model_name->get_parent_module();
			$this->form_validation->set_rules('ap_name', 'Name', 'alpha_numeric_spaces|required');
			$this->form_validation->set_rules('ap_privilege', 'Privilege', 'trim|required');
			$this->form_validation->set_error_delimiters('<li>', '</li>');

			if ($this->form_validation->run() == FALSE)
			{
				$this->load->view('admin/template/header', $asset);
				$this->load->view('admin/template/menu');
				$this->load->view('admin/' . $this->url . '/view');
				$this->load->view('admin/template/footer');
			}
			else
			{
				
				$this->$model_name->update_admin_privilege();
				$this->session->set_flashdata('success', 'Data has been changed!');
				redirect(base_url() . 'goadmin/' . $this->url);
			}

		} else
		{
			redirect(base_url() . 'goadmin/' . $this->url);
		}
		
		
	}
}
?>