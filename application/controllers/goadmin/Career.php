<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Career extends CI_Controller {

	// Also for table name
	var $url           = 'career'; //  nama table disarankan sama dengan  nama url
	var $model         = 'model_career';
	var $title         = 'Career';
	var $dt_serverside = FALSE;
	var $image_width         = 90;
	var $image_height        = 90;
	
	public function __construct()
	{
		parent::__construct();
		check_login();

		$this->load->model($this->model);
	}
	
	public function index()
	{
		check_access($this->url, 'menu', TRUE);
		
		$asset = array(
					'title'	=> $this->title,
					'url'	=> $this->url,
					'js'	=> array(),
					'css'	=> array()
				);

		$this->load->view('admin/template/header', $asset);
		$this->load->view('admin/template/menu');
		$this->load->view('admin/' . $this->url . '/list');
		$this->load->view('admin/template/footer');
	}

	public function list_data()
	{
		// SEND DATA TO DATATABLE
		$model_name = $this->model;
		check_access($this->url, 'menu', TRUE);

		$where = array('a.flag !=' => 3);
		if($this->dt_serverside){
			$asset['data'] = select_all_row($this->url);
			$output        = array(
				'draw'            => $this->input->post('draw'),
				"recordsTotal"    => dt_countTotal($this->url, $where, $join),
				"recordsFiltered" => dt_countFiltered($this->url, $where, $join),
				"data"            => $asset['data'],
			);

		} else {
			$asset = array(
				'data' => $this->$model_name->get_career()
			);
			$output = array('data' => $asset['data']);	
		}
		echo json_encode($output);
	}
	
	public function add()
	{
		check_access($this->url, 'add', TRUE);

		$model_name = $this->model;
		$asset = array(
					'title'	=> "Add " . $this->title,
					'url'	=> $this->url,
					'js'	=> array('form','fileFinder'),
					'css'	=> array('plugins/jqueryUI/jquery-ui.min')
				);
				
		// Get all parent careers
		$where = array('flag !=' => 3);

		$asset['career_category'] = $this->$model_name->get_career_category();
		$this->form_validation->set_rules('flag', 'Status', 'trim|required');
		$this->form_validation->set_rules('career_category', 'Career Category', 'is_natural|trim|required');
		
		$this->form_validation->set_error_delimiters('<li>', '</li>');
		
		if ($this->form_validation->run() == FALSE)
		{
			$this->load->view('admin/template/header', $asset);
			$this->load->view('admin/template/menu');
			$this->load->view('admin/' . $this->url . '/add');
			$this->load->view('admin/template/footer');
		}
		else
		{
			
			$this->$model_name->insert_career();
			$this->session->set_flashdata('success', 'Data succesfully saved!');
			redirect(base_url() . 'goadmin/' . $this->url);
		}
	}
	
	public function view($career_id)
	{

		check_access($this->url, 'read', TRUE);
		
		$check = select_all_row($this->url, array('id' => $career_id, 'flag !=' => 3),TRUE);
		$model_name = $this->model;
		$where = array('flag !=' => 3);
		if ($check)
		{
			$asset = array(
						'title'	=> $this->title,
						'url'	=> $this->url,
						'js'	=> array('form','list','log','fileFinder'),
						'css'	=> array()
					);
			$asset['row']             = $this->$model_name->get_detail($career_id);
			$asset['default']         = setting_value('default_language') ;
			$asset['title']           = $this->title .' - '. $asset['row'][$asset['default']]['name'] ;
			$asset['career_category'] = $this->$model_name->get_career_category();

			$this->form_validation->set_rules('flag', 'Status', 'trim|required');
			$this->form_validation->set_rules('career_category', 'career_category', 'is_natural|trim|required');
			


			$this->form_validation->set_error_delimiters('<li>', '</li>');

			if ($this->form_validation->run() == FALSE)
			{
				$this->load->view('admin/template/header', $asset);
				$this->load->view('admin/template/menu');
				$this->load->view('admin/' . $this->url . '/view');
				$this->load->view('admin/template/footer');
			}
			else
			{
				
				$this->$model_name->update_career();
				$this->session->set_flashdata('success', 'Data has been changed!');
				redirect(base_url() . 'goadmin/' . $this->url);
			}
		}
		else redirect(base_url() . 'goadmin/' . $this->url);
	}

}