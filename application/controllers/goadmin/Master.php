<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Master extends CI_Controller {

	// Also for table name
	var $url           = 'master'; //  nama table disarankan sama dengan  nama url
	var $model         = 'model_master';
	var $title         = 'Master';
	var $dt_serverside = FALSE;
	var $image_width         = 90;
	var $image_height        = 90;
	
	public function __construct()
	{
		parent::__construct();
		check_login();

		$this->load->model($this->model);
	}
	public function index(){
		redirect(base_url('goadmin'));
	}
	public function banned_ip($num="")
	{
		check_access($this->url, 'menu', TRUE);
		if($num){
			$model_name = $this->model;
			$this->$model_name->insert_banned('message',$num);
		}
		$this->session->set_userdata("tabel","master_banned_ip");
		$asset = array(
					'title'	=> 'Banned IP',
					'url'	=> $this->url,
					'js'	=> array(),
					'css'	=> array()
				);

		$this->load->view('admin/template/header', $asset);
		$this->load->view('admin/template/menu');
		$this->load->view('admin/' . $this->url . '/banned_ip');
		$this->load->view('admin/template/footer');
	}
	public function city()
	{
		check_access($this->url, 'menu', TRUE);
		$this->session->set_userdata("tabel","master_city");
		$asset = array(
					'title'	=> 'City',
					'url'	=> $this->url,
					'js'	=> array(),
					'css'	=> array()
				);

		$this->load->view('admin/template/header', $asset);
		$this->load->view('admin/template/menu');
		$this->load->view('admin/' . $this->url . '/city');
		$this->load->view('admin/template/footer');
	}

	public function country()
	{
		check_access($this->url, 'menu', TRUE);
		$this->session->set_userdata("tabel","master_country");
		
		$asset = array(
					'title'	=> 'Country',
					'url'	=> $this->url,
					'js'	=> array(),
					'css'	=> array()
				);

		$this->load->view('admin/template/header', $asset);
		$this->load->view('admin/template/menu');
		$this->load->view('admin/' . $this->url . '/country');
		$this->load->view('admin/template/footer');
	}

	public function province()
	{
		check_access($this->url, 'menu', TRUE);
		$this->session->set_userdata("tabel","master_province");
		
		$asset = array(
					'title'	=> 'Province',
					'url'	=> $this->url,
					'js'	=> array(),
					'css'	=> array()
				);

		$this->load->view('admin/template/header', $asset);
		$this->load->view('admin/template/menu');
		$this->load->view('admin/' . $this->url . '/list');
		$this->load->view('admin/template/footer');
	}

	public function order()
	{
		check_access($this->url, 'menu', TRUE);
		$this->session->set_userdata("tabel","master_order_status");
		
		$asset = array(
					'title'	=> 'Order Status',
					'url'	=> $this->url,
					'js'	=> array(),
					'css'	=> array()
				);

		$this->load->view('admin/template/header', $asset);
		$this->load->view('admin/template/menu');
		$this->load->view('admin/' . $this->url . '/list');
		$this->load->view('admin/template/footer');
	}

	public function list_data()
	{
		// SEND DATA TO DATATABLE
		$model_name = $this->model;
		check_access($this->url, 'menu', TRUE);

		$where = array('a.flag !=' => 3);
		if($this->dt_serverside){
			$asset['data'] = select_all_row("city");
			$output        = array(
				'draw'            => $this->input->post('draw'),
				"recordsTotal"    => dt_countTotal($this->url, $where, $join),
				"recordsFiltered" => dt_countFiltered($this->url, $where, $join),
				"data"            => $asset['data'],
			);

		} else {
			if($this->session->userdata("tabel") == 'master_city'){
				$asset = array(
					'data' =>  $this->$model_name->get_all_city()
				);

			} else {
				$asset = array(
					'data' =>  select_all_row($this->session->userdata("tabel"), array())
				);
			}
			
			$output = array('data' => $asset['data']);	
		}
		echo json_encode($output);
	}
	

	public function doku()
	{
		$title = 'Doku';
		check_access($title, 'read', TRUE);
		
		$model_name = $this->model;
			$asset = array(
						'title'	=> $this->title,
						'url'	=> $title,
						'js'	=> array('form','list','log','plugins/ckfinder/ckfinder','fileFinder'),
						'css'	=> array()
					);
			$asset['default'] = setting_value('default_language') ;
			// $asset['master_category'] = $this->$model_name->get_master_category();
			$asset['row']['id'] = 1;
			$this->form_validation->set_rules('mall_id', 'Mall ID', 'trim|required');
			$this->form_validation->set_rules('shared_key', 'Shared Key', 'trim|required');
			


			$this->form_validation->set_error_delimiters('<li>', '</li>');

			if ($this->form_validation->run() == FALSE)
			{
				$this->load->view('admin/template/header', $asset);
				$this->load->view('admin/template/menu');
				$this->load->view('admin/' . $this->url . '/doku');
				$this->load->view('admin/template/footer');
			}
			else
			{
				
				$this->$model_name->update_doku();
				$this->session->set_flashdata('success', 'Data has been changed!');
				redirect(base_url() . 'goadmin/' . $this->url .'/doku');
			}

	}

	public function midtrans()
	{
		$title = 'Midtrans';
		check_access($title, 'read', TRUE);
		
		$model_name = $this->model;
		$asset = array(
			'title'	=> $this->title,
			'url'	=> $title,
			'js'	=> array('form','list','log','plugins/ckfinder/ckfinder','fileFinder'),
			'css'	=> array()
		);
		$asset['default'] = setting_value('default_language') ;
		// $asset['master_category'] = $this->$model_name->get_master_category();
		$asset['row']['id'] = 1;
		$this->form_validation->set_rules('server_key', 'Server Key', 'trim|required');

		$this->form_validation->set_error_delimiters('<li>', '</li>');

		if ($this->form_validation->run() == FALSE)
		{
			$this->load->view('admin/template/header', $asset);
			$this->load->view('admin/template/menu');
			$this->load->view('admin/' . $this->url . '/midtrans');
			$this->load->view('admin/template/footer');
		}
		else
		{
			
			$this->$model_name->update_midtrans();
			$this->session->set_flashdata('success', 'Data has been changed!');
			redirect(base_url() . 'goadmin/' . $this->url .'/midtrans');
		}
	}


	public function view($master_id)
	{

		check_access($this->url, 'read', TRUE);
		
		$check = select_all_row($this->url, array('id' => $master_id, 'flag !=' => 3),TRUE);
		$model_name = $this->model;
		$where = array('flag !=' => 3);
		if ($check)
		{
			$asset = array(
						'title'	=> $this->title,
						'url'	=> $this->url,
						'js'	=> array('form','list','log','plugins/ckfinder/ckfinder','fileFinder'),
						'css'	=> array()
					);
			$asset['row'] = $this->$model_name->get_detail($master_id);
			$asset['default'] = setting_value('default_language') ;
			$asset['master_category'] = $this->$model_name->get_master_category();

			$this->form_validation->set_rules('flag', 'Status', 'trim|required');
			$this->form_validation->set_rules('master_category', 'master_category', 'is_natural|trim|required');
			


			$this->form_validation->set_error_delimiters('<li>', '</li>');

			if ($this->form_validation->run() == FALSE)
			{
				$this->load->view('admin/template/header', $asset);
				$this->load->view('admin/template/menu');
				$this->load->view('admin/' . $this->url . '/view');
				$this->load->view('admin/template/footer');
			}
			else
			{
				
				$this->$model_name->update_master();
				$this->session->set_flashdata('success', 'Data has been changed!');
				redirect(base_url() . 'goadmin/' . $this->url);
			}
		}
		else redirect(base_url() . 'goadmin/' . $this->url);
	}

}