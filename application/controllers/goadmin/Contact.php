<?php if ( ! defined('BASEPATH')) exit('No direct contact access allowed');

class contact extends CI_Controller {

	// Also for table name
	var $url           = 'contact'; 
	var $table         = 'setting'; 
	var $model         = 'model_setting';
	var $title         = 'Contact Info';
	var $dt_serverside = FALSE;
	public function __construct()
	{
		parent::__construct();
		check_login();

		$this->load->model($this->model);
	}
	
	public function index()
	{
		$model_name = $this->model;
		check_access($this->url, 'read', TRUE);
		
		
		$asset = array(
					'title'	=> $this->title,
					'url'	=> $this->url,
					'js'	=> array('form','log'),
					'css'	=> array()
				);
		$asset['row']['id'] = '1';
		$asset['row']['name'] = $this->title;
		$asset['setting'] = select_all_row($this->table, array('category' => $this->url, 'flag !=' => 3) , FALSE , 'asc','id');
		$asset['social'] = select_all_row($this->table, array('category' => 'social', 'flag !=' => 3) , FALSE , 'asc','id');

		$where = array('flag !=' => 3);		

		$this->form_validation->set_rules($asset['setting'][0]['key'], $asset['setting'][0]['label'], 'trim|required');

		$this->form_validation->set_error_delimiters('<li>', '</li>');

		if ($this->form_validation->run() == FALSE)
		{
			$this->load->view('admin/template/header', $asset);
			$this->load->view('admin/template/menu');
			$this->load->view('admin/setting/contact');
			$this->load->view('admin/template/footer');
		}
		else
		{
			
			$this->$model_name->update_contact();
			$this->session->set_flashdata('success', 'Data has been changed!');
			redirect(base_url() . 'goadmin/' . $this->url);
		}
		
	}

}