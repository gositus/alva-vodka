<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Module extends CI_Controller {

	// Also for table name
	var $url           = 'module'; //  nama table disarankan sama dengan  nama url
	var $model         = 'model_base';
	var $title         = 'Module';
	var $dt_serverside = FALSE;
	
	public function __construct()
	{
		parent::__construct();
		check_login();
		$this->load->model($this->model);
	}
	
	public function index()
	{
		check_access($this->url, 'menu', TRUE);
		
		$asset = array(
					'title'	=> $this->title,
					'url'	=> $this->url,
					'js'	=> array(),
					'css'	=> array()
				);

		$this->load->view('admin/template/header', $asset);
		$this->load->view('admin/template/menu');
		$this->load->view('admin/' . $this->url . '/list');
		$this->load->view('admin/template/footer');
	}

	public function list_data()
	{
		// SEND DATA TO DATATABLE
		check_access($this->url, 'menu', TRUE);

		$where = array('flag !=' => 3);

		// IF AJAX SERVER SIDE TRUE
		if($this->dt_serverside){
			$join = array();
			$asset['data'] = getDataTable($this->url, $where, $join);
			$output        = array(
				'draw'            => $this->input->post('draw'),
				"recordsTotal"    => dt_countTotal($this->url, $where, $join),
				"recordsFiltered" => dt_countFiltered($this->url, $where, $join),
				"data"            => $asset['data'],
			);

		} else {
			$model_name = $this->model;
			$asset = array(
				// 'data' => select_all_row($this->url, $where, FALSE , 'asc', 'name')
				'data' => $this->$model_name->get_modules($this->url, $where , 'asc', 'name')
			);
			$output = array('data' => $asset['data']);	
		}
		
		
		echo json_encode($output);
	}
	 
	public function add()
	{
		$model_name = $this->model;
		check_access($this->url, 'add', TRUE);
		
		$asset = array(
					'title'	=> "Add " .$this->title,
					'url'	=> $this->url,
					'js'	=> array('form'),
					'css'	=> array()
				);
				
		// Get all parent modules
		$asset['modules'] = $this->$model_name->get_module_parent();
		
		
		$this->form_validation->set_rules('m_name', 'Name', 'trim|required');
		$this->form_validation->set_rules('m_url', 'URL', 'trim|required');
		$this->form_validation->set_rules('m_parent', 'Parent', 'trim|required');
		$this->form_validation->set_error_delimiters('<li>', '</li>');
		
		if ($this->form_validation->run() == FALSE)
		{
			$this->load->view('admin/template/header', $asset);
			$this->load->view('admin/template/menu');
			$this->load->view('admin/' . $this->url . '/add');
			$this->load->view('admin/template/footer');
		}
		else
		{
			$model_name = $this->model;
			$this->$model_name->insert_module();
			$this->session->set_flashdata('success', 'Data succesfully saved!');
			redirect(base_url() . 'goadmin/' . $this->url);
		}
	}
	
	public function view($item_id)
	{
		$model_name = $this->model;
		check_access($this->url, 'read', TRUE);
		
		$check = select_all_row($this->url, array('id' => $item_id, 'flag !=' => 3),TRUE);
		if ($check)
		{
			$asset = array(
						'title'	=> $this->title . " - " .$check['name'],
						'url'	=> $this->url,
						'js'	=> array('form','log'),
						'css'	=> array(),
						'row'	=> $check
					);
					
			$asset['modules'] = $this->$model_name->get_module_parent();
		
			
			$this->form_validation->set_rules('m_name', 'Name', 'trim|required');
			$this->form_validation->set_rules('m_url', 'URL', 'trim|required');
			$this->form_validation->set_rules('m_parent', 'Parent', 'trim|required');
			$this->form_validation->set_error_delimiters('<li>', '</li>');
		
			if ($this->form_validation->run() == FALSE)
			{
				$this->load->view('admin/template/header', $asset);
				$this->load->view('admin/template/menu');
				$this->load->view('admin/' . $this->url . '/view');
				$this->load->view('admin/template/footer');
			}
			else
			{
				
				$model_name = $this->model;
				$this->$model_name->update_module();
				$this->session->set_flashdata('success', 'Data has been changed!');
				redirect(base_url() . 'goadmin/' . $this->url);
			}
		}
		else redirect(base_url() . 'goadmin/' . $this->url);
	}
}