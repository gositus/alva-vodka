<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class message extends CI_Controller {

	// Also for table name
	var $url           = 'message'; //  nama table disarankan sama dengan  nama url
	var $model         = 'model_message';
	var $title         = 'Inbox';
	var $dt_serverside = FALSE;

	
	public function __construct()
	{
		parent::__construct();
		check_login();

		$this->load->model($this->model);
	}
	
	public function index()
	{
		check_access($this->url, 'menu', TRUE);
		
		$asset = array(
					'title'	=> $this->title,
					'url'	=> $this->url,
					'js'	=> array(),
					'css'	=> array()
				);

		$this->load->view('admin/template/header', $asset);
		$this->load->view('admin/template/menu');
		$this->load->view('admin/' . $this->url . '/list');
		$this->load->view('admin/template/footer');
	}

	public function list_data()
	{
		// SEND DATA TO DATATABLE
		$model_name = $this->model;
		check_access($this->url, 'menu', TRUE);

		$where = array('a.flag !=' => 3);
		if($this->dt_serverside){
			$asset['data'] = select_all_row($this->url);
			$output        = array(
				'draw'            => $this->input->post('draw'),
				"recordsTotal"    => dt_countTotal($this->url, $where, $join),
				"recordsFiltered" => dt_countFiltered($this->url, $where, $join),
				"data"            => $asset['data'],
			);

		} else {
			$asset = array(
				'data' => get_filtered_message()
				// 'data' => select_all_row($this->url, array('flag !=' => 3), FALSE , 'desc', 'date')
			);
			$output = array('data' => $asset['data']);	
		}
		echo json_encode($output);
	}
	
	
	public function view($item_id)
	{
		check_access($this->url, 'read', TRUE);
		$model_name = $this->model;
		
		$check = select_all_row($this->url, array('id' => $item_id, 'flag !=' => 3),TRUE);
	
		if ($check)
		{
			$this->$model_name->update_flag($check);
			$asset = array(
						'title'	=> $this->title .' - '. $check['name'] ." (" . format_date($check['date']). ") - ",
						'url'	=> $this->url,
						'js'	=> array('form','log'),
						'css'	=> array(),
						'row'	=> $check
					);
			$where = array('flag !=' => 3);		

			$this->form_validation->set_rules('reply', 'Reply', 'trim|required');

			$this->form_validation->set_error_delimiters('<li>', '</li>');

			if ($this->form_validation->run() == FALSE)
			{
				$this->load->view('admin/template/header', $asset);
				$this->load->view('admin/template/menu');
				$this->load->view('admin/' . $this->url . '/view');
				$this->load->view('admin/template/footer');
			}
			else
			{
				
				$this->$model_name->update_message();
				$this->session->set_flashdata('success', 'Data has been changed!');
				redirect(base_url() . 'goadmin/' . $this->url);
			}
		}
		else {
			$next = get_filtered_message(TRUE);
			if($next){
				$this->session->set_flashdata('success', 'Data has been deleted');
				redirect(base_url() . 'goadmin/' . $this->url.'/view/'. $next['id']);
			} else {
				redirect(base_url() . 'goadmin/' . $this->url);
			}
		}
	}

}