<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Member extends CI_Controller {

	// Also for table name
	var $url           = 'member'; //  nama table disarankan sama dengan  nama url
	var $model         = 'model_member';
	var $title         = 'Member';
	var $dt_serverside = FALSE;
	
	public function __construct()
	{
		parent::__construct();
		check_login();

		$this->load->model($this->model);
	}
	
	public function index()
	{
		check_access($this->url, 'menu', TRUE);
		
		$asset = array(
					'title'	=> $this->title,
					'url'	=> $this->url,
					'js'	=> array('form'),
					'css'	=> array('plugins/jqueryUI/jquery-ui.min'),
					'filter_date_range' =>TRUE
				);

		$this->load->view('admin/template/header', $asset);
		$this->load->view('admin/template/menu');
		$this->load->view('admin/' . $this->url . '/list');
		$this->load->view('admin/template/footer');
	}

	public function list_data()
	{
		// SEND DATA TO DATATABLE
		$model_name = $this->model;
		check_access($this->url, 'menu', TRUE);

		$where = array('a.flag !=' => 3);
		if($this->dt_serverside){

			$join = array(
							'select' => ' a.* , ap.name as ap_name',
							'from'	=> 'member a ',
							'join'	=> array('member_privilege ap','a.member_privilege_id=ap.id','left')
			);
			$asset['data'] = getDataTable($this->url, $where, $join);
			$output        = array(
				'draw'            => $this->input->post('draw'),
				"recordsTotal"    => dt_countTotal($this->url, $where, $join),
				"recordsFiltered" => dt_countFiltered($this->url, $where, $join),
				"data"            => $asset['data'],
			);

		} else {
			$asset = array(
				'data' => select_all_row($this->url, array('flag !=' => 3), FALSE , 'desc', 'date_added')
			);
			$output = array('data' => $asset['data']);	
		}
		echo json_encode($output);
	}
	
	public function view($item_id)
	{
		check_access($this->url, 'read', TRUE);
		
		$check = select_all_row($this->url, array('id' => $item_id, 'flag !=' => 3),TRUE);
		$model_name = $this->model;
		if ($check)
		{
			$asset = array(
						'title'	=> $this->title. " - ". $check['name'],
						'url'	=> $this->url,
						'js'	=> array('form','log'),
						'css'	=> array('plugins/jqueryUI/jquery-ui.min'),
						'row'	=> $check
					);
			$where = array('flag !=' => 3);		
			$this->form_validation->set_rules('name', 'Name', 'alpha_numeric_spaces|required');
			$this->form_validation->set_rules('email', 'Email', 'trim|valid_email');
			$this->form_validation->set_error_delimiters('<li>', '</li>');

			if($this->input->post('password'))
			{
				$this->form_validation->set_rules('password', 'Password', 'trim|min_length[8]|required');
				$this->form_validation->set_rules('repassword', 'Re-Password', 'trim|min_length[8]|required');
			}
		
			if ($this->form_validation->run() == FALSE)
			{
				$asset['member_log'] = select_all_row('member_log', array('member_id'=>$item_id), FALSE, 'desc', 'date');
				$this->load->view('admin/template/header', $asset);
				$this->load->view('admin/template/menu');
				$this->load->view('admin/' . $this->url . '/view');
				$this->load->view('admin/template/footer');
			}
			else
			{
				
				$this->$model_name->update_member_cms($item_id);
				$this->session->set_flashdata('success', 'Data has been changed!');
				redirect(base_url() . 'goadmin/' . $this->url);
			}
		}
		else redirect(base_url() . 'goadmin/' . $this->url);
	}



	public function check_pattern(){
    	$str = $this->input->post("a_password");
		if(preg_match('#[0-9]+#', $str) && preg_match('#[A-Z]+#', $str) && preg_match('#[a-z]+#', $str) && preg_match('#[\W]+#', $str)){
			echo 'true';
			return TRUE;
			}
		else{
			echo 'false';
			return FALSE;
		}
	
	}
}