<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Sequential extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        check_login();
    }

	public function index(){
		redirect();
	}
    
    public function delete(){
        $idList = json_decode($this->input->post('id'));
        $tableName = input_clean($this->input->post('table'));
        $total = count($idList);
        $data = array(
            'flag' => 3
        );
        foreach($idList as $v1){

            $row = $this->db->order_by('id', 'asc')->get_where($tableName , array('id' => $v1))->row_array();

            if(empty($row['name'])){
                if($this->db->table_exists('content_to_'.$tableName)){
                    $row = $this->db->get_where('content_to_'.$tableName , array($tableName.'_id' => $v1))->row_array();
                } else {
                    $row['name'] = 'undefined name';
                }
            }
            $nama  = $row['name'];
            action_log('DELETE', $tableName , $v1, $nama, 'DELETED ' . $tableName  . ' ( ' . $nama . ' ) ');

            $image = select_all_row($tableName, array('id'=> $v1), TRUE);
            if(!empty($image['image'])){
                unlink(FCPATH.'lib/images/'.$tableName.'/'. $image['image']);
                if($tableName=='banner'){
                    unlink(FCPATH.'lib/images/'.$tableName.'/thumb/'. $image['image']);
                }
            }

            // $this->db->where('id', $v1)->update($tableName, $data); // maunya di delete
            $this->db->delete($tableName,array('id'=>$v1));

            if($this->db->table_exists('content_to_'.$tableName)){
                $this->db->delete('content_to_'.$tableName,array($tableName.'_id'=>$v1));
            }
            if($tableName=='news'||$tableName=='item'||$tableName=='item_category'){
                $this->db->delete('dashboard',array('category'=> $tableName, 'path_id'=> $v1));
            }
        }
        
        $return = array(
            'result' => 'success',
            'count' => $total
        );
        // echo json_encode($return);
        echo $this->security->get_csrf_hash();

    }
    public function add_category(){
        $data             = $this->input->post();
        $default_language = setting_value('default_language');
        $language         = language()->result_array();
        $seo_url          = url_title(trim(input_clean($this->input->post('name_' . $default_language))), 'dash', TRUE);
        $this->db->insert($data['table'], array("seo_url"=> $seo_url,'flag'=>1));
        $last_id          = $this->db->insert_id();
        foreach ($language as $lang_data) {
            $array = array(
                $data['table']."_id" => $last_id,
                "language_id"        => $lang_data['id'],
                "name"               => $data['name_'.$lang_data['id']]
            );
            $this->db->insert("content_to_".$data['table'], $array);
            
        }
        echo json_encode(array('option' => $data['name_'.$default_language], 'option_value'=>$last_id));

    }
    public function seo_url(){
        if($this->input->post('seo_url')){
            $seo = create_seo_url(input_clean($this->input->post('seo_url')), $this->input->post('table'));
            echo $seo;
        }
    }
    
}